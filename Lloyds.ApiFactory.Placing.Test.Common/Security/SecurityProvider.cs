//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Lloyds.ApiFactory.Placing.Test.Common.Model;

namespace Lloyds.ApiFactory.Placing.Test.Common.Security
{
    public class SecurityProvider
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IClientCertificateProvider _clientCertificateProvider;

        public SecurityProvider(ITokenProvider tokenProvider, IClientCertificateProvider clientCertificateProvider)
        {
            _tokenProvider = tokenProvider;
            _clientCertificateProvider = clientCertificateProvider;
        }

        public List<OrganisationClientCertificate> GetOrganisationClientCertificate(List<Organisation> organisationList)
        {
            var result = new List<OrganisationClientCertificate>();
            foreach (var organisation in organisationList)
            {
                var orgClientCert = new OrganisationClientCertificate
                {
                    OrganisationName = organisation.OrganisationName,
                    ClientCertificate = _clientCertificateProvider.GetClientCertificateFromCurrentUserStoreByThumbprint(organisation.ClientCertificate.Thumbprint)
                };
                result.Add(orgClientCert);
            }
            return result;
        }

        public List<UserBearerToken> GetBearerToken(List<Organisation> organisationList)
        {
            List<UserBearerToken> result = new List<UserBearerToken>();
            foreach (var organisation in organisationList)
            {
                foreach (var userCredential in organisation.UserCredentials)
                {
                    var userBearerToken = new UserBearerToken();
                    userBearerToken.EmailId = userCredential.EmailId;
                    userBearerToken.AuthToken = _tokenProvider.GetAccessTokenAsync(organisation.TokenEndpoint, userCredential).Result;
                    result.Add(userBearerToken);
                }
            }
            return result;
        }

        public List<UserCredential> GetUserCredentials(List<Organisation> organisationList)
        {
            List<UserCredential> result = new List<UserCredential>();
            foreach (var organisation in organisationList)
            {
                foreach (var userCredential in organisation.UserCredentials)
                {
                    result.Add(userCredential);
                }
            }
            return result;
        }
    }
}

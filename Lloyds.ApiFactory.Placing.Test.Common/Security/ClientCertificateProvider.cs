//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Security.Cryptography.X509Certificates;

namespace Lloyds.ApiFactory.Placing.Test.Common.Security
{
    public class ClientCertificateProvider : IClientCertificateProvider
    {
        public X509Certificate2 GetClientCertificateFromCurrentUserStoreByThumbprint(string thumbprint)
        {
            return GetClientCertificateByThumbprint(StoreLocation.CurrentUser, thumbprint);
        }

        private static X509Certificate2 GetClientCertificateByThumbprint(StoreLocation storeLocation, string thumbprint)
        {
            if (string.IsNullOrEmpty(thumbprint))
                throw new ArgumentNullException(nameof(thumbprint), "Argument 'thumbprint' cannot be 'null' or 'string.empty'");

            using (var userCaStore = new X509Store(StoreName.My, storeLocation))
            {
                userCaStore.Open(OpenFlags.ReadOnly);
                var certificatesInStore = userCaStore.Certificates;
                var findResult = certificatesInStore.Find(X509FindType.FindByThumbprint, thumbprint, true);
                X509Certificate2 clientCertificate = null;
                if (findResult.Count == 1)
                {
                    clientCertificate = findResult[0];
                }
                return clientCertificate;
            }
        }
    }
}

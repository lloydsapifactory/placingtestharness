//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Newtonsoft.Json.Linq;

namespace Lloyds.ApiFactory.Placing.Test.Common.Security
{
    public class BearerTokenProvider : ITokenProvider
    {
        public async Task<string> GetAccessTokenAsync(TokenEndpoint tokenEndpoint, UserCredential userCredential)
        {
            return await GetAccessTokenInternalAsync(tokenEndpoint, userCredential);
        }

        private async Task<string> GetAccessTokenInternalAsync(TokenEndpoint tokenEndpoint, UserCredential userCredential)
        {
            using (var client = new HttpClient())
            {
                string tokenEndpointUrl = string.Format($"{tokenEndpoint.Instance}{tokenEndpoint.TenantId}{tokenEndpoint.TokenUrlPart}");

                var body = $"resource={tokenEndpoint.Resource}&client_id={tokenEndpoint.ClientId}&grant_type=password&username={userCredential.EmailId}&password={userCredential.Password}";
                var stringContent = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");

                var result = await client.PostAsync(tokenEndpointUrl, stringContent).ContinueWith((response) =>
                {
                    return response.Result.Content.ReadAsStringAsync().Result;
                });

                JObject jobject = JObject.Parse(result);
                var token = jobject["access_token"].Value<string>();

                return $"Bearer {token}";
            }
        }
    }
}

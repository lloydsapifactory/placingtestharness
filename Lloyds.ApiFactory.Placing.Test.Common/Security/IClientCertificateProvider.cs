//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Security.Cryptography.X509Certificates;

namespace Lloyds.ApiFactory.Placing.Test.Common.Security
{
    public interface IClientCertificateProvider
    {
        X509Certificate2 GetClientCertificateFromCurrentUserStoreByThumbprint(string thumbprint);
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Threading.Tasks;
using Lloyds.ApiFactory.Placing.Test.Common.Model;

namespace Lloyds.ApiFactory.Placing.Test.Common.Security
{
    public interface ITokenProvider
    {
        Task<string> GetAccessTokenAsync(TokenEndpoint tokenEndpoint, UserCredential userCredential);
    }
}

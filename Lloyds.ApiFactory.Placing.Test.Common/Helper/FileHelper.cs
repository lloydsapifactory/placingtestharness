//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.IO;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.Test.Common.Helper
{
    public class FileHelper
    {
        public static T ReadFile<T>(string filePath) where T : class
        {
            return JsonConvert.DeserializeObject<T>(
                File.ReadAllText(filePath));
        }

        public static string ReadFile(string filePath)
        {
            return File.ReadAllText(filePath);
        }

        public static Submission LoadSubmissionFromFile(string filePath)
        {
            var path = Path.Combine(Environment.CurrentDirectory, filePath);
            var content = FileHelper.ReadFile<Submission>(path);
            return content;
        }
    }
}

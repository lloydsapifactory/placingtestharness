//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Linq;
using System.Reflection;

namespace Lloyds.ApiFactory.Placing.Test.Common.Helper
{
    public class ReflectionHelper
    {

        public static object GetPropertyValue(object c, string propertyName)
        {
            return c.GetType().GetProperties()
               .SingleOrDefault(pi => pi.Name.Equals(propertyName.Trim(), StringComparison.OrdinalIgnoreCase))
               .GetValue(c, null);
        }

        public static object GetPropertyCc(object c, string propertyName)
        {
            return c.GetType().GetProperties().SingleOrDefault(pi => pi.Name.Trim().Equals(propertyName, StringComparison.OrdinalIgnoreCase));
        }

        public static object GetPropertyValueCc(object c, string propertyName)
        {
            return c.GetType().GetProperties()
               .SingleOrDefault(pi => pi.Name.Equals(propertyName, 
               StringComparison.OrdinalIgnoreCase))
               .GetValue(c, null);
        }

        public static object GetProperty(object c, string propertyName)
        {
            return c.GetType().GetProperties()
               .Single(pi => pi.Name == propertyName)
               ;
        }

        public static PropertyInfo GetPropertyInfo(object c, string propertyName)
        {
            return c.GetType().GetProperty(propertyName.Trim());
        }

       
    }
}

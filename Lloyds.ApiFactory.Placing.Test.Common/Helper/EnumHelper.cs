//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Lloyds.ApiFactory.Placing.WebApi.Dto;

namespace Lloyds.ApiFactory.Placing.Test.Common.Helper
{
    public class EnumHelper
    {
        public static readonly Dictionary<ContractType, string> ContractTypeMappings = new Dictionary<ContractType, string>
        {
            { ContractType.direct_insurance_contract, "Direct" },
            { ContractType.reinsurance_contract, "Reinsurance" }
        };

        public static readonly Dictionary<string, string> ClassOfBusinessMappings = new Dictionary<string, string>
        {
            { "marine_hull", "Marine hull" }
        };

        public static readonly Dictionary<string, string> CoverTypeCodeMappings = new Dictionary<string, string>
        {
            { "facultative_proportional", "Facultative Proportional" }
        };

        public static readonly Dictionary<string, string> RiskRegionCodeMappings = new Dictionary<string, string>
        {
            { "worldwide", "Worldwide" }
        };

        public static readonly Dictionary<string, string> SubmissionStatusMappings = new Dictionary<string, string>
        {
            { "DRFT", "Draft" },
            { "SENT", "In Progress" }
        };

        public static string GetEnumDisplayName(Enum value)
        {
            if (value == null)
                return string.Empty;

            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description
                ?? value.ToString();
        }

        public static string GetReferenceValue(IDictionary dictionary, object key)
        {
            return key != null && dictionary.Contains(key) ? dictionary[key].ToString() : null;
        }
    }
}

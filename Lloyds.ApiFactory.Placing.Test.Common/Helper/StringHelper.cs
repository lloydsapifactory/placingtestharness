//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Lloyds.ApiFactory.Placing.Test.Common.Helper
{
    public class StringHelper
    {
        public static string GenerateRandomString(int length = 6)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new string(stringChars);
        }


        public static void FindRandomFieldValues(object[] array, 
            string fieldname, 
            ref List<string> valuesArray, 
            ref string previousValue)
        {
            for (var i = 0; i < array.Length; i++)
            {
                var rand = new Random();
                var index = rand.Next(array.Length);
                var randomDoc = array[index];

                var valueSet = ReflectionHelper.GetPropertyValue(randomDoc, fieldname.Trim());
                string valueToCheck;

                if (valueSet == null)
                    valueToCheck = "";
                else
                    valueToCheck = valueSet.ToString();

                if (!valuesArray.Contains(valueToCheck) && valuesArray.Count > 0)
                {
                    valuesArray.Add(valueToCheck);
                    break;
                }

                if (previousValue.Length == 0)
                    valuesArray.Add(valueToCheck);

                previousValue = valueToCheck;
            }
        }

        public static string ReturnRandomValueFromCollectionWithValue(object[] array,
           string fieldname,
           ref string valueToCheck)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                var rand = new Random();
                var index = rand.Next(array.Length);
                var randomDoc = array[index];
                var valueSet = ReflectionHelper.GetPropertyValue(randomDoc, fieldname.Trim());

                if (valueSet == null)
                    valueToCheck = "";
                else
                    valueToCheck = valueSet.ToString();

                if (valueToCheck.Length > 0)
                    break;
            }

            return valueToCheck;
        }
    }
}

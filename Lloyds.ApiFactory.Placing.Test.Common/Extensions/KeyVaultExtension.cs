//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Microsoft.Extensions.Configuration;

namespace Lloyds.ApiFactory.Placing.Test.Common.Extensions
{
    public static class KeyVaultExtension
    {
        public static IConfigurationBuilder AddAzureKeyVault(this IConfigurationBuilder builder)
        {
            var builtConfig = builder.Build();

            var keyVaultEnabled = builtConfig.GetValue<bool>("KeyVault:Enable");

            if (keyVaultEnabled)
            {
                var keyVaultEndpoint = builtConfig.GetValue<string>("KeyVault:Name");
                var resourceName = builtConfig.GetValue<string>("KeyVault:ResourceName");
                var versionNmber = builtConfig.GetValue<string>("KeyVault:Version");
                var clientId = builtConfig.GetValue<string>("KeyVault:ClientId");
                var clientSecret = builtConfig.GetValue<string>("KeyVault:ClientSecret");

                if (string.IsNullOrEmpty(keyVaultEndpoint) || string.IsNullOrEmpty(resourceName) || string.IsNullOrEmpty(versionNmber) ||
                  string.IsNullOrEmpty(clientId) || string.IsNullOrEmpty(clientSecret))
                {
                    throw new System.Exception($"The keyVault is enabled but the keyVault configuration are empty.");
                }

                var prefix = $"{resourceName}--{versionNmber}";

                builder.AddAzureKeyVault(
                        $"https://{keyVaultEndpoint}.vault.azure.net/",
                        clientId, clientSecret,
                        new PrefixKeyVaultSecretManager(prefix));

            }
            return builder;

        }
    }
}

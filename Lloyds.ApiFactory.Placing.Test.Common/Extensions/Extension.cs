//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.Common.Extensions
{
    public static class Extension
    {
        public static string GetOrganisationNameByUserEmailId(this List<Organisation> organisationList, string userEmailId)
        {
            var orgName = (from o in organisationList
                           from u in o.UserCredentials
                           where u.EmailId.Equals(userEmailId, StringComparison.OrdinalIgnoreCase)
                           select o.OrganisationName).FirstOrDefault();
            return orgName;
        }
      
        public static T ToEnum<T>(this string value, bool ignoreCase = true)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        public static string GenerateRandomString(int length = 6)
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return string.Join("",Enumerable.Repeat(chars, length).Select(a => a[random.Next(1, chars.Length)]));
        }

        public static string GenerateRandomStringNumbers(int length = 6)
        {
            var random = new Random();
            var chars = "0123456789";
            return string.Join("", Enumerable.Repeat(chars, length).Select(a => a[random.Next(1, chars.Length)]));
        }

        public static GenericType HandleNull<GenericType>(object value)
        {
            TypeConverter conv = TypeDescriptor.GetConverter(typeof(GenericType));
            return ((value == null)) ? default(GenericType) : (GenericType)conv.ConvertFrom(value.ToString());
        }

        public static string[] AsStrings(this Table t, string column)
        {
            return t.Rows.Select(r => r[column]).ToArray();
        }

        public static string NullIfEmpty(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }
            return str;
        }

        /// <summary>
        /// Determines if int array is sorted from 0 -> Max
        /// </summary>
        public static bool IsSortedString(object[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i - 1].ToString().CompareTo(arr[i].ToString()) > 0) // If previous is bigger, return false
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines if int array is sorted from 0 -> Max
        /// </summary>
        public static bool IsSorted(int[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i - 1] > arr[i])
                {
                    return false;
                }
            }
            return true;
        }

        // <summary>
        /// Determines if string array is sorted from A -> Z
        /// </summary>
        public static bool IsSorted(string[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i - 1].CompareTo(arr[i]) > 0) // If previous is bigger, return false
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines if int array is sorted from Max -> 0
        /// </summary>
        public static bool IsSortedDescending(int[] arr)
        {
            for (int i = arr.Length - 2; i >= 0; i--)
            {
                if (arr[i] < arr[i + 1])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines if string array is sorted from Z -> A
        /// </summary>
        public static bool IsSortedDescending(string[] arr)
        {
            for (int i = arr.Length - 2; i >= 0; i--)
            {
                if (arr[i].CompareTo(arr[i + 1]) < 0) // If previous is smaller, return false
                {
                    return false;
                }
            }
            return true;
        }

       

    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;

namespace Lloyds.ApiFactory.Placing.Test.Common
{
    public static class Constants
    {
        public const string UserContext = "UserContext";
        public const string SubmissionContext = "SubmissionContext";
        public const string SubmissionListContext = "SubmissionListContext";
        public const string SubmissionUniqueReference = "submissionUniqueReference";
        public const string SeleniumOptions = "seleniumOptions";
        public const string SubmissionAsJSON = "SubmissionAsJSON";
        public const string SubmissionCollectionWrapper = "SubmissionCollectionWrapper";
        public const string SubmissionRandomStr = "SubmissionRandomStr";
        public const string UnderwriterOrganisationContext = "UnderwriterOrganisationContext";
        public const string BrokerDepartmentContext = "BrokerDepartmentContext";
        public const string SubmissionDocumentContext = "SubmissionDocumentContext";
        public const string SubmissionDocumentId = "SubmissionDocumentId";
        public const string SubmissionDocumentCollectionWrapper = "SubmissionDocumentCollectionWrapper";
        public const string SubmissionDocumentModelCollection = "SubmissionDocumentModelCollection";
        public const string SubmissionReferenceforSubmissionDocument = "submissionReferenceforSubmissionDocument";
        public const string SubmissionUnderwriterContext = "SubmissionUnderwriterContext";
        public const string SubmissionUnderwriterCollectionWrapper = "SubmissionUnderwriterCollectionWrapper";
        public const string SubmissionUnderwriterId = "SubmissionUnderwriterId";
        public static string SubmissionDialogueId = "SubmissionDialogueId";
        public const string SubmissionDialogueContext = "SubmissionDialogueContext";
        public const string SubmissionDialogueCollectionWrapper = "SubmissionDialogueCollectionWrapper";
        public const string BrokerDepartmentId = "brokerDepartmentId";

        public static Dictionary<string, string> FileTypesDictionary { get; set; }

        static Constants()
        {
            FileTypesDictionary = new Dictionary<string, string>()
            {
                {"application/msword", ".doc"},
                {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", ".docx"},
                {"application/rtf", ".rtf"},
                {"application/pdf", ".pdf"}
            };
        }
    }
}

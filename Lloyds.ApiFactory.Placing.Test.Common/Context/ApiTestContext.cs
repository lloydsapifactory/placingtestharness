//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using RestSharp;

namespace Lloyds.ApiFactory.Placing.Test.Common.Context
{
    public class ApiTestContext : IDisposable
    {
        //private const int RetryCount = 10;
        //private const int WaitSecondsBeforeRetry = 2;
        private readonly IRestRequest _restRequest;
        private readonly IRestClient _restClient;
        private bool disposed = false;
        //private string _endpointRelativeUrlPath;
        public ApiTestContext()
        {
            _restRequest = new RestRequest();
            _restClient = new RestClient();
        }

        public IRestResponse Response { get; set; }

        /// <summary>
        ///  The API base url
        /// </summary>
        /// <example>
        /// https://sand-api.londonmarketgroup.co.uk/Lmtom
        /// </example>
        public string ApiBaseUrl { get; set; }
        public string PPLUIBaseUrl { get; set; }
        public string SeleniumOptions { get; set; }

        public string ExtentReportsPrefs { get; set; }

        public ApiTestContext AddEndpointUrlPath(string endpointUrlPath)
        {
            var url = new Uri(new Uri(ApiBaseUrl), endpointUrlPath);
            _restClient.BaseUrl = url;
            return this;
        }

        public ApiTestContext AddResource(string resource)
        {
            _restRequest.Resource = resource;
            return this;
        }

        public ApiTestContext AddAcceptType(string mimeType)
        {
            _restRequest.AddHeader("Accept", mimeType);
            return this;
        }

        public ApiTestContext AddContentType(string contentType)
        {
            _restRequest.AddHeader("Content-Type", contentType);
            return this;
        }

        /// <summary>
        /// Pass MimeType.File pass path
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="mimeType"></param>
        /// <returns></returns>
        public ApiTestContext AddContent<T>(T data, MimeEnumType mimeType)
        {
            switch (mimeType)
            {
                case MimeEnumType.Json:
                    _restRequest.Parameters.RemoveAll(a => a.Type == ParameterType.RequestBody);
                    _restRequest.AddJsonBody(data);
                    break;

                case MimeEnumType.Xml:
                    _restRequest.AddXmlBody(data);
                    break;
                case MimeEnumType.File:
                    _restRequest.AddFile("file", data.ToString());
                    _restRequest.AlwaysMultipartFormData = true;
                    break;
                case MimeEnumType.None:
                default:
                    throw new Exception("Set AddRequestFormat before AddContent");
            }
            return this;
        }

        public ApiTestContext ClearDown()
        {
            _restRequest.AlwaysMultipartFormData = false;

            _restRequest.Parameters.RemoveAll(a => a.Type == ParameterType.RequestBody);
            _restRequest.Parameters.RemoveAll(a => a.Name == "Content-Type");
            _restRequest.Parameters.RemoveAll(a => a.Name == "Expect");

            if (_restRequest.Files.Count > 0) _restRequest.Files.Clear();
            return this;
        }

        public ApiTestContext ClearDownWithHttpHeaders()
        {
            _restRequest.AlwaysMultipartFormData = false;

            _restRequest.Parameters.RemoveAll(a => a.Type == ParameterType.RequestBody);
            _restRequest.Parameters.RemoveAll(a => a.Name == "Content-Type");
            _restRequest.Parameters.RemoveAll(a => a.Name == "Expect");
             _restRequest.Parameters.RemoveAll(a => a.Type == ParameterType.HttpHeader);

            if (_restRequest.Files.Count > 0) _restRequest.Files.Clear();
            return this;
        }
        public ApiTestContext AddHTTPVerb(Method method)
        {
            _restRequest.Method = method;
            return this;
        }

        public ApiTestContext AddHeaders(IDictionary<string, string> headers)
        {
            foreach (var header in headers)
            {
                _restRequest.AddParameter(header.Key, header.Value, ParameterType.HttpHeader);
            }
            return this;
        }
        public ApiTestContext AddAuthorization(string token)
        {
            var authorizationHeader = _restRequest.Parameters.FirstOrDefault(i =>
                i.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase));

            if (authorizationHeader != null)
            {
                _restRequest.Parameters.Remove(authorizationHeader);
            }
            _restRequest.AddHeader("Authorization", token);
            return this;
        }
        public ApiTestContext AddClientCertificate(X509Certificate2 clientCertificate)
        {
            _restClient.ClientCertificates = new X509CertificateCollection
            {
                clientCertificate
            };
            return this;
        }
        public ApiTestContext AddEtagHeader(string value)
        {
            _restRequest.AddHeader("If-None-Match", value);
            return this;
        }


        public ApiTestContext AddParameterToRequestBody(string name, object value)
        {
            _restRequest.AddParameter(name, value, ParameterType.RequestBody);
            return this;
        }

        public ApiTestContext AddParameters(IDictionary<string, object> parameters)
        {
            foreach (var item in parameters)
            {
                _restRequest.AddParameter(item.Key, item.Value);
            }
            return this;
        }

        public IRestResponse Execute()
        {
            try
            {
                _restClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _restClient.ClearHandlers();
                var response = _restClient.Execute(_restRequest);
                return response;
            }
            catch
            {
                throw;
            }
        }

        public IRestResponse ExecuteGetBinary()
        {
            try
            {
                _restRequest.AddHeader("Content-Type", "application/octet-stream");

                _restClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _restClient.ClearHandlers();
                var response = _restClient.Execute(_restRequest);
                return response;
            }
            catch
            {
                throw;
            }
        }


        public IRestResponse ExecutePutIfUnmodifiedSince(DateTime dt, bool useUTC)
        {
            try
            {   string dtUtcFormatted;
                if (useUTC)
                {   dtUtcFormatted = TimeZoneInfo.ConvertTimeToUtc(dt, TimeZoneInfo.FindSystemTimeZoneById("UTC")).ToString("r");
                }
                else
                {   dtUtcFormatted = TimeZoneInfo.ConvertTimeToUtc(dt, TimeZoneInfo.Local).ToString("r");
                }
                
                _restRequest.AddHeader("If-Unmodified-Since", dtUtcFormatted);

                _restClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _restClient.ClearHandlers();
                var response = _restClient.Execute(_restRequest);
                return response;
            }
            catch
            {
                throw;
            }
        }


        public async Task<IRestResponse> ExecuteMultipartAsync()
        {
            try
            {
                _restRequest.AddHeader("Content-Type", "multipart/form-data");
                _restClient.Timeout = 300000;
                _restClient.ReadWriteTimeout = 300000;
                _restClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _restClient.ClearHandlers();

                _restClient.ConfigureWebRequest((r) =>
                    {
                        r.ServicePoint.Expect100Continue=true;
                        r.KeepAlive = true;
                    });

                var response = _restClient.Execute(_restRequest);
                //var response = await _restClient.ExecuteTaskAsync(_restRequest);
                return response;
            }
            finally
            {
                _restRequest.Parameters.RemoveAll(a => a.Name == "Content-Type");
                _restRequest.AlwaysMultipartFormData = false;
                _restRequest.Files.Clear();
            }
        }

        public T Execute<T>() where T : new()
        {
            try
            {
                _restClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _restClient.ClearHandlers();
                IRestResponse<T> response = _restClient.Execute<T>(_restRequest);
                return response.Data;
            }
            catch
            {
                throw;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                    
                }

                // Dispose unmanaged managed resources.
                disposed = true;
            }
        }
            public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

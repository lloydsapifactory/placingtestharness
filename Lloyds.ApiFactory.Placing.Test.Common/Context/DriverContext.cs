//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.Common.Context
{
    public class DriverContext : IDisposable
    {
        private bool disposed = false;

        public ScenarioContext ScenarioContext { get; set; }
        public FeatureContext FeatureContext {get;set; }
        public ScenarioStepContext StepContext { get; set; }

        public string ScenarioTitle { get; set; }
        public string ScenarioTags { get; set; }
        public string FeatureTitle { get; set; }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Dispose unmanaged managed resources.
                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

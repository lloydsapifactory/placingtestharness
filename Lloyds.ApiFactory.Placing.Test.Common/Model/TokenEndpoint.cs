//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class TokenEndpoint
    {
        public string Instance { get; set; }
        public string TenantId { get; set; }
        public string TokenUrlPart { get; set; }
        public string GrantType { get; set; }
        public string ClientId { get; set; }
        public string Resource { get; set; }
    }

}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class ClientCertificate
    {
        public string Thumbprint { get; set; }
    }

}

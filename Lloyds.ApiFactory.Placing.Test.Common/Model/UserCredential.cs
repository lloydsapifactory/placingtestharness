//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class UserCredential
    {
        public bool Enabled { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string PPLUsername { get; set; }
        public string PPLPassword { get; set; }
    }

}

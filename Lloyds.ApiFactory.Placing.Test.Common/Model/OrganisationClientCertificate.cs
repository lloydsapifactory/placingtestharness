//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Security.Cryptography.X509Certificates;

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class OrganisationClientCertificate
    {
        public string OrganisationName { get; set; }
        public X509Certificate2 ClientCertificate { get; set; }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class BrokerStore
    {
        public string EmailAddress { get; set; }
        public string BrokerCode { get; set; }
        public string BrokerDepartmentId { get; set; }
    }

}

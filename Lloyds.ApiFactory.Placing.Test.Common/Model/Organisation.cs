//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class Organisation
    {
        public string OrganisationName { get; set; }
        public bool Enabled { get; set; }
        public TokenEndpoint TokenEndpoint { get; set; }
        public ClientCertificate ClientCertificate { get; set; }
        public List<UserCredential> UserCredentials { get; set; }
    }

    public class OrganisationRoot
    {
        public List<Organisation> Organisations { get; set; }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{
    public class UserBearerToken
    {
        public string EmailId { get; set; }
        public string AuthToken { get; set; }
    }

}

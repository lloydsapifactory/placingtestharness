//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.ComponentModel;

namespace Lloyds.ApiFactory.Placing.Test.Common.Model
{

    public enum MimeEnumType
    {
        None = 0,

        [Description("application/json")]
        Json = 1,

        [Description("application/xml")]
        Xml = 2,

        [Description("application/octet-stream")]
        File = 3
    }
}

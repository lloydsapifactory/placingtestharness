//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto
{
    public partial class UnderwriterOrganisation : UnderwriterOrganisationBase
    { }

    public partial class UnderwriterOrganisationGet : UnderwriterOrganisationBase
    {
        string _nameEmailOrg;
        [XmlIgnore]
        [JsonIgnore]
        public string NameEmailOrg
        {
            get
            {
                if (UnderwriterUsers.Count > 0)
                {
                    _nameEmailOrg = string.Join("_", Enumerable.Select<UnderwriterUser, string>(UnderwriterUsers, x => $"{x.UnderwriterUserFullName}#{x.UnderwriterUserEmailAddress}"));
                }
                return $"{UnderwriterOrganisationName}&{_nameEmailOrg}";
            }
        }
    }

    public class UnderwriterOrganisationBase : ResourceBase
    {
        [XmlIgnore]
        [JsonIgnore]
        public override string Identity
        {
            get => Id;
            set => Id = value;
        }

        [XmlAttribute(AttributeName = "id", Form = XmlSchemaForm.Unqualified)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public override DateTimeOffset? Modified { get; set; }
    }
}

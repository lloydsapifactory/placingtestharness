//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Text;
using Lloyds.ApiFactory.Placing.WebApi.Dto.Custom;
using Lloyds.ApiFactory.ViewModel.Abstractions;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto
{
    public partial class GetAllUnderwriterOrganisationArgs : ResourceCollectionQueryArgs
    {

        public override Dictionary<string, string> orderList()
        {
            OrderList[nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationName)] = nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationName);
            return OrderList;
        }
        public override Dictionary<string, string> selectList()
        {
            var list = new Dictionary<string, string>();
            list[nameof(Dto.UnderwriterOrganisation.UnderwriterUsers)] = $"{nameof(Dto.UnderwriterOrganisation.UnderwriterUsers)}";
            list[nameof(Dto.UnderwriterOrganisation.BrokerDepartmentId)] = $"{nameof(Dto.UnderwriterOrganisation.BrokerDepartmentId)}";
            list[nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationName)] = $"{nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationName)}";
            list[nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationId)] = $"{nameof(Dto.UnderwriterOrganisation.UnderwriterOrganisationId)}";
            list[nameof(UnderwriterUser.UnderwriterUserContactNumber)] = $"{nameof(UnderwriterUser.UnderwriterUserContactNumber)}";
            list[nameof(UnderwriterUser.UnderwriterUserFullName)] = $"{nameof(UnderwriterUser.UnderwriterUserFullName)}";
            list[nameof(UnderwriterUser.UnderwriterUserEmailAddress)] = $"{nameof(UnderwriterUser.UnderwriterUserEmailAddress)}";

            return list;
        }

        public override Dictionary<string, string> expandList()
        {
            return new Dictionary<string, string>()
            {
            };
        }

        public override void ResourceFiltersInURIQueryString(StringBuilder sb)
        {
            base.ResourceFiltersInURIQueryString(sb);

            sb.AppendFilter<string>(nameof(Dto.GetAllUnderwriterOrganisationArgs.BrokerDepartmentId), BrokerDepartmentId);
            sb.AppendFilter<string>(nameof(Dto.GetAllUnderwriterOrganisationArgs.NameEmailOrg), NameEmailOrg);
        }
    }
}

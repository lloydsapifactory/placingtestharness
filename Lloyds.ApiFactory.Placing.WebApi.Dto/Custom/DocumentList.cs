//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Xml.Schema;
using System.Xml.Serialization;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto
{
    public partial class DocumentList : ResourceBase
    {
        [XmlIgnore]
        [JsonIgnore]
        public override string Identity
        {
            get => Id;
            set => Id = value;
        }

        [XmlAttribute(AttributeName = "id", Form = XmlSchemaForm.Unqualified)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public override DateTimeOffset? Modified { get; set; }
    }
}

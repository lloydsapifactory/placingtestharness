//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Lloyds.ApiFactory.Placing.WebApi.Dto.Custom;
using Lloyds.ApiFactory.ViewModel.Abstractions;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto
{
    public partial class GetAllSubmissionArgs : ResourceCollectionQueryArgs
    {
        public string ProgrammeReference { get; set; }
        
        public string BrokerCode { get; set; }
        public string BrokerDepartmentId { get; set; }
        public string TechnicianUserEmailAddress { get; set; }
       
        public override Dictionary<string, string> orderList()
        {
            OrderList[nameof(Dto.GetAllSubmissionArgs.CreatedDateTime)] = nameof(Dto.GetAllSubmissionArgs.CreatedDateTime);

            return OrderList;
        }

        public override Dictionary<string, string> selectList()
        {
            var list = new Dictionary<string, string>();

            List<string> excludedFieldNameList = new List<string>() { "id", "identity", "modified" };
            List<string> classPropertyNameList = typeof(Dto.Submission).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).Where(o => excludedFieldNameList.All(e => e != o.Name) || !o.Name.EndsWith("Specified")).Select(o => o.Name).ToList();
            foreach (var classPropertyName in classPropertyNameList)
            {
                list[classPropertyName] = classPropertyName;
            }
            return list;
        }

        public override Dictionary<string, string> expandList()
        {
            return new Dictionary<string, string>()
            {
            };
        }

        public override void ResourceFiltersInURIQueryString(StringBuilder sb)
        {
            base.ResourceFiltersInURIQueryString(sb);

            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.SubmissionVersionDescription), SubmissionVersionDescription);
            sb.AppendFilter(nameof(TechnicianUserEmailAddress), TechnicianUserEmailAddress);
            sb.AppendFilter(nameof(ProgrammeReference), ProgrammeReference);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.ClassOfBusinessCode), ClassOfBusinessCode);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.CoverTypeCode), CoverTypeCode);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.OriginalPolicyholder), OriginalPolicyholder);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.InsuredOrReinsured), InsuredOrReinsured);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.ContractDescription), ContractDescription);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.ContractReference), ContractReference);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.ProgrammeDescription), ProgrammeDescription);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.BrokerUserEmailAddress), BrokerUserEmailAddress);
            sb.AppendFilter(nameof(BrokerDepartmentId), BrokerDepartmentId);
            sb.AppendFilter<string>(nameof(Dto.GetAllSubmissionArgs.SubmissionReference), SubmissionReference);
            sb.AppendFilter(nameof(BrokerCode), BrokerCode);

            if (!string.IsNullOrWhiteSpace(ContractTypeCode))
            {
                sb.Append($"{nameof(Dto.GetAllSubmissionArgs.ContractTypeCode)}=");
                sb.Append((string) ContractTypeCode);
                sb.Append("&");
            }

            if (!string.IsNullOrWhiteSpace(SubmissionStatusCode))
            {
                sb.Append($"{nameof(Dto.GetAllSubmissionArgs.SubmissionStatusCode)}=");
                sb.Append((string) SubmissionStatusCode);
                sb.Append("&");
            }
        }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Lloyds.ApiFactory.Placing.WebApi.Dto.Custom;
using Lloyds.ApiFactory.ViewModel.Abstractions;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto
{
    public partial class GetAllBrokerDepartmentArgs : ResourceCollectionQueryArgs
    {

        private string BrokerUsers { get; set; }

        public override Dictionary<string, string> orderList()
        {
            OrderList[nameof(Dto.GetAllBrokerDepartmentArgs.BrokerDepartmentName)] = nameof(Dto.GetAllBrokerDepartmentArgs.BrokerDepartmentName);
            return OrderList;
        }

        public override Dictionary<string, string> selectList()
        {
            var list = new Dictionary<string, string>();

            List<string> excludedFieldNameList = new List<string>() { "id", "identity", "modified" };
            List<string> classPropertyNameList = typeof(Dto.Submission).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).Where(o => excludedFieldNameList.All(e => e != o.Name) || !o.Name.EndsWith("Specified")).Select(o => o.Name).ToList();
            foreach (var classPropertyName in classPropertyNameList)
            {
                list[classPropertyName] = classPropertyName;
            }
            return list;
        }

        public override Dictionary<string, string> expandList()
        {
            return new Dictionary<string, string>()
            {
            };
        }

        public override void ResourceFiltersInURIQueryString(StringBuilder sb)
        {
            base.ResourceFiltersInURIQueryString(sb);
            sb.AppendFilter<string>($"{nameof(BrokerUsers)}.{nameof(Dto.GetAllBrokerDepartmentArgs.BrokerUserEmailAddress)}", BrokerUserEmailAddress);
            sb.AppendFilter<string>($"{nameof(BrokerUsers)}.{nameof(Dto.GetAllBrokerDepartmentArgs.BrokerUserFullName)}", BrokerUserFullName);
            sb.AppendFilter<string>(nameof(Dto.GetAllBrokerDepartmentArgs.BrokerCodes), BrokerCodes);
            sb.AppendFilter<string>(nameof(Dto.GetAllBrokerDepartmentArgs.BrokerDepartmentName), BrokerDepartmentName);
        }
    }
}

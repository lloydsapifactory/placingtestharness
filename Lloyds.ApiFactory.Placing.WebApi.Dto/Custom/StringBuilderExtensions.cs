//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Text;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto.Custom
{
    public static class StringBuilderExtensions
    {
        public static void AppendFilter<T>(this StringBuilder sb, string filterName, T filterValue) 
        {
            if (IsValidDateTime() || IsValidString())
            {
                sb.Append($"{filterName}=");
                sb.Append(filterValue);
                sb.Append("&");
            }

            bool IsValidDateTime() => filterValue != null && filterValue.GetType() == typeof(DateTimeOffset);

            bool IsValidString() => filterValue != null && filterValue.GetType() == typeof(string) && !string.IsNullOrEmpty(filterValue as string);
        }
    }
}

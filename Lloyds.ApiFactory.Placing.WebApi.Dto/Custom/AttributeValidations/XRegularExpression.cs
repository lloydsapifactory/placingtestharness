//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.ComponentModel.DataAnnotations;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto.Custom.AttributeValidations
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
    public class XRegularExpression : RegularExpressionAttribute
    {
        public XRegularExpression(string pattern) : base(pattern)
        {
           
        }
    }
}

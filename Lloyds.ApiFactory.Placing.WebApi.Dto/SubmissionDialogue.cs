//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from LIMOSS Doc Generator Tool.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Lloyds.ApiFactory.Placing.WebApi.Dto.Custom.AttributeValidations;
using Lloyds.ApiFactory.ViewModel.Abstractions.Converter;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto {

	/// <summary>
	/// A collection of data items, representing the transaction-based dialogue between broker and underwriter (or facility administrator), about the submission the underwriter/underwriting organisation has been assigned to.
	/// </summary>
 	[XmlRoot(Namespace = "http://www.londonmarketgroup.co.uk/schemas/endpoints/LMTOM/Placing/v1", ElementName = "SubmissionDialogue", DataType = "SubmissionDialogueType", IsNullable = false)]
	public partial class SubmissionDialogue{
		/// <summary>
		/// The unique identifier for the resource.
		/// </summary>
		[XmlElement(ElementName = "SubmissionDialogueId", Order = 10 , IsNullable = false)]
		[JsonProperty(PropertyName = "submissionDialogueId", NullValueHandling = NullValueHandling.Ignore, Order = 10)]
		public string SubmissionDialogueId { get; set; }

		/// <summary>
		/// The broker reference for this quotation
		/// </summary>
		[Required()]
		[XRegularExpression(@".*[^\s].*?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No whitespace
		[MaxLength(50, ErrorMessage = "[StringTooLong] The field contained a string that was too long.")] //Max length of 50 characters
		[XRegularExpression(@"[A-Za-z0-9]*", ErrorMessage = "[ContainsSpecialChar] The field contains special characters")] //No special characters
		[MinLength(1, ErrorMessage = "[FieldIsEmpty] The field was empty when a value is required e.g. \"\".")] //Cannot be empty
		[XmlElement(ElementName = "SubmissionReference", Order = 20)]
		[JsonProperty(PropertyName = "submissionReference", Required = Required.Always, Order = 20)]
		public string SubmissionReference { get; set; }

		/// <summary>
		/// The version of this submission
		/// </summary>
		[Required()]
		[Range(1, Int32.MaxValue, ErrorMessage = "[InvalidNumber] The field should contain a valid number")] //Greater than zero
		[XmlElement(ElementName = "SubmissionVersionNumber", Order = 21)]
		[JsonProperty(PropertyName = "submissionVersionNumber", Required = Required.Always, Order = 21)]
		public int SubmissionVersionNumber { get; set; }

		/// <summary>
		/// The unique identifier of the submission underwriter.
		/// </summary>
		[Required()]
		[XmlElement(ElementName = "SubmissionUnderwriterId", Order = 22)]
		[JsonProperty(PropertyName = "submissionUnderwriterId", Required = Required.Always, Order = 22)]
		public string SubmissionUnderwriterId { get; set; }

		/// <summary>
		/// The code that defines the role of the sender of the message.
		/// </summary>
		[XmlElement(ElementName = "SenderParticipantCode", Order = 31 )]
		[JsonProperty(PropertyName = "senderParticipantCode", NullValueHandling = NullValueHandling.Ignore, Order = 31)]
		public ParticipantRole? SenderParticipantCode { get; set; }

		/// <summary>
		/// The name of the sender of the dialogue.
		/// </summary>
		[XRegularExpression(@".*[^\s]{2,}.*?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No more than 2 consecutive whitespace characters
		[XRegularExpression(@"[^\s](.*[^\s])?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No leading or trailing whitespace
		[XmlElement(ElementName = "SenderUserFullName", Order = 32 , IsNullable = false)]
		[JsonProperty(PropertyName = "senderUserFullName", NullValueHandling = NullValueHandling.Ignore, Order = 32)]
		public string SenderUserFullName { get; set; }

		/// <summary>
		/// The e-mail address of the sender of the dialogue.
		/// </summary>
		[EmailAddress(ErrorMessage = "[InvalidEmailFormat] The field should contain a valid email address")] //Conforms to a valid e-mail address
		[MaxLength(100, ErrorMessage = "[StringTooLong] The field contained a string that was too long.")] //Max length of 100 characters
		[XmlElement(ElementName = "SenderUserEmailAddress", Order = 33 , IsNullable = false)]
		[JsonProperty(PropertyName = "senderUserEmailAddress", NullValueHandling = NullValueHandling.Ignore, Order = 33)]
		public string SenderUserEmailAddress { get; set; }

		/// <summary>
		/// Identifies if the resource submitted by the sender is in draft state or not.
		/// </summary>
		[XmlElement(ElementName = "IsDraftFlag", Order = 35 )]
		[JsonProperty(PropertyName = "isDraftFlag", NullValueHandling = NullValueHandling.Ignore, Order = 35)]
		public bool? IsDraftFlag { get; set; }

		/// <summary>
		/// The code that defines the type of dialogue this resource is representing, which ultimately supports the various business processes of the submission process.
		/// </summary>
		[XmlElement(ElementName = "StatusCode", Order = 40 )]
		[JsonProperty(PropertyName = "statusCode", NullValueHandling = NullValueHandling.Ignore, Order = 40)]
		public SubmissionDialogueStatusType? StatusCode { get; set; }

		/// <summary>
		/// The code that defines the type of dialogue this resource is representing, which ultimately supports the various business processes of the submission process.
		/// </summary>
		[Required()]
		[XmlElement(ElementName = "SenderActionCode", Order = 50)]
		[JsonProperty(PropertyName = "senderActionCode", Required = Required.Always, Order = 50)]
		public SubmissionDialogueFilterModel SenderActionCode { get; set; }

		/// <summary>
		/// A short explanatory note from the sender.
		/// </summary>
		[XmlElement(ElementName = "SenderNote", Order = 60 , IsNullable = false)]
		[JsonProperty(PropertyName = "senderNote", NullValueHandling = NullValueHandling.Ignore, Order = 60)]
		public string SenderNote { get; set; }

		/// <summary>
		/// The collection of document references that identify documents that are dialogue-related to support the quotation process.
		/// </summary>
		[XmlArray(ElementName = "SenderDocumentIds", Order = 70, Namespace = "http://www.londonmarketgroup.co.uk/schemas/endpoints/LMTOM/Placing/v1" , IsNullable = false)]
		[XmlArrayItem(ElementName = "SenderDocumentIdsItem", IsNullable = false, Namespace = "http://www.londonmarketgroup.co.uk/schemas/endpoints/LMTOM/Placing/v1")]
		[JsonProperty(PropertyName = "senderDocumentIds", NullValueHandling = NullValueHandling.Ignore, Order = 70)]
		public List<string> SenderDocumentIds { get; set; }

		public bool ShouldSerializeSenderDocumentIds()
		{
			return SenderDocumentIds != null && SenderDocumentIds.Count > 0;
		}

		[JsonIgnore]
		public bool SenderDocumentIdsSpecified => ShouldSerializeSenderDocumentIds();

		/// <summary>
		/// The underwriter reference for the submission.
		/// </summary>
		[XRegularExpression(@".*[^\s].*?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No whitespace
		[XmlElement(ElementName = "UnderwriterQuoteReference", Order = 100 , IsNullable = false)]
		[JsonProperty(PropertyName = "underwriterQuoteReference", NullValueHandling = NullValueHandling.Ignore, Order = 100)]
		public string UnderwriterQuoteReference { get; set; }

		/// <summary>
		/// The date that the submission from the underwriter is valid until.
		/// </summary>
		[XmlIgnore()]
		[JsonProperty(PropertyName = "underwriterQuoteValidUntilDate", NullValueHandling = NullValueHandling.Ignore, Order = 110)]
		public DateTimeOffset? UnderwriterQuoteValidUntilDate { get; set; }

		[JsonIgnore]
		[XmlElement("UnderwriterQuoteValidUntilDate", Order = 110)]
		public string UnderwriterQuoteValidUntilDateXmlDate {
			get { 
				 if(UnderwriterQuoteValidUntilDate.HasValue) { 
					var val = UnderwriterQuoteValidUntilDate.GetValueOrDefault();
					if(val != DateTimeOffset.MinValue) { 
						return val.ToString();
					}
				}
				return null;
			}

			set => UnderwriterQuoteValidUntilDate = value.TruncateTime();
		}

		[JsonIgnore]
		public bool UnderwriterQuoteValidUntilDateSpecified => UnderwriterQuoteValidUntilDate.HasValue;

		/// <summary>
		/// The date and time of when the resource was created.
		/// </summary>
		[XmlIgnore()]
		[JsonProperty(PropertyName = "createdDateTime", NullValueHandling = NullValueHandling.Ignore, Order = 200)]
		public DateTimeOffset? CreatedDateTime { get; set; }

        /// <summary>
		/// The date and time of when the resource was created.
		/// </summary>
		[XmlIgnore()]
        [JsonProperty(PropertyName = "sentDateTime", NullValueHandling = NullValueHandling.Ignore, Order = 200)]
        public DateTimeOffset? SentDateTime { get; set; }

        [JsonIgnore]
		[XmlElement("CreatedDateTime", Order = 200)]
		public string CreatedDateTimeXmlDate {
			get { 
				 if(CreatedDateTime.HasValue) { 
					var val = CreatedDateTime.GetValueOrDefault();
					if(val != DateTimeOffset.MinValue) { 
						return val.ToString();
					}
				}
				return null;
			}

			set => CreatedDateTime = value.TruncateTime();
		}

        [JsonIgnore]
        [XmlElement("SentDateTime", Order = 200)]
        public string SentDateTimeXmlDate
        {
            get
            {
                if (SentDateTime.HasValue)
                {
                    var val = SentDateTime.GetValueOrDefault();
                    if (val != DateTimeOffset.MinValue)
                    {
                        return val.ToString();
                    }
                }
                return null;
            }

            set => CreatedDateTime = value.TruncateTime();
        }

        [JsonIgnore]
		public bool CreatedDateTimeSpecified => CreatedDateTime.HasValue;

	}
}

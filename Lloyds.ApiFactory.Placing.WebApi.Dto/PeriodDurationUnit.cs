//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from LIMOSS Doc Generator Tool.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto {

	/// <summary>
	/// This is a bespoke standard for the unit of measurement.
	/// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PeriodDurationUnit {

		[Description("Days")]
		days,

		[Description("Months")]
		months,

		[Description("Weeks")]
		weeks,

		[Description("Years")]
		years,
    }
}

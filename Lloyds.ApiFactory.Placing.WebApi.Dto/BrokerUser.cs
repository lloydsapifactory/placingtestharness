//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from LIMOSS Doc Generator Tool.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Lloyds.ApiFactory.Placing.WebApi.Dto.Custom.AttributeValidations;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.WebApi.Dto {

	/// <summary>
	/// A collection of data items to represent brokers, their departments and the organisations that they belong to.
	/// </summary>
 	public partial class BrokerUser{
		/// <summary>
		/// The e-mail address of the broker.
		/// </summary>
		[Required()]
		[EmailAddress(ErrorMessage = "[InvalidEmailFormat] The field should contain a valid email address")] //Conforms to a valid e-mail address
		[XmlElement(ElementName = "BrokerUserEmailAddress", Order = 10)]
		[JsonProperty(PropertyName = "brokerUserEmailAddress", Required = Required.Always, Order = 10)]
		public string BrokerUserEmailAddress { get; set; }

		/// <summary>
		/// The name of the broker assigned to the submission.
		/// </summary>
		[Required()]
		[XRegularExpression(@".*[^\s]{2,}.*?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No more than 2 consecutive whitespace characters
		[XRegularExpression(@"[^\s](.*[^\s])?", ErrorMessage = "[ContainsWhitespace] The field contains whitespace")] //No leading or trailing whitespace
		[XmlElement(ElementName = "BrokerUserFullName", Order = 20)]
		[JsonProperty(PropertyName = "brokerUserFullName", Required = Required.Always, Order = 20)]
		public string BrokerUserFullName { get; set; }

		/// <summary>
		/// The contact number of the broker.
		/// </summary>
		[XRegularExpression(@"^(\+\d{0,2})?([\s]?(\(0\))[\s]?)?((\d)+([\s]? | [\-]?)?\d)+\d*$", ErrorMessage = "[InvalidTeleNoFormat] The field should contain a valid telephone number in the correct format")] //Conforms to a valid telephone number (landline or mobile, with or without country code)
		[XmlElement(ElementName = "BrokerUserContactNumber", Order = 40 , IsNullable = false)]
		[JsonProperty(PropertyName = "brokerUserContactNumber", NullValueHandling = NullValueHandling.Ignore, Order = 40)]
		public string BrokerUserContactNumber { get; set; }

	}
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using BoDi;
using LMTOM.Placing.Test.AcceptanceTests.StepHelpers;
using LMTOM.Placing.Test.Common.Context;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using TechTalk.SpecFlow;

namespace LMTOM.Placing.Test.AcceptanceTests.Hooks
{
    public class FrameworkHooks : ApiBaseSteps
    {
        //private readonly IObjectContainer objectContainer;

        //private static DriverContext DriverContext { get; } = new DriverContext();

        //private readonly ScenarioContext scenarioContext;
        //private readonly FeatureContext featureContext;
        //private readonly ScenarioStepContext stepContext;
        //private static WebDriverWait _wait;

        //ScenarioContext _scenarioContext;
        //FeatureContext _featureContext;

        //public FrameworkHooks(IObjectContainer objectContainer, 
        //    ApiTestContext apiTestContext,
        //    ScenarioContext scenarioContext,
        //    FeatureContext featureContext, 
        //    ScenarioStepContext stepContext) : base(objectContainer, apiTestContext, scenarioContext)
        //{
        //    this.stepContext = stepContext;
        //    this.scenarioContext = scenarioContext;
        //    this.featureContext = featureContext;
        //    this.objectContainer = objectContainer ?? throw new ArgumentNullException("scenarioContext");
        //}

        //[BeforeScenario]
        //public void BeforeTest()
        //{
        //    DriverContext.ScenarioTitle = this.scenarioContext.ScenarioInfo.Title;
        //    DriverContext.FeatureTitle = this.featureContext.FeatureInfo.Title;

        //    //DriverContext.Start();
        //    //DriverContext.NavigateToApp();
        //    //DriverContext.WindowMaximize();

        //    //LogTest.LogTestStarting(DriverContext.ScenarioTitle);
        //    //HtmlReporter.CreateScenarioContext(this.scenarioContext.ScenarioInfo.Title);

        //   // objectContainer.RegisterInstanceAs<IWebDriver>(DriverContext.Driver);
        //}

        //[BeforeScenario]
        //public void BefoeScenario()
        //{
        //    ObjectRepository.Driver = GetChromeDriver();

        //    DriverContext.ScenarioContext = _scenarioContext.ScenarioInfo.Title;
        //    DriverContext.FeatureTitle = _featureContext.FeatureInfo.Title;

        //    _objectContainer.RegisterInstanceAs(ObjectRepository.Driver);

        //}


        //private static ChromeDriver GetChromeDriver()
        //{
        //    // using a hardcoded value for now until we find a way to deal with this issue or configure it from ChromeOptions
        //    //String path = ConfigurationManager.AppSettings["chromeDriverPath"];
        //    //ChromeDriver driver = new ChromeDriver(path, GetChromeOptions());
        //    //return driver;

        //    var chromeOptions = new ChromeOptions();
        //    var seleniumOptions = apiTestContext.SeleniumOptions;
        //    if (!string.IsNullOrWhiteSpace(seleniumOptions))
        //    {
        //        chromeOptions.AddArgument(seleniumOptions);
        //    }

        //   var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
        //    _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

        //    return driver;
        //}

        //public void Dispose()
        //{
        //    //if (_chromeDriver != null)
        //    //{
        //    //    _chromeDriver.Dispose();
        //    //    _chromeDriver = null;
        //    //}

        //}
    }
}

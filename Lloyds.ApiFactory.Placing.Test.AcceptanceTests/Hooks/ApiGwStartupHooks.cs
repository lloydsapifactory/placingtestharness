//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.Test.Common.Security;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Hooks
{

    [Binding]
    public class ApiGwStartupHooks : ApiBaseSteps
    {
        private static ExtentTest _featureName;
        private static ExtentTest _scenario;
        private static DriverContext DriverContext { get; } = new DriverContext();
        private readonly ScenarioContext scenarioContext;
        private readonly FeatureContext featureContext;
        private static AventStack.ExtentReports.ExtentReports _extentReport;
        private static StateHolder _stateHolder;
        private static ApiTestContext _apiTestContext;

        public ApiGwStartupHooks(IObjectContainer objectContainer,
             ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
        {
            this.scenarioContext = scenarioContext;
            this.featureContext = featureContext;
            _stateHolder = stateHolder;
            _apiTestContext = apiTestContext;
        }

        [BeforeTestRun(Order = 1)]
        public static void CommonSetup(IObjectContainer objectContainer)
        {
            IConfiguration configuration = ReadAppSettingsJsonConfigFile();
            objectContainer.RegisterInstanceAs(configuration, typeof(IConfiguration));
            var apiTestContext = objectContainer.Resolve<ApiTestContext>();
            SetContextBaseUrl(apiTestContext, configuration.GetValue<string>("API:BaseUrl"),
                configuration.GetValue<string>("PPLUI:BaseUrl"),
                configuration.GetValue<string>("Selenium:Arguments"),
                configuration.GetValue<string>("ExtentReports:Prefs"));

            var reportsPath = apiTestContext.ExtentReportsPrefs;

            var htmlReporter = new ExtentHtmlReporter(reportsPath);

            _extentReport = new AventStack.ExtentReports.ExtentReports();
            _extentReport.AttachReporter(htmlReporter);

        }

        [BeforeTestRun]
        public static void BeforeTestRun(IObjectContainer objectContainer)
        {
            //
        }

        [BeforeFeature(Order = BindingOrderSequence.LoadAPIGwSecuritySetup)]
        [Scope(Tag = "ApiGwSecurity")]
        public static void LoadAPIGwSecuritySetup(IObjectContainer objectContainer)
        {
            if (!objectContainer.IsRegistered<IConfiguration>())
            {
                IConfiguration configuration = objectContainer.Resolve<IConfiguration>();
                //Register dependencies
                RegisterDependencies(objectContainer);

                OrganisationRoot orgConfigRoot = GetOrganisationListFromConfiguration(configuration);
                objectContainer.RegisterInstanceAs(orgConfigRoot, typeof(OrganisationRoot));

                List<UserBearerToken> userBearerTokens = GetUserBearerTokenList(objectContainer);
                List<OrganisationClientCertificate> certificate = GetOrganisationClientCertificate(objectContainer);
                List<UserCredential> userCredentials = GetUserCredentialsList(objectContainer);

                RegisterUserBearerTokenListIntoCache(objectContainer, userBearerTokens);
                RegisterOrgClientCertificateListIntoContainer(objectContainer, certificate);
                RegisterUserCredentialListIntoCache(objectContainer, userCredentials);

                //List<BrokerStore> brokerStores = GetBrokersInfo(objectContainer);
                //RegisterBrokerCode(objectContainer, brokerStores);
                //_commonSteps = new CommonSteps(objectContainer, _apiTestContext, null, null, _stateHolder);
                //_commonSteps.GivenISetSecurityTokenAndCertificateForTheUser("bellbroker.bella@limossdidev.onmicrosoft.com");
            }
        }

        [BeforeFeature]
        public static void BeforeFeature(FeatureContext featureContext)
        {
            DriverContext.FeatureContext = featureContext;

            _featureName = _extentReport.CreateTest<Feature>(BuildFeatureInfoTags() + " " + featureContext.FeatureInfo.Title);
        }

        [BeforeScenario]
        public void BeforeEveryScenario(IObjectContainer objectContainer)
        {
            Console.WriteLine("BeforeEveryScenario");

            _apiTestContext.ClearDown();

            DriverContext.ScenarioContext = scenarioContext;
            DriverContext.FeatureContext = featureContext;

            if (featureContext.Any())
            {
                if (featureContext["stateholder"] != null)
                    _stateHolder = (StateHolder)featureContext["stateholder"];

            }

            _scenario = _featureName.CreateNode<Scenario>(BuildScenarioInfoTags() + " " + DriverContext.ScenarioContext.ScenarioInfo.Title);
        }

        [AfterScenario]
        public void AfterScenario()
        {
             _apiTestContext.ClearDownWithHttpHeaders();

            //StoreFeatureContext(); // it doesnt work for now..

            Console.WriteLine("AfterEveryScenario");

            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
            {
                _scenario.Skip(DriverContext.ScenarioContext.TestError);
                _scenario.Log(Status.Skip, DriverContext.ScenarioContext.TestError);
            }

            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Warning)
            {
                _scenario.Warning(DriverContext.ScenarioContext.TestError);
                _scenario.Log(Status.Warning, DriverContext.ScenarioContext.TestError);
            }

            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ||
                TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Inconclusive)
            {
                _scenario.Fail(DriverContext.ScenarioContext.TestError);
                _scenario.Error(DriverContext.ScenarioContext.TestError);
                _scenario.Log(Status.Fail, DriverContext.ScenarioContext.TestError);

                if (_stateHolder.JsonRequestBody != null)
                {
                    _scenario.Error(_stateHolder.JsonRequestBody.JsonPrettify());
                    Console.WriteLine($"Request body JSON is : {_stateHolder.JsonRequestBody.JsonPrettify()}");
                }
            }

            BuildCategoryTags();
        }
       
        [After]
        public static void AfterTestRun()
        {
            Console.WriteLine("AfterTestRun");

            _extentReport.Flush();
        }

        [AfterStep]
        public void InsertReportingSteps()
        {
            Console.WriteLine("AfterEveryStep");

            var stepType = DriverContext.ScenarioContext.StepContext.StepInfo.StepDefinitionType;

            var testStatusProp = typeof(ScenarioContext).GetProperty("TestStatus", BindingFlags.Instance | BindingFlags.NonPublic);
            if (testStatusProp != null)
            {
                var getting = testStatusProp.GetGetMethod(nonPublic: true);
                var testResult = getting.Invoke(DriverContext.ScenarioContext, null);

                if (testResult != null && testResult == "StepDefinitionPending")
                    CreateSkipTestScenarioNode(stepType);
            }

            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                CreateFailingTestScenarioNode(stepType);
            else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                CreateSkipTestScenarioNode(stepType);
            else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                CreateWarningTestScenarioNode(stepType);
            //else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Inconclusive)
            //    CreateInconclusiveTestScenarioNode(stepType);
            else
                CreateTestScenarioNode(stepType);

            if (DriverContext.ScenarioContext.TestError != null)
            {
                if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                {
                    _scenario.Skip(DriverContext.ScenarioContext.TestError);
                    _scenario.Log(Status.Skip, DriverContext.ScenarioContext.TestError);
                }

                if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Warning)
                {
                    _scenario.Warning(DriverContext.ScenarioContext.TestError);
                    _scenario.Log(Status.Warning, DriverContext.ScenarioContext.TestError);
                }

                if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                {
                    _scenario.Fail(DriverContext.ScenarioContext.TestError);
                    _scenario.Error(DriverContext.ScenarioContext.TestError);
                    _scenario.Log(Status.Fail, DriverContext.ScenarioContext.TestError);
                }

                if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Inconclusive)
                {
                    CreateInconclusiveTestScenarioNode(stepType);

                    _scenario.Fail(DriverContext.ScenarioContext.TestError);
                    _scenario.Error(DriverContext.ScenarioContext.TestError);
                    _scenario.Log(Status.Fail, DriverContext.ScenarioContext.TestError);
                }
            }
        }

        #region private methods

        #region ExtentReports
        private void CreateTestScenarioNode(StepDefinitionType stepType)
        {
            switch (stepType)
            {
                case StepDefinitionType.Given:
                    _scenario.CreateNode<Given>(DriverContext.ScenarioContext.StepContext.StepInfo.Text);
                    break;
                case StepDefinitionType.When:
                    _scenario.CreateNode<When>(DriverContext.ScenarioContext.StepContext.StepInfo.Text);
                    break;
                default:
                    _scenario.CreateNode<Then>(DriverContext.ScenarioContext.StepContext.StepInfo.Text);
                    break;
            }
        }

        private void CreateSkipTestScenarioNode(StepDefinitionType stepType)
        {
            switch (stepType)
            {
                case StepDefinitionType.Given:
                    _scenario.CreateNode<Given>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Skip("Step Definition Pending");
                    break;
                case StepDefinitionType.When:
                    _scenario.CreateNode<When>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Skip("Step Definition Pending");
                    break;
                default:
                    _scenario.CreateNode<Then>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Skip("Step Definition Pending");
                    break;
            }
        }

        private void CreateFailingTestScenarioNode(StepDefinitionType stepType)
        {
            
            switch (stepType)
            {
                case StepDefinitionType.Given:
                    _scenario.CreateNode<Given>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Fail(DriverContext.ScenarioContext.TestError.Message);
                    break;
                case StepDefinitionType.When:
                    _scenario.CreateNode<When>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Fail(DriverContext.ScenarioContext.TestError.Message);
                    break;
                default:
                    _scenario.CreateNode<Then>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Fail(DriverContext.ScenarioContext.TestError.Message);
                    break;
            }
        }

        private void CreateWarningTestScenarioNode(StepDefinitionType stepType)
        {

            switch (stepType)
            {
                case StepDefinitionType.Given:
                    _scenario.CreateNode<Given>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Warning(DriverContext.ScenarioContext.TestError.Message);
                    break;
                case StepDefinitionType.When:
                    _scenario.CreateNode<When>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Warning(DriverContext.ScenarioContext.TestError.Message);
                    break;
                default:
                    _scenario.CreateNode<Then>(DriverContext.ScenarioContext.StepContext.StepInfo.Text).Warning(DriverContext.ScenarioContext.TestError.Message);
                    break;
            }
        }

        private void CreateInconclusiveTestScenarioNode(StepDefinitionType stepType)
        {
            var testContext = TestContext.CurrentContext;
            var testContext1 = ScenarioStepContext.Current;

            switch (stepType)
            {
                case StepDefinitionType.Given:
                    _scenario.CreateNode<Given>(testContext.Test.Name).Fail(testContext.Result.Message);
                    break;
                case StepDefinitionType.When:
                    _scenario.CreateNode<When>(testContext.Test.Name).Fail(testContext.Result.Message);
                    break;
                default:
                    _scenario.CreateNode<Then>(testContext.Test.Name).Fail(testContext.Result.Message);
                    break;
            }
        }
        #endregion

        private void StoreFeatureContext()
        {
            if (featureContext.Any(x => x.Key == "stateholder"))
                featureContext["stateholder"] = _stateHolder;
            else
                featureContext.Add("stateholder", _stateHolder);

            if (featureContext.Any(x => x.Key == "submissiondialoguecontext"))
                featureContext["submissiondialoguecontext"] = _submissionDialogueContext;
            else
                featureContext.Add("submissiondialoguecontext", _submissionDialogueContext);

            if (featureContext.Any(x => x.Key == "submissioncontext"))
                featureContext["submissioncontext"] = _submissionContext;
            else
                featureContext.Add("submissioncontext", _submissionContext);

            if (featureContext.Any(x => x.Key == "SubmissionUnderwriterContext"))
                featureContext["SubmissionUnderwriterContext"] = _SubmissionUnderwriterContext;
            else
                featureContext.Add("SubmissionUnderwriterContext", _SubmissionUnderwriterContext);

            if (featureContext.Any(x => x.Key == "SubmissionDocumentCollectionContext"))
                featureContext["SubmissionDocumentCollectionContext"] = _submissionDocumentCollectionContext;
            else
                featureContext.Add("SubmissionDocumentCollectionContext", _submissionDocumentCollectionContext);
        }

        private static string BuildFeatureInfoTags()
        {
            var tagsText = String.Empty;
            var tags = DriverContext.FeatureContext.FeatureInfo.Tags;
            foreach (var t in tags)
            {
                if (!t.Contains("ApiGwSecurity"))
                    tagsText += t + " | ";
            }

            return tagsText;
        }

        private static string BuildScenarioInfoTags()
        {
            var tagsText = String.Empty;
            var tags = DriverContext.ScenarioContext.ScenarioInfo.Tags;

            List<string> ignoredTags = new List<string> { "ApiGwSecurity" };

            foreach (var t in tags)
            {
                if (ignoredTags.Any(x=> x!=t))
                    tagsText += t + " | ";
            }

            return tagsText;
        }

        private static void BuildCategoryTags()
        {
            var tags = DriverContext.ScenarioContext.ScenarioInfo.Tags;

            List<string> ignoredTags = new List<string> { "ApiGwSecurity" };

            foreach (var t in tags)
            {
                if (ignoredTags.Any(x => x != t))
                    _scenario.AssignCategory(t);
            }
        }

        private static void RegisterBrokerCode(IObjectContainer objectContainer, List<BrokerStore> brokerStores)
        {
            foreach (BrokerStore br in brokerStores)
            {
                objectContainer.RegisterInstanceAs(br, typeof(BrokerStore), br.EmailAddress);
            }
        }

        private static void RegisterUserBearerTokenListIntoCache(IObjectContainer objectContainer, List<UserBearerToken> userBearerTokens)
        {
            foreach (UserBearerToken userBearerToken in userBearerTokens)
            {
                objectContainer.RegisterInstanceAs(userBearerToken, typeof(UserBearerToken), userBearerToken.EmailId);
            }
        }

        private static void RegisterUserCredentialListIntoCache(IObjectContainer objectContainer, List<UserCredential> userCredentials)
        {
            foreach (UserCredential userCredential in userCredentials)
            {
                objectContainer.RegisterInstanceAs(userCredential, typeof(UserCredential), userCredential.EmailId);
            }
        }

        private static void RegisterOrgClientCertificateListIntoContainer(IObjectContainer objectContainer, List<OrganisationClientCertificate> orgClientCertificateList)
        {
            //var cache = objectContainer.Resolve<IMemoryCache>();
            foreach (var orgClientCertificate in orgClientCertificateList)
            {
                objectContainer.RegisterInstanceAs(orgClientCertificate, typeof(OrganisationClientCertificate), orgClientCertificate.OrganisationName);
            }
        }

        private static IConfiguration ReadAppSettingsJsonConfigFile()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
             .AddJsonFile($"appsettings.{environmentName}.json", optional: true)
             .AddEnvironmentVariables()
             .AddUserSecrets(Assembly.GetExecutingAssembly())
             .AddEnvironmentVariables()
             .AddAzureKeyVault();

            return builder.Build();
        }

        private static void SetContextBaseUrl(ApiTestContext context, string apiBaseUrl, string uiBaseUrl, string seleniumOptions, string extentReportsPrefs)
        {
            context.ApiBaseUrl = apiBaseUrl;
            context.PPLUIBaseUrl = uiBaseUrl;
            context.SeleniumOptions = seleniumOptions;
            context.ExtentReportsPrefs = extentReportsPrefs;
        }

        private static List<UserBearerToken> GetUserBearerTokenList(IObjectContainer objectContainer)
        {
            var securityProvider = objectContainer.Resolve<SecurityProvider>();
            var orgRoot = objectContainer.Resolve<OrganisationRoot>();

            return securityProvider.GetBearerToken(orgRoot.Organisations);
        }

        private static List<UserCredential> GetUserCredentialsList(IObjectContainer objectContainer)
        {
            var securityProvider = objectContainer.Resolve<SecurityProvider>();
            var orgRoot = objectContainer.Resolve<OrganisationRoot>();

            return securityProvider.GetUserCredentials(orgRoot.Organisations);
        }

        private static List<OrganisationClientCertificate> GetOrganisationClientCertificate(IObjectContainer objectContainer)
        {
            var securityProvider = objectContainer.Resolve<SecurityProvider>();
            var orgRoot = objectContainer.Resolve<OrganisationRoot>();

            return securityProvider.GetOrganisationClientCertificate(orgRoot.Organisations);
        }

        private static OrganisationRoot GetOrganisationListFromConfiguration(IConfiguration configuration)
        {
            var organisations = configuration.GetSection("Organisations")
                .Get<List<Organisation>>();

            List<Organisation> fillteredOrgList = (from org in organisations
                                                   where org.Enabled
                                                   let userCredentials = org.UserCredentials.Where(o => o.Enabled).ToList()
                                                   select new Organisation
                                                   {
                                                       Enabled = org.Enabled,
                                                       OrganisationName = org.OrganisationName,
                                                       TokenEndpoint = org.TokenEndpoint ?? configuration.GetSection("CommonServiceTenantTokenEndpoint").Get<TokenEndpoint>(),
                                                       ClientCertificate = org.ClientCertificate,
                                                       UserCredentials = userCredentials
                                                   }).ToList();
            var organisationRoot = new OrganisationRoot
            {
                Organisations = fillteredOrgList
            };
            return organisationRoot;
        }

        private static void RegisterDependencies(IObjectContainer objectContainer)
        {
            IConfiguration configuration = objectContainer.Resolve<IConfiguration>();

            if (configuration.GetValue<bool>("ViaAPIGw"))
            {
                objectContainer.RegisterTypeAs<BearerTokenProvider, ITokenProvider>();
            }
            else
            {
                objectContainer.RegisterTypeAs<XAssertTokenProvider, ITokenProvider>();                
            }

            objectContainer.RegisterTypeAs<ClientCertificateProvider, IClientCertificateProvider>();
            objectContainer.RegisterTypeAs<SecurityProvider, SecurityProvider>();
            objectContainer.RegisterTypeAs<MemoryCache, IMemoryCache>();
        }

        #endregion

    }
}

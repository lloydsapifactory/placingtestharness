//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using OpenQA.Selenium;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Pages.PPLPage
{
    public class LoginPage : BaseMap
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {

        }


        public void EnterLoginName(string username)
        {
            var userNameInput = FindById("username");
            WaitElementToBeVisible(By.Id("userName"));
            userNameInput.SendKeys(username);
            //var userNameInput = Driver.FindElementById("userName");
            //_wait.Until(ExpectedConditions.ElementIsVisible(By.Id("userName")));
            //userNameInput.SendKeys(userCredential.PPLUsername);
        }


    }
}

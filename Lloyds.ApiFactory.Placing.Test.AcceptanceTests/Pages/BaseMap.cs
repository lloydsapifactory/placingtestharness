//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AventStack.ExtentReports.Utils;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Pages
{
    public abstract class BaseMap 
    {
        protected readonly IWebDriver Driver;

        protected BaseMap(IWebDriver driver)
        {
            Driver = driver;
        }

        public IWebElement Find(By by)
        {
            IWebElement find = null;
            Monitor(() =>
            {
                var element = FindElementWhenNotNull(by);

                WaitElementToBeNotNull(element);

                find = Driver.FindElement(by);
            });

            return find;
        }

        public void NavigateTo(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        protected IWebElement FindById(string id) => Find(By.Id(id));
        protected IWebElement FindByCss(string CssSelector) => Find(By.CssSelector(CssSelector));
        protected IWebElement FindByName(string name) => Find(By.Name(name));
        protected IWebElement FindByTagName(string name) => Find(By.TagName(name));

        protected IWebElement FindByXPath(string XPath) => Find(By.XPath(XPath));
        protected IWebElement FindButtonByContent(string content)
        {
            IWebElement element = null;

            Monitor(() =>
            {
                element = Find(By.XPath($"//button[contains(.,'{content}')]"));
            });

            return element;
        }

        protected IWebElement FindHeaderByText(string text)
        {
            IWebElement element = null;

            Monitor(() =>
            {
                element = Find(By.XPath($"//h1[contains(.,'{text}')]"));
            });

            return element;
        }

        protected IWebElement FindByLinkText(string text) => Find(By.LinkText(text));

        protected IWebElement FindLabelForId(string id) => Find(By.XPath($"//label[@for='{id}']"));

        protected IWebElement FindByAttributeContainsText(string element, string text) => Find(By.CssSelector($"{element}[data-bind *= '{text}']"));
        protected By LabelForId(string id)
        {
            By label = null;

            Monitor(() =>
            {
                Monitor(() =>
                {
                    label = By.XPath($"//label[@for='{id}']");
                });
            });
            return label;
        }

        protected IWebElement FindBy(By by) => Find(by);

        protected IWebElement FindByClass(string clazz)
            => Find(By.ClassName(clazz));

        protected IWebElement FindByXClass(string clazz)
            => Find(By.ClassName(clazz));

        protected IWebElement FindByTextToFind(string textToFind)
        {
            return Find(By.XPath("//*[contains(., '" + textToFind + "')]"));
        }

        protected By FindByTextToFindBy(string textToFind)
        {
            return By.XPath("//*[contains(., '" + textToFind + "')]");
        }

        protected void SwitchToFrame(string id)
        {
            Driver.SwitchTo().Frame(id);
        }

        internal bool? AssertWarningOrDangerPage()
        {
            var warningPage = FindElementReturningNull(By.ClassName("warning"));

            var dangerPage = FindElementReturningNull(By.ClassName("danger"));

            if (warningPage != null || dangerPage != null)
                return true;

            return false;
        }

        public void RefreshPage()
        {
            Monitor(() =>
            {
                var url = Driver.Url;
                Driver.Navigate().GoToUrl(url);
            });
        }

        protected void Monitor(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                //TakesScreenshot(); //no needed here anymore, as we embed it in the extent report instead
                throw ex;
            }
        }

        protected void ExecuteScript(string script)
        {
            Driver.ExecuteJavaScript(script);
        }

        protected void SwitchToMainPage() => Driver.SwitchTo().DefaultContent();

        protected IWebElement[] MultiFindByClass(string clazz)
        {
            Monitor(() =>
            {
                WaitElementToBeNotNull(Find(By.ClassName(clazz)));
                WaitElementToBeDisplayed(Find(By.ClassName(clazz)));
            });
            return Driver.FindElements(By.ClassName(clazz)).ToArray();
        }

        protected IWebElement[] MultiFindById(string id) => Driver.FindElements(By.Id(id)).ToArray();

        public IWebElement WaitElementToBeDisplayed(IWebElement element, int timeoutsec = 90)
        {
            Monitor(() =>
            {
                var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeoutsec));
                wait.Until(d => element.Displayed);
            });
            return element;

        }

        protected IWebElement WaitElementToBeVisible(By by, int timeoutsec = 90)
        {
            Monitor(() =>
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(90));
                wait.Until(ExpectedConditions.ElementIsVisible(by));
            });

            return Find(by);
        }

        public IWebElement WaitElementToBeNotNull(IWebElement element, int timeoutsec = 90)
        {
            try
            {
                var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeoutsec));
                wait.Until(d => element != null);
                return element;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }

        public void ClickNext(By by)
        {
            var nextButton = FindBy(by);
            WaitElementToBeNotNull(nextButton);
            WaitElementToBeDisplayed(nextButton);
            WaitTobeClickableAnClick(by);
        }

        protected IWebElement WaitTobeVisibleAndReturnElement(By by)
        {
            var pin1 = FindBy(by);
            WaitElementToBeDisplayed(pin1);
            return pin1;
        }

        protected void WaitTobeClickableAnClick(By by)
        {
            var element = FindBy(by);
            WaitElementToBeNotNull(element);
            Driver.WaitForElementToBeClickableAndClick(element);
        }
        public string Title() => Driver.Title;
        public string Url() => Driver.Url;

        public bool TitleIs(string title, int timeout = 10)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(ExpectedConditions.TitleIs(title));
        }

        public bool TitleIsCustom(string title)
        {
            try
            {
                if (TitleIs(title))
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsDisplayed2(Func<IWebElement> element)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
            return wait.Until(driver => element().Displayed); // this doesn't work?l
        }

        public bool IsDisplayed(Func<IWebElement> element)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
            return wait.Until(driver => !element().Text.IsNullOrEmpty());
        }

        public bool IsNotDisplayed(Func<IWebElement> element)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
            return wait.Until(driver => element().Text.IsNullOrEmpty());
        }

        public void ClickElementUsingJavascript(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("arguments[0].click();", element);
        }

        public void ClickElementUsingOnClick(IWebElement element)
        {
            string script = element.GetAttribute("onclick");
            Driver.ExecuteJavaScript<object>(script);
        }

        public void EnterTextUSingJScript(IWebElement element, string value)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("document.getElementById('pin').value = '" + value + "';", element);

        }

        //public string TakesScreenshot()
        //{
        //    var path = ConfigurationManager.AppSettings["screenshotFolder"];
        //    DirectoryInfo validation = new DirectoryInfo(path); //System IO object

        //    if (validation.Exists) //Capture screen if the path is available
        //    {
        //        string currTime = DateTime.Now.ToString(@"MMM-ddd-d-HH.mm.ss");
        //        var fileName = $"LoansXSell_Err-{StateHolder.TestEnvironment.ToUpper()}-{currTime}";

        //        string fn = Path.Combine(path, path + "\\" + fileName + ".png");

        //        var ss = ((ITakesScreenshot)Driver).GetScreenshot();

        //        using (var ms = new MemoryStream(ss.AsByteArray))
        //        using (var screenShotImage = Image.FromStream(ms))
        //        {
        //            var cp = new Bitmap(screenShotImage);
        //            cp.Save(fn, ImageFormat.Png);
        //            cp.Dispose();
        //        }

        //        UploadFileToAzureStorage(fn);

        //        return fileName;

        //    }

        //    return string.Empty;

        //}
               
        public IWebElement FindElementWhenNotNull(By by)
        {
            IWebElement element = null;

            Monitor(() =>
            {
                element = Driver.FindElementWhenNotNull(by);
            });

            return element;

        }

        public IWebElement FindElementReturningNull(By by)
        {
            IWebElement element = null;

            try
            {
                element = Driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                return null;
            }
            catch (StaleElementReferenceException)
            {
                return null;
            }

            return element;
        }

        public IWebElement WaitAndFindElement(By selector, int timeoutsec = 90)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    var element = Driver.FindElement(selector);
                    return element;
                }
                catch (NoSuchElementException)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
                catch (StaleElementReferenceException)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
            }
            return null;

        }

        public List<IWebElement> FindElementsBy(By by)
        {
            return Driver.FindElements(by).ToList();
        }

        public void WaitUntiPresence(By by)
        {
            WebDriverWait pageWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            pageWait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));

        }

        public bool IsElementExists(By by, int timeoutsec = 2)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeoutsec));
                wait.Until(ExpectedConditions.ElementExists(by));

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        //public void WriteTestOutput(string message)
        //{
        //    EventLog.WriteEntry("mysource", "output: " + message);
        //}

        //public void WriteToolOutput(string message)
        //{
        //    EventLog.WriteEntry("mysource", "specflow: " + message);
        //}

        public IWebElement TitleButton(string title)
        {
            var titleButton = FindElementWhenNotNull(By.XPath($"//label[@for='{title}']"));
            return titleButton;
        }


    }
}

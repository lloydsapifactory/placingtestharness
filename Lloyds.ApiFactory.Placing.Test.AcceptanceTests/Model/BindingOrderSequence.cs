//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class BindingOrderSequence
    {
        public const int CommonSetup = 1;
        public const int LoadAPIGwSecuritySetup = 1;
    };
}

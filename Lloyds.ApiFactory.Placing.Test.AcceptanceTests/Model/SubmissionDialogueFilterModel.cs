﻿using Lloyds.ApiFactory.Placing.WebApi.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class SubmissionDialogueFilterModel
    {
        public bool IsDraftFlag { get; set; }

        public WebApi.Dto.SubmissionDialogueFilterModel SenderActionCode { get; set; }

    }
}

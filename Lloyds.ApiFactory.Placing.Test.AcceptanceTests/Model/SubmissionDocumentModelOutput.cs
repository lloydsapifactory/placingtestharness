//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class SubmissionDocumentModelOutput : SubmissionDocument
    {
        [JsonIgnore]
        public int ExpectedOutput { get; set; }
        [JsonIgnore]
        public int ActualOutput { get; set; }

    }
}

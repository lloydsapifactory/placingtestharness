//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.WebApi.Dto;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class SubmissionDocumentUpdateModel 
    {
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
        public bool ShareableToAllMarketsFlag { get; set; }

        public string SubmissionReference { get; set; }
        public string SubmissionVersionNumber { get; set; }
    }
}

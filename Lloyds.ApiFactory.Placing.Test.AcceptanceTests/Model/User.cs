//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class User
    {   public string UserEmailAddress;
        public int BrokerCode;
    }
}

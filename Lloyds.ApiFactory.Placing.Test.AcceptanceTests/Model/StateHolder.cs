//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Collections.Generic;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
   public  class StateHolder
    {

        public StateHolder(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext)
        {
        }

        public string LoginUser { get; set; }
        public string BrokerUserEmailAddress { get; set; }
        public string UnderwriterUserEmailAddress { get; set; }

        public Dictionary<string, string> _sParamsBuilder { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> _sTestParameters { get; set; } = new Dictionary<string, string>();

        public string ResourceRequest { get; set; }

        public string ParamsBuilderValueToValidate { get; set; }

        public List<string> ParamsBuilderValuesToValidate { get; set; } = new List<string>();

        public SubmissionDocument CurrentSubmissionDocument { get; set; }
        public SubmissionDocumentCollectionWrapper CurrentSubmissionDocumentCollection { get; set; }

        public List<SubmissionDocument> CurrentSubmissionDocuments { get; set; } = new List<SubmissionDocument>();

        public Submission CurrentSubmission { get; set; }

        public List<Submission> CurrentSubmissions { get; set; } = new List<Submission>();

        public string JsonRequestBody { get; set; }

        public List<WebApi.Dto.BrokerDepartment> BrokerDepartments { get; set; }

        public List<UnderwriterOrganisation> UnderwriterOrganisations { get; set; }

        public UnderwriterOrganisation UnderwriterOrganisation { get; set; }

        public List<SubmissionUnderwriter> CurrentSubmissionUnderwriters { get; set; } = new List<SubmissionUnderwriter>();
        
        public SubmissionUnderwriter CurrentSubmissionUnderwriter { get; set; } 

        public SubmissionDialogue CurrentSubmissionDialogue { get; set; }

        public string CurrentPlacingSlipId { get; set; }

        public List<string> CurrentSenderDocumentIds { get; set; }

        public int GlobalBrokerDepartmentId { get; set; }

        public string CurrentBrokerDepartmentId { get; internal set; }

        public System.DateTime CreatedDateTime { get; set; }
        public System.DateTime LastModifiedDateTime { get; set; }


        public List<SubmissionDialogue> CurrentSubmisionDialogues { get; set; } = new List<SubmissionDialogue>();

        public string DocumentId { get; set; }

    }
}

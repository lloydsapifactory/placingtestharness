//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.WebApi.Dto;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class SubmissionDocumentCreateModel : SubmissionDocument
    {

        public string BrokerCode { get; set; }
        public string BrokerDepartmentId { get; set; }

        public string BrokerUserEmailAddress { get; set; }
        public string JsonPayload { get; set; }

        public string File { get; set; }


    }
}

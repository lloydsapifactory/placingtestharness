//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.WebApi.Dto;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model
{
    public class SubmissionUnderwriterPUT 
    {

        public string SubmissionUnderwriterId { get; set; }
        public string SubmissionReference { get; set; }
        public int SubmissionVersionNumber { get; set; }
        public string UnderwriterUserFullName { get; set; }
        public string UnderwriterUserEmailAddress { get; set; }
        public CommunicationMethod? CommunicationsMethodCode { get; set; }

    }
}

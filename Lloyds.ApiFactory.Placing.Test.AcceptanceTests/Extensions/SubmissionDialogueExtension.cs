//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions
{
    public static class SubmissionDialogueExtension 
    {
        public static void ValidateSubmissionDialogueWhenPostedAs(SubmissionDialogue qmd, string postedBy, StateHolder _stateHolder)
        {
            if (postedBy == "broker")
            {
                if (qmd.Id.Equals("B1225QR0CMd39AmlqdBk_1_RQ13215")) //dodgy ebix-Db data
                    return;

                Assert.AreEqual(qmd.SenderParticipantCode, ParticipantRole.B);
                Assert.True(qmd.SenderNote.Length > 0);

                if (qmd.SenderParticipantCode != ParticipantRole.U) // when we post back as U/W we dont necessarily have SenderDocumentIds
                {
                    Assert.IsTrue(qmd.SenderDocumentIds.Any());

                    // check sender document ids are included in posted ones
                    foreach (var returnSenderDocumentId in qmd.SenderDocumentIds)
                    {
                        Assert.IsTrue(_stateHolder.CurrentSubmissionDocuments.Select(x => x.DocumentId)
                            .Any(x => x.Equals(returnSenderDocumentId, StringComparison.OrdinalIgnoreCase)));
                    }
                }
            }

            if (postedBy == "underwriter")
            {
                Assert.AreEqual(qmd.SenderParticipantCode, ParticipantRole.U);
                Assert.IsNotNull(qmd.SenderUserEmailAddress);
                Assert.IsTrue(qmd.SenderUserEmailAddress.Length > 0);
                Assert.IsNotNull(qmd.SenderUserFullName);
                Assert.IsTrue(qmd.SenderUserFullName.Length > 0);

                if (_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteReference != null)
                {
                    Assert.IsTrue(qmd.UnderwriterQuoteReference.Length > 0);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteReference, qmd.UnderwriterQuoteReference);
                }

                Assert.IsNotNull(qmd.IsDraftFlag);
                Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.IsDraftFlag, qmd.IsDraftFlag);

                if (_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteValidUntilDate != null)
                {
                    Assert.IsNotNull(qmd.UnderwriterQuoteValidUntilDate);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteValidUntilDate, qmd.UnderwriterQuoteValidUntilDate);
                }

                if (_stateHolder.CurrentSubmissionDialogue.SenderNote != null)
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderNote, qmd.SenderNote);

                Assert.IsNotNull(qmd.SenderActionCode);
                Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderActionCode, qmd.SenderActionCode);
                Assert.IsTrue(qmd.SenderUserEmailAddress.Equals(_stateHolder.UnderwriterUserEmailAddress, 
                    StringComparison.OrdinalIgnoreCase));
            }
        }

        public static void ValidateSubmissionDocumentAttached(SubmissionDialogue qmd, 
            StateHolder _stateHolder)
        {
            if (qmd.SenderDocumentIds != null)
            {
                if (_stateHolder.CurrentSenderDocumentIds != null)
                {
                    foreach (var docAttached in qmd.SenderDocumentIds)
                        _stateHolder.CurrentSenderDocumentIds.Any(x => x.Equals(docAttached,
                            StringComparison.OrdinalIgnoreCase));
                }
            }
        }

        public static void ValidateDocumentUploadAndAttached(SubmissionDialogue qmd,
            SubmissionDocument[] allSubmissionDocuments,
            List<string> documentNames)
        {
            bool found = false;

            foreach (var senderDocumentId in qmd.SenderDocumentIds)
            {
                foreach (var qd in allSubmissionDocuments)
                {
                    if (qd.DocumentId.Equals(senderDocumentId, 
                        StringComparison.OrdinalIgnoreCase))
                    {
                        found = true;

                        Assert.IsTrue(documentNames.Any(x => x.Equals(qd.FileName,
                            StringComparison.OrdinalIgnoreCase)));
                        break;
                    }
                }
            }

            Assert.IsTrue(found, "Document not found in at all.");
        }

        public static void ValidateDateTimeAndStatusCode(SubmissionDialogue qmd)
        {
            Assert.IsNotNull(qmd.CreatedDateTime);
            Assert.IsNotNull(qmd.CreatedDateTime != DateTime.MinValue);

            if (qmd.IsDraftFlag ?? false)
            {
                Assert.IsNull(qmd.SentDateTime, "SentDateTime should be null");
                Assert.IsTrue(qmd.StatusCode == SubmissionDialogueStatusType.DRFT, "The StatusCode should be in DRFT state");
            }

            if (!(qmd.IsDraftFlag ?? false))
            {
                Assert.IsNotNull(qmd.SentDateTime, "SentDateTime should not be null");
                Assert.IsNotNull(qmd.SentDateTime != DateTime.MinValue);
                Assert.IsTrue(qmd.StatusCode == SubmissionDialogueStatusType.DELV, "The StatusCode should be in DELV state");
            }
        }

            public static void ValidateMandatoryFields(SubmissionDialogue qmd)
            {
                Assert.IsNotNull(qmd.SubmissionDialogueId);
                Assert.IsNotNull(qmd.SubmissionReference);
                Assert.IsNotNull(qmd.SubmissionVersionNumber);
                Assert.IsNotNull(qmd.SenderParticipantCode);
                Assert.IsNotNull(qmd.SenderUserFullName);
                Assert.IsNotNull(qmd.SenderUserEmailAddress);
                Assert.IsNotNull(qmd.StatusCode);
                Assert.IsNotNull(qmd.SenderActionCode);
                Assert.IsNotNull(qmd.CreatedDateTime);
            }

            public static void AssertSubmissionDialogue(SubmissionDialogue current, 
                string postedBy,
                StateHolder _stateHolder)
            {
                if (postedBy == "broker")
                {
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.CreatedDateTime, current.CreatedDateTime);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.IsDraftFlag, current.IsDraftFlag);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SubmissionReference, current.SubmissionReference);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SubmissionVersionNumber, current.SubmissionVersionNumber);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderActionCode, current.SenderActionCode);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderNote, current.SenderNote);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderUserEmailAddress, current.SenderUserEmailAddress);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderUserFullName, current.SenderUserFullName);
                    Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.SenderDocumentIds.Count(), current.SenderDocumentIds.Count());

                    foreach (var docId in _stateHolder.CurrentSubmissionDialogue.SenderDocumentIds)
                        Assert.IsTrue(current.SenderDocumentIds.Any(x => x.Equals(docId, StringComparison.OrdinalIgnoreCase)));

                    if (current.UnderwriterQuoteReference != null && _stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteReference != null)
                        Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteReference, current.UnderwriterQuoteReference);

                    if (current.UnderwriterQuoteReference != null)
                        Assert.AreEqual(_stateHolder.CurrentSubmissionDialogue.UnderwriterQuoteValidUntilDate, current.UnderwriterQuoteValidUntilDate);
                }
            }


            public static void AssertSubmissionDialogueWithSubmission(SubmissionDialogue qmd, Submission submissionContext)
            {
                Assert.AreEqual(qmd.SubmissionReference, submissionContext.SubmissionReference);
                Assert.AreEqual(qmd.SubmissionVersionNumber, submissionContext.SubmissionVersionNumber);
            }

            public static void AssertSenderDocumentIds(SubmissionDialogue qmd, StateHolder _stateHolder)
            {
                if (_stateHolder.CurrentSubmissionDialogue.SenderDocumentIds != null)
                {
                    foreach (var senderDocId in _stateHolder.CurrentSubmissionDialogue.SenderDocumentIds)
                    {
                        Assert.True(qmd.SenderDocumentIds.Any(x => x.Equals(senderDocId,
                            StringComparison.OrdinalIgnoreCase)));
                    }
                }
            }

        public static SubmissionDialogue ReturnRandomSubmissionDialogue(object[] array)
        {
            var random = new Random();

            if (array.Any())
            {
                var AllQmds = (SubmissionDialogue[])array;
                var selectedQMDs = AllQmds;

                if (selectedQMDs.Any())
                {
                    int index = random.Next(selectedQMDs.Count());

                    var selectedQMD = selectedQMDs[index];
                    return selectedQMD;
                }
                else
                    Assert.Fail($"Couldnt find any SubmissionDialogue.");
            }
            else
                Assert.Inconclusive($"Submission Dialogues are empty, so cannot return any random.");

            return null;
        }

        public static SubmissionDialogue ReturnRandomSubmissionDialogueWithSenderDocIds(object[] array)
        {
            var random = new Random();

            if (array.Any())
            {
                var AllQmds = (SubmissionDialogue[])array;
                var selectedQMDs = AllQmds;

                if (selectedQMDs.Any())
                {
                    SubmissionDialogue selectedQMD = null;

                    for (int i = 0; i < 20; i++)
                    {
                        int index = random.Next(selectedQMDs.Count());

                        selectedQMD = selectedQMDs[index];

                        if (selectedQMD.SenderDocumentIds != null)
                            break;
                    }

                    return selectedQMD;
                }
                else
                    Assert.Fail($"Couldnt find SubmissionDialogue.");
            }

            return null;
        }



        public static SubmissionDocumentCreateModel SetSubmissionDocumentModel(TableRow row, 
            SubmissionDialogue submissionDialogueContext)
        {
            var addSubmissionDoc = new SubmissionDocumentCreateModel()
            {
                DocumentType = row["DocumentType"].ToString(),
                DocumentDescription = row["DocumentDescription"].ToString(),
                SubmissionReference = submissionDialogueContext.SubmissionReference,
                SubmissionVersionNumber = submissionDialogueContext.SubmissionVersionNumber,
                JsonPayload = row["JSONPayload"].ToString(),
                SuppliedBy = ParticipantRole.U,

                ShareableToAllMarketsFlag = row.FirstOrDefault(x=> x.Key == "ShareableToAllMarketsFlag").Value != null ?
                (row["ShareableToAllMarketsFlag"] == "Y" ? true : false) : false,

                File = row["File"].ToString(),
                FileMIMEType = row["FileMimeType"].ToString(),
                FileName = row["FileName"].ToString(),
                ReplacingDocumentId = row.Any(x => x.Key == "ReplacingDocumentId") ? row["ReplacingDocumentId"] : ""

                //FileSizeInBytes = docRow["FileSizeInBytes"] != null ? Convert.ToInt32(docRow["FileSizeInBytes"]) : 0
            };

            return addSubmissionDoc;
        }

        public static SubmissionDocumentCreateModel SetSubmissionDocumentModel(SubmissionDocument sd,
            TableRow row)
        {
            var addSubmissionDoc = new SubmissionDocumentCreateModel()
            {
                DocumentType = sd.DocumentType,
                DocumentDescription = sd.DocumentDescription,
                SubmissionReference = sd.SubmissionReference,

                SubmissionVersionNumber = row.Any(x => x.Key == "SubmissionVersionNumber") ? 
                Convert.ToInt32(row["SubmissionVersionNumber"]) : sd.SubmissionVersionNumber,

                ReplacingDocumentId = row.Any(x => x.Key == "ReplacingDocumentId") ? row["ReplacingDocumentId"] : sd.DocumentId,

                JsonPayload = @"Data\1_2_Model\SubmissionDocument_1.json",
                SuppliedBy = ParticipantRole.U,
                ShareableToAllMarketsFlag = sd.ShareableToAllMarketsFlag,
                File = row["File"].ToString(),
                FileMIMEType = row["FileMimeType"].ToString(),
                FileName = row["ReplaceWith"].ToString()
            };

            return addSubmissionDoc;
        }


        public static SubmissionDocument FindFileInSubmissionDocuments(StateHolder stateHolder, string filename)
        {
           return stateHolder.CurrentSubmissionDocuments
                    .FirstOrDefault(x => x.FileName.Equals(filename,
                        StringComparison.OrdinalIgnoreCase));
        }

    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions
{
    public static class SubmissionExtension
    {
        public static bool SubmissionEquals(this Submission submission,  Submission other)
        {
            if (other is null)
                return false;

            var equals =
                (submission.BrokerUserEmailAddress ?? string.Empty) == (other.BrokerUserEmailAddress ?? string.Empty) &&
                (submission.BrokerCode ?? string.Empty) == (other.BrokerCode  ?? string.Empty) &&
                (submission.ClassOfBusinessCode ?? string.Empty) == (other.ClassOfBusinessCode  ?? string.Empty) &&
                (submission.ContractDescription ?? string.Empty) == (other.ContractDescription  ?? string.Empty) &&
                (submission.InsuredOrReinsured ?? string.Empty) == (other.InsuredOrReinsured  ?? string.Empty) &&
                (submission.OriginalPolicyholder ?? string.Empty) == (other.OriginalPolicyholder  ?? string.Empty) &&
                (submission.CoverTypeCode ?? string.Empty) == (other.CoverTypeCode  ?? string.Empty) &&
                (submission.ProgrammeReference ?? string.Empty) == (other.ProgrammeReference  ?? string.Empty) &&
                (submission.ProgrammeDescription ?? string.Empty) == (other.ProgrammeDescription  ?? string.Empty) &&
                (submission.RiskRegionCode ?? string.Empty) == (other.RiskRegionCode  ?? string.Empty) &&
                (submission.RiskCountryCode ?? string.Empty) == (other.RiskCountryCode ?? string.Empty) &&
                (submission.SubmissionReference ?? string.Empty) == (other.SubmissionReference ?? string.Empty);

            if (submission.PeriodOfCover != null && other.PeriodOfCover != null
                && submission.PeriodOfCover.InsurancePeriod != null && (other.PeriodOfCover.InsurancePeriod != null))
            {
                equals = (DateTimeOffset.Compare(
                    Extension.HandleNull<DateTimeOffset>(submission.PeriodOfCover.InsurancePeriod.InceptionDate),
                    Extension.HandleNull<DateTimeOffset>(other.PeriodOfCover.InsurancePeriod.InceptionDate)) == 0) &&
                    (DateTimeOffset.Compare(
                    Extension.HandleNull<DateTimeOffset>(submission.PeriodOfCover.InsurancePeriod.ExpiryDate),
                    Extension.HandleNull<DateTimeOffset>(other.PeriodOfCover.InsurancePeriod.ExpiryDate)) == 0) && equals;
            }

            return equals;
        }

        public static Submission MapFieldsAbsent(Submission submissionContext,
            Submission submission)
        {
            foreach (var pInfo in submissionContext.GetType().GetProperties())
            {
                var submissionproperty = submission.GetType().GetProperty(pInfo.Name);

                if (submissionproperty is null)
                    throw new AssertionException($"The submission scenario cannot find the property {pInfo.Name}");

                if (submissionproperty.GetValue(submission) == null)
                    submissionproperty.SetValue(submission, pInfo.GetValue(submissionContext));
            }

            return submission;
        }

        public static SubmissionCreate MapFieldsAbsent(Submission submissionContext,
            SubmissionCreate submission)
        {
            foreach (var pInfo in submissionContext.GetType().GetProperties())
            {
                var submissionproperty = submission.GetType().GetProperty(pInfo.Name);

                if (submissionproperty is null)
                    throw new AssertionException($"The submission scenario cannot find the property {pInfo.Name}");

                if (submissionproperty.GetValue(submission) == null)
                    submissionproperty.SetValue(submission, pInfo.GetValue(submissionContext));
            }

            return submission;
        }

        public static Submission MapFieldsAbsentInHeader(Submission submissionContext, 
            Submission submission,
            List<string> headers)
        {
            foreach (var pInfo in submissionContext.GetType().GetProperties())
            {
                if (!headers.Any(a => a.Equals(pInfo.Name)) && (pInfo.CanWrite) && (pInfo.GetValue(submissionContext) != null))
                {
                    var submissionproperty = submission.GetType().GetProperty(pInfo.Name);
                    if (submissionproperty is null)
                        throw new AssertionException($"The submission scenario cannot find the property {pInfo.Name}");

                    submissionproperty.SetValue(submission, pInfo.GetValue(submissionContext));
                }
            }

            return submission;
        }

        public static Submission MapFieldsAbsentInHeader(SubmissionDocument submissionContext,
            Submission submission,
            List<string> headers)
        {
            foreach (var pInfo in submissionContext.GetType().GetProperties())
            {
                if (!headers.Any(a => a.Equals(pInfo.Name)) && (pInfo.CanWrite) && (pInfo.GetValue(submissionContext) != null))
                {
                    var submissionproperty = submission.GetType().GetProperty(pInfo.Name);
                    if (submissionproperty is null)
                        throw new AssertionException($"The submission scenario cannot find the property {pInfo.Name}");

                    submissionproperty.SetValue(submission, pInfo.GetValue(submissionContext));
                }
            }

            return submission;
        }

        public static void SetSubmissionValues(Submission submission, Submission submissionContext)
        {
            var randomStr = Extension.GenerateRandomString(8);

            var factorString = $"B{submission.BrokerCode}{randomStr}";

            submission.SubmissionReference = $"{factorString}";
            submission.ProgrammeReference = $"{factorString}";
            submission.ContractReference = $"{factorString}";

            MapFieldsAbsent(submissionContext, submission);
        }

        public static void SetSubmissionValues(SubmissionCreate submission, Submission submissionContext)
        {
            var randomStr = Extension.GenerateRandomString(8);

            var factorString = $"B{submission.BrokerCode}{randomStr}";

            submission.SubmissionReference = $"{factorString}";
            submission.ProgrammeReference = $"{factorString}";
            submission.ContractReference = $"{factorString}";

            MapFieldsAbsent(submissionContext, submission);
        }

        public static void ValidateSubmissionsForSameDepartment(object[] submissions, 
            BrokerDepartment[] brokerDepartments, 
            string brokerUserEmailAddress)
        {
            foreach (var submission in submissions)
            {
                var subm = (Submission)submission;

                var submissionBrokerDepartmentId = subm.BrokerDepartmentId;

                foreach (var department in brokerDepartments)
                {
                    if (department.BrokerUsers.ToList().Any(x => x.BrokerUserEmailAddress.Equals(brokerUserEmailAddress,
                        StringComparison.OrdinalIgnoreCase)))
                    {
                        Assert.IsTrue(department.BrokerDepartmentId.Equals(submissionBrokerDepartmentId,
                            StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
        }


        public static Submission ReturnRandomSubmission(object[] array)
        {
            var random = new Random();

            if (array.Any())
            {
                var selectedSubmissions = (Submission[])array;

                if (selectedSubmissions.Any())
                {
                    int index = random.Next(selectedSubmissions.Count());

                    var selected = selectedSubmissions[index];
                    return selected;
                }
                else
                    Assert.Fail($"Couldnt find Submission.");
            }
            else
                Assert.Fail($"Coudn't find any Submission to selecte a random one.");

            return null;
        }

        public static string ReturnSubmissionReference(string brokercode, 
            string randomStr)
        {
            return $"B{brokercode}QR{randomStr}9A";
        }


        public static void ReplaceSubmissionValue(Object obj,
            string fieldname,
            string oldValue,
            string newValue)
        {
            var submissionproperty = obj.GetType().GetProperty(fieldname);

            var valueAssert = ReflectionHelper.GetPropertyValue(obj, fieldname).ToString();

            var replaceWith = valueAssert.Replace(oldValue, newValue);

            submissionproperty.SetValue(obj, replaceWith);
        }

    }
}

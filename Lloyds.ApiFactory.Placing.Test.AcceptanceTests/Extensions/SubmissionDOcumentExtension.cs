//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions
{
    public static class SubmissionDocumentExtension
    {
        public static Submission MapDocumentCreateModelToSubmission(string brokerEmailAddress, 
            IEnumerable<Model.SubmissionDocumentCreateModel> submissionDocuments)
        {
            var submission = TransformsSubmissionDocumenttoSubmission(submissionDocuments.First());
            submission.BrokerUserEmailAddress = brokerEmailAddress;

            return submission;
        }

        private static SubmissionCreate TransformsSubmissionDocumenttoSubmissionCreate(SubmissionDocumentCreateModel submissionDocumentCreateModel)
        {
            return new SubmissionCreate()
            {
                BrokerCode = submissionDocumentCreateModel.BrokerCode,
                BrokerDepartmentId = submissionDocumentCreateModel.BrokerDepartmentId,
                BrokerUserEmailAddress = submissionDocumentCreateModel.BrokerUserEmailAddress,
                SubmissionReference = submissionDocumentCreateModel.SubmissionReference,
                SubmissionVersionDescription = submissionDocumentCreateModel.SubmissionVersionNumber.ToString(),

            };

        }

        private static Submission TransformsSubmissionDocumenttoSubmission(SubmissionDocumentCreateModel submissionDocumentCreateModel)
        {
            return new Submission()
            {
                BrokerCode = submissionDocumentCreateModel.BrokerCode,
                BrokerDepartmentId = submissionDocumentCreateModel.BrokerDepartmentId,
                BrokerUserEmailAddress = submissionDocumentCreateModel.BrokerUserEmailAddress,
                SubmissionReference = submissionDocumentCreateModel.SubmissionReference,
                SubmissionVersionDescription = submissionDocumentCreateModel.SubmissionVersionNumber.ToString(),

            };

        }

        public static SubmissionDocument MapFieldsAbsentInHeaderDocument(SubmissionDocument submissionDocContext,
        SubmissionDocument submissionDoc,
        List<string> headers)
        {
            foreach (var pInfo in submissionDocContext.GetType().GetProperties())
            {
                if (!headers.Any(a => a.Equals(pInfo.Name)) && (pInfo.CanWrite) && (pInfo.GetValue(submissionDocContext) != null))
                {
                    var submissionproperty = submissionDoc.GetType().GetProperty(pInfo.Name);
                    if (submissionproperty is null)
                        throw new AssertionException($"The submission scenario cannot find the property {pInfo.Name}");

                    submissionproperty.SetValue(submissionDoc, pInfo.GetValue(submissionDocContext));
                }
            }

            return submissionDoc;
        }

    }

    
}

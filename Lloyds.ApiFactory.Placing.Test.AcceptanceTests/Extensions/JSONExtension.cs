//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.IO;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions
{
    public static class JSONExtension
    {
        public static string JsonPrettify(this string json)
        {
            using (var stringReader = new StringReader(json))
            using (var stringWriter = new StringWriter())
            {
                var jsonReader = new JsonTextReader(stringReader);
                var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
                jsonWriter.WriteToken(jsonReader);
                return stringWriter.ToString();
            }
        }

        public static string UpdateJsonRequestField(string originaljson,
           string field,
           string valueToReplace,
           StateHolder stateHolder)
        {
            JObject requestObj = JObject.Parse(originaljson);

            // replace the value of the field
            requestObj[field] = valueToReplace;

            string newJson = requestObj.ToString(Formatting.None);

            // updating or saving the json with the new value
            stateHolder.JsonRequestBody = newJson;

            return newJson;
        }
    }
}

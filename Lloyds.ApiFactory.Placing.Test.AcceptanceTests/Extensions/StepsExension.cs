//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions
{
    public static class StepsExension
    {
        public static void AssertPropertyAndValue(Object obj, string fieldname)
        {
            var propAssert = ReflectionHelper.GetPropertyCc(obj, fieldname.Trim().ToLower());

            if (propAssert == null)
                Assert.Fail($"Couldn't find the field {fieldname}.");

            var valueAssert = ReflectionHelper.GetPropertyValue(obj, fieldname);

            if (valueAssert == null)
                Assert.Fail($"The value of the field {fieldname} is null.");

            var submissionproperty = obj.GetType().GetProperty(fieldname);

            if ((submissionproperty.PropertyType == typeof(DateTimeOffset)) || (submissionproperty.PropertyType == typeof(DateTimeOffset?)))
            {
                var dto = (DateTimeOffset)valueAssert;

                if (dto.UtcDateTime == DateTime.MinValue)
                    Assert.Fail($"{fieldname} datetime is {dto.UtcDateTime} minimum datetime value.");
            }
        }

        public static void ValidateDateTimeSort(DateTime currentDateTime,
            DateTime previousDateTime, 
            string ordering = "ascending")
        {

            if (currentDateTime != DateTime.MinValue)
            {
                int resultDt = DateTime.Compare(previousDateTime, currentDateTime); // previousDatetime < currentDt RETURNS -1

                if (previousDateTime != DateTime.MinValue)
                {
                    if (resultDt < 0 && ordering == "descending")
                        Assert.Fail("Ordering is not correct.");

                    if (resultDt > 0 && ordering == "ascending")
                        Assert.Fail("Ordering is not correct.");
                }
            }

        }

       

        public static void ValidateOkResponse(RestSharp.IRestResponse restResponse)
        {
            if (restResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Assert.Fail($"An api level error has occured : {restResponse.StatusCode}");
                throw new AssertionException($"An api level error has occured : {restResponse.StatusCode}");
            }
        }

        public static void ValidateOkCreatedResponse(RestSharp.IRestResponse restResponse)
        {
            if (restResponse.StatusCode != System.Net.HttpStatusCode.OK &&
                restResponse.StatusCode != System.Net.HttpStatusCode.Created)
            {
                Assert.Fail($"An api level error has occured : {restResponse.StatusCode}");
                throw new AssertionException($"An api level error has occured : {restResponse.StatusCode}");
            }
        }

        public static void CompareAndValidateDateTime(string comparison,
            DateTimeOffset currentValue,
            DateTimeOffset dtToCompareWith)
        {
            if (currentValue != DateTime.MinValue)
            {
                if (currentValue == dtToCompareWith)
                    Assert.True(currentValue.Date == dtToCompareWith.Date);

                if (comparison == "before")
                    Assert.True(currentValue.Date <= dtToCompareWith.Date);

                if (comparison == "after")
                    Assert.True(currentValue.Date >= dtToCompareWith.Date);

                if (comparison == "between") //works only for yesterday - today 
                {
                    Assert.False(currentValue.Date >= dtToCompareWith.Date.AddDays(1),
                        $"{currentValue.Date} cannot be more than {dtToCompareWith.AddDays(1)}");   //datetime is no greater than today
                  
                    Assert.False(currentValue.Date <= dtToCompareWith.Date.AddDays(-2),
                        $"{currentValue.Date} cannot be less than {dtToCompareWith.Date.AddDays(-2)}");  //datetime is no lesser than yesterday, -2 days as it was returning 
                }
            }
            else
            {
                // NOT SURE WHETHER WE SHOULD VALIDATE ANY MIN VALUES HERE, AS IT RESULTS INTO DODGY DATA SOME TIMES
            }
        }

    public static void ValidateStringSort(string lastStringFound, object property, string ordering = "ascending")
        {
            if (lastStringFound.Length > 0)
            {
                if (!lastStringFound.Equals(property.ToString(),
                    StringComparison.OrdinalIgnoreCase))
                {
                    var strResult = StringComparer.OrdinalIgnoreCase.Compare(
                     lastStringFound,
                     property) > 0;

                    if (strResult && ordering == "ascending")
                        Assert.Fail("Ordering is not correct.");

                    if (!strResult && ordering == "descending")
                        Assert.Fail("Ordering is not correct.");
                }
            }
        }

        public static void BuildStructure(string[] fields, object currentObject, out string structuredString)
        {
            var structure = string.Empty;

            var last = fields.Last();
            foreach (var f in fields)
            {
                if (f != "Guid")
                {
                    var valueOfTheField = ReflectionHelper.GetPropertyValueCc(currentObject, f);

                    if (!f.Equals(last))
                        structure += $"{valueOfTheField}_";
                    else
                        structure += $"{valueOfTheField}";
                }
            }

            structuredString = structure;
        }

        public static Table ConstructSpecflowTableForSubmissions()
        {
            Table table83 = new Table(new string[] {
                            "SubmissionVersionNumber",
                            "BrokerCode",
                            "BrokerDepartmentId",
                            "JsonPayload",
                            "FileName",
                            "FileMimeType",
                            "DocumentType",
                            "File",
                            "DocumentDescription"});
            table83.AddRow(new string[] {
                            "1",
                            "1225",
                            "558",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "MRC_Placingslip.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "document_placing_slip",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Placing Slip test"});
            table83.AddRow(new string[] {
                            "1",
                            "1225",
                            "558",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "sample_mrc.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Client Corrspondence test"});
            table83.AddRow(new string[] {
                            "1",
                            "1225",
                            "558",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "sample_mrc.txt",
                            "text/plain",
                            "document_file_note",
                            "Data\\mrc_documents\\sample_mrc.txt",
                            "additional non placing test"});

            return table83;
        }


        public static Table ConstructTableForSubmissionUnderwriter(string underwriterUserEmailAddress)
        {
            TechTalk.SpecFlow.Table table84 = new TechTalk.SpecFlow.Table(new string[] {
                            "UnderwriterUserEmailAddress",
                            "CommunicationsMethodCode"});
            table84.AddRow(new string[] {
                           underwriterUserEmailAddress,
                            "PLATFORM"});

            return table84;
        }

        public static Table ConstructTableForSubmissionUnderwritersWithSubmissionDocuments()
        {
            TechTalk.SpecFlow.Table table85 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName"});
            table85.AddRow(new string[] {
                            "MRC_Placingslip.docx"});
            table85.AddRow(new string[] {
                            "sample_mrc.docx"});

            return table85;
        }

    }
}

﻿using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using RestSharp;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Core
{
    public class APIService 
    {
        private SubmissionDocumentsSteps SubmissionDocumentsSteps;
        private CommonSteps CommonSteps;
        private SubmissionUnderwriterSteps SubmissionUnderwriterSteps;
        private SubmissionGET SubmissionSubmissionSteps;
        private AbstractSteps AbstractSteps;
        private AbstractStepsFilters AbstractStepsFilters;
        private AbstractStepsResults AbstractStepsResults;
        private SubmissionDialogueSteps SubmissionDialogueSteps;
        private BrokerDepartment BrokerDepartmentSteps;

        public APIService(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) 
        {
            CommonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionUnderwriterSteps = new SubmissionUnderwriterSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionDocumentsSteps = new SubmissionDocumentsSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionSubmissionSteps = new SubmissionGET(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            AbstractStepsFilters = new AbstractStepsFilters(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            AbstractStepsResults = new AbstractStepsResults(objectContainer, apiTestContext, scenarioContext, stateHolder);
            SubmissionDialogueSteps = new SubmissionDialogueSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            BrokerDepartmentSteps = new BrokerDepartment(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
        }

        public void GetSubmissionUnderwriters()
        {
            CommonSteps.WhenIMakeRequestToResource(Method.GET, "/SubmissionUnderwriters");
            CommonSteps.IsValidResponse("application/json; charset=utf-8", "submission underwriters");
        }

        public void GetSubmissionUnderwritersAndStoreSubmissionUnderwriter()
        {
            CommonSteps.WhenIMakeRequestToResource(Method.GET, "/SubmissionUnderwriters");
            CommonSteps.IsValidResponse("application/json; charset=utf-8", "submission underwriters");
            SubmissionUnderwriterSteps.GivenIStoreTheSubmissionUnderwriter();
        }

        public void GetSubmissionsByFilter(string parameter, string value)
        {
            AbstractStepsFilters.GivenIAddAFilterOnWithAValue(parameter, value);
            AbstractStepsFilters.WhenIMakeAGETRequestToResourceAndAppendTheFilterS("/submissions", StepHelpers.Enums.EntityEnum.Entity.submissions);
            AbstractStepsResults.ResponseBodyShouldContainACollectionOf(EntityEnum.Entity.submissions);
        }

        public void FilterSubmissionDialogues(string senderActionCode, string submissionReference)
        {
            AbstractStepsFilters.GivenIAddAFilterOnWithAValue("SenderActionCode", senderActionCode);
            AbstractStepsFilters.GivenIAddAFilterOnWithAValue("SubmissionReference", submissionReference);
            AbstractStepsFilters.WhenIMakeAGETRequestToResourceAndAppendTheFilterS("/SubmissionDialogues", EntityEnum.Entity.submission_dialogue);
            CommonSteps.ThenTheResponseStatusCodeShouldBe(200);
            CommonSteps.IsValidResponse("application/json; charset=utf-8", "submission dialogue");
        }

        public void GetBrokerDepartments(string userEmailAddress)
        {
            CommonSteps.GivenISetSecurityTokenAndCertificateForTheUser(userEmailAddress);
            CommonSteps.WhenIMakeRequestToResource(Method.GET, "/brokerdepartments");
            BrokerDepartmentSteps.ThenStoreBrokerDepartments();
        }

        public void GetSubmissionDocumentById(string Id)
        {
            //             When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource
            CommonSteps.WhenIMakeRequestToResource(Method.GET, $"/SubmissionDocuments/{Id}");
            CommonSteps.ThenTheResponseStatusCodeShouldBe(200);
            CommonSteps.IsValidResponse("application/json; charset=utf-8", "submission documents");
            SubmissionDocumentsSteps.RefreshTheSubmissioncontext();
        }


        
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using OpenQA.Selenium;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Core
{
    public class ObjectRepository
    {
        public static IWebDriver Driver { get; set; }

    }
}

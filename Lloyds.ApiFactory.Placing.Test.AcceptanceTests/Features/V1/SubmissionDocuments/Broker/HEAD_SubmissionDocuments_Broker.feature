#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_SubmissionDocuments_Broker
	In order to retrieve a submission document
	As a broker
	I want to be able to do a HEAD request to SubmissionDocuments via the API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

 #---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDocument_HEAD_Broker_HeadSubmissionDocumentbyId] For a valid submission, I want to be able to do a HEAD returning 2xx
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
 	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added
	When  I make a HEAD request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range

 #---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDocument_HEAD_Broker_SubmissionDocumentNotFound] For an INVALID submission document, I want to be able to do a HEAD returning 404
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	When I make a HEAD request to '/SubmissionDocuments/666' resource
	Then the response status code should be "404"

 #---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_HEAD_Broker_BrokerNotAuthorised] - As an unathorisedbroker from another company, that I dont have access, I want to be able to return 400/403/404
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
 	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
 	When I make a HEAD request to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "403" or "400" or "404"

 #---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDocument_HEAD_Broker_BrokerNotAuthorised] - As an unathorisedbroker from the same company but different department, that I dont have access, I want to be able to return 400/403/404
	Given I POST a submission on behalf of the broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
 	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
 	When I make a HEAD request to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "403" or "400" or "404"

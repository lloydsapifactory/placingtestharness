#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: PUT_SubmissionDocuments_Broker_Negative
	In order to update submission document for a draft submission
	As an underwrite or broker
	I need to be able to update the description, type and shareable to all markets

Background: 
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName               | FileMimeType                                                            | DocumentType   | File                                             | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx   | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx               | Client Corrspondence |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_clientcorr.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Data\supporting_documents\sample_clientcorr.docx | Client Corrspondence |
	And store the CreatedDateTime from submission documents context

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 Non Placing Slip - Mandatory fields: DocumentId, DocumentDescription, DocumentType. if not provided return 400
	Given for the submission documents the '<field>' is '<value>'
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'Required property '<field>' expects a value but got null'

Examples:
	| field               | value |
	| DocumentDescription |       |
	| DocumentType        |       |
	| DocumentId          |       |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 Non Placing Slip - Mandatory field: ShareableToAllMarketsFlag. if not provided return 400
	Given 'ShareableToAllMarketsFlag' is null
	And I save document submission update information
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'Required property 'ShareableToAllMarketsFlag' expects a value but got null'

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
# THIS SEEMS TO BE NOT TESTABLE AS IT WILL FIRST FAIL ON TEH VALIDATION RULE ON THE ID BETWEEN PAYLOAD AND SUBMISSIONDOCUMENTID
Scenario: 03 [SubmissionDocument_PUT_Broker_SubmissionDocumentNotFound] Placing Slip - Broker cannot do any update if it the Id is different that the DocumentId
	And store the CreatedDateTime from submission documents context
	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'DocumentType' is 'document_file_note'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	
	Given for the submission document I set an invalid DocumentId
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains a message error 'Ids don't match between the url and the request body.'

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 07 [SubmissionDocument_PUT_Broker_BrokerNotAuthorisedForSubmission] - Updating document as an unathorised Broker from another company, returns 400/403/404
	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save document submission update information for PUT

	Given I log in as a broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400" or "404"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 08 [SubmissionDocument_PUT_Broker_BrokerNotAuthorisedForSubmission] - Updating document as an unathorised Broker from the same company but different department, returns 400/403/404
	And I clear down test context
	And I POST a submission on behalf of the broker 'aonbroker.ema@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType   | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Data\mrc_documents\sample_mrc.docx | Client Corrspondence |
	And store the CreatedDateTime from submission documents context
	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save document submission update information for PUT

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400" or "404"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |


#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
Scenario: 10 [SubmissionDocument_PUT_Broker_SubmissionNotInDraft] Broker tries to do a PUT to a document that the submission related is not in DRFT, returns 400
	And I clear down test context
	
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith			| File											| FileMimeType																	|
	| MRC_Placingslip.docx | sample_mrc_new.docx	| Data\mrc_documents\sample_mrc_new.docx		| application/vnd.openxmlformats-officedocument.wordprocessingml.document		|
	| sample_mrc.docx	   | PremiumAdvise_2.docx	| Data\supporting_documents\PremiumAdvise_2.docx	| application/vnd.openxmlformats-officedocument.wordprocessingml.document	|
	And I save the submission document id response to the submissiondocumentidecontext
	And store the CreatedDateTime from submission documents context
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'DocumentType' is 'advice_premium'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a message error 'SubmissionNotInDraft'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 11 [SubmissionDocument_PUT_Broker_DocumentTypeNotSupportingDocument] Non Placing Slip - Updating document with the document type as placing slip, returns 400 
	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'

	Given for the submission documents the 'DocumentType' is 'document_placing_slip'
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 12 [SubmissionDocument_PUT_Broker_MRCUpdateNotAllowed] Broker tries to do a PUT to an MRC, returns 400
	And I clear down test context
	And I POST a submission on behalf of the broker 'aonbroker.ema@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType   | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Client Corrspondence |
	And I save the submission document id response to the submissiondocumentidecontext
	And store the CreatedDateTime from submission documents context
	And for the submission documents the 'DocumentDescription' is 'test'
	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save document submission update information for PUT

	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a message error 'MRCCannotBeUpdated'


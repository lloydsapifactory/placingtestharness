#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: PUT_SubmissionDocuments_Broker
	In order to update submission document for a draft submission
	As a broker
	I need to be able to update the description, type and shareable to all markets

Background: 
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType   | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Data\mrc_documents\sample_mrc.docx | Client Corrspondence |
	And store the CreatedDateTime from submission documents context

@CoreAPI
Scenario Outline: 01 [SubmissionDocument_PUT_Broker_UpdateSupportingDocument] Non Placing Slip - Updating document description and document type by the broker when non placing slip, 
	Given for the submission documents the 'DocumentDescription' is '<DocumentDescriptionValue>'
	Given for the submission documents the 'DocumentType' is '<DocumentTypeValue>'
	Given for the submission documents the 'ShareableToAllMarketsFlag' is '<ShareableToAllMarketsFlagValue>'
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And the submission document response should contain the 'DocumentDescription' set to '<DocumentDescriptionValue>'
	And the submission document response should contain the 'DocumentType' set to '<DocumentTypeValue>'
	And the submission document response should contain the 'ShareableToAllMarketsFlag' set to flag <ShareableToAllMarketsFlagValue>
	
	And I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And the response status code should be "200"
	And the submission document response should contain the 'DocumentDescription' set to '<DocumentDescriptionValue>'
	And the submission document response should contain the 'DocumentType' set to '<DocumentTypeValue>'
	And the submission document response should contain the 'ShareableToAllMarketsFlag' set to flag <ShareableToAllMarketsFlagValue>

	Examples:
	| DocumentDescriptionValue | DocumentTypeValue            | ShareableToAllMarketsFlagValue |
	| test                     | document_file_note           | false                          |
	| test1                    | correspondence_client        | false                          |
	| test2                    | document_cover_note_addenda  | false                          |
	| test3                    | form_declaration             | false                          |
	| test4                    | document_market_presentation | false                          |
	| test5                    | document                     | false                          |
	| test6                    | advice_premium               | false                          |
	| test7                    | wording_addenda              | false                          |
	| test8                    | advice_premium               | false                          |
	| test9                    | document_file_note           | true                           |
	| test10                   | correspondence_client        | true                           |
	| test11                   | document_cover_note_addenda  | true                           |
	| test12                   | form_declaration             | true                           |
	| test13                   | document_market_presentation | true                           |
	| test14                   | document                     | true                           |
	| test15                   | advice_premium               | true                           |
	| test16                   | wording_addenda              | true                           |
	| test17                   | advice_premium               | true                           |


		
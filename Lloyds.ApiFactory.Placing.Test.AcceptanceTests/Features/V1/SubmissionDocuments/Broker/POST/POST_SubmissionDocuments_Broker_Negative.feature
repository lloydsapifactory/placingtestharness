#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: POST_SubmissionDocuments_Broker_Negative
	In order to test POST for a submission document resource
	As a user I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I set the submission document context from the json payload Data\1_2_Model\SubmissionDocument_1.json

#---------------------------------------------------------------------------	
	@CoreAPI
Scenario: 01 [MIMETypeNotSupportedForMRC] Creates and adds a Placing slip as an indalid file, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'sample_mrc.txt'
	And for the submission documents the 'FileMIMEType' is 'application/txt'
	And I attach the file Data\mrc_documents\sample_mrc.txt to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
		
#---------------------------------------------------------------------------
	@CoreAPI
Scenario: 02 Creates a submission document and sends an empty string for DocumentType, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is ''
	And for the submission documents the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the submission documents the 'FileName' is 'sample_clientcorr.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window
	
#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 Creates a submission document and sends an empty FileName, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the submission documents the 'FileName' is ''
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

#---------------------------------------------------------------------------
	@CoreAPI
Scenario: 04 Creates a submission document and sends an empty FileMIMEType, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the submission documents the 'DocumentDescription' is 'MRC Placing slip'
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is ''
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 Creates a submission document and sends no value for ShareabletoAllMarketsFlag, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Client corr'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'null'
	And for the submission documents the 'FileName' is 'sample_clientcorr.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 Creates a submission document and sends an empty value for SubmissionReference, returns 400
	And for the submission documents the 'SubmissionReference' is ''
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Correspondence Client [z]'
	And for the submission documents the 'FileName' is 'sample_clientcorr.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 07 Creates a submission document and sends an empty value for SubmissionVersionNumber, returns 400
	And for the feature submission documents context the 'SubmissionVersionNumber' is 0
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the submission documents the 'DocumentDescription' is 'Sample Placing Slip'
	And for the submission documents the 'FileName' is 'Sample_MRC.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 08 BELL Adds a submission document and sends an incorrect document type[clc_correspondence_client which does not exist], returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'clc_correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the submission documents the 'FileName' is 'SampleClientCorr.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 09 [SubmissionDocument_POST_MULTIPART_Broker_BrokerNotAuthorisedForSubmission] - An unauthorised broker tries to add the Placing slip to the submission(submission document), returns 400/403
	And I set security token and certificate for the 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com' user
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 10 [MIMETypeNotSupportedForMRC] Creates and adds a Placing slip (submission document) as a txt file with a File Mime type = application/txt, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.txt'
	And for the submission documents the 'FileMIMEType' is 'application/txt'
	And I attach the file Data\mrc_documents\sample_mrc.txt to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 [MIMETypeNotSupportedForMRC] Creates and adds a Placing slip (submission document) - a file with no extension and a File Mime type = application/pdf, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip_NoExtension'
	And for the submission documents the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\MRC_Placingslip_NoExtension to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 12 [MIMETypeNotSupportedForMRC] Creates and adds a Placing slip (submission document) as a jpeg file with a File Mime type = application/jpeg, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.jpg'
	And for the submission documents the 'FileMIMEType' is 'application/jpeg'
	And I attach the file Data\mrc_documents\MRC_Placingslip.jpg to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 13 [SubmissionDocument_POST_MULTIPART_Broker_NoLinkedSubmission] Broker creates submission document with wrong submission/version, returns 400
	And generate a random string of 6 characters and save it to the context
	And for the submission document context the 'SubmissionReference' is an invalid SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionInPayloadDoesNotExist |
	| SubmissionDoesNotExist          |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 14 [SubmissionDocument_POST_MULTIPART_Broker_SubmissionNotInDraft] Broker tries to do a POST to a document that the submission related is not in DRFT, returns 400
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I get a random submission document from the added
	And for the submission document context the 'SubmissionReference' is an invalid SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [SubmissionDocument_POST_MULTIPART_Broker_SupportingDocumentMIMETypeExpected] - Creates a submission document with invalid FileMimeType, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Correspondence Client [z]'
	And for the submission documents the 'FileName' is 'supporting_wrong.html'
	And for the submission documents the 'FileMIMEType' is 'text/html'
	And I attach the file Data\supporting_documents\supporting_wrong.html to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'FileExtensionNotSupportedForSupportingDocument'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 17 [SubmissionDocument_POST_MULTIPART_Broker_ReplacementDocumentIdNotExpected] Broker creates submission document with no replacing document id, returns 400
	And generate a random string of 6 characters and save it to the context
	Given for the submission documents the 'ReplacingDocumentId' is '666'
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacementDocumentIdNotExpected'


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 18 [SubmissionDocument_POST_MULTIPART_Broker_ReplacingDocumentNotOnFirstVersion] POST submission document for replacing the supporting document on the initial subnmission, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription   | FilePath                                         |
	| MRC_Placingslip.docx   | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx               |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx |
	And I will save the documentId from the response

	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | ExpectedOutput |
	| sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 201            |
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                                |
	| ReplacementDocumentIdNotExpected            |
	| ReplacingDocumentOnPrimarySubmissionVersion |

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: POST_SubmissionDocuments_Broker
	In order to test POST for a submission document resource
	As a user I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I set the submission document context from the json payload Data\1_2_Model\SubmissionDocument_1.json

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 01 [SubmissionDocument_POST_MULTIPART_Broker_UploadMRCSubmissionDocument] Broker creates and adds a Placing slip (submission document)
	And generate a random string of 6 characters and save it to the context
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "201"
	And I refresh the SubmissionDocumentcontext with the response content
	And validate submission documents result contain item added on 'ShareableToAllMarketsFlag' with a value 'true'


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 BELL broker Bella creates a submission document and sends 0 for FileSizeInBytes
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the feature submission documents context the 'FileSizeInBytes' is 0
	And for the submission documents the 'DocumentDescription' is 'Sample MRC'
	And for the submission documents the 'FileName' is 'Sample_MRC.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I clear down test context
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "201"

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 03 BELL broker Bella creates a submission and Bell technician Hulk Tom to add the Placing slip to the submission(submission document)
	And I set security token and certificate for the 'BellTechnician.hulk@LIMOSSDIDEV.onmicrosoft.com' user
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'DocumentDescription' is 'Placing slip'
	And for the submission documents the 'FileName' is 'MRC_Placingslip.pdf'
	And for the submission documents the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\sample_mrc.pdf to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "201"

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 04 BELL broker Bella creates a submission and adds 2 documents [Placing slip & Client Corr].
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType    | DocumentType          | DocumentDescription  | FilePath                                        |
	| MRC_Placingslip.pdf   | 1          | application/pdf | document_placing_slip | Placing Slip         | Data\mrc_documents\sample_mrc.pdf               |
	| sample_clientcorr.pdf | 2          | application/pdf | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.pdf |
	And I add a GET filter on 'SubmissionReference' with a value '$#SubmissionReferenceforSubmissionDocument'
	When I make a GET request to '/SubmissionDocuments' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submission documents
	And submission documents should have at least 2 document

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 05 BELL broker Bella creates a submission and adds 2 documents [Placing slip & Client Corr].
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType    | DocumentType          | DocumentDescription  | FilePath                                        | ExpectedOutput |
 	| MRC_Placingslip.pdf   | 1          | application/pdf | document_placing_slip | Placing Slip         | Data\mrc_documents\sample_mrc.pdf               | 201            |
	| sample_clientcorr.pdf | 2          | application/pdf | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.pdf| 201            |
	Then the expected outputs should match the actual outputs
	And the response HTTP header ContentType should be to application/json type

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 06 BELL broker Bella creates and adds a Placing slip (submission document) which is of type pdf
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.pdf'
	And for the submission documents the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\MRC_Placingslip.pdf to the request
	And the submission document is saved for the scenario
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "201"

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 07 BELL broker Bella creates a submission and adds 2 documents [Placing slip & Client Corr].
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	Then the expected outputs should match the actual outputs
	And the response HTTP header ContentType should be to application/json type

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 08 BELL broker Bella creates a submission and adds 1 supporting document [Client Corr] the file name has a length of 4 characters. (BUG - Regex)
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType				   | DocumentType          | DocumentDescription   | FilePath						| ExpectedOutput |
 	| m.7z					| 1          | application/x-7z-compressed | correspondence_client | Client Corr           | Data\supporting_documents\T.7z	| 201            |
	Then the expected outputs should match the actual outputs
	And the response HTTP header ContentType should be to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 BELL broker Bella creates a submission and adds 1 placing slip the file is of type rtf.
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType		| DocumentType          | DocumentDescription   | FilePath						| ExpectedOutput |
 	| sample.rtf			| 1          | application/rtf	| document_placing_slip | Placing Slip          | Data\mrc_documents\sample.rtf	| 201            |
	Then the expected outputs should match the actual outputs
	And the response HTTP header ContentType should be to application/json type

#---------------------------------------------------------------------------
@NightlyBuild
Scenario: 10 BELL broker Bella creates a submission and adds 1 placing document that has a file size = 49 MB.
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName							| DocumentId | FileMIMEType		| DocumentType          | DocumentDescription   | FilePath												| ExpectedOutput |
 	| MRC_PlacingSlip49MB_DOCX_2.docx	| 1          | application/docx | document_placing_slip | Placing Slip          | Data\mrc_documents\MRC_PlacingSlip49MB_DOCX_2.docx	| 201            |
	Then the expected outputs should match the actual outputs
	And the response HTTP header ContentType should be to application/json type






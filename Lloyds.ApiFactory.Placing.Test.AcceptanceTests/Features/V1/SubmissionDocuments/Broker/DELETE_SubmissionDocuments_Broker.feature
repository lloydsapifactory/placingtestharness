#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: DELETE_SubmissionDocuments_Broker
	In order to test DELETE for a submission document resource
	As a user I would like to create [POST] a submission, create submission documents [POST] for the submission and then [DELETE] them

	Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And set up version number Submissions creation
	And I log in as a broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	And I retrieve the broker departments for 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I set the submission document context from the json payload Data\1_2_Model\SubmissionDocument_1.json 

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDocument_DELETE_Broker_DeleteSubmissionDocumentById] Delete a document placing slip. - DeleteSubmissionDocumentById
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType																| DocumentType          | DocumentDescription  | FilePath                                        | ExpectedOutput |
 	| MRC_Placingslip.docx  | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document  | document_placing_slip | Placing Slip         | Data\mrc_documents\MRC_Placingslip.docx		 | 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range

	#VALIDATE DELETION
	And  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "4xx" range


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDocument_DELETE_Broker_DeleteSubmissionDocumentById] Delete a NON placing slip. - DeleteSubmissionDocumentById
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType																| DocumentType          | DocumentDescription  | FilePath                                        | ExpectedOutput |
 	| MRC_Placingslip.docx  | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document  | document_placing_slip | Placing Slip         | Data\mrc_documents\MRC_Placingslip.docx		 | 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range

	#VALIDATE DELETION
	And  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "4xx" range

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionDocument_DELETE_Broker_SubmissionDocumentNotFound] Delete a submission document that does not exist, return 404
	When  I make a DELETE request to '/SubmissionDocuments/6666666' resource 
	Then the response status code should be "404"
	And the response contains an error 'ResourceDoesNotExist'

 #---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDocument_DELETE_Broker_BrokerNotAuthorised] Delete a document placing slip as an unauthorised broker from different company, returns 400/403
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I set the submission document context from the json payload Data\1_2_Model\SubmissionDocument_1.json
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType																| DocumentType          | DocumentDescription  | FilePath                                        | ExpectedOutput |
 	| MRC_Placingslip.docx  | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document  | document_placing_slip | Placing Slip         | Data\mrc_documents\MRC_Placingslip.docx		 | 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |
	| ResourceAccessDenied                 |

	
 #---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 [SubmissionDocument_DELETE_Broker_BrokerNotAuthorised] Delete a document placing slip as an unauthorised broker from the same company BUT different department, returns 400/403
	Given I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I retrieve the broker departments for 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I set the submission document context from the json payload Data\1_2_Model\SubmissionDocument_1.json
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	When I POST a set of submission documents from the data
	| FileName              | DocumentId | FileMIMEType																| DocumentType          | DocumentDescription  | FilePath                                        | ExpectedOutput |
 	| MRC_Placingslip.docx  | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document  | document_placing_slip | Placing Slip         | Data\mrc_documents\MRC_Placingslip.docx		 | 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |
	| ResourceAccessDenied                 |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 06 [SubmissionDocument_DELETE_Broker_NotDocumentOwner] Delete a document 
	And set new submission dialogue context
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | False                     |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "400"
	And the response contains a validation error 'NotDocumentOwner'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 07 [SubmissionDocument_DELETE_Broker_SubmissionVersionNotInDraft] Delete a document when the submission is not in DRFT, returns 400
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 

	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionNotInDraft'

	#VALIDATE DELETION
	And  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range
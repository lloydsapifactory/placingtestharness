#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionDocuments_Broker
	In order to retrieve a submission document
	As a broker 
	I want to be able to do a GET to submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 01 GetALL: I want to be able to get all submission documents as a broker
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission 
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have a link with proper value

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 GetById: I want to be able to GetById a valid submission document as a broker and return all mandatory fields
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test   |
	And I set the SubmissionReference
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added
	When  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I refresh the SubmissionDocumentcontext with the response content
	And validate submission documents context contain the following fields
	| fieldname                 |
	| FileName                  |
	| DocumentType              |
	| DocumentDescription       |
	| SuppliedBy                |
	| SubmissionReference            |
	| SubmissionVersionNumber        |
	| DocumentId                |
	| FileMIMEType              |
	| FileSizeInBytes           |
	| ShareableToAllMarketsFlag |
	| CreatedDateTime           | 


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 GetALL: I want to be able to get all submission documents as a broker and return all mandatory fields
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test   |
	And I set the SubmissionReference
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	Then the response is a valid application/json; charset=utf-8 type of submission documents collection
	And validate submission documents results contain the following fields
	| fieldname                 |
	| FileName                  |
	| DocumentType              |
	| DocumentDescription       |
	| SuppliedBy                |
	| SubmissionReference            |
	| SubmissionVersionNumber        |
	| DocumentId                |
	| FileMIMEType              |
	| FileSizeInBytes           |
	| ShareableToAllMarketsFlag |
	| CreatedDateTime           | 

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 04 GetALL ('contains' partial match filtering):  I want to be able to get all submission documents as a broker, with contains filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference      |
		| FileName            |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 GetALL (pagination): I want to be able to get all submission documents with pagination 
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | form_declaration        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I reset all the filters

	Given I add a filter on '_pageNum' with a value '2'
	And I add a filter on '_pageSize' with a value '2'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document
	And I add a filter on '_pageNum' with a value '1'
	And I add a filter on '_pageSize' with a value '6'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 5 document

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 GetALL orderby: As a broker I want to be able to get all submission document ordered by CreatedDateTime
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I add a filter on '_order' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 5 document
	And the submission documents response is order by '<value>' 'ascending'

		Examples:
| value           |
| CreatedDateTime |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 07 GetALL: As a broker I want to be able to get all submission documents, including the ones that have been just created
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
	| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have 2 document
	And I get a random submission document from the added
	When  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 09 GetById: As a broker I want to be able to get a submission document, that have been created by the broker
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| SubmissionVersionNumber | ProgrammeReference | ContractReference | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| 1                  | B1225QR0           | B1225QR0          | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
	| 1                  | B1225QR0           | B1225QR0          | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence |
 	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added
	When  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 11 GetALL (exact match filtering):  I want to be able to get all submission documents as a broker, with filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I add a '' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have at least 1 document

	Examples:
		| field               |
		| SubmissionVersionNumber  |
		| DocumentType        |
		| SuppliedBy          |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 12 GetALL ('prefix' partial match filtering):  I want to be able to get all submission documents as a broker, with contains filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I add a 'prefix' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference      |
		| FileName            |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 13 GetALL ('suffix' partial match filtering):  I want to be able to get all submission documents as a broker, with contains filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I add a 'suffix' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference      |
		| FileName            |

#---------------------------------------------------------------------------
@CoreAPI
		Scenario: 14 GetALL: Id should have the structure of SubmissionReference_SubmissionVersionNumber_DocumentId
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And for the submission documents and the returned results the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_DocumentId'

#---------------------------------------------------------------------------
	@CoreAPI
Scenario Outline: 15 (GetALL [multiple values filter]) Broker perform a GET on the submissions resource and adds a multiple values filter for SubmissionVersionNumber 
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I add a filter on '<field>' with a value '<values>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document
	And for submission documents I validate I am getting the correct results for <field> with values of (<values>)

		Examples:
| field              | values | number of found |
| SubmissionVersionNumber | 1,2    | 2               |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 16 GetById: As a broker I can retrieve a submission document that belongs to same company and same department
	And I log in as a broker '<BrokerUserEmailAddress>'
	And store the broker
	And I POST a submission on behalf of the broker '<BrokerUserEmailAddress>' with a submission document from the following data
	| SubmissionVersionNumber | ProgrammeReference | ContractReference | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| 1                  | B1225QR0           | B1225QR0          | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I set the SubmissionDocumentId

	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"

		Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |
	| AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com    | AON                  |

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 17 GETALL: As a broker from the same company and the same department, validate the reults and returns 200
	And I log in as a broker '<BrokerUserEmailAddress>'
	And store the broker
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection

	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	And I add a 'contains' filter on 'SubmissionReference' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"


	Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |
	| AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com    | AON                  |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18 GetALL orderby last_modified(CREATE): Create 3 documents, when doing GETALL order by last modified then the ones last created will be sorted first
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                              | FileName               | FileMimeType                                                            | DocumentType          | File                                             | DocumentDescription       |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx   | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx               | Placing Slip test         |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx        | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx               | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_clientcorr.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\supporting_documents\sample_clientcorr.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	Then submission documents should have 3 document

	And the submission documents response is ordered by '_last_modified' of the field 'FileName' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| MRC_Placingslip.docx   | sample_clientcorr.docx  |
	| sample_mrc.docx        | sample_mrc.docx         |
	| sample_clientcorr.docx | MRC_Placingslip.docx    |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 19 GetALL orderby last_modified(UPDATE): Create 3 documents and UPDATE one of then, when GETALL order by last modified then the last one updated will be sorted first
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx   | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx               | Placing Slip test         |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx        | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx               | Client Corrspondence test |
	| Data\1_2_Model\SubmissionDocument_1.json | sample_clientcorr.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\supporting_documents\sample_clientcorr.docx | Client Corrspondence test |
	And I set the SubmissionReference

	And I store the submission document with filename 'sample_mrc.docx'
	And store the CreatedDateTime from submission documents context
	And for the submission documents the 'DocumentDescription' is 'change to the description'
	And for the submission documents the 'DocumentType' is 'advice_premium'
	And I save document submission update information for PUT
	And I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header

	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	Then submission documents should have 3 document

	And the submission documents response is ordered by '_last_modified' of the field 'FileName' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| MRC_Placingslip.docx   | sample_mrc.docx         |
	| sample_clientcorr.docx | sample_clientcorr.docx  |
	| sample_mrc.docx        | MRC_Placingslip.docx    |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 23 Adding _select parameter will return results with the exact fields
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 
	And I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,documentType,documentDescription,shareableToAllMarketsFlag'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have a link with proper value


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 24 (DATE before,after filtering): GETALL Submission Documents, before or after a datetime for CreatedDateTime
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date filter on the '<field>' <comparison> the date of <when>
	And I make a GET request to '/Submissions' resource and append the filter(s) 
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And I reset all the filters
	
	Given I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And validate that the submission documents results for the field '<field>' are <comparison> the date of <when>
	
	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | before     | one week  |
		| CreatedDateTime | after      | yesterday |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 25 (DATE range Filter): GETALL Submission documents as a broker, with a date range 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date filter on the '<field>' <comparison> the date of <when>
	And I make a GET request to '/Submissions' resource and append the filter(s) 
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And I reset all the filters

	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission documents
	And validate that the submission documents results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 26 (range adatetime..bdatetime): GETALL Submission documents as a broker, with a date range 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=a week ago and bdatetime=today
	And I make a GET request to '/Submissions' resource and append the filter(s) 
	And the response body should contain a collection of submissions
	And I get a random submission that contains submission documents
	And I reset all the filters

	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=a week ago and bdatetime=today
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission documents
	And submission documents should have at least 1 document
	And validate that the submission documents date results for the field '<field>' are  for yesterday

	Examples:
		| field           | 
		| CreatedDateTime | 



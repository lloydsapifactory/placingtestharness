#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETBINARY_SubmissionDocuments_Broker
	In order to retrieve a submission document
	that has been assigned by the broker after a submission dialogue
	As a underwriter
	I want to be able to do a GET to submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDocument_GET_BINARY_Broker_GetBinarySubmissionDocumentById]: GET_BINARY to a submission document as a broker 
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test   |
	And I set the SubmissionReference
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added

	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "200"

	And store feature context

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 02 [SubmissionDocument_GET_BINARY_Broker_SubmissionDocumentNotFound] - GET_BINARY to an invalid submission document, returns 404 
	And restore feature context
	And I get a random submission document from the collection

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set a valid but not existing SubmissionDocumentId
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_GET_BINARY_Broker_BrokerNotAuthorisedForSubmission] As an unauthorised broker, GetById returns 400/403
	And restore feature context
	And I get a random submission document from the collection

	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'



#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDocument_GET_BINARY_Broker_BrokerNotAuthorisedForDocument] As an unauthorised broker, GetById returns 400/403
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test   |
	And I set the SubmissionReference
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I get a random submission document from the added

	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'ResourceAccessDenied'

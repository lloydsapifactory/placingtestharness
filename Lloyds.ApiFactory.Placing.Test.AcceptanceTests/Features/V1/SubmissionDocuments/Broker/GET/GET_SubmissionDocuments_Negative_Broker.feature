#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionDocuments_Negative_Broker
	In order to retrieve a submission document
	As a broker or underwriter
	I want to be able to do a GET to submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 GetById 404: As a broker when I try to retrieve a submission document that does not exists, returns 404
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/SubmissionDocuments/6i6i6' resource 
	Then the response status code should be "404"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDocument_GET_Broker_BrokerNotAuthorised] - As a broker when I try to retrieve a submission document that belongs to another broker from another company, I get a 400/403
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| 1                  |Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I set the SubmissionDocumentId
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |
	| ResourceAccessDenied                 |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_GET_Broker_BrokerNotAuthorised] - As a broker when I try to retrieve a submission document that belongs to another broker from same company but different department, returns 403/400
	Given I POST a submission on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com' with a submission document from the following data
	| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
	| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip         |
	And the response status code should be "201"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I set the SubmissionDocumentId
	And I select broker that is in a different broker department with 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And I log in as a the saved broker
	When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |
	| ResourceAccessDenied                 |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 ([InvalidFilterCriterion] exact match filter) - parameter=value
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 
	And I add a filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
	| field                     | value                                                                   |
	| DocumentId                | 1                                                                       |
	| FileMIMEType              | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	| FileSizeInBytes           | 100                                                                     |
	| ShareableToAllMarketsFlag | true                                                                    |
	| ReplacingDocumentId       | 1                                                                       |
	| DocumentDescription       | test                                                                    |
	| FileName                  | testfilename                                                            |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 ([InvalidFilterCriterion] match filter) - parameter=match(value)
	And I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 

	Given I add a 'match' filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                   | value    |
		| FileName                | testfile |
		| DocumentDescription     | test     |
		| SubmissionVersionNumber | 1        |
		| DocumentId              | 1        |
		| ReplacingDocumentId     | 1        |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 06 ([InvalidFilterCriterion] contains filter) - parameter=contains(value)
	And I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 

	Given I add a 'contains' filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                   | value |
		| SubmissionVersionNumber | 1     |
		| DocumentId              | 1     |
		| ReplacingDocumentId     | 1     |
		| SuppliedBy              | test  |
		| DocumentType            | test  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07 ([InvalidFilterCriterion] prefix filter) - parameter=prefix(value)
	And I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 

	Given I add a 'prefix' filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                   | value |
		| SubmissionVersionNumber | 1     |
		| DocumentId              | 1     |
		| ReplacingDocumentId     | 1     |
		| SuppliedBy              | test  |
		| DocumentType            | test  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 08 ([InvalidFilterCriterion] suffix filter) - parameter=suffix(value)
	And I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response body should contain a collection of submissions
	And I get a random submission 

	Given I add a 'suffix' filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                   | value |
		| SubmissionVersionNumber | 1     |
		| DocumentId              | 1     |
		| ReplacingDocumentId     | 1     |
		| SuppliedBy              | test  |
		| DocumentType            | test  |

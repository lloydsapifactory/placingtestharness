#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: DELETE_SubmissionDocuments_Undewriter_Negative
	In order to test DELETE for a submission document resource as underwriter
	Create a submission, QM & QMD as RFQ as Broker, 
	As a underwriter user I attach a new document and Delete it

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	
#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 [SubmissionDocument_DELETE_Underwriter_DeleteSubmissionDocumentById] Delete a non existing supporting document returns 404
	Given I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/66666' resource 
	Then the response status code should be "404"
	And the response contains an error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 02 [SubmissionDocument_DELETE_Underwriter_UnderwriterNotAuthorised] Delete a supporting document as an unauthorised underwriter, returns 400/403
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		  | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	# Underwriter - post document
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I POST a set of submission documents from the data
	| FileName        | DocumentId | FileMIMEType                                                            | DocumentType   | DocumentDescription | FilePath                               | ExpectedOutput |
	| sample_mrc.docx | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Supporting Document | Data\mrc_documents\sample_mrc_new.docx | 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection

	Given I log in as an underwriter 'beazleyunderwriter.fiona@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains an error 'AccessToReferencedResourceNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 03 [SubmissionDocument_DELETE_Underwriter_DocumentReferencedPreviously] Delete a supporting document that was used for a previous Underwriter Submission Dialogue in SenderDocumentIds, returns 400/403
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

	#SAVE THE SUBMISSION DOCUMENT THAT IS ABOUT TO BE USED FOR THE SUBMISSION DIALOGUE, IN ORDER TO USE IT IN THE DELETE LATER
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And I save the submission document id response to the submissiondocumentidecontext

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage			|
 	| DocumentUsedInSubmissionDialogue	|
 	| DocumentUsedPreviously	|

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 [SubmissionDocument_DELETE_Underwriter_NotDocumentOwner] Delete a supporting document that has been supplied by the Broker , returns 400/403
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		  | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains an error 'NotDocumentOwner'
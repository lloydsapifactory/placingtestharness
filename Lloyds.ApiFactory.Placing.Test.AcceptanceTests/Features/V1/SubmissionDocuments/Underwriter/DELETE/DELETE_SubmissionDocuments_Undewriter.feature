#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: DELETE_SubmissionDocuments_Undewriter
	In order to test DELETE for a submission document resource as underwriter
	Create a submission, QM & QMD as RFQ as Broker, 
	As a underwriter user I attach a new document and Delete it

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		  | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"

	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	# Underwriter - post document
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I POST a set of submission documents from the data
	| FileName        | DocumentId | FileMIMEType                                                            | DocumentType   | DocumentDescription | FilePath                               | ExpectedOutput |
	| sample_mrc.docx | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Supporting Document | Data\mrc_documents\sample_mrc_new.docx | 201            |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 [SubmissionDocument_DELETE_Underwriter_DeleteSubmissionDocumentById] Delete a supporting document.
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range

	#VALIDATE DELITION
    And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have 2 documents


#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDocuments_Underwriter_Negative
	In order to test POST for a submission document resource as an underwriter
	As an underwriter I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName               | FileMimeType                                                            | DocumentType          | File                                             | DocumentDescription       | ShareableToAllMarketsFlag |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx   | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx               | Placing Slip test         | False                     |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_clientcorr.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\supporting_documents\sample_clientcorr.docx | Client Corrspondence test | False                     |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName               |
	| MRC_Placingslip.docx   |
	| sample_clientcorr.docx |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context
		And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | False                     |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDocument_POST_MULTIPART_Underwriter_UnderwriterNotAuthorisedForSubmissionUnderwriter] - POST a submission document as an unauthorised underwriter, returns 400/403/404
	Given I log in as an underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And Set the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription             | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2NEW.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json | False                        |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400" or "403" or "404"
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
#THIS HAS BECOME OBSOLETE BASED ON THE CHANGE FOR U2231
Scenario: 03 [SubmissionDocument_POST_MULTIPART_Underwriter_DocumentNotShareableToAllMarkets] - POST a submission document with ShareableToAllMarketsFlag as True, returns 400
	Given Set the SubmissionDialogue context with the following documents for underwriter
	| ShareableToAllMarketsFlag | FileName                | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription             | JSONPayload                              |
	| True                      | PremiumAdvise_2NEW.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ShareableToAllMarketsFlagExpectedFalse'

#---------------------------------------------------------------------------

@CoreAPI @TestNotImplemented
#THIS TEST IS NOT TESTABLE, RESULTING INTO TIMEOUTS BECAUSE OF THE FILESIZEINBYTES!
Scenario: 04 [SubmissionDocument_POST_MULTIPART_Underwriter_BinaryFileSizeMismatch] - POST a submission document with a FileSize that doesnt match the actual file size, returns 400
	Given Set the SubmissionDialogue context with the following documents for underwriter
	| FileSizeInBytes | FileName                | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription             | JSONPayload                              | ShareableToAllMarketsFlag |
	| 666             | PremiumAdvise_2NEW.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'BinaryFileSizeMismatch'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 [SubmissionDocument_POST_MULTIPART_Underwriter_SupportingDocumentMIMETypeExpected] - POST a submission document with a type not supported, returns 400
	Given Set the SubmissionDialogue context with the following documents for underwriter
	| FileName              | File                                            | FileMimeType | DocumentType          | DocumentDescription             | JSONPayload                              | ShareableToAllMarketsFlag |
	| supporting_wrong.html | Data\supporting_documents\supporting_wrong.html | text/html    | correspondence_client | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'FileExtensionNotSupportedForSupportingDocument'

#---------------------------------------------------------------------------


@CoreAPI 
Scenario: 07 [SubmissionDocument_POST_MULTIPART_Underwriter_NoLinkedReplacingDocument] - POST a submission document ith ReplacingDocumentId that does not related to an existent document, returns 400
	Given Set the SubmissionDialogue context with the following documents for underwriter
	| ReplacingDocumentId | FileName                | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription             | JSONPayload                              | ShareableToAllMarketsFlag |
	| 666                 | PremiumAdvise_2NEW.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 08 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkSubmissionMismatchForMRC] - POST a submission document with Submissionversion that doesnt match with the initial submission, returns 400
	Given set for replace the SubmissionDialogue context with the following documents
	| SubmissionVersionNumber | FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| 66                      | MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "403" or "400" or "404"
	And the response contains one of the following error messages
	| ErrorMessage                               |
	| ReplacingDocumentSubmissionVersionMismatch |
	| AccessToReferencedResourceNotAllowed       |
	| SubmissionInPayloadDoesNotExist            |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewMRCSubmissionDocumentNotAllowed] POST Submission Document Underwriter attempts to upload a new MRC submission document, returns 400.
	Given Set the SubmissionDialogue context with the following documents for underwriter
	| FileName              | File                                     | FileMimeType                                                            | DocumentType          | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| MRCPlacingSlipKB.docx | Data\mrc_documents\MRCPlacingSlipKB.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip - UW   | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                 |
	| [MRCCannotBeAddedAsNew] |



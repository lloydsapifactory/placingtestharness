#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDocuments_Underwriter_Negative_Replace
	In order to test POST for a submission document resource as an underwriter
	As an underwriter I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | False                         |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | False                     |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | False                     |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDocument_POST_MULTIPART_Underwriter_NoLinkedReplacingDocument] - POST a submission document with ReplacingDocumentId that doesnt exist, returns 400
	Given set for replace the SubmissionDialogue context with the following documents
	| ReplacingDocumentId | FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | 
	| 666                 | MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
#LOOKS LIKE AN INDALID TEST?
Scenario: 02 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkSubmissionMismatch] - POST a submission document with Replacing submission version that doesnt match with the initial submission, returns 400
	Given set for replace the SubmissionDialogue context with the following documents
	| SubmissionVersionNumber | FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | 
	| 3                       | MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentSubmissionVersionMismatch'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkDocumentTypeMismatchForMRC] - POST a replacning submission document with a document type that doesnt match the MRC, returns 400
	Given set for replace the SubmissionDialogue context with the following documents
	| DocumentType   | FileName             | ReplaceWith         |File                                   | FileMimeType                                                            | 
	| advice_premium | MRC_Placingslip.docx | sample_mrc_new.docx |Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDocumentTypeMismatch'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkDocumentNotOwnedByBrokerForMRC] POST with a MRC document that has not been supplied by the broker, returns 400 
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith     | File                               | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx| Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |

	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True        | godzillaTesting           | QUOTE            | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters
	And I set the SubmissionDialogue context with the submission for underwriter

	And set for replace the SubmissionDialogue context with the following documents
	| FileName               | ReplaceWith         | File                                   | FileMimeType                                                            |
	| sample_mrc_new.docx | sample_mrc_new.docx.doc | Data\mrc_documents\sample_mrc_new.docx.doc | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| ReplacingDocumentNotSuppliedByBroker |
	| ReplacingDocumentDoesNotExist        |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkDocumentNotOwnedByBrokerForSupDoc] POST with a supporting document that has not been supplied by the broker, returns 400 
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith     | File                               | FileMimeType                                                            |
	| sample_mrc.docx  | sample_mrc_new.docx| Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |

	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True        | godzillaTesting           | QUOTE            | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters
	And I set the SubmissionDialogue context with the submission for underwriter

	And set for replace the SubmissionDialogue context with the following documents
	| FileName               | ReplaceWith         | File                                   | FileMimeType                                                            |
	| sample_mrc_new.docx | sample_mrc_new.docx.doc | Data\mrc_documents\sample_mrc_new.docx.doc | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| ReplacingDocumentNotSuppliedByBroker |
	| ReplacingDocumentDoesNotExist        |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionDocument_POST_MULTIPART_Underwriter_ReplacingDocumentLinkDocumentTypeMismatchForSupDoc] - POST a supporting submission document with a different document type, returns 400
	Given set for replace the SubmissionDialogue context with the following documents
	| DocumentType       | FileName        | ReplaceWith         | File                                   | FileMimeType                                                            |
	| document_file_note | sample_mrc.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
	When I POST to '/SubmissionDocuments' resource with multipart
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDocumentTypeMismatch'
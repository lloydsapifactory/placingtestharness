#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDocuments_Underwriter
	In order to test POST for a submission document resource as an underwriter
	As an underwriter I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | False                         |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | False                         |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | False                         |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedMRCSubmissionDocument] - POST Submission Document Underwriter attempts to upload proposed MRC submission document, returns 201.
	And reset the SenderDocumentIds
	When replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And I save the submission document response to the submissiondocumentcontext 
	Then the document 'sample_mrc_new.docx' should contain a valid ReplacingDocumentId
	And the document 'sample_mrc_new.docx' should replace 'MRC_Placingslip.docx'

	# PICK UP THE CORRECT SUBMISSIONUNDERWRITERID
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	# SET THE CORRECT SUBMISSIONUNDERWRITERID
	And set the SubmissionUnderwriterId

	# DO A POST TO SUBMISSIONDIALOGUES AS AN UW WITH THE CORRECT SUBMISSIONUNDERWRITERID
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test sender npte from the UW'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testupdated'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource 

	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 2 document
	And the collection of submission documents contains 1 document where the ReplacingDocumentId has a value

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedSupportingDocument] - POST Submission Document Underwriter attempts to upload supporting submission document, returns 201.
	And reset the SenderDocumentIds
	When replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith          | File                                           | FileMimeType                                                            |
	| sample_mrc.docx | PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And I save the submission document response to the submissiondocumentcontext 
	Then the document 'PremiumAdvise_2.docx' should contain a valid ReplacingDocumentId
	And the document 'PremiumAdvise_2.docx' should replace 'sample_mrc.docx'

	# PICK UP THE CORRECT SUBMISSIONUNDERWRITERID
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	# SET THE CORRECT SUBMISSIONUNDERWRITERID
	And set the SubmissionUnderwriterId

	# DO A POST TO SUBMISSIONDIALOGUES AS AN UW WITH THE CORRECT SUBMISSIONUNDERWRITERID
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test sender npte from the UW'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testupdated'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource 

	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 2 document
	And the collection of submission documents contains 1 document where the ReplacingDocumentId has a value

@CoreAPI 
Scenario: 03 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewSupportingDocument] - POST Submission Document Underwriter attempts to upload a new supporting submission document, returns 201.
	And reset the SenderDocumentIds
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | 
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json |  
	And I save the submission document response to the submissiondocumentcontext 

	# PICK UP THE CORRECT SUBMISSIONUNDERWRITERID
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	# SET THE CORRECT SUBMISSIONUNDERWRITERID
	And set the SubmissionUnderwriterId

	# DO A POST TO SUBMISSIONDIALOGUES AS AN UW WITH THE CORRECT SUBMISSIONUNDERWRITERID
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test sender npte from the UW'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testupdated'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource 

	Then  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 2 document
	And the collection of submission documents contains 0 document where the ReplacingDocumentId has a value









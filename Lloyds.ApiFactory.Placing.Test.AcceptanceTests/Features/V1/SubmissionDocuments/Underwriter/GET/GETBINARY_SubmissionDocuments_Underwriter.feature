#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETBINARY_SubmissionDocuments_Underwriter
	In order to retrieve a submission document
	that has been assigned by the broker after a submission dialogue
	As an underwriter
	I want to be able to do a GET to submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDocument_GET_BINARY_Underwriter_GetBinarySubmissionDocumentById]: GET_BINARY to a submission document as an underwriter 
And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  |Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response is a valid application/json; charset=utf-8 type of submission documents collection

	And I get a random submission document from the collection
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "200"

	And store feature context

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDocument_GET_BINARY_Underwriter_SubmissionDocumentNotFound] - GET_BINARY to an invalid submission document, returns 404 
	And restore feature context
	And I get a random submission document from the collection

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set a valid but not existing SubmissionDocumentId
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_GET_BINARY_Underwriter_UnderwriterNotAuthorisedForSubmission] As an unauthorised underwriter GetById returns 400/403
	And restore feature context
	And I get a random submission document from the collection

	Given I log in as an underwriter 'BeazleyUnderwriter.Fiona@LIMOSSDIDEV.onmicrosoft.com'
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 04 [SubmissionDocument_GET_BINARY_Underwriter_UnderwriterNotAuthorisedForDocument] GET_BINARY to a submission document that has not been delivered to the underwriter by the broker, returns 400/403
	And restore feature context
	And I get a random submission document that has not been sent to the underwriter

	Given I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	When make a GET_BINARY request to '/SubmissionDocuments/{SubmissionDocumentId}/content' resource 
	Then the response status code should be "400" or "403"

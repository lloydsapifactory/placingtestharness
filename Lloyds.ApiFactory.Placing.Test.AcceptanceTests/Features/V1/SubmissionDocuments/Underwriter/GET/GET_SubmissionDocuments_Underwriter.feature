#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionDocuments_Underwriter
	In order to retrieve a submission document
	that has been assigned by the broker after a submission dialogue
	As an underwriter
	I want to be able to do a GET to submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDocument_GET_ALL_Underwriter_GetSubmissionDocuments]: I want to be able to do a GET ALL to the ubmission documents as an underwriter, returns 200 
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have a link with proper value
	And submission documents should have 2 document

	And store feature context

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDocument_GET_ALL_Underwriter_GetSubmissionDocuments]: I want to be able to do GETALL and return all MANDATORY fields
	And restore feature context
	And I set the SubmissionReference

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And validate submission documents results contain the following fields
	| fieldname                 |
	| FileName                  |
	| DocumentType              |
	| DocumentDescription       |
	| SuppliedBy                |
	| SubmissionReference       |
	| SubmissionVersionNumber   |
	| DocumentId                |
	| FileMIMEType              |
	| FileSizeInBytes           |
	| ShareableToAllMarketsFlag |
	| CreatedDateTime           |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_GET_Underwriter_GetSubmissionDocumentById]: I want to be able to do GetById to a submission document as an underwriter 
	And restore feature context

	And I get a random submission document from the collection
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"
	And I refresh the SubmissionDocumentcontext with the response content


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDocument_GET_Underwriter_GetSubmissionDocumentById]: I want to be able to do GetById and return all MANDATORY fields
	And restore feature context
	And I get a random submission document from the collection
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "200"
	And I refresh the SubmissionDocumentcontext with the response content
	And I refresh the SubmissionDocumentcontext with the response content
	And validate submission documents context contain the following fields
	| fieldname                 |
	| FileName                  |
	| DocumentType              |
	| DocumentDescription       |
	| SuppliedBy                |
	| SubmissionReference       |
	| SubmissionVersionNumber   |
	| DocumentId                |
	| FileMIMEType              |
	| FileSizeInBytes           |
	| ShareableToAllMarketsFlag |
	| CreatedDateTime           |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 05 GetALL ('contains' partial match filtering):  I want to be able to get all submission documents, with contains filter 
	And restore feature context
	And I set the SubmissionReference

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference |
		| FileName            |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 GetALL (pagination): I want to be able to get all submission documents with pagination 
	And restore feature context
	And I set the SubmissionReference

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document
	And I add a filter on '_pageNum' with a value '2'
	And I add a filter on '_pageSize' with a value '1'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 1 document

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 07 GetALL orderby: As a broker I want to be able to get all submission document ordered by CreatedDateTime
	And restore feature context
	And I set the SubmissionReference

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_order' with a value '<value>'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document
	And the submission documents response is order by '<value>' 'ascending'

			Examples:
| value           |
| CreatedDateTime |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 08 GetALL (exact match filtering):  I want to be able to get all submission documents, with filter 
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	And I add a '' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have at least 1 document

	Examples:
		| field                   |
		| SubmissionVersionNumber |
		| DocumentType            |
		| SuppliedBy              |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 09 GetALL ('prefix' partial match filtering):  I want to be able to get all submission documents, with contains filter 
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	And I add a 'prefix' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference      |
		| FileName            |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 10 GetALL ('suffix' partial match filtering):  I want to be able to get all submission documents, with contains filter 
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	And I add a 'suffix' filter on '<field>' with a random value from the response of submission documents
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have at least 1 link
	And submission documents should have at least 0 document
	And validate submission documents results contain the value of <field>

	Examples:
		| field               |
		| DocumentDescription |
		| SubmissionReference      |
		| FileName            |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 11 GetALL: Id should have the structure of SubmissionReference_SubmissionVersionNumber_DocumentId
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And for the submission documents and the returned results the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_DocumentId'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 12 (GetALL [multiple values filter]) Broker perform a GET on the submissions resource and adds a multiple values filter for SubmissionVersionNumber 
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document
	And for submission documents I validate I am getting the correct results for <field> with values of (<values>)

		Examples:
| field                   | values | number of found |
| SubmissionVersionNumber | 1,2    | 2               |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 13 Doing a GET ALL to submission documents with an invalid submission reference returns 0 results, returns 200 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference=66666' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have a link with proper value
	And submission documents should have 0 document


#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
Scenario Outline: 14 GetALL orderby last_modified: get all submission document ordered by last modified, when underwriter posts a new supporting document
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | JsonPayload                              |
		 | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | Data\1_2_Model\SubmissionDocument_1.json |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	| sample_mrc.txt       |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| True        | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 4 document

	And the submission documents response is ordered by '_last_modified' of the field 'FileName' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| MRC_Placingslip.docx   | PremiumAdvise_2.docx    |
	| sample_mrc.docx        | sample_mrc.txt          |
	| sample_mrc.txt         | sample_mrc.docx         |
	| PremiumAdvise_2.docx   | MRC_Placingslip.docx    |


Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
Scenario Outline: 15 GetALL orderby last_modified: get all submission document ordered by last modified, when underwriter replaces the placing slip
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | JsonPayload                              |
		 | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | Data\1_2_Model\SubmissionDocument_1.json |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	| sample_mrc.txt       |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_order' with a value '_last_modified'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 4 document

	And the submission documents response is ordered by '_last_modified' of the field 'FileName' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| MRC_Placingslip.docx   | sample_mrc_new.docx     |
	| sample_mrc.docx        | sample_mrc.txt          |
	| sample_mrc.txt         | sample_mrc.docx         |
	| sample_mrc_new.docx    | MRC_Placingslip.docx    |


Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
Scenario Outline: 16 GetALL orderby last_modified: get all submission document ordered by last modified, to update the uploaded new supporting submission document
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | JsonPayload                              |
		 | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | Data\1_2_Model\SubmissionDocument_1.json |
		 | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | Data\1_2_Model\SubmissionDocument_1.json |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	| sample_mrc.txt       |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And I save the submission document response to the submissiondocumentcontext 
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	
	And I set the SubmissionDocumentId
	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save SubmissionDocument update information for PUT
	And I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header	

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_order' with a value '_last_modified'
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 4 document

	And the submission documents response is ordered by '_last_modified' of the field 'FileName' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| MRC_Placingslip.docx   | PremiumAdvise_2.docx     |
	| sample_mrc.docx        | sample_mrc.txt          |
	| sample_mrc.txt         | sample_mrc.docx         |
	| PremiumAdvise_2.docx   | MRC_Placingslip.docx    |


Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 17 Adding _select parameter will return results with the exact fields
	And restore feature context
	And I set the SubmissionReference
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	Given I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,documentType,documentDescription,shareableToAllMarketsFlag'
	When  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And collection of submission documents should have a link with proper value


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 18 (DATE before,after filtering): GETALL Submission Documents as an UW, before or after a datetime for CreatedDateTime
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a date filter on the '<field>' <comparison> the date of <when>
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	And the response body should contain a collection of submission dialogue
	And get random submission dialogue from the submission dialogue collection
	And I set the SubmissionReference from the SubmissionDialogue context
	And I reset all the filters
	
	Given I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And validate that the submission documents results for the field '<field>' are <comparison> the date of <when>
	
	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | before     | one week  |
		| CreatedDateTime | after      | yesterday |


		
#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 19 (DATE range Filter): GETALL Submission documents as an underwriter, with a date range 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a date filter on the '<field>' <comparison> the date of <when>
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	And the response body should contain a collection of submission dialogue
	And get random submission dialogue from the submission dialogue collection
	And I set the SubmissionReference from the SubmissionDialogue context
	And I reset all the filters

	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission documents
	And validate that the submission documents results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 20 (range adatetime..bdatetime): GETALL Submission documents as an underwriter, with a date range 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=a week ago and bdatetime=today
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	And the response body should contain a collection of submission dialogue
	And get random submission dialogue from the submission dialogue collection
	And I set the SubmissionReference from the SubmissionDialogue context
	And I reset all the filters

	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=a week ago and bdatetime=today
	When I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission documents
	And submission documents should have at least 1 document
	And validate that the submission documents date results for the field '<field>' are  for one week

	Examples:
		| field           | 
		| CreatedDateTime | 



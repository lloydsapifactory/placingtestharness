#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_SubmissionDocuments_Underwriter
	In order to retrieve a submission document
	As a broker
	I want to be able to do a HEAD request to SubmissionDocuments via the API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

 #---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDocument_HEAD_Underwriter_HeadSubmissionDocumentById] For a valid submission document thathas been assigned to an underwriter, I want to be able to do a HEAD returning 2xx
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I get a random submission document from the added
	When  I make a HEAD request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDocument_HEAD_Underwriter_SubmissionDocumentNotFound] For an INVALID submission document, I want to be able to do a HEAD returning 404
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a HEAD request to '/SubmissionDocuments/6666666' resource
	Then the response status code should be "404"

 #---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_HEAD_Underwriter_UnderwriterNotAuthorised] - As an unathorised underwriter I want to be able to return 400/403
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And I get a random submission document from the added
	When  I make a HEAD request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "403" or "400"


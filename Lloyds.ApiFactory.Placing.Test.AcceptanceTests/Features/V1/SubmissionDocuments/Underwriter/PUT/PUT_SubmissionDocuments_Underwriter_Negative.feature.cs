// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.SubmissionDocuments.Underwriter.PUT
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("PUT_SubmissionDocuments_Underwriter_Negative")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class PUT_SubmissionDocuments_Underwriter_NegativeFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "PUT_SubmissionDocuments_Underwriter_Negative.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "PUT_SubmissionDocuments_Underwriter_Negative", "\tIn order to test POST for a submission document resource as an underwriter\r\n\tAs " +
                    "an underwriter I would like to create [POST] a submission and then create submis" +
                    "sion documents [POST] for the submission", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 10
#line hidden
#line 11
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 12
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 13
testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table473 = new TechTalk.SpecFlow.Table(new string[] {
                        "JsonPayload",
                        "FileName",
                        "FileMimeType",
                        "DocumentType",
                        "File",
                        "DocumentDescription",
                        "ShareableToAllMarketsFlag"});
            table473.AddRow(new string[] {
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "MRC_Placingslip.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "document_placing_slip",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Placing Slip test",
                        "False"});
            table473.AddRow(new string[] {
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "advice_premium",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Client Corrspondence test",
                        "False"});
            table473.AddRow(new string[] {
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.txt",
                        "text/plain",
                        "document_file_note",
                        "Data\\mrc_documents\\sample_mrc.txt",
                        "additional non placing test",
                        "False"});
#line 14
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                    "soft.com\' with the following submission documents", ((string)(null)), table473, "And ");
#line hidden
#line 19
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table474 = new TechTalk.SpecFlow.Table(new string[] {
                        "UnderwriterUserEmailAddress",
                        "CommunicationsMethodCode"});
            table474.AddRow(new string[] {
                        "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                        "PLATFORM"});
#line 20
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                    "n posted", ((string)(null)), table474, "And ");
#line hidden
#line 23
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table475 = new TechTalk.SpecFlow.Table(new string[] {
                        "FileName"});
            table475.AddRow(new string[] {
                        "MRC_Placingslip.docx"});
            table475.AddRow(new string[] {
                        "sample_mrc.docx"});
#line 24
 testRunner.And("I set the SubmissionDialogue context with the following documents for broker", ((string)(null)), table475, "And ");
#line hidden
            TechTalk.SpecFlow.Table table476 = new TechTalk.SpecFlow.Table(new string[] {
                        "IsDraftFlag",
                        "SenderActionCode",
                        "SenderNote"});
            table476.AddRow(new string[] {
                        "False",
                        "RFQ",
                        "a note from the broker"});
#line 28
 testRunner.And("POST as a broker to SubmissionDialogues with following info", ((string)(null)), table476, "And ");
#line hidden
#line 31
 testRunner.And("I clear down test context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 33
 testRunner.And("I refresh the SubmissionDialogeContext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 34
 testRunner.And("I set the SubmissionDialogueId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 35
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 36
 testRunner.And("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 37
 testRunner.And("store the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 [SubmissionDocument_PUT_Underwriter_UpdateNewMRCDocumentNotAllowed] - PUT a su" +
            "bmission document updating new MRC document, returns 400")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.CategoryAttribute("TestNotImplemented")]
        public virtual void _01SubmissionDocument_PUT_Underwriter_UpdateNewMRCDocumentNotAllowed_PUTASubmissionDocumentUpdatingNewMRCDocumentReturns400()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI",
                    "TestNotImplemented"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 [SubmissionDocument_PUT_Underwriter_UpdateNewMRCDocumentNotAllowed] - PUT a su" +
                    "bmission document updating new MRC document, returns 400", null, new string[] {
                        "CoreAPI",
                        "TestNotImplemented"});
#line 42
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 10
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table477 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "ReplaceWith",
                            "File",
                            "FileMimeType"});
                table477.AddRow(new string[] {
                            "MRC_Placingslip.docx",
                            "sample_mrc_new.docx",
                            "Data\\mrc_documents\\sample_mrc_new.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"});
#line 44
 testRunner.Given("set for replace the SubmissionDialogue context with the following documents", ((string)(null)), table477, "Given ");
#line hidden
#line 47
 testRunner.And("store the CreatedDateTime from submission documents context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 49
 testRunner.And("for the submission documents the \'DocumentType\' is \'document_placing_slip\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 50
 testRunner.And("for the submission documents the \'DocumentDescription\' is \'Update to new MRC docu" +
                        "ment\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 51
 testRunner.And("for the submission documents the \'ShareableToAllMarketsFlag\' is \'false\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 52
 testRunner.And("I save SubmissionDocument update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 53
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("02 [SubmissionDocument_PUT_Underwriter_SubmissionDocumentNotFound] - PUT Submissi" +
            "on Document Underwriter to update the uploaded new supporting submission documen" +
            "t with invalid DocumentId, returns 400.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _02SubmissionDocument_PUT_Underwriter_SubmissionDocumentNotFound_PUTSubmissionDocumentUnderwriterToUpdateTheUploadedNewSupportingSubmissionDocumentWithInvalidDocumentIdReturns400_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 [SubmissionDocument_PUT_Underwriter_SubmissionDocumentNotFound] - PUT Submissi" +
                    "on Document Underwriter to update the uploaded new supporting submission documen" +
                    "t with invalid DocumentId, returns 400.", null, new string[] {
                        "CoreAPI"});
#line 57
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 10
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table478 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "File",
                            "FileMimeType",
                            "DocumentType",
                            "DocumentDescription",
                            "JSONPayload",
                            "ShareableToAllMarketsFlag"});
                table478.AddRow(new string[] {
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Premium Advise",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "False"});
#line 58
 testRunner.And("Add the SubmissionDialogue context with the following documents for underwriter", ((string)(null)), table478, "And ");
#line hidden
#line 61
 testRunner.And("I set the SubmissionDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 62
 testRunner.And("store the CreatedDateTime from submission documents context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 63
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 64
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 65
 testRunner.And("for the submission documents the \'DocumentType\' is \'correspondence_client\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 66
 testRunner.And("for the submission documents the \'DocumentDescription\' is \'Support Premium Advise" +
                        " Document updated v2 -UW\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 67
 testRunner.And("for the submission documents the \'ShareableToAllMarketsFlag\' is \'false\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 69
 testRunner.Given("for the submission document I set an invalid DocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 70
 testRunner.And("I save SubmissionDocument update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 71
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 72
 testRunner.Then("the response status code should be \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 73
 testRunner.And("the response contains a validation error \'ResourceDoesNotExist\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("03 [SubmissionDocument_PUT_Underwriter_ResourceKeysMismatch] - PUT Submission Doc" +
            "ument Underwriter to update the uploaded new supporting submission document with" +
            " resource key that doesnt match withthe SubmissionDocument, returns 400.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _03SubmissionDocument_PUT_Underwriter_ResourceKeysMismatch_PUTSubmissionDocumentUnderwriterToUpdateTheUploadedNewSupportingSubmissionDocumentWithResourceKeyThatDoesntMatchWiththeSubmissionDocumentReturns400_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("03 [SubmissionDocument_PUT_Underwriter_ResourceKeysMismatch] - PUT Submission Doc" +
                    "ument Underwriter to update the uploaded new supporting submission document with" +
                    " resource key that doesnt match withthe SubmissionDocument, returns 400.", null, new string[] {
                        "CoreAPI"});
#line 78
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 10
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table479 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "File",
                            "FileMimeType",
                            "DocumentType",
                            "DocumentDescription",
                            "JSONPayload",
                            "ShareableToAllMarketsFlag"});
                table479.AddRow(new string[] {
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Premium Advise",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "False"});
#line 80
 testRunner.And("Add the SubmissionDialogue context with the following documents for underwriter", ((string)(null)), table479, "And ");
#line hidden
#line 83
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 84
 testRunner.And("I set the SubmissionDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 85
 testRunner.And("store the CreatedDateTime from submission documents context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 86
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 87
 testRunner.And("for the submission documents the \'DocumentType\' is \'correspondence_client\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 88
 testRunner.And("for the submission documents the \'DocumentDescription\' is \'Support Premium Advise" +
                        " Document updated v2 -UW\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 89
 testRunner.And("for the submission documents the \'ShareableToAllMarketsFlag\' is \'false\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 91
 testRunner.Given("for the submission documents the \'SubmissionVersionNumber\' is \'2\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 92
 testRunner.And("I save SubmissionDocument update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 93
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 94
 testRunner.Then("the response status code should be \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("05 [SubmissionDocument_PUT_Underwriter_UnderwriterNotAuthorisedForSubmissionUnder" +
            "writer] - PUT Submission Document Underwriter as an unauthorised underwriter, re" +
            "turns 400/403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _05SubmissionDocument_PUT_Underwriter_UnderwriterNotAuthorisedForSubmissionUnderwriter_PUTSubmissionDocumentUnderwriterAsAnUnauthorisedUnderwriterReturns400403()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("05 [SubmissionDocument_PUT_Underwriter_UnderwriterNotAuthorisedForSubmissionUnder" +
                    "writer] - PUT Submission Document Underwriter as an unauthorised underwriter, re" +
                    "turns 400/403", null, new string[] {
                        "CoreAPI"});
#line 98
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 10
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table480 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "File",
                            "FileMimeType",
                            "DocumentType",
                            "DocumentDescription",
                            "JSONPayload",
                            "ShareableToAllMarketsFlag"});
                table480.AddRow(new string[] {
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Premium Advise",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "False"});
#line 99
 testRunner.And("Add the SubmissionDialogue context with the following documents for underwriter", ((string)(null)), table480, "And ");
#line hidden
#line 102
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 103
 testRunner.And("I set the SubmissionDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 104
 testRunner.And("store the CreatedDateTime from submission documents context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 105
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 106
 testRunner.And("for the submission documents the \'DocumentType\' is \'correspondence_client\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 107
 testRunner.And("for the submission documents the \'DocumentDescription\' is \'Support Premium Advise" +
                        " Document updated v2 -UW\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 108
 testRunner.And("for the submission documents the \'ShareableToAllMarketsFlag\' is \'false\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 110
 testRunner.Given("I log in as an underwriter \'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com" +
                        "\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 111
 testRunner.And("I save SubmissionDocument update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 112
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 113
 testRunner.Then("the response status code should be \"400\" or \"403\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
                TechTalk.SpecFlow.Table table481 = new TechTalk.SpecFlow.Table(new string[] {
                            "ErrorMessage"});
                table481.AddRow(new string[] {
                            "AccessToReferencedResourceNotAllowed"});
#line 114
 testRunner.And("the response contains one of the following error messages", ((string)(null)), table481, "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("06 [SubmissionDocument_PUT_Underwriter_DocumentNotShareableToAllMarkets] - PUT Su" +
            "bmission Document Underwriter with ShareableToAllMarketsFlag set to true, return" +
            "s 400.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _06SubmissionDocument_PUT_Underwriter_DocumentNotShareableToAllMarkets_PUTSubmissionDocumentUnderwriterWithShareableToAllMarketsFlagSetToTrueReturns400_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("06 [SubmissionDocument_PUT_Underwriter_DocumentNotShareableToAllMarkets] - PUT Su" +
                    "bmission Document Underwriter with ShareableToAllMarketsFlag set to true, return" +
                    "s 400.", null, new string[] {
                        "CoreAPI"});
#line 121
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 10
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table482 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "File",
                            "FileMimeType",
                            "DocumentType",
                            "DocumentDescription",
                            "JSONPayload",
                            "ShareableToAllMarketsFlag"});
                table482.AddRow(new string[] {
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Premium Advise",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "False"});
#line 122
 testRunner.And("Add the SubmissionDialogue context with the following documents for underwriter", ((string)(null)), table482, "And ");
#line hidden
#line 125
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 126
 testRunner.And("I set the SubmissionDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 127
 testRunner.And("store the CreatedDateTime from submission documents context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 128
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 129
 testRunner.And("for the submission documents the \'DocumentType\' is \'correspondence_client\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 130
 testRunner.And("for the submission documents the \'DocumentDescription\' is \'Support Premium Advise" +
                        " Document updated v2 -UW\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 131
 testRunner.And("for the submission documents the \'ShareableToAllMarketsFlag\' is \'true\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 133
 testRunner.And("I save SubmissionDocument update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 134
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 135
 testRunner.Then("the response status code should be \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 136
 testRunner.And("the response contains a validation error \'ShareableToAllMarketsFlagExpectedFalse\'" +
                        "", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

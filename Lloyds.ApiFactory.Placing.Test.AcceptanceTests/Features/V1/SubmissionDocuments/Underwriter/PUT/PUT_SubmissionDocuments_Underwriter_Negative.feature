#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDocuments_Underwriter_Negative
	In order to test POST for a submission document resource as an underwriter
	As an underwriter I would like to create [POST] a submission and then create submission documents [POST] for the submission

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | False                         |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | False                     |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | False                     |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
# THIS IS NOT TESTABLE AS IT RELIES FROM A PREVIOUS SCENARIO WHICH IS A NEGATIVE CASE
Scenario: 01 [SubmissionDocument_PUT_Underwriter_UpdateNewMRCDocumentNotAllowed] - PUT a submission document updating new MRC document, returns 400
	#???
	Given set for replace the SubmissionDialogue context with the following documents
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | 
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | 
	And store the CreatedDateTime from submission documents context

	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the submission documents the 'DocumentDescription' is 'Update to new MRC document'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDocument_PUT_Underwriter_SubmissionDocumentNotFound] - PUT Submission Document Underwriter to update the uploaded new supporting submission document with invalid DocumentId, returns 400.
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                        |
	And I set the SubmissionDocumentId
	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And I save the submission document response to the submissiondocumentcontext 
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	
	Given for the submission document I set an invalid DocumentId
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'ResourceDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionDocument_PUT_Underwriter_ResourceKeysMismatch] - PUT Submission Document Underwriter to update the uploaded new supporting submission document with resource key that doesnt match withthe SubmissionDocument, returns 400.
	#SUBMISSION VERSION NUMBER OR SUBMISSION REFERENCE OR DOCUMENTID
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                        |
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	
	Given for the submission documents the 'SubmissionVersionNumber' is '2'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 [SubmissionDocument_PUT_Underwriter_UnderwriterNotAuthorisedForSubmissionUnderwriter] - PUT Submission Document Underwriter as an unauthorised underwriter, returns 400/403
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	
	Given I log in as an underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionDocument_PUT_Underwriter_DocumentNotShareableToAllMarkets] - PUT Submission Document Underwriter with ShareableToAllMarketsFlag set to true, returns 400.
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'true'
	
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'ShareableToAllMarketsFlagExpectedFalse'







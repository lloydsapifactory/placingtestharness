#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDocuments_Underwriter
	In order to test POST for a submission document resource as an underwriter
	As an underwriter I would like to create [POST] a submission and then create submission documents [POST] for the submission


	Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDocument_PUT_Underwriter_UpdateNewSupportingDocument] - PUT Submission Document Underwriter to update the uploaded new supporting submission document, returns 201.
	And reset the SenderDocumentIds
	And Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId

	# PICK UP THE CORRECT SUBMISSIONUNDERWRITERID
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	# SET THE CORRECT SUBMISSIONUNDERWRITERID
	And set the SubmissionUnderwriterId

	# DO A POST TO SUBMISSIONDIALOGUES AS AN UW WITH THE CORRECT SUBMISSIONUNDERWRITERID
	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test sender npte from the UW'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testupdated'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource 

	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'correspondence_client'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updated v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 

	# VALIDATE PUT
	And I GET to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	And I save the submission document response to the submissiondocumentcontext 
	And the submission document response should contain the 'DocumentDescription' set to 'Support Premium Advise Document updated v2 -UW'
	And the submission document response should contain the 'DocumentType' set to 'correspondence_client'
	And the submission document response should contain the 'ShareableToAllMarketsFlag' set to flag false

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDocument_PUT_Underwriter_UpdateReplacingSupportingDocument] - PUT Submission Document Underwriter to update the uploaded proposed supporting submission document, returns 201.
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith          | File                                           | FileMimeType                                                            | ShareableToAllMarketsFlag | DocumentType   |
	| sample_mrc.docx | PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | False                         | advice_premium |
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId

	# PICK UP THE CORRECT SUBMISSIONUNDERWRITERID
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	# SET THE CORRECT SUBMISSIONUNDERWRITERID
	And set the SubmissionUnderwriterId

	# DO A POST TO SUBMISSIONDIALOGUES AS AN UW WITH THE CORRECT SUBMISSIONUNDERWRITERID
	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test sender npte from the UW'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testupdated'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource 

	And store the CreatedDateTime from submission documents context
	And I reset all the filters
	And for the submission documents the 'DocumentType' is 'advice_premium'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document updatedv2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'false'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 

	# VALIDATE PUT
	And I GET to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	And I save the submission document response to the submissiondocumentcontext 
	And the submission document response should contain the 'DocumentDescription' set to 'Support Premium Advise Document updatedv2 -UW'
	And the submission document response should contain the 'DocumentType' set to 'advice_premium'
	And the submission document response should contain the 'ShareableToAllMarketsFlag' set to flag false

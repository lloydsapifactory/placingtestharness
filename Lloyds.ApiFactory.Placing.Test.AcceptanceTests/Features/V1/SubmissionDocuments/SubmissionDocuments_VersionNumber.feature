#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: SubmissionDocuments_VersionNumber
	In order to test POST for a submission document resource
	for submission with different version numbers
	As a user I would like to create [POST] a submission and then create submission documents [POST] for the submission(s)

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And set up version number Submissions creation

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 Broker creates submissions with different version number with a placing slip - Subsequent submissions inherit the placing slip 
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	And  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And I save the submission document response to the submissiondocumentcontext
	And validate submission documents result contain item added on 'SubmissionVersionNumber' with a value '1'

	#POST Submission with subsequent version number and a supporting document
	And I POST 1 more submissions with different version for stored Submission
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.txt  | 2          | text/plain                                                              | document_file_note | additional non placing test | Data\mrc_documents\sample_mrc.txt  | 201            |
	And I save the submission document id response to the submissiondocumentidecontext

	And I set the SubmissionReference
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And submission documents should have 2 document

	And  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And I save the submission document response to the submissiondocumentcontext
	Then validate submission documents result contain item added on 'SubmissionVersionNumber' with a value '2'

	And the Submission with 'SubmissionVersionNumber'='1' has 1 documents
	And the Submission with 'SubmissionVersionNumber'='2' has 1 documents

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 Broker creates submissions with different version number and DELETE the supporting document from the initial Submission, returns 201
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	And I POST 1 more submissions with different version for stored Submission
	And I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.txt  | 2          | text/plain                                                              | document_file_note | additional non placing test | Data\mrc_documents\sample_mrc.txt  | 201            |

	#DELETE THE SUPPORTING DOCUMENT OF THE VERSION1 SUBMISSION
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionDocument_DELETE_Broker_DeleteInheritedSubmissionDocumentById] Broker creates submissions with different version number and DELETE the supporting document from a subsequent version number, returns 201
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	And I POST 1 more submissions with different version for stored Submission
	And I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.txt  | 2          | text/plain                                                              | document_file_note | additional non placing test | Data\mrc_documents\sample_mrc.txt  | 201            |
	| 2                       | sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium     | Client Corrspondence test   | Data\mrc_documents\sample_mrc.docx | 201            |
	And I save the submission document id response to the submissiondocumentidecontext

	#DELETE THE LAST SUPPORTING DOCUMENT OF THE VERSION2 SUBMISSION
	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "2xx" range
	#VALIDATE DELETION
	When  I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be in "4xx" range

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 DELETE [SubmissionNotInDraft] - DELETE Document, when there are Submissions with different version number and one of them is not in DRFT, return 403
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	#POST subsequent version number Submissions with the same Documents
	And I POST 1 more submissions with different version for stored Submission

	# MOVE TO NON DRFT
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And for the submission the 'SubmissionVersionNumber' is '1'
	And I set the SubmissionDialogue context for broker
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I save the current submission dialogue 
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And the response status code should be "201"

	# DELETE THE SUBMISSION, WHILE THERE IS ONE NOT IN DRFT
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be in "4xx" range


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 [SubmissionDocument_DELETE_Broker_SubmissionVersionNotInDraft] Delete a document when subsequent version and the inherited version is not in DRFT, returns 400
	#USING ANOTHER BROKER AS IT CLASHES WITH THE PREREQUISITES
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I set the SubmissionReference
	And I store the initial submission

	And I POST 1 more submissions with different version for stored Submission
	And I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.txt  | 2          | text/plain                                                              | document_file_note | additional non placing test | Data\mrc_documents\sample_mrc.txt  | 201            |

	#MOVE VERSION1 INTO NON DRAFT SUBMISSION
	And I restore the initial submission
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	And I set the current submission documents with the SenderDocumentIds
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 

	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionNotInDraft'


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 06 [SubmissionDocument_DELETE_Broker_InheritedSubmissionVersionNotInDraft] Delete a document when subsequent version and the inherited version is not in DRFT, returns 400
	#USING ANOTHER BROKER AS IT CLASHES WITH THE PREREQUISITES
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I set the SubmissionReference
	And I store the initial submission

	#SET THE DOCUMENT FROM THE INHERITED
	And I POST 1 more submissions with different version for stored Submission
	And  I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	And get the submission document from version 2
	And I set the SubmissionDocumentId

	#MOVE VERSION1 INTO NON DRAFT SUBMISSION
	And I restore the initial submission
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	And I set the current submission documents with the SenderDocumentIds
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 

	When  I make a DELETE request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionDocumentInherited'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 07 [SubmissionDocument_POST_MULTIPART_Broker_UploadReplacingMRCSubmissionDocument] Create subsequent submission and try to replace the MRC with the initial MRC, returns 4xx
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I will store the documentId

	#POST Submission with subsequent version number and a supporting document
	And I POST 1 more submissions with different version for stored Submission
	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName       | DocumentId | FileMIMEType | DocumentType          | DocumentDescription   | FilePath                          | ExpectedOutput |
	| 2                       | sample_mrc.docx | 2          | text/plain   | document_placing_slip | document_placing_slip | Data\mrc_documents\sample_mrc.docx | 201            |
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                      |
	| DocumentTypeNotSupportingDocument |
	| ReplacingDocumentDoesNotExist     |



#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 08 [SubmissionDocument_POST_MULTIPART_Broker_ReplacingDocumentLinkDocumentTypeMismatchForSupDoc] Create subsequent submission and replace supporting document with different document type, returns 4xx
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission
	
	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.docx | 4          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium     | Client Corrspondence test   | Data\mrc_documents\sample_mrc.docx | 201            |
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDocumentTypeMismatch'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 [SubmissionDocument_POST_MULTIPART_Broker_UploadReplacingSupportingDocument] Replacing supoorting document of the subsequent submission
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission
	
	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType       | DocumentDescription         | FilePath                           | ExpectedOutput |
	| 2                       | sample_mrc.docx | 4          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client     | Client Corrspondence test   | Data\mrc_documents\sample_mrc.docx | 201            |
	Then the response status code should be in "2xx" range


	
#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 10 [SubmissionDocument_POST_MULTIPART_Broker_UniqueReplacementDocumentLinkedForVersion] When trying to do two replacements with for a subsequent version and the supporting document, returns 400
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission

	#replace supporting document of version once
	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	And I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | 
	| 2                       | sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 
	And the response status code should be in "2xx" range
	And I clear down test context
	
	#replace supporting document of version again with the same ReplacingDocumentId
	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName       | DocumentId | FileMIMEType | DocumentType          | DocumentDescription       | FilePath                          |
	| 2                       | sample_mrc.txt | 4          | text/plain   | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.txt |
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                      |
	| DocumentsLinkedReplacementsRepeated |
	#ReplacingDocumentSubmissionVersionMismatch ????

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 [SubmissionDocument_POST_MULTIPART_Broker_ReplacingSupportingDocumentMIMETypeExpected] Replacing supporting document with invalid file extension, returns 400
Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission

	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName              | DocumentId | FileMIMEType | DocumentType          | DocumentDescription       | FilePath                                        |
	| 2                       | supporting_wrong.html | 3          | text/html    | correspondence_client | Correspondence Client [z] | Data\supporting_documents\supporting_wrong.html | 
	Then the response status code should be "400"
	And the response contains a validation error 'FileExtensionNotSupportedForSupportingDocument'


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 12 [SubmissionDocument_POST_MULTIPART_Broker_ReplacingDocumentLinkSubmissionMismatchForSupDoc]
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission

	And I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | 
	| 2                       | sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 
	And I will store the documentId

	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | 
	| 2                       | sample_mrc.docx | 4          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentLinkNotInEarlierSubmission'
	#ReplacingDocumentDoesNotExist ???


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 13 [SubmissionDocument_POST_MULTIPART_Broker_ReplacingDocumentLinkDocumentNotOwnedByBrokerForSupDoc]
	And set new submission dialogue context
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | False                         |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | False                     |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And I will store the documentId
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

	And I POST 1 more submissions with different version for stored Submission

	And for the submission documents the 'ReplacingDocumentId' is stored by the previous document context
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | 
	| 2                       | sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentNotSuppliedByBroker'
	#ReplacingDocumentDoesNotExist ???

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 14 [SubmissionDocument_POST_MULTIPART_Broker_NoLinkedReplacingDocumentForSupDoc] Replace supporting document for a subsequent submission given an invalid ReplacingDocumentId, returns 400
	Given for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I will store the documentId

	And I POST 1 more submissions with different version for stored Submission

	And for the submission documents the 'ReplacingDocumentId' is '6666666'
	When I POST a set of submission documents from the data
	| SubmissionVersionNumber | FileName        | DocumentId | FileMIMEType                                                            | DocumentType          | DocumentDescription       | FilePath                           | 
	| 2                       | sample_mrc.docx | 3          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Corrspondence test | Data\mrc_documents\sample_mrc.docx | 
	Then the response status code should be "400"
	And the response contains a validation error 'ReplacingDocumentDoesNotExist'

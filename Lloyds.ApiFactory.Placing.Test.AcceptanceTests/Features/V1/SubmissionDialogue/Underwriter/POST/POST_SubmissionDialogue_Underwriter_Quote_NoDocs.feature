#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_Quote_NoDocs
	In order to reply to a submission dialogue back for a QUOTE
	that I have been assigned to
	As an underwriter 
	I want to be able to do a POST to SubmissionDialogue QUOTE DRAFT/NO DRAFT WITH NO DOCUMENTS

	Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftQuoteNoDocs] POST for a NON DRAFT Quote with No Docs, returns 201
	Given I set the SubmissionDialogue context with the submission for underwriter
	When POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	#Creates a brand new submission dialogue for the underwriter
	Then validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#Validate the submission dialogue created for the underwriter with one posted
	And validate new submission dialogue created
	And get the submission dialogues as a underwriter posted by the underwriter
	Then validate the submission dialogues are 1
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#the submission dialogue can be seen by the broker and validate with one posted
	And get the submission dialogues as a broker posted by the underwriter
	Then validate the submission dialogues are 1
	And validate submission dialogue context with the submission dialogue posted by underwriter
	And validate submission dialogue context as an broker matching the submission dialogue posted by underwriter
	
	#SubmissionUnderwriterStatusCode is updated and QuoteStatus is updated
	Then validate SubmissionUnderwriterStatusCode is updated
	And validate Submission Status is updated

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftQuoteNoDocs] POST for a DRAFT Quote with No Docs, returns 201
	Given I set the SubmissionDialogue context with the submission for underwriter
	When POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| True       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	#Creates a brand new submission dialogue for the underwriter
	Then validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	#Validate the submission dialogue created for the underwriter with one posted
	Then validate new submission dialogue created
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 1
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	#the submission dialogue CANNOT be seen by the broker and validate with one posted
	Then get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 0
	
	#SubmissionUnderwriterStatusCode is updated and SubmissionStatus is updated
	Then validate SubmissionUnderwriterStatusCode is updated
	And validate Submission Status is updated

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 POST multiple times for the same submission reference/submission version, broker can ONLY see the NON DRAFT posted QMD
	Given I set the SubmissionDialogue context with the submission for underwriter

	#POST 1st with ISDraft = False
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                    |
	| False       | testingUnderwriterQuoteReference | QUOTE            | 1st SenderNote from NON DRAFT |
	And the response status code should be "201"

	And I clear down test context 

	#POST 2nd time ISDraft = False
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                    |
	| False       | testingUnderwriterQuoteReference | QUOTE            | 2ND SenderNote from NON DRAFT |
	And the response status code should be "201"

	And I clear down test context 

	#POST 3rd time with ISDraft = True
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                    |
	| True        | testingUnderwriterQuoteReference | QUOTE            | 3rd SenderNote from NON DRAFT |
	Then the response status code should be "201"

	And I clear down test context 
	
	#QMD UW VALIDATION
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 3

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 2

#---------------------------------------------------------------------------
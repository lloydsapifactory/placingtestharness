#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_NonQuote_NoDocs
	In order to reply to a submission dialogue back FOR EOI,RFI,DECL
	that I have been assigned to
	As an underwriter 
	I want to be able to do a POST to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

#---------------------------------------------------------------------------
@CoreAPI 
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftEOI
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftRFI
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftReject
Scenario Outline: 01 POST for a NON DRAFT EOI/RFI/REJECT, returns 201
	Given for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1'
	And add the submission dialogue request adding the field
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context
	
	# RESPONSE VALIDATION
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	# UW VALIDATION
	And validate new submission dialogue created
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 1
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	# SubmissionUnderwriterStatusCode/QUOTESTATUS VALIDATION
	And validate SubmissionUnderwriterStatusCode is updated
	And validate Submission Status is updated
	
	# BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 1
	And validate submission dialogue context with the submission dialogue posted by underwriter
	And validate submission dialogue context as an broker matching the submission dialogue posted by underwriter

	Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 

#---------------------------------------------------------------------------
@CoreAPI 
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftEOI
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftRFI
#SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftReject
Scenario Outline: 02 POST for a DRAFT EOI/RFI/REJECT, returns 201
	Given for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 2'
	And add the submission dialogue request adding the field
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context
	
	# RESPONSE VALIDATION
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	# UW VALIDATION
	And validate new submission dialogue created
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 1
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	# SubmissionUnderwriterStatusCode/QUOTESTATUS VALIDATION
	And validate SubmissionUnderwriterStatusCode is updated
	And validate Submission Status is updated
	
	# BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 0

	Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 

#---------------------------------------------------------------------------
#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

# This is not Implemented yet - the functionality for updating/replacing documents
# as part of the POST QMD response has been decided that will be handled in the future.

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_AdditionalDocs_Negative
	In order to reply to a submission dialogue back 
	And add or replace the existing docs
	As an underwriter 
	I want to be able to do a POST to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | False                     |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | False                     |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | False                     |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter


@CoreAPI 
Scenario: 1 [SubmissionDialogue_POST_Underwriter_SenderDocumentsNotExpected] POST for a non Submission QMD replacing the placing slip document with a new one, returns 400 when the SenderActionCode is not Quote.
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| False       |                           | EOI              | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                                                                 |
	| [SenderDocumentIdsNotExpected] |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 2 [SubmissionDialogue_POST_Underwriter_DocumentsLinkedUsedAlready] POST for a QUOTE QMD where docment id have been referenced in a previous QMD, returns 400 
	#POST with a new document, BUT NOT a Placing Slip and ShareableToAllMarkets = False
	Given Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription             | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Non Placing slip advice premium | Data\1_2_Model\SubmissionDocument_1.json | False                         |
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'QMD_POST_UW Negative Scenario 2.'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'POSTQMDUW_NEGATIVE'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

	# POST again with the same document
	And provide the same documents
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'QMD_POST_UW Negative Scenario 2.'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'POSTQMDUW_NEGATIVE'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                                                                 |
	| [DocumentLinkedUsedAlready] |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 3 [SubmissionDialogue_POST_Underwriter_DocumentsLinkedMRCRepeated] POST for a QUOTE QMD having more than one placing slips, returns 400
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| False       |             test              | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                |
	| [DocumentLinkedMRCRepeated] |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 4 [SubmissionDialogue_POST_Underwriter_DocumentsLinkedNotDocumentOwner] POST for a QUOTE QMD with a submission document that is supplied by a Broker, returns 400
	Given provide broker initial submission documents
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| False       | test                      | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage       |
	| [NotDocumentOwner] |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 5 [SubmissionDialogue_POST_Underwriter_DocumentsLinkedReplacementsUnique] POST with replacing documents that are not unique, returns 400
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| sample_mrc.docx  | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	| sample_mrc.docx  | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |False                         |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| False       | test                      | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                        |
	| DocumentsLinkedReplacementsRepeated |
	
#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

# This is not Implemented yet - the functionality for updating/replacing documents
# as part of the POST QMD response has been decided that will be handled in the future.

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_POSTSubmissionDocuments
	In order to reply to a submission dialogue back 
	And add or replace the existing docs
	As an underwriter 
	I want to be able to do a POST to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         | ShareableToAllMarketsFlag |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           | F                         |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   | F                         |
		 | 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test | F                         |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And for the submission dialogue the 'SenderActionCode' is 'RFQ'
	And for the submission dialogue the 'SenderNote' is 'return test to sender'   
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And the response status code should be "201"

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	##And I set the SubmissionDialogue context with the submission for underwriter

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory 
Scenario: 01 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedMRCSubmissionDocument] - POST Submission Document Underwriter attempts to upload proposed MRC submission document, returns 201.
	When replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |F                         |
	Then the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 
	And the document 'sample_mrc_new.docx' should contain a valid ReplacingDocumentId
	And the document 'sample_mrc_new.docx' should replace 'MRC_Placingslip.docx'

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory 
Scenario: 02 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedSupportingDocument] - POST Submission Document Underwriter attempts to upload supporting submission document, returns 201.
	When replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith          | File                                           | FileMimeType                                                            |ShareableToAllMarketsFlag |
	| sample_mrc.docx | PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |F                         |
	Then the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 
	And the document 'PremiumAdvise_2.docx' should contain a valid ReplacingDocumentId
	And the document 'PremiumAdvise_2.docx' should replace 'sample_mrc.docx'

@CoreAPI @BackendNotImplemented-Factory 
Scenario: 04 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewSupportingDocument] - POST Submission Document Underwriter attempts to upload a new supporting submission document, returns 201.
	When Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription | JSONPayload                              | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise      | Data\1_2_Model\SubmissionDocument_1.json | F                         | 
	Then the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 


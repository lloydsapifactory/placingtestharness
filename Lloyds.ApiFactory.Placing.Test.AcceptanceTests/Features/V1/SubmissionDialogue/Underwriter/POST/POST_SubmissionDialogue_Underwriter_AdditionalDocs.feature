#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

# This is not Implemented yet - the functionality for updating/replacing documents
# as part of the POST QMD response has been decided that will be handled in the future.

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_AdditionalDocs
	In order to reply to a submission dialogue back 
	And add or replace the existing docs
	As an underwriter 
	I want to be able to do a POST to QUOTE SubmissionDialogue DRAFT/NO DRAFT WITH DOCUMENTS

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftQuotewithDocs] POST for a DRAFT QMD replacing the placing slip document with a new one, returns 201
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            | ShareableToAllMarketsFlag |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | False                     |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        | godzillaTesting           | QUOTE            | POST QUOTE NO DRAFT message back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'
	And the submission dialogue contains "1" SenderDocumentId

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 0

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftQuotewithDocs] POST for NON DRAFT replacing the placing slip document with a new one, returns 201
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And the submission dialogue contains "1" SenderDocumentId
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 1

@CoreAPI 
Scenario: 03 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftQuotewithDocs] POST for a DRAFT replacing BOTH documents, returns 201
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith			| File											| FileMimeType																	|
	| MRC_Placingslip.docx | sample_mrc_new.docx	| Data\mrc_documents\sample_mrc_new.docx		| application/vnd.openxmlformats-officedocument.wordprocessingml.document		|
	| sample_mrc.docx	   | PremiumAdvise_2.docx	| Data\supporting_documents\PremiumAdvise_2.docx	| application/vnd.openxmlformats-officedocument.wordprocessingml.document	|
	When POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| True        | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 0

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftQuotewithDocs] POST for NON DRAFT adding two additional documents, returns 201
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith			| File											| FileMimeType																	|
	| MRC_Placingslip.docx | sample_mrc_new.docx	| Data\mrc_documents\sample_mrc_new.docx		| application/vnd.openxmlformats-officedocument.wordprocessingml.document		|
	| sample_mrc.docx	   | PremiumAdvise_2.docx	| Data\supporting_documents\PremiumAdvise_2.docx	| application/vnd.openxmlformats-officedocument.wordprocessingml.document	|
	When POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 1

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsDraftQuotewithDocs] POST for a DRAFT QUOTE adding a new supporting document, the broker should not see the new dialogue since it is in DRFT state. 
	# The underwriter replies to a broker's submission dialogue 
	Given Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription   | JSONPayload                              | 
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Client Premium advise | Data\1_2_Model\SubmissionDocument_1.json | 
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| True        | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And the submission dialogue contains "1" SenderDocumentId
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DRFT'

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 0

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionDialogue_POST_Underwriter_CreateUnderwriterQuoteResponseAsNonDraftQuotewithDocs] POST for NON DRAFT adding a new supporting document, the broker should not see the new dialogue since it is in DRFT state. 
# The underwriter replies to a broker's submission dialogue 
	Given Add the SubmissionDialogue context with the following documents for underwriter
	| FileName             | File                                           | FileMimeType                                                            | DocumentType   | DocumentDescription   | JSONPayload                              | 
	| PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Client Premium advise | Data\1_2_Model\SubmissionDocument_1.json | 
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	And validate submission dialogue context as an underwriter matching the submission dialogue posted by underwriter
	And validate submission dialogue SenderUserEmailAddress with the requester 
	And validate new submission dialogue created
	And the submission dialogue contains "1" SenderDocumentId
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#QMD BROKER VALIDATION
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are 1




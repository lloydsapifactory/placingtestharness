#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_QMD_UW_NonQuote_NoDocs
	In order to reply to a quote market dialogue back FOR EOI,RFI,DECL
	that I have been assigned to
	As an underwriter 
	I want to be able to do a POST to QuoteMarketDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a quote on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a quote document from the following data
		 | QuoteVersionNumber | BrokerCode | BrokerDepartmentId | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | 1225       | 558                | Data\1_2_Model\QuoteDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  | 1225       | 558                | Data\1_2_Model\QuoteDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | 1225       | 558                | Data\1_2_Model\QuoteDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the QuoteReference
	And I POST QuoteMarket from the data provided quote documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/quotemarkets/{quotemarketid}' resource 
	And the response status code should be "200"
	And I set the QuoteMarketDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And for the quote market dialogue the 'SenderActionCode' is 'RFQ'
	And for the quote market dialogue the 'SenderNote' is 'return test to sender'   
	And add the quote market dialogue request
	And I POST to '/quotemarketdialogues' resource
	And the response status code should be "201"

	And I clear down test context
	And I refresh the QuoteMarketDialogeContext with the response content
	And I set the QuoteMarketDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the QuoteMarketDialogue context for underwriter

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory @BackendNotImplemented-Ebix
Scenario Outline: 01 POST for a NON DRAFT EOI/RFI/REJECT, returns 201
	Given for the quote market dialogue the 'IsDraftFlag' is 'False'
	And for the quote market dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the quote market dialogue the 'UnderwriterQuoteReference' is ''
	And for the quote market dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the quote market dialogue the 'SenderNote' is 'test message back from the SCENARIO 5'
	And add the quote market dialogue request adding the field
	And the current quote market dialogue is saved for the scenario
	When I POST to '/quotemarketdialogues' resource
	Then the response status code should be "201"
	And I store the quote market dialogue response in the context
	#QMD RESPONSE VALIDATION
	And validate quote market dialogue context as an underwriter matching the quote market dialogue posted by underwriter
	And validate quote market dialogue SenderUserEmailAddress with the requester 
	#QMD UW VALIDATION
	And validate new quote market dialogue created
	And get the quote market dialogues as a underwriter posted by the underwriter
	And validate the quote market dialogues are 1
	And validate quote market dialogue context as an underwriter matching the quote market dialogue posted by underwriter
	# QUOTEMARKETSTATUS/QUOTESTATUS VALIDATION
	And validate QuoteMarketStatusCode is updated
	And validate Quote Status is updated
	#QMD BROKER VALIDATION
	And get the quote market dialogues as a broker posted by the underwriter
	And validate the quote market dialogues are 1
	And validate quote market dialogue context with the quote market dialogue posted by underwriter
	And validate quote market dialogue context as an broker matching the quote market dialogue posted by underwriter

	Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory @BackendNotImplemented-Ebix
Scenario Outline: 02 POST for a DRAFT EOI/RFI/REJECT, returns 201
	Given for the quote market dialogue the 'IsDraftFlag' is 'True'
	And for the quote market dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the quote market dialogue the 'UnderwriterQuoteReference' is ''
	And for the quote market dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the quote market dialogue the 'SenderNote' is 'test message back from the SCENARIO 2'
	And add the quote market dialogue request adding the field
	And the current quote market dialogue is saved for the scenario
	When I POST to '/quotemarketdialogues' resource
	Then the response status code should be "201"
	And I store the quote market dialogue response in the context
	#QMD RESPONSE VALIDATION
	And validate quote market dialogue context as an underwriter matching the quote market dialogue posted by underwriter
	And validate quote market dialogue SenderUserEmailAddress with the requester 
	#QMD UW VALIDATION
	And validate new quote market dialogue created
	And get the quote market dialogues as a underwriter posted by the underwriter
	And validate the quote market dialogues are 1
	And validate quote market dialogue context as an underwriter matching the quote market dialogue posted by underwriter
	# QUOTEMARKETSTATUS/QUOTESTATUS VALIDATION
	And validate QuoteMarketStatusCode is updated
	And validate Quote Status is updated
	#QMD BROKER VALIDATION
	And get the quote market dialogues as a broker posted by the underwriter
	And validate the quote market dialogues are 0

	Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 

#---------------------------------------------------------------------------
#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Underwriter_Negative
	In order to be sure that I cannot do a POST
	As an underwriter
	I want to be able to return client error codes
	And the right validation error

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I add a 'match' filter on 'SenderActionCode' with a value 'QUOTE'
	And I add a '' filter on 'IsDraftFlag' with a value 'false'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I reset all the filters
	And get random submission dialogue from the submission dialogue collection that doesnt have any QMD in draft
	And I set the SubmissionDialogue context without the submission for underwriter
	And I reset all the filters

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDialogue_POST_Underwriter_NoLinkedSubmission] POST with a SubmissionUnderwriterId that doesnt exist to the related submission, returns 400
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And for the submission dialogue the 'SubmissionUnderwriterId' is '666666666'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionUnderwriterDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_POST_Underwriter_UnderwriterNotAuthorisedForSubmissionUnderwriter] POST with an UNAUTHORISED Underwriter, returns 400/403
	Given I log in as an underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix @TestNotImplemented
Scenario: 03 POST with an UNAUTHORISED Underwriter from the same organisation, returns 4xx
	Given I log in as an underwriter 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'AccessNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix @TestNotImplemented
# -- NOT TESTABLE --
Scenario: 04 [SubmissionDialogue_POST_Underwriter_RFQNotPresent] POST when no submission dialogue with 'RFQ' exists, returns 400
	#question: How can i change the current QMD.SenderActionCode to something different than RFQ?
	#Given there is NO submission dialogue with SenderActionCode = 'RFQ' for this SubmissionUnderwriterId????
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'RequestForSubmissionDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteResponseAlreadyExistsInDraft] POST when there is already a QMD that is in 'DRFT', returns 400
	# POST a QMD with IsDraft=True
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| True        |                           | EOI              | a note from the underwriter back |
	And add the submission dialogue request
	And I POST to '/SubmissionDialogues' resource

	# POST again
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                       |
	| <IsDraftFlag>        |     TestUnderwriterQuoteReference                      | QUOTE              | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionDialogueInDraftStatusExists'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 06 [SubmissionDialogue_POST_Underwriter_InvalidActionCode] POST with an invalid SenderActionCode, returns 400
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode   | SenderNote                       |
	| False       | TestUnderwriterQuoteReference | <SenderActionCode> | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'InvalidUnderwriterActionCode'

		Examples:
		| SenderActionCode |
		| CLSDB            |
		| CLSDU            |
		| DECNT            |
		| QNTU             |
		| RFQ              |
		| WTHDR            |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07 POST without IsDraftFlag for QUOTE, returns 400
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SenderActionCode' with a value 'QUOTE'
	And I add a '' filter on 'IsDraftFlag' with a value 'False'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection that doesnt have any QMD in draft
	And I set the SubmissionDialogue context without the submission for underwriter
	And I reset all the filters

	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode   | SenderNote                       |
	|             | TestUnderwriterQuoteReference | <SenderActionCode> | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"

	Examples:
	| SenderActionCode |
	| QUOTE              |
	

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07a POST with empty IsDraftFlag for QUOTE, returns 400
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SenderActionCode' with a value 'QUOTE'
	And I add a '' filter on 'IsDraftFlag' with a value 'False'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection that doesnt have any QMD in draft
	And I set the SubmissionDialogue context without the submission for underwriter
	And I reset all the filters

	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode   | SenderNote                       |
	|             | TestUnderwriterQuoteReference | <SenderActionCode> | a note from the underwriter back |
	And add the submission dialogue request adding the field
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"

	Examples:
	| SenderActionCode |
	| QUOTE              |
	

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 08 [SubmissionDialogue_POST_Underwriter_DocumentsNoLinkedSubmissionDocument] POST with a submission document that does not exist, returns 400 
	Given a supporting document referenced does not exist for the related SubmissionDocument
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode | SenderNote                       |
	| False       | TestUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'DocumentDoesNotExist'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix @TestNotImplemented
# to be tested manually -- NOT TESTABLE --
Scenario: 09 [SubmissionDialogue_POST_Underwriter_DocumentsNoLinkedSubmission] POST with a submission document that is no linked to the referenced submission underwriter, returns 400 
	Given provide the same documents for SenderDocumentIds
	And a supporting document referenced is not referenced for the related Submission
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode | SenderNote                       |
	| False       | TestUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'DocumentsNotLinkedtoSubmissionUnderwriter'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 10 [SubmissionDialogue_POST_Underwriter_SenderNoteExpected] POST without a SenderNote for EIO,RFI returns 400 
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode   | SenderNote |
	| False       | TestUnderwriterQuoteReference | <SenderActionCode> |            |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SenderNoteMandatory'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 11 [SubmissionDialogue_POST_Underwriter_SenderNoteExpected] POST with an empty SenderNote for EIO,RFI, returns 400 
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference     | SenderActionCode   | SenderNote |
	| False       | TestUnderwriterQuoteReference | <SenderActionCode> |            |
	And add the submission dialogue request adding the field
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SenderNoteMandatory'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 12 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteReferenceExpected] POST without UnderwriterQuoteReference for QUOTE, returns 400 
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag   | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| <IsDraftFlag> |                           | QUOTE            | test message back from the SCENARIO 17 |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteReferenceExpected'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 13 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteReferenceNotExpected] POST with UnderwriterQuoteReference for a EOI,RFI, DECL SenderActionCode, returns 400 
	Given preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag   | UnderwriterQuoteReference     | SenderActionCode   | SenderNote                             |
	| <IsDraftFlag> | TestUnderwriterQuoteReference | <SenderActionCode> | test message back from the SCENARIO 18 |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteReferenceNotExpected'

		Examples:
		| SenderActionCode | IsDraftFlag |
		| EOI              | True        |
		| RFI              | True        |
		| DECL             | True        |
		| EOI              | False       |
		| RFI              | False       |
		| DECL             | False       |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 14 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteValidDateExpected] POST without UnderwriterQuoteValidUntilDate for QUOTE, returns 400 
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the UW'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateExpected'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteValidDateExpected] POST with empty UnderwriterQuoteValidUntilDate for QUOTE, returns 400 
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the UW'
	And add the submission dialogue request adding the field
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateExpected'

		Examples:
	| IsDraftFlag |
	| True        |
	| False       |
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteValidDateNotExpected] POST with UnderwriterQuoteValidUntilDate for a EOI,RFI, DECL SenderActionCode, returns 400 
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'SenderNote' is 'test message back from the UW'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateNotExpected'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 17 [SubmissionDialogue_POST_Underwriter_SenderDocumentsNotExpected] POST with SenderDocumentIds for EOI/RFI/DECL, returns 400 
	Given get random submission dialogue from the submission dialogue collection that does have SenderDocumentIds
	And provide the same documents for SenderDocumentIds
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode   | SenderNote                             |
	| False       |                           | <SenderActionCode> | test message back from the SCENARIO 15 |
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SenderDocumentIdsNotExpected'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18 Min,Max String Chars Quote - POST with fields having chars more or less than the value specs rules, returns 4xx
	Given for the feature submission dialogue context the '<field>' with a random value <assess> than <Characters>
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'godzillaTesting' string enhanced
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ErrorCode>'
	
	Examples:
		| field              | assess | rule | Characters | ErrorCode     | IsDraftFlag |
		| SubmissionReference     | more   | max  | 50         | StringTooLong | True        |
		| SubmissionReference     | more   | max  | 50         | StringTooLong | False       |
		| SubmissionReference     | less   | min  | 6          | StringTooShort  | True        |
		| SubmissionReference     | less   | min  | 6          | StringTooShort  | False       |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber | True        |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber | False        |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 19 Min,Max String Chars Non Quote - POST with fields having chars more or less than the value specs rules, returns 4xx
	Given for the feature submission dialogue context the '<field>' with a random value <assess> than <Characters>
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request adding the field
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ErrorCode>'
	
	Examples:
		| field              | assess | rule | Characters | ErrorCode     | IsDraftFlag |
		| SubmissionReference     | more   | max  | 50         | StringTooLong | True        |
		| SubmissionReference     | more   | max  | 50         | StringTooLong | False       |
		| SubmissionReference     | less   | min  | 6          | StringTooShort  | True        |
		| SubmissionReference     | less   | min  | 6          | StringTooShort  | False       |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber | True        |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber | False        |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 20 String character rules for Quote (SubmissionReference) - POST with SubmissionReference violating the string rules, returns 4xx
	Given for the submission dialogue the '<field>' is '<value>'
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'godzillaTesting' string enhanced
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ErrorCode>'

	Examples:
		| field          | rule                  | value | ErrorCode           | IsDraftFlag |
		| SubmissionReference | No whitespace         | t t f | ContainsWhitespace  | True        |
		| SubmissionReference | No whitespace         | t t f | ContainsWhitespace  | False       |
		| SubmissionReference | No special characters | t!@st | ContainsSpecialChar | True        |
		| SubmissionReference | No special characters | t!@st | ContainsSpecialChar | False        |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 21 String character rules for Non Quote (SubmissionReference) - POST with SubmissionReference violating the string rules, returns 4xx
	Given for the submission dialogue the '<field>' is '<value>'
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request adding the field
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ErrorCode>'

	Examples:
		| field          | rule                  | value | ErrorCode           | IsDraftFlag |
		| SubmissionReference | No whitespace         | t t f | ContainsWhitespace  | True        |
		| SubmissionReference | No whitespace         | t t f | ContainsWhitespace  | False       |
		| SubmissionReference | No special characters | t!@st | ContainsSpecialChar | True        |
		| SubmissionReference | No special characters | t!@st | ContainsSpecialChar | False        |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 22 String character rules for Quote (UnderwriterQuoteReference) - POST with UnderwriterQuoteReference violating the string rules, returns 4xx
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'c o n t a i n s w h i t espace' string enhanced
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ContainsWhitespace'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

	#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 23 String character rules for Non Quote (UnderwriterQuoteReference) - POST with UnderwriterQuoteReference violating the string rules, returns 4xx
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'c o n t a i n s w h i t espace' string enhanced
	And for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'RFI'
	And for the submission dialogue the 'SenderNote' is 'test whitespace'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ContainsWhitespace'

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 24 POST with UnderwriterQuoteValidUntilDate not having the correct valid format YYYY-MM-DD, returns 4xx
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'testUnderwriterQuoteReference' string enhanced
	And for the submission dialogue the 'IsDraftFlag' is 'False'
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	Given the submission dialogue is saved for the scenario - updating json request value of 'underwriterQuoteValidUntilDate' with '<UnderwriterQuoteValidUntilDateValue>'
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range

Examples:
| UnderwriterQuoteValidUntilDateValue |
| 10-01-2021T14:56:08.981139+00:00    |
| 01-01-2021T14:56:08.981139+00:00    |
| 10-01-01T14:56:08.981139+00:00      |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 25 [SubmissionDialogue_POST_Underwriter_UnderwriterQuoteValidUntilDateInThePast] POST for Quote with UnderwriterQuoteValidUntilDate that is in the past, returns 400
	Given for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'godzillaTesting' string enhanced
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of yesterday
	And for the submission dialogue the 'SenderActionCode' is 'QUOTE'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                            |
	| UnderwriterQuoteValidUntilDateInThePast |
	| DateInThePast                           |

	Examples:
	| IsDraftFlag |
	| True        |
	| False       |




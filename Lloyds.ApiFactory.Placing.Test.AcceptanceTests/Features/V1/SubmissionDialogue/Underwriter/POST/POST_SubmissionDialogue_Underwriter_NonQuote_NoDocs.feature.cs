// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.SubmissionDialogue.Underwriter.POST
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("POST_SubmissionDialogue_Underwriter_NonQuote_NoDocs")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class POST_SubmissionDialogue_Underwriter_NonQuote_NoDocsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "POST_SubmissionDialogue_Underwriter_NonQuote_NoDocs.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "POST_SubmissionDialogue_Underwriter_NonQuote_NoDocs", "\tIn order to reply to a submission dialogue back FOR EOI,RFI,DECL\r\n\tthat I have b" +
                    "een assigned to\r\n\tAs an underwriter \r\n\tI want to be able to do a POST to Submiss" +
                    "ionDialogue", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 12
#line hidden
#line 13
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 14
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 15
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 16
 testRunner.And("store the broker", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table214 = new TechTalk.SpecFlow.Table(new string[] {
                        "SubmissionVersionNumber",
                        "JsonPayload",
                        "FileName",
                        "FileMimeType",
                        "DocumentType",
                        "File",
                        "DocumentDescription"});
            table214.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "MRC_Placingslip.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "document_placing_slip",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Placing Slip test"});
            table214.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "advice_premium",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Client Corrspondence test"});
            table214.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.txt",
                        "text/plain",
                        "document_file_note",
                        "Data\\mrc_documents\\sample_mrc.txt",
                        "additional non placing test"});
#line 17
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                    "soft.com\' with the following submission documents", ((string)(null)), table214, "And ");
#line hidden
#line 22
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table215 = new TechTalk.SpecFlow.Table(new string[] {
                        "UnderwriterUserEmailAddress",
                        "CommunicationsMethodCode"});
            table215.AddRow(new string[] {
                        "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                        "PLATFORM"});
#line 23
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                    "n posted", ((string)(null)), table215, "And ");
#line hidden
#line 26
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 27
 testRunner.And("I make a GET request to \'/SubmissionUnderwriters/{SubmissionUnderwriterId}\' resou" +
                    "rce", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 28
 testRunner.And("the response status code should be \"200\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table216 = new TechTalk.SpecFlow.Table(new string[] {
                        "FileName"});
            table216.AddRow(new string[] {
                        "MRC_Placingslip.docx"});
            table216.AddRow(new string[] {
                        "sample_mrc.docx"});
#line 29
 testRunner.And("I set the SubmissionDialogue context with the following documents for broker", ((string)(null)), table216, "And ");
#line hidden
            TechTalk.SpecFlow.Table table217 = new TechTalk.SpecFlow.Table(new string[] {
                        "IsDraftFlag",
                        "SenderActionCode",
                        "SenderNote"});
            table217.AddRow(new string[] {
                        "False",
                        "RFQ",
                        "a note from the broker"});
#line 33
 testRunner.And("POST as a broker to SubmissionDialogues with following info", ((string)(null)), table217, "And ");
#line hidden
#line 37
 testRunner.And("I clear down test context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 38
 testRunner.And("I refresh the SubmissionDialogeContext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 39
 testRunner.And("I set the SubmissionDialogueId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 40
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 41
 testRunner.And("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 42
 testRunner.And("store the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 43
 testRunner.And("I set the SubmissionDialogue context with the submission for underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 POST for a NON DRAFT EOI/RFI/REJECT, returns 201")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("EOI", null)]
        [NUnit.Framework.TestCaseAttribute("RFI", null)]
        [NUnit.Framework.TestCaseAttribute("DECL", null)]
        public virtual void _01POSTForANONDRAFTEOIRFIREJECTReturns201(string senderActionCode, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 POST for a NON DRAFT EOI/RFI/REJECT, returns 201", null, @__tags);
#line 50
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 12
this.FeatureBackground();
#line hidden
#line 51
 testRunner.Given("for the submission dialogue the \'IsDraftFlag\' is \'False\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 52
 testRunner.And(string.Format("for the submission dialogue the \'SenderActionCode\' is \'{0}\'", senderActionCode), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 53
 testRunner.And("for the submission dialogue the \'UnderwriterQuoteReference\' is \'\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 54
 testRunner.And("for the submission dialogue the \'UnderwriterQuoteValidUntilDate\' is \'\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 55
 testRunner.And("for the submission dialogue the \'SenderNote\' is \'test message back from the SCENA" +
                        "RIO 1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 56
 testRunner.And("add the submission dialogue request adding the field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 57
 testRunner.And("the current submission dialogue is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 58
 testRunner.When("I POST to \'/SubmissionDialogues\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 59
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 60
 testRunner.And("I store the submission dialogue response in the context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 63
 testRunner.And("validate submission dialogue context as an underwriter matching the submission di" +
                        "alogue posted by underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 64
 testRunner.And("validate submission dialogue SenderUserEmailAddress with the requester", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 65
 testRunner.And("validate submission dialogue result contain item added on \'StatusCode\' with a val" +
                        "ue \'DELV\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 68
 testRunner.And("validate new submission dialogue created", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 69
 testRunner.And("get the submission dialogues as a underwriter posted by the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 70
 testRunner.And("validate the submission dialogues are 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 71
 testRunner.And("validate submission dialogue context as an underwriter matching the submission di" +
                        "alogue posted by underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 72
 testRunner.And("validate submission dialogue result contain item added on \'StatusCode\' with a val" +
                        "ue \'DELV\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 75
 testRunner.And("validate SubmissionUnderwriterStatusCode is updated", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 76
 testRunner.And("validate Submission Status is updated", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 79
 testRunner.And("get the submission dialogues as a broker posted by the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 80
 testRunner.And("validate the submission dialogues are 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 81
 testRunner.And("validate submission dialogue context with the submission dialogue posted by under" +
                        "writer", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 82
 testRunner.And("validate submission dialogue context as an broker matching the submission dialogu" +
                        "e posted by underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("02 POST for a DRAFT EOI/RFI/REJECT, returns 201")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("EOI", null)]
        [NUnit.Framework.TestCaseAttribute("RFI", null)]
        [NUnit.Framework.TestCaseAttribute("DECL", null)]
        public virtual void _02POSTForADRAFTEOIRFIREJECTReturns201(string senderActionCode, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 POST for a DRAFT EOI/RFI/REJECT, returns 201", null, @__tags);
#line 95
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 12
this.FeatureBackground();
#line hidden
#line 96
 testRunner.Given("for the submission dialogue the \'IsDraftFlag\' is \'True\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 97
 testRunner.And(string.Format("for the submission dialogue the \'SenderActionCode\' is \'{0}\'", senderActionCode), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 98
 testRunner.And("for the submission dialogue the \'UnderwriterQuoteReference\' is \'\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 99
 testRunner.And("for the submission dialogue the \'UnderwriterQuoteValidUntilDate\' is \'\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 100
 testRunner.And("for the submission dialogue the \'SenderNote\' is \'test message back from the SCENA" +
                        "RIO 2\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 101
 testRunner.And("add the submission dialogue request adding the field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 102
 testRunner.And("the current submission dialogue is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 103
 testRunner.When("I POST to \'/SubmissionDialogues\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 104
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 105
 testRunner.And("I store the submission dialogue response in the context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 108
 testRunner.And("validate submission dialogue context as an underwriter matching the submission di" +
                        "alogue posted by underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 109
 testRunner.And("validate submission dialogue SenderUserEmailAddress with the requester", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 110
 testRunner.And("validate submission dialogue result contain item added on \'StatusCode\' with a val" +
                        "ue \'DRFT\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 113
 testRunner.And("validate new submission dialogue created", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 114
 testRunner.And("get the submission dialogues as a underwriter posted by the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 115
 testRunner.And("validate the submission dialogues are 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 116
 testRunner.And("validate submission dialogue context as an underwriter matching the submission di" +
                        "alogue posted by underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 117
 testRunner.And("validate submission dialogue result contain item added on \'StatusCode\' with a val" +
                        "ue \'DRFT\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 120
 testRunner.And("validate SubmissionUnderwriterStatusCode is updated", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 121
 testRunner.And("validate Submission Status is updated", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 124
 testRunner.And("get the submission dialogues as a broker posted by the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 125
 testRunner.And("validate the submission dialogues are 0", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

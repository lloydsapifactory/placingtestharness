// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.SubmissionDialogue.Underwriter.POST
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("POST_SubmissionDialogue_Underwriter_POSTSubmissionDocuments")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class POST_SubmissionDialogue_Underwriter_POSTSubmissionDocumentsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "POST_SubmissionDialogue_Underwriter_POSTQuoteDocuments.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "POST_SubmissionDialogue_Underwriter_POSTSubmissionDocuments", "\tIn order to reply to a submission dialogue back \r\n\tAnd add or replace the existi" +
                    "ng docs\r\n\tAs an underwriter \r\n\tI want to be able to do a POST to SubmissionDialo" +
                    "gue", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 15
#line hidden
#line 16
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 17
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 19
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table103 = new TechTalk.SpecFlow.Table(new string[] {
                        "SubmissionVersionNumber",
                        "JsonPayload",
                        "FileName",
                        "FileMimeType",
                        "DocumentType",
                        "File",
                        "DocumentDescription",
                        "ShareableToAllMarketsFlag"});
            table103.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "MRC_Placingslip.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "document_placing_slip",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Placing Slip test",
                        "F"});
            table103.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "advice_premium",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Client Corrspondence test",
                        "F"});
            table103.AddRow(new string[] {
                        "1",
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.txt",
                        "text/plain",
                        "document_file_note",
                        "Data\\mrc_documents\\sample_mrc.txt",
                        "additional non placing test",
                        "F"});
#line 20
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                    "soft.com\' with the following submission documents", ((string)(null)), table103, "And ");
#line hidden
#line 25
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table104 = new TechTalk.SpecFlow.Table(new string[] {
                        "UnderwriterUserEmailAddress",
                        "CommunicationsMethodCode"});
            table104.AddRow(new string[] {
                        "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                        "PLATFORM"});
#line 26
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                    "n posted", ((string)(null)), table104, "And ");
#line hidden
#line 29
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 30
 testRunner.And("I make a GET request to \'/SubmissionUnderwriters/{SubmissionUnderwriterId}\' resou" +
                    "rce", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 31
 testRunner.And("the response status code should be \"200\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table105 = new TechTalk.SpecFlow.Table(new string[] {
                        "FileName"});
            table105.AddRow(new string[] {
                        "MRC_Placingslip.docx"});
            table105.AddRow(new string[] {
                        "sample_mrc.docx"});
#line 32
 testRunner.And("I set the SubmissionDialogue context with the following documents for broker", ((string)(null)), table105, "And ");
#line hidden
#line 36
 testRunner.And("for the submission dialogue the \'SenderActionCode\' is \'RFQ\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 37
 testRunner.And("for the submission dialogue the \'SenderNote\' is \'return test to sender\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 38
 testRunner.And("add the submission dialogue request", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 39
 testRunner.And("the current submission dialogue is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 40
 testRunner.And("I POST to \'/SubmissionDialogues\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 41
 testRunner.And("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 43
 testRunner.And("I clear down test context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 44
 testRunner.And("I refresh the SubmissionDialogeContext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 45
 testRunner.And("I set the SubmissionDialogueId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 46
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 47
 testRunner.And("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 48
 testRunner.And("store the underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedMRCSubmissionDocu" +
            "ment] - POST Submission Document Underwriter attempts to upload proposed MRC sub" +
            "mission document, returns 201.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.CategoryAttribute("BackendNotImplemented-Factory")]
        public virtual void _01SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedMRCSubmissionDocument_POSTSubmissionDocumentUnderwriterAttemptsToUploadProposedMRCSubmissionDocumentReturns201_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI",
                    "BackendNotImplemented-Factory"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedMRCSubmissionDocu" +
                    "ment] - POST Submission Document Underwriter attempts to upload proposed MRC sub" +
                    "mission document, returns 201.", null, new string[] {
                        "CoreAPI",
                        "BackendNotImplemented-Factory"});
#line 53
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 15
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table106 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "ReplaceWith",
                            "File",
                            "FileMimeType",
                            "ShareableToAllMarketsFlag"});
                table106.AddRow(new string[] {
                            "MRC_Placingslip.docx",
                            "sample_mrc_new.docx",
                            "Data\\mrc_documents\\sample_mrc_new.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "F"});
#line 54
 testRunner.When("replace the SubmissionDialogue context with the following documents for underwrit" +
                        "er", ((string)(null)), table106, "When ");
#line hidden
#line 57
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 58
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 59
 testRunner.And("the document \'sample_mrc_new.docx\' should contain a valid ReplacingDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 60
 testRunner.And("the document \'sample_mrc_new.docx\' should replace \'MRC_Placingslip.docx\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("02 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedSupportingDocumen" +
            "t] - POST Submission Document Underwriter attempts to upload supporting submissi" +
            "on document, returns 201.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.CategoryAttribute("BackendNotImplemented-Factory")]
        public virtual void _02SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedSupportingDocument_POSTSubmissionDocumentUnderwriterAttemptsToUploadSupportingSubmissionDocumentReturns201_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI",
                    "BackendNotImplemented-Factory"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadProposedSupportingDocumen" +
                    "t] - POST Submission Document Underwriter attempts to upload supporting submissi" +
                    "on document, returns 201.", null, new string[] {
                        "CoreAPI",
                        "BackendNotImplemented-Factory"});
#line 64
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 15
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table107 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "ReplaceWith",
                            "File",
                            "FileMimeType",
                            "ShareableToAllMarketsFlag"});
                table107.AddRow(new string[] {
                            "sample_mrc.docx",
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "F"});
#line 65
 testRunner.When("replace the SubmissionDialogue context with the following documents for underwrit" +
                        "er", ((string)(null)), table107, "When ");
#line hidden
#line 68
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 69
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 70
 testRunner.And("the document \'PremiumAdvise_2.docx\' should contain a valid ReplacingDocumentId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 71
 testRunner.And("the document \'PremiumAdvise_2.docx\' should replace \'sample_mrc.docx\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("04 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewSupportingDocument] - " +
            "POST Submission Document Underwriter attempts to upload a new supporting submiss" +
            "ion document, returns 201.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.CategoryAttribute("BackendNotImplemented-Factory")]
        public virtual void _04SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewSupportingDocument_POSTSubmissionDocumentUnderwriterAttemptsToUploadANewSupportingSubmissionDocumentReturns201_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI",
                    "BackendNotImplemented-Factory"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("04 [SubmissionDocument_POST_MULTIPART_Underwriter_UploadNewSupportingDocument] - " +
                    "POST Submission Document Underwriter attempts to upload a new supporting submiss" +
                    "ion document, returns 201.", null, new string[] {
                        "CoreAPI",
                        "BackendNotImplemented-Factory"});
#line 74
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 15
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table108 = new TechTalk.SpecFlow.Table(new string[] {
                            "FileName",
                            "File",
                            "FileMimeType",
                            "DocumentType",
                            "DocumentDescription",
                            "JSONPayload",
                            "ShareableToAllMarketsFlag"});
                table108.AddRow(new string[] {
                            "PremiumAdvise_2.docx",
                            "Data\\supporting_documents\\PremiumAdvise_2.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Premium Advise",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "F"});
#line 75
 testRunner.When("Add the SubmissionDialogue context with the following documents for underwriter", ((string)(null)), table108, "When ");
#line hidden
#line 78
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 79
 testRunner.And("I save the submission document response to the submissiondocumentcontext", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

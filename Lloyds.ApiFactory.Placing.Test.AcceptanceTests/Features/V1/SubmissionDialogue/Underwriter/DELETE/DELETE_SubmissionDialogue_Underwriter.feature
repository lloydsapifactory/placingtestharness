#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: DELETE_SubmissionDialogue_Underwriter
	In order to check that I cannot delete a submission dialogue
	As an underwriter 
	I want to be able to do a DELETE to SubmissionDialogue
	And receive an error


@CoreAPI 
Scenario: 01 [SubmissionDialogue_DELETE_Underwriter_UnderwriterNotAllowed] For a valid SubmissionDialogueId that has been created by an authorised broker, when I do a DELETE as an authorised underwriter, returns 4xx
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context for underwriter

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogueId
	When  I make a DELETE request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages

	| ErrorMessage         |
	| [MethodNotAvailable] |
	| API operations not found |

	#verify QMD has not been deleted
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	And the response status code should be "200" 
	And submission dialogue context should have at least 1 link



#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETById_SubmissionDialogue_Underwriter_Negative
	In order to get a submission dialogue 
	that a broker has assigned to me
	As an authorised underwriter 
	I want to be able to do a GETById to SubissionDialogues


Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDialogue_GET_Underwriter_SubmissionDialogueNotFound] GetById to a submission dialogue that does not exists, returns 404
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/SubmissionDialogues/666666' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_GET_Underwriter_UnderwriterNotAuthorised] GETById from an UNAUTHORISED underwriter, returns 400/403
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	When I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage			|
 	| AccessToReferencedResourceNotAllowed	|
 	| [SubmissionUnderwriterDoesNotExist] |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 03 ([InvalidFilterCriterion] filtering) - parameter=value
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	And I add a filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |
		| UnderwriterQuoteReference |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 ([InvalidFilterCriterion] match filter) - parameter=match(value)
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	And I add a 'match' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| UnderwriterQuoteReference |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 ([InvalidFilterCriterion] contains filter) - parameter=contains(value)
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	And I add a 'contains' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 06 ([InvalidFilterCriterion] prefix filter) - parameter=prefix(value)
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a 'prefix' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07 ([InvalidFilterCriterion] suffix filter) - parameter=suffix(value)
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	And I add a 'suffix' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |


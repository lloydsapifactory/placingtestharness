#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETById_SubmissionDialogue_Underwriter
	In order to get a submission dialogue 
	that a broker has assigned to me
	As an authorised underwriter 
	I want to be able to do a GETById to SubissionDialogues

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	#POST QMD TO AMLIN-LEE WITH 2 submission documents
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker | 
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 1 GETById submission dialogue that the Broker has assigned to, returns 200
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	And I reset all the filters
	
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And submission dialogue context should have at least 1 link
	And submission dialogue context should have a link with proper value
	And validate mandatory fields for the context of submission dialogue
	And submission dialogue context belong to amlinunderwriter.lee@limossdidev.onmicrosoft.com
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by broker
	And validate submission documents attached are the following
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 2 GetById returns MANDATORY fields
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	And I reset all the filters
	
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And validate submission dialogue context contain the following fields
		| field                   |
		| SubmissionDialogueId    |
		| SubmissionReference     |
		| SubmissionVersionNumber |
		| SubmissionUnderwriterId |
		| SenderParticipantCode   |
		| SenderUserEmailAddress  |
		| SenderUserFullName      |
		| StatusCode              |
		| SenderActionCode        |
		| CreatedDateTime         |
		| SentDateTime            |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 3 GetById validate that includes the correct submission documents
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	And I reset all the filters
	
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	And the response status code should be "200" 
	And I store the submission dialogue response in the context
	Then validate submission documents attached are the following
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 4 Broker does a POST to two different underwriters with different submission documents, each underwriter can do a GETById and retrieve their submission dialogues that have been assigned to
	#POST QMD TO BEAZLEY-TAYLOR
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                           | CommunicationsMethodCode |
		| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.txt       |
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker | 
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I reset all the filters

	#VALIDATE AMLIN-LEE
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And validate submission documents attached are the following
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |

	#VALIDATE BEAZLEY-TAYLOR
	Given I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And validate submission documents attached are the following
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.txt       |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 5 GetById check that the Id has the correct structure SubmissionReference_SubmissionVersionNumber_SubmissionDialogueId
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	And I reset all the filters
	
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	And the response status code should be "200" 
	And I store the submission dialogue response in the context
	Then for the submission dialogue and the returned result the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_SubmissionDialogueId' 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 6 GETById from another underwriter in the same organisation, returns 200
	Given I log in as an underwriter 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)

	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And store the submission dialogue id by submission reference related
	And I reset all the filters
	When I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And submission dialogue context belong to amlinunderwriter.sidney@limossdidev.onmicrosoft.com


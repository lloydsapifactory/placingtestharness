#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDialogue_Underwriter_WithDocs_Negative
	In order to check that I CANNOT update a submission dialogue
	As an authorised underwriter 
	I want to be able to do a PUT to SubmissionDialogue
	And return client error

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName             |
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith     | File                               | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx| Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 [SubmissionDialogue_PUT_Underwriter_SenderDocumentsNotExpected] PUT with SenderDocumentIds for EOI/RFI/DECL, returns 400 (SubmissionDialogue_PUT_Underwriter_SenderDocumentsNotExpected)
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode   | SenderNote                             |
	| True        |                           | <SenderActionCode> | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters
	
	And I set the SubmissionDialogue context with the submission for underwriter
	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith         | File                                   | FileMimeType                                                            |
	| sample_mrc.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection

	And for the submission dialogue the 'SenderNote' is 'test sender note'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'SenderDocumentIdsNotExpected'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_PUT_Underwriter_DocumentsLinkedUsedAlready] PUT with document Ids that have been referenced in other QMDs, returns 400 
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith          | File                                           | FileMimeType                                                            |
	| sample_mrc.docx | PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |z

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters	

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith     | File                               | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc.docx | Data\mrc_documents\sample_mrc.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True       | godzillaTesting           | QUOTE            | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters

	And I set the SubmissionDialogue context with the submission for underwriter
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a month from now
	And for the submission dialogue the 'SenderNote' is 'test sender note updated'

	Given attach non placing slip document from the current sender documents as underwriter
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage              |
	| DocumentLinkedUsedAlready |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionDialogue_PUT_Underwriter_DocumentsLinkedMRCRepeated] PUT with more than one placing slip referenced, returns 400 
	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith     | File                               | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc.docx | Data\mrc_documents\sample_mrc.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True       | godzillaTesting           | QUOTE            | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 
	And I reset all the filters

	And I set the SubmissionDialogue context with the submission for underwriter
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a month from now
	And for the submission dialogue the 'SenderNote' is 'test sender note updated'
	
	Given attach placing slip document from the current sender documents as underwriter
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage              |
	| DocumentLinkedUsedAlready |
	| DocumentLinkedMRCRepeated |


	
#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
#THIS IS A COMPLICATED SCENARIO AND NEEDS A BIT OF REWORK
Scenario: 04 [SubmissionDialogue_PUT_Underwriter_DocumentsNoLinkedSubmissionUnderwriter] PUT with a submission document that is no linked to the referenced submission underwriter, returns 400 
	And replace the SubmissionDialogue context with the following documents for underwriter
	| FileName        | ReplaceWith          | File                                           | FileMimeType                                                            |
	| sample_mrc.docx | PremiumAdvise_2.docx | Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |z

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| True       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource
	And store the LastModifiedDateTime from the response
	And I clear down test context 
	And I reset all the filters	

	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'updateUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a month from now
	And for the submission dialogue the 'SenderNote' is 'updating SenderNote for PUT Scenario1'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header using LastModifiedDate
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                              |
	| DocumentsNotLinkedToSubmissionUnderwriter |

	#AYTO EPISTREFEI REPLACINGDOCUMENTDOESNOTEXIST!
	#EBIX: REPLACINGDOCUMENTNOTSUPPLIEDBYTHEBROKER
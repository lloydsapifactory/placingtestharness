#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDialogue_Underwriter_NonQuote_NoDocs
	In order to check that I CAN update a submission dialogue from a non Submission (EOI,RFI,DECL)
	to Submission, EOI, RFI, DECL
	As an authorised underwriter 
	I want to be able to do a PUT to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	
	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True        |                           | EOI              | test message back from the underwriter |
	And add the submission dialogue request adding the field
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 

#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsNonDraftEOI
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsDraftEOI
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsNonDraftRFI
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsDraftRFI
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsNonDraftReject
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsDraftReject
@CoreAPI 
Scenario Outline: 01 PUT to NON DRAFT EOI/RFI/DECL updating ISDraft, SenderNote
	Given for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderNote' is 'updating SenderNote for PUT Scenario2'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

	# UW VALIDATION
	And I reset all the filters
	And I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	And the response status code should be "200" 
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 1
	And store the submission dialogue context from the submission dialogues
	And validate submission dialogue context as an underwriter matching the submission dialogue updated by underwriter
	And validate submission dialogue result contain the value of SenderNote
	And validate submission dialogue result contain item added on 'StatusCode' with a value '<StatusCode>'

	And I reset all the filters

	# BROKER VALIDATION 
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are <QMDs>

	Examples:
		| SenderActionCode | IsDraftFlag | QMDs | StatusCode |
		| EOI              | True        | 0    | DRFT       |
		| RFI              | True        | 0    | DRFT       |
		| DECL             | True        | 0    | DRFT       |
		| EOI              | False       | 1    | DELV       |
		| RFI              | False       | 1    | DELV       |
		| DECL             | False       | 1    | DELV       |



#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDialogue_Underwriter_Negative
	In order to check that I CANNOT update a submission dialogue
	As an authorised underwriter 
	I want to be able to do a PUT to SubmissionDialogue
	And return client error

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
#---------------------------------------------------------------------------
	#PICKS UP A RANDOM QMD FROM THE U/W BROKER BEING IN QUOTE STATUS>AMLINLEE --> REQUIRES THAT THERE ARE QMDs FOR AMLIN LEE
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SenderActionCode' with value equals 'QUOTE'
	And I add a '' filter on 'IsDraftFlag' with value equals 'true'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection
	And store the CreatedDateTime from submission dialogue context
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogue context for underwriter
	And I reset all the filters

#---------------------------------------------------------------------------
@CoreAPI
#This is not really testable as it will fail on the Id-Payload request validation
Scenario: 01 [SubmissionDialogue_PUT_Underwriter_ResourceKeysMismatch] PUT with request id different than the payload request id, returns 400
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario1'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I pick another random submission dialogue Id
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	#And the response contains a validation error 'ResourceKeysDoNotMatch'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_PUT_Underwriter_SubmissionDialogueNotDraft] PUT when SubmissionDialogue is not in Draft, returns 400
	#FIND ONE SubmissionDialogue that is not IN DRAFT FIRST
	Given I add a 'match' filter on 'SenderActionCode' with value equals 'QUOTE'
	And I add a '' filter on 'IsDraftFlag' with value equals 'False'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection
	And store the CreatedDateTime from submission dialogue context

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogue context for underwriter
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario1'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'SubmissionDialogueNotInDraft'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDialogue_PUT_Underwriter_UnderwriterNotAuthorisedForSubmissionUnderwriter] PUT with an UNAUTHORISED Underwriter, returns 400/403
	And store the CreatedDateTime from submission dialogue context
	Given I log in as an underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessNotAllowed                     |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 [SubmissionDialogue_PUT_Underwriter_InvalidActionCode] PUT with an invalid SenderActionCode, returns 400
	Given for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 11'
	And add the submission dialogue request
    And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'InvalidUnderwriterActionCode'

		Examples:
		| SenderActionCode |
		| CLSDB            |
		| CLSDU            |
		| DECNT            |
		| QNTU             |
		| RFQ              |
		| WTHDR            |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 PUT with an empty IsDraftFlag, returns 400
	Given for the submission dialogue the 'IsDraftFlag' is ''
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request adding the field
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 PUT without IsDraftFlag, returns 400
	Given for the submission dialogue the 'IsDraftFlag' is 'null'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 07 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteReferenceExpected] PUT with an empty UnderwriterQuoteReference, returns 400
	Given for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request adding the field
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteReferenceExpected'



#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 08 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteReferenceExpected] PUT without UnderwriterQuoteReference, returns 400
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'null'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteReferenceExpected'

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteValidDateExpected] PUT with an empty UnderwriterQuoteValidUntilDate, returns 400
	Given for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request adding the field
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateExpected'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 10 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteValidDateExpected] PUT without UnderwriterQuoteValidUntilDate, returns 400
	Given for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'PUTQMDTesting'
	And for the submission dialogue the 'SenderNote' is 'PUT from Scenario4'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateExpected'

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 [SubmissionDialogue_PUT_Underwriter_DocumentsNoLinkedSubmissionDocument] PUT with a submission document that does not exist, returns 400 
	Given a supporting document referenced does not exist for the related SubmissionDocument
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 14'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'DocumentDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 12 [SubmissionDialogue_PUT_Underwriter_SenderNoteExpected] PUT with an empty a SenderNote, returns 400 
	Given I add a 'match' filter on 'SenderActionCode' with value equals 'EOI'
	And I add a '' filter on 'IsDraftFlag' with value equals 'true'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogue context for underwriter
	And I reset all the filters

	Given for the submission dialogue the 'SenderNote' is ''
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And add the submission dialogue request adding the field
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'SenderNoteMandatory'

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 13 PUT without a SenderNote, returns 400 
	Given I add a 'match' filter on 'SenderActionCode' with value equals 'EOI'
	And I add a '' filter on 'IsDraftFlag' with value equals 'true'
	And I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And get random submission dialogue from the submission dialogue collection
	And store the CreatedDateTime from submission dialogue context

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogue context for underwriter
	And I reset all the filters

	Given for the submission dialogue the 'SenderNote' is ''
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'SenderNoteMandatory'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 14 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteReferenceNotExpected] PUT with UnderwriterQuoteReference for a EOI,RFI, DECL SenderActionCode, returns 400 
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I set the SubmissionDialogue context for underwriter
	Given for the submission dialogue the 'UnderwriterQuoteReference' is 'TestUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 18'
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteReferenceNotExpected'

		Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteValidUntilDateInThePast] PUT with UnderwriterQuoteValidUntilDate that is in the past, returns 400 
	Given for the submission dialogue I set 'UnderwriterQuoteValidUntilDate' with datetime value of yesterday
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'godzillaTesting' string enhanced
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'DateInThePast'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteValidDateNotExpected] PUT with UnderwriterQuoteValidUntilDate when not allowed, returns 400 (SubmissionDialogue_PUT_Underwriter_UnderwriterQuoteValidDateNotExpected)
	Given for the submission dialogue I set 'UnderwriterQuoteValidUntilDate' with datetime value of today
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'godzillaTesting' string enhanced
	And for the submission dialogue the 'SenderNote' is 'test message back from the SCENARIO 1' string enhanced
	And for the submission dialogue the 'SenderActionCode' is '<SenderActionCode>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And add the submission dialogue request
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateNotExpected'
	Examples:
		| SenderActionCode | 
		| EOI              | 
		| RFI              | 
		| DECL             | 


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 17 [SubmissionDialogue_PUT_Underwriter_MissingHTTPRequestHeader]
	Given for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderNote' is 'updating SenderNote for PUT Scenario2'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'MissingRequiredHTTPHeader'

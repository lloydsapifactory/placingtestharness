#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: PUT_SubmissionDialogue_Underwriter_WithSubmissionDocuments
	In order to reply to a submission dialogue back 
	And add or replace the existing docs
	As an underwriter 
	I want to be able to do a perform a PUT on the submission documents

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And for the submission dialogue the 'SenderActionCode' is 'RFQ'
	And for the submission dialogue the 'SenderNote' is 'return test to sender'   
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And the response status code should be "201"

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory @BackendNotImplemented-Ebix
Scenario: 01 [SubmissionDocument_PUT_Underwriter_UpdateReplacingMRCSubmissionDocument] - PUT Submission Document Underwriter attempts to edit the uploaded proposed MRC submission document, returns 200.
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith         | File                                   | FileMimeType                                                            |
	| MRC_Placingslip.docx | sample_mrc_new.docx | Data\mrc_documents\sample_mrc_new.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document |
	And the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And for the submission documents the 'DocumentType' is 'document_placing_slip'
	And for the submission documents the 'DocumentDescription' is 'MRC Document v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'True'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And the document 'sample_mrc_new.docx' should contain a valid ReplacingDocumentId
	And the document 'sample_mrc_new.docx' should replace 'MRC_Placingslip.docx'

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Factory @BackendNotImplemented-Ebix
Scenario: 02 PUT Submission Document Underwriter attempts edit the uploaded new supporting submission document, returns 201.
	#SubmissionDocument_PUT_Underwriter_UpdateNewSupportingDocument
	Given Add the SubmissionDialogue context with the following documents for underwriter
	| FileName				| File											 | FileMimeType					| DocumentType   | DocumentDescription   | JSONPayload                         | ShareableToAllMarketsFlag |
	| PremiumAdvise_2.docx	| Data\supporting_documents\PremiumAdvise_2.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Premium Advise | Data\1_2_Model\SubmissionDocument_1.json | Y  |
	And the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And for the submission documents the 'DocumentType' is 'advice_premium'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'False'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And the document 'PremiumAdvise_2.docx' should contain a valid ReplacingDocumentId
	And the document 'PremiumAdvise_2.docx' should replace 'sample_mrc.docx'


@CoreAPI @BackendNotImplemented-Factory @BackendNotImplemented-Ebix
Scenario: 03 PUT Submission Document Underwriter attempts edit the uploaded proposed supporting submission document, returns 201.
	#SubmissionDocument_PUT_Underwriter_UpdateReplacingSupportingDocument
	Given replace the SubmissionDialogue context with the following documents for underwriter
	| FileName             | ReplaceWith			| File												| FileMimeType																|
	| sample_mrc.docx	   | PremiumAdvise_2.docx	| Data\supporting_documents\PremiumAdvise_2.docx	| application/vnd.openxmlformats-officedocument.wordprocessingml.document	|
	And the response status code should be "201"
	And I save the submission document response to the submissiondocumentcontext 
	And I set the SubmissionDocumentId
	And I make a GET request to '/SubmissionDocuments/{SubmissionDocumentId}' resource 
	And the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And for the submission documents the 'DocumentType' is 'advice_premium'
	And for the submission documents the 'DocumentDescription' is 'Support Premium Advise Document v2 -UW'
	And for the submission documents the 'ShareableToAllMarketsFlag' is 'False'
	And I save SubmissionDocument update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "200"
	And I save the submission document response to the submissiondocumentcontext 
	And the document 'PremiumAdvise_2.docx' should contain a valid ReplacingDocumentId
	And the document 'PremiumAdvise_2.docx' should replace 'sample_mrc.docx'

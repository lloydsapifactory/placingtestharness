#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDialogue_Underwriter_Submission_NoDocs
	In order to check that I CAN update a submission dialogue from Submission
	to Submission, EOI, RFI, DECL
	As an authorised underwriter 
	I want to be able to do a PUT to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And I set the SubmissionDialogue context with the submission for underwriter

	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True        |           godzillaTesting                | QUOTE              | test message back from the underwriter |
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context 

#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsNonDraftQuoteNoDocs
#SubmissionDialogue_PUT_Underwriter_UpdateUnderwriterQuoteResponseAsDraftQuoteNoDocs
@CoreAPI 
Scenario Outline: 01 PUT to QUOTE updating IsDraft, UnderwriterQuoteReference, UnderwriterQuoteValidUntilDate, SenderNote
	Given for the submission dialogue the 'IsDraftFlag' is '<IsDraftFlag>'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'updateUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a month from now
	And for the submission dialogue the 'SenderNote' is 'updating SenderNote for PUT Scenario1'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

	# UW VALIDATION
	And I reset all the filters
	And I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	And get the submission dialogues as a underwriter posted by the underwriter
	And validate the submission dialogues are 1
	And store the submission dialogue context from the submission dialogues
	And validate submission dialogue context as an underwriter matching the submission dialogue updated by underwriter
	And validate submission dialogue result contain the value of UnderwriterQuoteReference
	And validate submission dialogue result contain the value of SenderNote
	And validate submission dialogue result contain item added on 'StatusCode' with a value '<StatusCode>'
	And I reset all the filters

	# BROKER VALIDATION 
	And get the submission dialogues as a broker posted by the underwriter
	And validate the submission dialogues are <QMDs>

	Examples:
		| IsDraftFlag | QMDs | StatusCode |
		| True        | 0    | DRFT       |
		| False       | 1    | DELV       | 
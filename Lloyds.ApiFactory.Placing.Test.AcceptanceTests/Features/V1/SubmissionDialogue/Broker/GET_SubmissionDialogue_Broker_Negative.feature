#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionDialogue_Broker_negative
	In order to RETRIEVE all the submission dialogues
	As an authorised broker 
	I want to be able to do a GETALL to SubissionDialogues

Background:
		Given I have placing V1 base Uri
		And I set Accept header equal to application/json type
		Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 01 ([InvalidFilterCriterion] exact match filter) - parameter=value
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |
		| UnderwriterQuoteReference |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionDialogue_GET_Broker_BrokerNotAuthorised] As a broker from another company, returns 400/403
	Given I POST a submission on behalf of the broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	Given I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDialogue_GET_Broker_BrokerNotAuthorised] As a broker from the same company but different department, returns 400/403
	Given I POST a submission on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 [SubmissionDialogue_GET_Broker_SubmissionDialogueNotFound] POST a submission dialogue ID that does not exists, I get a 404
	When I make a GET request to '/SubmissionDialogues/666666' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 ([InvalidFilterCriterion] match filter) - parameter=match(value)
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a 'match' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| UnderwriterQuoteReference |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 06 ([InvalidFilterCriterion] contains filter) - parameter=contains(value)
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a 'contains' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07 ([InvalidFilterCriterion] prefix filter) - parameter=prefix(value)
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a 'prefix' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 08 ([InvalidFilterCriterion] suffix filter) - parameter=suffix(value)
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	
	Given I add a 'suffix' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
		| field                     |
		| SubmissionVersionNumber   |
		| SubmissionReference       |
		| SubmissionUnderwriterId   |
		| SenderParticipantCode     |
		| StatusCode                |
		| SenderActionCode          |
		| SenderUserFullName        |
		| SenderNote                |
		| SenderDocumentIds         |
		| SubmissionDialogueId      |



#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Broker
	In order to create a submission dialogue
	As an authorised broker 
	I want to be able to do a POST to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context for broker

@CoreAPI
Scenario: 01 [SubmissionDialogue_POST_Broker_CreateBrokerQuoteRequest] POST as a Broker, returns 201 and the authorised underwriter can see the posted QMD
		Given I set the SubmissionDialogue context for broker
		When POST as a broker to SubmissionDialogues with following info
		| IsDraftFlag | SenderActionCode | SenderNote             |
		| False       | RFQ              | a note from the broker |
		Then the response status code should be "201"
		And I store the submission dialogue response in the context
		And for the submission dialogue and the returned result the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_SubmissionDialogueId' 
		
		#BROKER VALIDATION
		And get the submission dialogues as a broker posted by the broker
		And validate the submission dialogues are 1
		And store the submission dialogue context from the submission dialogues
		And validate submission dialogue context with the submission dialogue posted by broker
		And validate submission dialogue context as an broker matching the submission dialogue posted by broker
		And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

		#UNDERWRITER VALIDATION
		And store the underwriter as 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
		And get the submission dialogues as a underwriter posted by the broker
		And validate the submission dialogues are 1
		And store the submission dialogue context from the submission dialogues
		And store the submission dialogue context from the submission dialogues
		And validate submission dialogue context as an underwriter matching the submission dialogue posted by broker
		And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 Another authorised broker from the same broker department, with different submission underwriter CAN DO a POST
	Given I log in as a broker 'bellbroker.morgan@limossdidev.onmicrosoft.com'
	#POST A NEW QM
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                         | CommunicationsMethodCode | SubmissionVersionNumber | ContractTypeCode          | ClassOfBusinessCode    |
		| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 | 1                  | direct_insurance_contract | marine_increased_value |
	When POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	Then the response status code should be "201"
	And I store the submission dialogue response in the context

	#BROKER VALIDATION
	And get the submission dialogues as a broker posted by the broker
	And validate the submission dialogues are 1
	And store the submission dialogue context from the submission dialogues
	And validate submission dialogue context with the submission dialogue posted by broker
	And validate submission dialogue context as an broker matching the submission dialogue posted by broker
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'

	#UNDERWRITER VALIDATION
	And store the underwriter as 'beazleyunderwriter.fiona@limossdidev.onmicrosoft.com'
	And get the submission dialogues as a underwriter posted by the broker
	And validate the submission dialogues are 1
	And store the submission dialogue context from the submission dialogues
	And validate submission dialogue context as an underwriter matching the submission dialogue posted by broker
	And validate submission dialogue result contain item added on 'StatusCode' with a value 'DELV'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 03 I can do a POST to two different underwriter organisation with the same Submission
	Given POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                           | CommunicationsMethodCode |
		| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | PLATFORM                 |
	And I reset all the filters	
	When POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	Then the response status code should be "201"


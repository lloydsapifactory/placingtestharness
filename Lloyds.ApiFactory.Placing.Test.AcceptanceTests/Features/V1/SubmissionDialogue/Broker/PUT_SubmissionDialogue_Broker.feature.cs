// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.SubmissionDialogue.Broker
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("PUT_SubmissionDialogue_Broker")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class PUT_SubmissionDialogue_BrokerFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "PUT_SubmissionDialogue_Broker.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "PUT_SubmissionDialogue_Broker", "\tIn order to check that I CANNOT update a submission dialogue\r\n\tAs an authorised " +
                    "broker \r\n\tI want to be able to do a PUT to SubmissionDialogue\r\n\tAnd retrieve an " +
                    "error", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 12
#line hidden
#line 13
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 14
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 15
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table85 = new TechTalk.SpecFlow.Table(new string[] {
                        "JsonPayload",
                        "FileName",
                        "FileMimeType",
                        "DocumentType",
                        "File",
                        "DocumentDescription"});
            table85.AddRow(new string[] {
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "MRC_Placingslip.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "document_placing_slip",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Placing Slip test"});
            table85.AddRow(new string[] {
                        "Data\\1_2_Model\\SubmissionDocument_1.json",
                        "sample_mrc.docx",
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                        "advice_premium",
                        "Data\\mrc_documents\\sample_mrc.docx",
                        "Client Corrspondence test"});
#line 16
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                    "soft.com\' with the following submission documents", ((string)(null)), table85, "And ");
#line hidden
#line 20
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table86 = new TechTalk.SpecFlow.Table(new string[] {
                        "UnderwriterUserEmailAddress",
                        "CommunicationsMethodCode"});
            table86.AddRow(new string[] {
                        "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                        "PLATFORM"});
#line 21
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                    "n posted", ((string)(null)), table86, "And ");
#line hidden
#line 24
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table87 = new TechTalk.SpecFlow.Table(new string[] {
                        "IsDraftFlag",
                        "SenderActionCode",
                        "SenderNote"});
            table87.AddRow(new string[] {
                        "False",
                        "RFQ",
                        "a note from the broker"});
#line 25
 testRunner.And("POST as a broker to SubmissionDialogues with following info", ((string)(null)), table87, "And ");
#line hidden
#line 28
 testRunner.And("I refresh the SubmissionDialogeContext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 29
 testRunner.And("store the CreatedDateTime from submission dialogue context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 30
 testRunner.And("I set the SubmissionDialogue context for underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 31
 testRunner.And("I clear down test context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 [SubmissionDialogue_PUT_Broker_BrokerNotAllowed] For a valid SubmissionDialogu" +
            "eId when I do a PUT, returns 400/403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _01SubmissionDialogue_PUT_Broker_BrokerNotAllowedForAValidSubmissionDialogueIdWhenIDoAPUTReturns400403()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 [SubmissionDialogue_PUT_Broker_BrokerNotAllowed] For a valid SubmissionDialogu" +
                    "eId when I do a PUT, returns 400/403", null, new string[] {
                        "CoreAPI"});
#line 35
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 12
this.FeatureBackground();
#line hidden
#line 36
 testRunner.Given("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 37
 testRunner.And("for the submission dialogue the \'IsDraftFlag\' is \'True\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 38
 testRunner.And("for the submission dialogue the \'UnderwriterQuoteReference\' is \'updateUnderwriter" +
                        "QuoteReference\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 39
 testRunner.And("for the submission dialogue I add a date filter on \'UnderwriterQuoteValidUntilDat" +
                        "e\' with a datetime value of a month from now", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 40
 testRunner.And("for the submission dialogue the \'SenderNote\' is \'updating SenderNote for PUT Scen" +
                        "ario1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 41
 testRunner.And("add the submission dialogue request", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 42
 testRunner.And("the current submission dialogue is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 43
 testRunner.And("I set the SubmissionDialogueId", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 44
 testRunner.When("I PUT to \'/SubmissionDialogues/{SubmissionDialogueId}\' resource with \'If-Unmodifi" +
                        "ed-Since\' header", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 45
 testRunner.Then("the response status code should be \"400\" or \"403\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

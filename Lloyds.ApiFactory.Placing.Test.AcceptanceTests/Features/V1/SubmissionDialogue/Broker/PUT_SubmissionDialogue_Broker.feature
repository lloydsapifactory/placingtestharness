#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionDialogue_Broker
	In order to check that I CANNOT update a submission dialogue
	As an authorised broker 
	I want to be able to do a PUT to SubmissionDialogue
	And retrieve an error

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And store the CreatedDateTime from submission dialogue context
	And I set the SubmissionDialogue context for underwriter
	And I clear down test context


@CoreAPI 
Scenario: 01 [SubmissionDialogue_PUT_Broker_BrokerNotAllowed] For a valid SubmissionDialogueId when I do a PUT, returns 400/403
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And for the submission dialogue the 'IsDraftFlag' is 'True'
	And for the submission dialogue the 'UnderwriterQuoteReference' is 'updateUnderwriterQuoteReference'
	And for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a month from now
	And for the submission dialogue the 'SenderNote' is 'updating SenderNote for PUT Scenario1'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	When I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400" or "403"
	



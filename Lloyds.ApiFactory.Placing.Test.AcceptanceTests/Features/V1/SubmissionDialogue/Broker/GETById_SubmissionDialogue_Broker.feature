#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETById_SubmissionDialogue_Broker
	In order to RETRIEVE a submission dialogue
	As an authorised broker 
	I want to be able to do a GETById to SubissionDialogues

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionDialogue_GET_Broker_GetSubmissionDialoguebyId] I CAN do a GETById to a submission dialogue that my organisation has access to
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And submission dialogue context should have at least 1 link
	And submission dialogue context should have a link with proper value
	And context of submission dialogue includes all broker department resources for 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' 
	And validate submission dialogue context for the broker

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 I want to be able to get a submission dialogue and check that the Id has the correct structure SubmissionReference_SubmissionVersionNumber_SubmissionDialogueId
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And for the submission dialogue and the returned result the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_SubmissionDialogueId' 

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 GetById from another broker in the same department, returns 200
	Given I log in as a broker 'bellbroker.morgan@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And context of submission dialogue includes all broker department resources for 'bellbroker.morgan@limossdidev.onmicrosoft.com' 
	And validate submission dialogue context for the broker


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 I want to be able to GETById and return MANDATORY fields
	Given I log in as a broker 'bellbroker.morgan@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "200" 
	And I store the submission dialogue response in the context
	And validate submission dialogue context contain the following fields
		| field                   |
		| SubmissionDialogueId    |
		| SubmissionReference     |
		| SubmissionVersionNumber |
		| SubmissionUnderwriterId |
		| SenderParticipantCode   |
		| SenderUserEmailAddress  |
		| SenderUserFullName      |
		| StatusCode              |
		| SenderActionCode        |
		| CreatedDateTime         |
		| SentDateTime            |

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionDialogue_Broker_Negative
	In order to create a submission dialogue
	As an authorised broker 
	I want to be able to do a POST to SubmissionDialogue

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context for broker
	And preparing to POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I save the current submission dialogue 

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 Mandatory fields should be provided, if not return 4xx
	Given for the submission dialogue the '<field>' is ''
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range

	Examples:
		| field                   |
		| SubmissionReference     |
		| SubmissionVersionNumber |
		| SubmissionUnderwriterId |
		| SenderActionCode        |
		| SenderNote              |
		| IsDraftFlag             |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 02 Min,Max Chars - POST with fields having chars more or less than the value specs rules, returns 4xx
	Given for the feature submission dialogue context the '<field>' with a random value <assess> than <Characters>
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ErrorCode>'
	
	Examples:
		| field              | assess | rule | Characters | ErrorCode    |
		| SubmissionReference     | more   | max  | 50         | StringTooLong |
		| SubmissionReference     | less   | min  | 6          | StringTooShort |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 POST with an invalid submission version number, returns 4xx
	Given for the submission dialogue the 'SubmissionVersionNumber' is '6666999'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDialogue_POST_Broker_NoLinkedSubmission] - POST with an invalid submission version number which exists for another Submission, returns 4xx 
	Given for the submission dialogue the 'SubmissionVersionNumber' is '8'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 [SubmissionDialogue_POST_Broker_NoLinkedSubmission] - POST with an invalid submission reference, returns 4xx
	Given for the submission dialogue the 'SubmissionReference' is '9999999'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionDialogue_POST_Broker_NoLinkedSubmission] POST with an invalid Submission which exists, returns 4xx
	Given submission is set from a different submission underwriter that exists
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage			|
 	| [SubmissionDoesNotExist]	|
 	| [SubmissionUnderwriterDoesNotExist] |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 07 [SubmissionDialogue_POST_Broker_BrokerNotAuthorisedForSubmission] - Unauthorised broker does a POST for the related Submission, returns 400/403
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'AccessToReferencedResourceNotAllowed'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 08 [SubmissionDialogue_POST_Broker_NoLinkedSubmissionUnderwriter] POST with an INVALID SubmissionUnderwriterId, returns 4xx
	And for the submission dialogue the 'SubmissionUnderwriterId' is '666'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'SubmissionUnderwriterDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 09 [SubmissionDialogue_POST_Broker_NoLinkedSubmissionUnderwriter] POST with an INVALID SubmissionUnderwriterId that does exists, returns 4xx
	Given submission underwriter id is set from a different submission underwriter that exists
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'SubmissionUnderwriterDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 10 [SubmissionDialogue_POST_Broker_InvalidSubmitAsDraftValue] POST with IsDraft flag = True, returns 4xx
	Given for the submission dialogue the 'IsDraftFlag' is 'True'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'DraftSubmissionNotExpected'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 11 [SubmissionDialogue_POST_Broker_InvalidActionCode] POST with INVALID SenderActionCode != RFQ, returns 4xx
	Given for the submission dialogue the 'SenderActionCode' is '<value>'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidBrokerActionCode'

	Examples:
		| value |
		| DECL  |
		| EOI   |
		| QUOTE |
		| RFI   |
		| WTHDR |
		| CLSDU |
		| DECNT |
		| QNTU  |
		| CLSDB |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 12 [SubmissionDialogue_POST_Broker_EmptySenderNote] POST with NO SenderNote, returns 4xx
	Given for the submission dialogue the 'SenderNote' is ''
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'SenderNoteMandatory'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 13 POST with an invalid Document for the referenced Submission, returns 4xx
	Given for the submission dialogue the 'SenderDocumentIds' is '667' 
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidDocumentForSubmission'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 14 [SubmissionDialogue_POST_Broker_UnderwriterQuoteReferenceNotExpected] POST with NOT EMPTY UnderwriterQuoteReference, returns 4xx
	Given for the submission dialogue the 'UnderwriterQuoteReference' is '666'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterQuoteReferenceNotExpected'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [SubmissionDialogue_POST_Broker_UnderwriterQuoteValidDateNotExpected] POST with NOT EMPTY UnderwriterQuoteValidUntilDate, returns 4xx
	Given for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is '2019-10-10'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterQuoteValidUntilDateNotExpected'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [SubmissionDialogue_POST_Broker_SubmissionDocumentMissing] POST with NO DocumentId being a PLACING SLIP for the referenced Submission Docment, returns 4xx
	Given there are no document ids that will be identified as a placing slip
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                   |
	| SubmissionDocumentMissing      |
	| SubmissionDocumentDoesNotExist |
	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 17 POST with two documents as PLACING SLIPS for the referenced Submission Docment, returns 4xx
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with a submission document from the following data
	| SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription |
	| 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test   |
	And there are more than one document ids that will be identified as a placement slip
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 18 [SubmissionDialogue_POST_Broker_SupportingDocumentMissing] POST with a supporting document that does not exist for the related SubmissionDocument, returns 4xx
	Given a supporting document referenced does not exist for the related SubmissionDocument
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                            |
	| InvalidDocumentForSubmission            |
	| SupportingDocumentNotLinkedToSubmission |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 [SubmissionDialogue_POST_Broker_DocumentsNotLinkedToSubmission] POST with a supporting document that does not exist for the related Submission, returns 4xx
	Given a supporting document referenced is not referenced for the related Submission 
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                            |
	| InvalidDocumentForSubmission            |
	| SupportingDocumentNotLinkedToSubmission |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 20 [SubmissionDialogue_POST_Broker_DuplicateBrokerRequestForQuote] POST twice for the same SubmissionReference/SubmissionVersionNumber/SubmissionUnderwriterId, returns 4xx
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource

	#POST SECOND TIME
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'BrokerRequestForQuoteAlreadyExists'

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 21 String character rules - CANNOt do a POST that violate the string character value spec rules, returns 4xx
	Given for the submission dialogue the '<field>' is '<value>'
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range

	Examples:
		| field                     | rule                  | value | error               |
		| SubmissionReference       | No whitespace         | t t f | ContainsWhitespace  |
		| SubmissionReference       | No special characters | t!@st | ContainsSpecialChar |
		| UnderwriterQuoteReference | No whitespace         | t t f | ContainsWhitespace  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 22 [SubmissionDialogue_POST_Broker_BrokerNotAuthorisedForSubmission] POST as an unauthorised Broker from another company, returns 400/403
		Given I log in as a broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
		And add the submission dialogue request
		And the current submission dialogue is saved for the scenario
		When I POST to '/SubmissionDialogues' resource
		Then the response status code should be '400' or '403'
		And the response contains a validation error 'AccessToReferencedResourceNotAllowed'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 23 [SubmissionDialogue_POST_Broker_NoLinkedSupportingDocument] POST with a supporting document that is not linked to the related Submission, returns 4xx
	Given a supporting document referenced is not referenced for the related Submission 
	And add the submission dialogue request
	When I POST to '/SubmissionDialogues' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                            |
	| InvalidDocumentForSubmission            |
	| SupportingDocumentNotLinkedToSubmission |

		

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_SubmissionDialogue_Broker
	In order to assess the submission dialogue
	As an authorised broker 
	I want to be able to do a HEAD to SubmissionMarketDialogue

@CoreAPI
Scenario: 01 [SubmissionDialogue_HEAD_Broker_HeadSubmissionDialogueById] For a valid SubmissionDialogueId, I want to be able to do a HEAD returning 2xx
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	When  I make a HEAD request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be in "2xx" range
	
#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionDialogue_HEAD_Broker_SubmissionDialogueNotFound] For an INVALID SubmissionDialogueId, I want to be able to do a HEAD returning 404
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionDialogues/666' resource 
	Then the response status code should be "404"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionDialogue_HEAD_Broker_BrokerNotAuthorised] SubmissionDialogue_HEAD_Broker_BrokerNotAuthorised - As an unauthorised Broker from a different company and a valid SubmissionDialogueId, I want to be able to return 400/403
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
 	When  I make a HEAD request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "400" or "403"

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionDialogue_HEAD_Broker_BrokerNotAuthorised] - As an unauthorised Broker from a different company and a valid SubmissionDialogueId, I want to be able to return 400/403
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' 
	And I POST a submission on behalf of the broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
 	When  I make a HEAD request to '/SubmissionDialogues/{SubmissionDialogueId}' resource 
	Then the response status code should be "400" or "403"

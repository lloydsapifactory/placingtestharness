#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GETALL_SubmissionDialogue_Broker
	In order to RETRIEVE all the submission dialogues
	As an authorised broker 
	I want to be able to do a GETALL to SubissionDialogues

Background:
		Given I have placing V1 base Uri
		And I set Accept header equal to application/json type
		Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'

@CoreAPI
Scenario: 01 [SubmissionDialogue_GET_ALL_Broker_GetSubmissionDialogues] GET ALL submission dialogues
	And I POST a submission on behalf of the broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue should have at least 1 link
	And collection of submission dialogue should have a link with proper value
	And validate mandatory fields for the collection of submission dialogue as a broker

	@CoreAPI
Scenario Outline: 02 GET ALL submission dialogues for broker with different departments, can ONLY see their own Submission Dialogues
	Given I log in as a broker '<BrokerUserEmailAddress>'
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue include all broker department resources for '<BrokerUserEmailAddress>'
	And collection of submission dialogue include all NON DRAFT underwriter resources

	Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |

#---------------------------------------------------------------------------

@CoreAPI
Scenario Outline: 02a GET ALL submission dialogues for broker within the same company and department, can validate the results
	Given I log in as a broker '<BrokerUserEmailAddress>'
	And store the broker
	Given I POST a submission on behalf of the broker '<BrokerUserEmailAddress>' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I refresh the SubmissionDialogeContext with the response content
	And the current submission dialogue is saved for the scenario
	And I reset all the filters

	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue posted by the broker is included in the submission dialogue collection
	
	Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com    | AON                  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 GETALL and return submission dialogues, including & validating the one POSTED
	Given I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I refresh the SubmissionDialogeContext with the response content
	And the current submission dialogue is saved for the scenario
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue posted by the broker is included in the submission dialogue collection

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 I want to be able to GET ALL submission dialogues and return MANDATORY fields
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue results contain the following fields
		| field                   |
		| SubmissionDialogueId    |
		| SubmissionReference     |
		| SubmissionVersionNumber |
		| SubmissionUnderwriterId |
		| SenderParticipantCode   |
		| SenderUserEmailAddress  |
		| SenderUserFullName      |
		| StatusCode              |
		| SenderActionCode        |
		| CreatedDateTime         |
		| SentDateTime            |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 ('CONTAINS/PREFIX/SUFFIX' filtering):  I want to be able to GET ALL submission dialogues with CONTAINS filter 
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | SubmissionVersionNumber |JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
		 | 1                  |  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.txt       | text/plain                                                              | document_file_note    | Data\mrc_documents\sample_mrc.txt  | additional non placing test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I clear down test context
	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter back |
	
	And I reset all the filters

	Given I add a 'contains' filter on '<field>' with value equals 'testingUnderwriterQuoteReference'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue results contain the value of <field>

	Examples:
		| field                     | filterType |
		| UnderwriterQuoteReference | contains   |
		| UnderwriterQuoteReference | prefix     |
		| UnderwriterQuoteReference | suffix     |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 (SINGLE-value EXACT filtering): I want to be able to GET ALL submission dialogues, with single value EXACT MATCH field filtering 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue results contain the value of <field>

	Examples:
		| field                   |
		| SubmissionReference     |
		| SubmissionUnderwriterId |
		| SenderUserEmailAddress  |
		| SubmissionVersionNumber |
		| IsDraftFlag             |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 07 ('match' single value filtering): I want to be able to GET ALL submission dialogues with MATCH single value filtering 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a 'match' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue results contain the value of <field>

	Examples:
		| field                  |
		| SubmissionReference         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 08 ('match' single value filtering): I want to be able to GET ALL submission dialogues with MATCH single value filtering 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a 'match' filter on '<field>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate submission dialogue results contain the value of <field>

	Examples:
		| field                   |
		| SubmissionUnderwriterId |
		| SenderActionCode        |
		| SenderParticipantCode   |
		| SenderUserEmailAddress  |
		| StatusCode              |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 09 OrderBy: As an authorised broker I want to be able to GET ALL Submission Underwriters ordered by 
	And I add a filter on '_order' with a value '<field>'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And the submission dialogue response is order by '<field>' 'ascending'

	Examples:
		| field           |
		| CreatedDateTime |
		| SentDateTime    |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 10 (pagination): I want to to do a GETALL with pagination 
	And I add a filter on '_pageNum' with a value '1'
	And I add a filter on '_pageSize' with a value '2'
	When  I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And the count of submission dialogue returned will be = 2

	And I clear down test context

	Given I add a filter on '_pageNum' with a value '2'
	And I add a filter on '_pageSize' with a value '1'
	When  I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And the count of submission dialogue returned will be = 1

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 11 (multiple field AND multiple value-filtering): I want to be able to GET ALL submission dialogues with multiple fields filtering 
	Given  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I reset all the filters
	
	Given I add a 'match' filter on '<field1>' with a random value from the response of submission dialogue
	And I add a 'match' filter on '<field2>' with a random value from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate the collection of submission dialogue I am getting contain the values of '<fields>'

	Examples:
	| field1                  | field2                  | fields                                         |
	| SubmissionReference     | SubmissionUnderwriterId | SubmissionReference,SubmissionUnderwriterId    |
	| SubmissionReference     | SenderParticipantCode   | SubmissionReference,SenderParticipantCode      |
	| SubmissionReference     | SenderUserEmailAddress  | SubmissionReference,SenderUserEmailAddress     |
	| SubmissionReference     | SenderActionCode        | SubmissionReference,SenderActionCode           |
	| SubmissionUnderwriterId | SenderParticipantCode   | SubmissionUnderwriterId,SenderParticipantCode  |
	| SubmissionUnderwriterId | SenderUserEmailAddress  | SubmissionUnderwriterId,SenderUserEmailAddress |
	| SenderParticipantCode   | SenderUserEmailAddress  | SenderParticipantCode,SenderUserEmailAddress   |
	| SenderParticipantCode   | SenderActionCode        | SenderParticipantCode,SenderActionCode         |
	| SenderUserEmailAddress  | SenderActionCode        | SenderUserEmailAddress,SenderActionCode        |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 12 (multiple value filtering Filter=A,B,C):  I want to be able to GET ALL submission dialogues, with multiple value filtering 
	And I make a GET request to '/SubmissionDialogues' resource
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a filter on '<field>' with different random values from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate the collection of submission dialogue I am getting the correct results for '<field>'
	
	Examples:
		| field                   |
		| SubmissionReference     |
		| SubmissionUnderwriterId |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 13 ('match' multiple value filtering match(A,B,C)):  I want to be able to GET ALL submission dialogues, with multiple value filtering 
	And I make a GET request to '/SubmissionDialogues' resource
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a 'match' filter on '<field>' with different random values from the response of submission dialogue
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate the collection of submission dialogue I am getting the correct results for '<field>'
	
	Examples:
		| field                   |
		| SubmissionReference     |
		| SubmissionUnderwriterId |
		| SenderActionCode        |
		| SenderParticipantCode   |
		| SenderUserEmailAddress  | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 16 (DATE range Filter): I want to be able to GET ALL submission dialogues, with a date range 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate that the submission dialogue results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 17 (DATE before,after filtering): I want to be able to GET ALL submission dialogues, with a date range 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate that the submission dialogue results for the field '<field>' are <comparison> the date of <when>

	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | after      | yesterday |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 18 GETALL returns submission dialogues validating the one POSTED
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| 1                       | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I reset all the filters

	And I refresh the SubmissionDialogeContext with the response content

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate that the submission dialogues is included in the collection


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 GETALL validating sender documents with the submission documents
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And store the broker
	When I make a GET request to '/SubmissionDialogues' resource
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And for the collection of submission dialogues validate document ids matching the correct submission documents


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 20 OrderBy last_modified(POST): As a BROKER When there is a SubmissionDialogue CREATED I want to be able to GET ALL Submission Underwriters ordered by last_modified
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context for broker
	And POST as a broker to SubmissionDialogues with following info
		| IsDraftFlag | SenderActionCode | SenderNote    |
		| False       | RFQ              | created first |

	#POST SECOND SUBMISSION DIALOGUE TO DIFFERENT UNDERWRITER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                           | CommunicationsMethodCode |
		| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | PLATFORM                 |
	And I reset all the filters	
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote     |
	| False       | RFQ              | created second |

	And I add a 'match' filter on 'SubmissionReference' with a value from the submission dialogue context
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue should have a total equal to 2

	And the submission dialogue response is ordered by '_last_modified' of the field 'SenderNote' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| created first         | created second        |
	| created second        | created first         |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 21 OrderBy last_modified(POST AS uw): As a BROKER When there SubmissionDialogues created by the Underwriter I want to be able to GET ALL Submission Underwriters ordered by last_modified
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		|JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		|  Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		|  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

	And I set the SubmissionDialogue context with the submission for underwriter
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter once |
	And I reset all the filters
	And I clear down test context

	And I set the SubmissionDialogue context with the submission for underwriter
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference        | SenderActionCode | SenderNote                       |
	| False       | testingUnderwriterQuoteReference | QUOTE            | a note from the underwriter twice |
	And I reset all the filters

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with a value from the submission dialogue context
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue should have a total equal to 3

	And the submission dialogue response is ordered by '_last_modified' of the field 'SenderNote' with the following order as <sortingOrder>
	| valueOfField_ascending            | valueOfField_descending           |
	| a note from the broker            | a note from the underwriter twice |
	| a note from the underwriter once  | a note from the underwriter once  |
	| a note from the underwriter twice | a note from the broker            |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |



#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 22 OrderBy last_modified(PUT AS UW): As a BROKER When there is a SubmissionDialogue UPDATED by the Underwriter I want to be able to GET ALL Submission Underwriters ordered by last_modified
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		|JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription         |
		|  Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test           |
		|  Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test   |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker 
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And I refresh the SubmissionDialogeContext with the response content
	And I set the SubmissionDialogueId
	And I reset all the filters
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And store the underwriter

	And I set the SubmissionDialogue context with the submission for underwriter
	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| False        |           godzillaTesting                | QUOTE              | a note from the underwriter once |
	And I reset all the filters
	And I clear down test context

	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| False        |                           | EOI              | a note from the underwriter twice  |
	And I reset all the filters
	And I clear down test context

	And POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| False        | godzillaTesting           | QUOTE            | a note from the underwriter thrice |
	And I reset all the filters
	And I clear down test context

	And preparing to POST as an underwriter to SubmissionDialogues with following info
	| IsDraftFlag | UnderwriterQuoteReference | SenderActionCode | SenderNote                             |
	| True        |                           | EOI              | test message back from the underwriter |
	And add the submission dialogue request adding the field
	And the current submission dialogue is saved for the scenario
	And I POST to '/SubmissionDialogues' resource
	And I store the submission dialogue response in the context
	And store the CreatedDateTime from submission dialogue context
	And I clear down test context

	And for the submission dialogue the 'IsDraftFlag' is 'false'
	And for the submission dialogue the 'SenderActionCode' is 'EOI'
	And for the submission dialogue the 'UnderwriterQuoteReference' is ''
	And for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''
	And for the submission dialogue the 'SenderNote' is 'recent update'
	And add the submission dialogue request
	And the current submission dialogue is saved for the scenario
	And I set the SubmissionDialogueId
	And I PUT to '/SubmissionDialogues/{SubmissionDialogueId}' resource with 'If-Unmodified-Since' header

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with a value from the submission dialogue context
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue should have a total equal to 5

	And the submission dialogue response is ordered by '_last_modified' of the field 'SenderNote' with the following order as <sortingOrder>
	| valueOfField_ascending             | valueOfField_descending            |
	| a note from the broker             | recent update                      |
	| a note from the underwriter once   | a note from the underwriter thrice |
	| a note from the underwriter twice  | a note from the underwriter twice  |
	| a note from the underwriter thrice | a note from the underwriter once   |
	| recent update                      | a note from the broker             |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 23 Adding _select parameter will return results with the exact fields
	Given I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,senderActionCode,SubmissionUnderwriterId'
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submission dialogue
	And collection of submission dialogue should have at least 1 link
	And collection of submission dialogue should have a link with proper value

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 24 (last_modified before,after filtering): I want to be able to GET ALL submission dialogues, with a date range 
	And I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate that the submission dialogue results for the field '<field>' are <comparison> the date of <when>

	Examples:
		| field          | comparison | when      |
		| _last_modified | before     | yesterday |
		| _last_modified | after      | yesterday |
		| _last_modified | before     | one week  |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 25 (DATE range Filter): GETALL submissions dialogues, with a date range 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And validate that the submission dialogue results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 
		| _last_modified | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 26 (range adatetime..bdatetime): I want to be able to GET ALL submission dialogues, with a date range 
	And  I make a GET request to '/SubmissionDialogues' resource 
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=a week ago and bdatetime=today
	When I make a GET request to '/SubmissionDialogues' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission dialogue collection
	And collection of submission dialogue should have at least 1 link
	And validate that the submission dialogue date results for the field '<field>' are  for one week

	Examples:
		| field           | 
		| CreatedDateTime | 

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: BrokerDepartment_Negative
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to see the correct number of broker departments 

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 GetById returns 404 for an invalid broker department id
	And I retrieve the broker departments for 'aonbroker.ema@limossdidev.onmicrosoft.com'
	Given I select invalid broker department id
	When I make a GET request to '/brokerdepartments/{brokerDepartmentId}' resource
	Then the response status code should be "404"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 GetById returns 404 for a valid broker department id
	And I retrieve the broker departments for 'aonbroker.ema@limossdidev.onmicrosoft.com'
	Given I select valid broker department id
	When I make a GET request to '/brokerdepartments/{brokerDepartmentId}' resource
	Then the response status code should be "404"


##---------------------------------------------------------------------------
#@CoreAPI @BackendNotImplemented-Ebix
#Scenario: 04 [BrokerDepartment_GET_ALL_Underwriter_GetBrokerDepartments] GETALL as an Underwiter returns 400
#	Given I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
#	When I make a GET request to '/brokerdepartments' resource
#	Then the response status code should be "400" or "403"
#	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 03 [InvalidFilterCriterion] Returns 4xx
	And I add a filter on '<field>' with a value '<value>'
	When I make a GET request to '/brokerdepartments' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                | value |
	| BrokerDepartmentId   | 1     |
	| BrokerDepartmentName | test  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 [InvalidFilterCriterion with match] 
	And I add a 'match' filter on '<field>' with a value '<value>'
	When I make a GET request to '/brokerdepartments' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                | value |
	| BrokerDepartmentId   | 1     |
	| BrokerDepartmentName | test  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 [InvalidFilterCriterion with prefix] 
	And I add a 'prefix' filter on '<field>' with a value '<value>'
	When I make a GET request to '/brokerdepartments' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                | value |
	| BrokerDepartmentId   | 1     |
	| BrokerDepartmentName | test  |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 06 [InvalidFilterCriterion with suffix] 
	And I add a 'suffix' filter on '<field>' with a value '<value>'
	When I make a GET request to '/brokerdepartments' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                | value |
	| BrokerDepartmentId   | 1     |
	| BrokerDepartmentName | test  |

	#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 07 [InvalidFilterCriterion with contains] 
	And I add a 'contains' filter on '<field>' with a value '<value>'
	When I make a GET request to '/brokerdepartments' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                | value |
	| BrokerDepartmentId   | 1     |
	| BrokerDepartmentName | test  |


#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: BrokerDepartment
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to see the correct number of broker departments 

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 01 [BrokerDepartment_GET_ALL_Broker_GetBrokerDepartments] View the correct number of departments
	When I make a GET request to '/brokerdepartments' resource
	Then the response status code should be "200"
	And I store broker departments
	And count of broker departments returned will be >= 1

#---------------------------------------------------------------------------
#@CoreAPI @BackendNotImplemented-Ebix
#Scenario: 05 [BrokerDepartment_GET_ALL_Broker_GetBrokerDepartments] View the correct number of departments
#	And I make a GET request to '/brokerdepartments' resource
#	And I store broker departments
#	And I select valid broker department id
#	When I make a GET request to '/brokerdepartments/{brokerDepartmentId}' resource
#	Then the response status code should be "200"
#	And I store broker departments
#	And count of broker departments returned will be >= 1


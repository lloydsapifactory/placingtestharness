#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

 @ApiGwSecurity
Feature: UnderwriterOrganisation
	In order to assign a submission to underwriter
	As an onboarded broker
	I need to be able to see the correct number of underwriter organisations 

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 01 [UnderwriterOrganisation_GET_ALL_Broker_GetUnderewriterOrganisations] Doing GETALL filtering with NameEmailOrg, return all related underwriter organisations with user email address
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And collection of underwriter organistations should have a total greater than 0
	And validate underwriter user email address contain the value from the field NameEmailOrg


Examples:
| NameEmailOrgValue                                    |
| amlinunderwriter.lee@limossdidev.onmicrosoft.com     |
| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com |
| underwrit                                            |
| y@L                                                  |
| AMLINUNDERWRITER.LEE@LIMOSSDIDEV.ONMICROSOFT.COM     |    

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 02 View the correct number of underwriter organisations with org name filter and for the department that the broker belongs to
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'beazleyunderwriter.fiona@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And collection of underwriter organistations should have a total greater than 0
	And validate underwriter user email address contain the value from the field NameEmailOrg

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 03 [UnderwriterOrganisation_GET_ALL_Broker_GetUnderewriterOrganisations] Doing GETALL filtering with NameEmailOrg with full name, return all related underwriter organisations with user full name
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  'Underwriter Lee'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And collection of underwriter organistations should have a total greater than 0
	And validate underwriter user full name contain the value from the field NameEmailOrg

	Examples:
| NameEmailOrgValue |
| Underwriter Lee   |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 View the correct number of underwriter organisations with invalid value in filter
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  'testing a non persited value'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And save the collection of underwriter organistations
	And collection of underwriter organisations should have a total equal to 0


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 05 Return MANDATORY fields
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And collection of underwriter organistations should have a total greater than 0
	And validate underwriter organisations results contain the following fields
	| fieldname                   |
	| UnderwriterOrganisationId   |
	| UnderwriterUsers            |
	| UnderwriterOrganisationName |
	| BrokerDepartmentId          |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 GETALL underwriter organisations ordered by 
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a filter on '_order' with a value '<field>'
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "200"
	And collection of underwriter organistations should have a total greater than 0
	And the underwriter organisations response is order by '<field>' 'ascending'

	Examples:
		| field                       |
		| UnderwriterOrganisationName |

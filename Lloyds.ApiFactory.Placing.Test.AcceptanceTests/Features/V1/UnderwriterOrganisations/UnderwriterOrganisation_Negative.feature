#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

 @ApiGwSecurity
Feature: UnderwriterOrganisation_Negative
	In order to assign a submission to underwriter
	As an onboarded broker
	I need to be able to see the correct number of underwriter organisations 

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 Respond with Bad Request when BrokerDepartmentId not provided
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a 'contains' filter on 'NameEmailOrg' with  'Underwriter Lee'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400"
	And the response contains an error 'BrokerDepartmentId'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 Respond with Bad Request when NameEmailOrg not provided
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	When I make a GET request to '/UnderwriterOrganisations' resource
	Then the response status code should be "400"
	And the response contains an error 'NameEmailOrg'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [UnderwriterOrganisation_GET_ALL_Broker_BrokerRequesterNotMemberOfDepartment] Provide an invalid broker department Id, returns 400/403
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a filter on 'BrokerDepartmentId' with the broker does not belong to '99999'
	And I add a 'contains' filter on 'NameEmailOrg' with  'Underwriter Lee'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                                                                    |
	| BrokerNotMemberOfDepartment                                                     |
	| Broker department does not exist or the authenticated user does not have access |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 [UnderwriterOrganisation_GET_ALL_Broker_BrokerRequesterNotMemberOfDepartment] Respond with Bad Request when invalid broker department id provided
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a filter on 'BrokerDepartmentId' with invalid broker id 'invalid'
	And I add a 'contains' filter on 'NameEmailOrg' with  'Underwriter Lee'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                                                                    |
	| BrokerNotMemberOfDepartment                                                     |
	| Broker department does not exist or the authenticated user does not have access |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 Respond with Bad Request when login as an underwriter
	Given I set security token and certificate for the 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' user
	And I add a filter on 'BrokerDepartmentId' with the broker does not belong to '99999'
	And I add a 'contains' filter on 'NameEmailOrg' with  'Underwriter Lee'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400" or "403"


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 Respond with Bad Request when less than 3 chars supplied in NameEmailOrg filter
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  'ao'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400"
	And the response contains a validation error 'InvalidFilterCriterion'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 07 Repsond with 404 when performing a GetById
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	When I make a GET request to '/UnderwriterOrganisations/558' resource
	Then the response status code should be "404"


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 08 [UnderwriterOrganisation_GET_ALL_Broker_BrokerRequesterNotMemberOfDepartment] GET with a broker department id from another broker, returns 400
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department

	Given I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                                                                    |
	| BrokerNotMemberOfDepartment                                                     |
	| Broker department does not exist or the authenticated user does not have access |


	#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 09 [UnderwriterOrganisation_GET_ALL_Underwriter_GetUnderewriterOrganisations] GET as an underwriter, returns 400/403
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400" or "403"
	And the response contains an error 'ResourceAccessDenied'


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario: 10 Provide NameEmailOrg with more than 50 chars supplied in NameEmailOrg filter, returns 400
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a 'contains' filter on 'NameEmailOrg' with  'TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 11 [InvalidFilterCriterion] Returns 4xx
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/brokerdepartments' resource
	And I Save Broker Departments
	And I add a filter on 'BrokerDepartmentId' with the first broker department
	And I add a filter on '<field>' with a value '<value>'
	And I add a 'contains' filter on 'NameEmailOrg' with  part of 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/UnderwriterOrganisations' resource with filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
	| field                       | value |
	| UnderwriterOrganisationId   | 1     |
	| UnderwriterOrganisationName | test  |


#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: SubmissionDocuments
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to post(create), put(update), get(retrieve) and delete submissions via API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

@BLogicAPIandUI
Scenario Outline: 01 Broker cannot do a PUT to a submission document that is in a submission that has been moved from 'DRFT'
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
	| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType   | File                               | DocumentDescription  |
	| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium | Data\mrc_documents\sample_mrc.docx | Client Corrspondence |
	And I login to PPL UI as 'bellbroker.bella@limossdidev.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And I click on the add/remove underwriter link
	And I upload an MRC document <mrcFile>
	And I add an underwriter <underwriter>
	And I send the submission
	And I save document submission update information for PUT
	When I PUT to '/SubmissionDocuments/{SubmissionDocumentId}' resource
	Then the response status code should be "400"

Examples:
		| inputFile                   | mrcFile                            | underwriter		|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee	|



#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: Create Submission in PPL
	In order to retrieve submissions via the API
	As an onboarded broker user
	I need to be able to create a submission first in PPL

Background:
	Given I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the create submission screen
	And I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I set Content Type header equal to application/json type

@BLogicAPIandUI
Scenario Outline: Broker successfully creates a submission in PPL
	And I set the feature submission context from the json payload <baseTemplate>
	And for the submission the 'SubmissionReference' is 'SFTEST' plus a random string	
	And I create and save the submission in the UI using the base template <baseTemplate> and MRC <mrcFile> for underwriter <underwriter>
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response submission should match the original submission
	And the submission status should be 'Draft'

	Examples:
		| baseTemplate                | mrcFile                            | underwriter			|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee		|

@BLogicAPIandUI
Scenario Outline: Broker successfully creates a submission in PPL and sends to underwriter
	And I set the feature submission context from the json payload <baseTemplate>
	And for the submission the 'SubmissionReference' is 'SFTEST' plus a random string
	And I create and send the submission in the UI using the base template <baseTemplate> and MRC <mrcFile> for underwriter <underwriter>
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response submission should match the original submission
	And the submission status should be 'In Progress'

	Examples:
		| baseTemplate                | mrcFile                            | underwriter			|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee		|


@BLogicAPIandUI
Scenario Outline: Broker attempts to update a submission that has already been sent to underwriter
	And I set the feature submission context from the json payload <baseTemplate>
	And for the submission the 'SubmissionReference' is 'SFTEST' plus a random string
	And I create and send the submission in the UI using the base template <baseTemplate> and MRC <mrcFile> for underwriter <underwriter>
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource
	When I construct a PUT
	And for the submission the 'SubmissionVersionDescription' is 'QVDPUT17092019'
	When I PUT to '/Submissions/{submissionUniqueReference}' resource
	Then the response status code should be "400"

	Examples:
		| baseTemplate                | mrcFile                            | underwriter			|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee		|

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: Submission
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to post(create), put(update), get(retrieve) and delete submissions via API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

@BLogicAPIandUI
Scenario Outline: 01a Broker creates a worldwide direct Contract with specific period of cover
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'direct_insurance_contract'
	And for the submission the 'RiskRegionCode' is 'worldwide'
	And for the submission the PeriodofCover has an Insurance period where the inception date is '2019-12-31' and the expiry date is '2020-12-31'
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: 01b Brokes creates a wordlwide direct Contract for 12 months
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'direct_insurance_contract'
	And for the submission the 'RiskRegionCode' is 'worldwide'
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	And for the submission the PeriodOfCover has an insurance duration of 12 months
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: 01c Broker creates UK direct Contract for 12 months
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'direct_insurance_contract'
	And for the submission the PeriodOfCover has an insurance duration of 12 months
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: o1d Broker creates a worldwide reinsurance Contract with specific period of cover
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'reinsurance_contract'
	And for the submission the 'RiskRegionCode' is 'worldwide'
	And for the submission the PeriodofCover has an Insurance period where the inception date is '2019-12-31' and the expiry date is '2020-12-31'
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: 01e Broker creates a worldwide reinsurance Contract for 12 months
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'reinsurance_contract'
	And for the submission the 'RiskRegionCode' is 'worldwide'
	And for the submission the PeriodOfCover has an insurance duration of 12 months
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: Broker creates GB reinsurance Contract for 12 months
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'ContractTypeCode' is an enum 'reinsurance_contract'
	And for the submission the PeriodOfCover has an insurance duration of 12 months
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And the Draft submission details in the UI are correct

	Examples:
		| inputFile                   |
		| Data\1_4_Model\Submission_1.json |

@BLogicAPIandUI
Scenario Outline: Submission has been created by broker and assigned to an underwriter 
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I store the submission in the context
	And I set the SubmissionUniqueReference
	
	Given I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	Then I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And I click on the add/remove underwriter link
	And I upload an MRC document <mrcFile>
	And I add an underwriter <underwriter>
	And I send the submission
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource
	Then the response status code should be "200"
	
	Examples:
		| inputFile                   | mrcFile                            | underwriter		|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee	|


@BLogicAPIandUI
Scenario Outline: Underwriter retrieves an assigned submission via filtering  
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I store the submission in the context
	And I set the SubmissionUniqueReference
	
	Given I make a GET request to '/Submissions?SubmissionDialogueId=DRFT' resource fetching all the results
	And the submission with reference '{submissionUniqueReference}' is present in the results
	When I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	Then I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And I click on the add/remove underwriter link
	And I upload an MRC document <mrcFile>
	And I add an underwriter <underwriter>
	And I send the submission
	When I make a GET request to '/Submissions?SubmissionDialogueId=DRFT' resource fetching all the results
	Then the submission with reference '{submissionUniqueReference}' is absent in the results
	When I make a GET request to '/Submissions?SubmissionDialogueId=SENT' resource fetching all the results
	Then the submission with reference '{submissionUniqueReference}' is present in the results

	
	Examples:
		| inputFile                   | mrcFile                            | underwriter		|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee	|


@BLogicAPIandUI
Scenario Outline: Underwriter cannot update an assigned submission
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set a json payload <inputFile>
	And for the submission the 'InsuredOrReinsured' property is appended with the current value of 'SubmissionReference'
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And I store the submission in the context
	And I set the SubmissionUniqueReference
	
	Given I login to PPL UI as 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	Then I navigate to the draft placements page
	And I search for the submission reference in the ReInsured field
	And I click on the first search result
	And I click on the add/remove underwriter link
	And I upload an MRC document <mrcFile>
	And I add an underwriter <underwriter>
	And I send the submission
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource
	When I construct a PUT
	And for the submission the 'SubmissionVersionDescription' is 'QVDPUT17092019'
	When I PUT to '/Submissions/{submissionUniqueReference}' resource
	Then the response status code should be "400"
	
	Examples:
		| inputFile                   | mrcFile                            | underwriter		|
		| Data\1_4_Model\Submission_1.json | Data\mrc_documents\sample_mrc.docx | Underwriter Lee	|



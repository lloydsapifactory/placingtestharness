// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.UITests
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("SubmissionDocuments")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class SubmissionDocumentsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "SubmissionDocuments.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "SubmissionDocuments", "\tIn order to place and view submissions\r\n\tAs an onboarded broker or underwriter u" +
                    "ser\r\n\tI need to be able to post(create), put(update), get(retrieve) and delete s" +
                    "ubmissions via API", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 11
#line hidden
#line 12
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 13
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 Broker cannot do a PUT to a submission document that is in a submission that h" +
            "as been moved from \'DRFT\'")]
        [NUnit.Framework.CategoryAttribute("BLogicAPIandUI")]
        [NUnit.Framework.TestCaseAttribute("Data\\1_4_Model\\Submission_1.json", "Data\\mrc_documents\\sample_mrc.docx", "Underwriter Lee", null)]
        public virtual void _01BrokerCannotDoAPUTToASubmissionDocumentThatIsInASubmissionThatHasBeenMovedFromDRFT(string inputFile, string mrcFile, string underwriter, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "BLogicAPIandUI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 Broker cannot do a PUT to a submission document that is in a submission that h" +
                    "as been moved from \'DRFT\'", null, @__tags);
#line 16
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 17
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 18
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 19
 testRunner.And("I clear down test context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table618 = new TechTalk.SpecFlow.Table(new string[] {
                            "JsonPayload",
                            "FileName",
                            "FileMimeType",
                            "DocumentType",
                            "File",
                            "DocumentDescription"});
                table618.AddRow(new string[] {
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "MRC_Placingslip.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Client Corrspondence"});
#line 20
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                        "soft.com\' with the following submission documents", ((string)(null)), table618, "And ");
#line hidden
#line 23
 testRunner.And("I login to PPL UI as \'bellbroker.bella@limossdidev.onmicrosoft.com\' user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 24
 testRunner.And("I navigate to the draft placements page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 25
 testRunner.And("I search for the submission reference in the ReInsured field", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 26
 testRunner.And("I click on the first search result", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 27
 testRunner.And("I click on the add/remove underwriter link", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 28
 testRunner.And(string.Format("I upload an MRC document {0}", mrcFile), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 29
 testRunner.And(string.Format("I add an underwriter {0}", underwriter), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 30
 testRunner.And("I send the submission", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 31
 testRunner.And("I save document submission update information for PUT", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 32
 testRunner.When("I PUT to \'/SubmissionDocuments/{SubmissionDocumentId}\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 33
 testRunner.Then("the response status code should be \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: Version
	In order to check the version of the service
	As an api service
	I want to be able to request the version of the Placing api

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

@CoreAPI @SmokeTests
Scenario: Check API Version as JSON
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	When I make a GET request to '/version' resource
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should have value
	And the version resource has 1.10.2 property

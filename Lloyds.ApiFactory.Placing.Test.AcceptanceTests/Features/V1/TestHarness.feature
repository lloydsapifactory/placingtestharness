#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity @ignore
Feature: TestHarness stuff
	In order to do various operations
	As a tester
	I want to be able to have a test harness

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	@ignore
Scenario: Delete all SubmissionUnderwriters when CreatedDateTime is null
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	Then I delete all the submission underwriters with minimum createddatetime value

		@ignore
Scenario: Delete all Submissions when TechnicianUserEmailAddress is null
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	Then I delete all the submissions with 'TechnicianUserEmailAddress' is null


	

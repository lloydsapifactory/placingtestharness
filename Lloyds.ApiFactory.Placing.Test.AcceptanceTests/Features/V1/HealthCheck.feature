#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

Feature: Health
	In order to assess the status of the service
	As an api service
	I want to be able to request the health of the Placing api

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
  
@CoreAPI
Scenario: Calling Get Health should return Status OK
	When I make a GET request to '/health' resource
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type


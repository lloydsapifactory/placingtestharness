// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.Submission
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("POST_Submission")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class POST_SubmissionFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "POST_Submission.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "POST_Submission", "\tIn order to test POST for a submission resource\r\n\tAs a user I would like to crea" +
                    "te [POST] a number of submissions, look for validation errors and expected outpu" +
                    "ts.", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 11
#line hidden
#line 12
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 13
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 14
 testRunner.And("I set security token and certificate for the \'AONBroker.Ema@LIMOSSDIDEV.onmicroso" +
                    "ft.com\' user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 15
 testRunner.And("I set the feature submission context from the json payload Data\\1_4_Model\\Submiss" +
                    "ion_1.json", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table571 = new TechTalk.SpecFlow.Table(new string[] {
                        "BrokerUserEmailAddress",
                        "ContractDescription",
                        "ClassOfBusinessCode"});
            table571.AddRow(new string[] {
                        "aonbroker.ema@limossdidev.onmicrosoft.com",
                        "TS090919A UMR Description",
                        "marine_hull"});
#line 16
 testRunner.And("I prepare the submission to be posted with the following information", ((string)(null)), table571, "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("1 Submission_POST_Broker_CreateSubmission - Broker creates a submission and verif" +
            "y the response")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.CategoryAttribute("SmokeTests")]
        public virtual void _1Submission_POST_Broker_CreateSubmission_BrokerCreatesASubmissionAndVerifyTheResponse()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI",
                    "SmokeTests"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("1 Submission_POST_Broker_CreateSubmission - Broker creates a submission and verif" +
                    "y the response", null, new string[] {
                        "CoreAPI",
                        "SmokeTests"});
#line 22
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 23
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 24
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 25
 testRunner.And("the request feature submission context matches the response submission data", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 26
 testRunner.And("validate the POST submission context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("2 Broker creates a submission, the submission reponse has a value for the field S" +
            "ubmissionVersionDescription")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _2BrokerCreatesASubmissionTheSubmissionReponseHasAValueForTheFieldSubmissionVersionDescription()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("2 Broker creates a submission, the submission reponse has a value for the field S" +
                    "ubmissionVersionDescription", null, new string[] {
                        "CoreAPI"});
#line 30
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 31
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 32
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 33
 testRunner.And("the request feature submission context matches the response submission data", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 34
 testRunner.And("the request feature submission context contains a value for \'SubmissionVersionDes" +
                        "cription\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 35
 testRunner.And("validate the POST submission context", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("3 Broker creates a submission with the insurance duration set.")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _3BrokerCreatesASubmissionWithTheInsuranceDurationSet_()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("3 Broker creates a submission with the insurance duration set.", null, new string[] {
                        "CoreAPI"});
#line 39
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 40
 testRunner.Given("for the submission the PeriodofCover only has an Insurance duration where the dur" +
                        "ation number is \'6\' and the duration unit is \'months\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 41
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 42
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 43
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("4 Broker creates a submission where the broker email address is in  uppercase")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _4BrokerCreatesASubmissionWhereTheBrokerEmailAddressIsInUppercase()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("4 Broker creates a submission where the broker email address is in  uppercase", null, new string[] {
                        "CoreAPI"});
#line 48
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 49
 testRunner.Given("for the submission the \'BrokerUserEmailAddress\' is \'AONBROKER.EMA@LIMOSSDIDEV.ONM" +
                        "ICROSOFT.COM\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 50
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 51
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 52
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("5 Broker creates a submission where the broker email address is in lowercase")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _5BrokerCreatesASubmissionWhereTheBrokerEmailAddressIsInLowercase()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("5 Broker creates a submission where the broker email address is in lowercase", null, new string[] {
                        "CoreAPI"});
#line 56
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 57
 testRunner.Given("for the submission the \'BrokerUserEmailAddress\' is \'aonbroker.ema@limossdidev.onm" +
                        "icrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 58
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 59
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 60
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("6 Broker do a POST without OPTIONAL fields, should return 201")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("ProgrammeDescription", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionVersionDescription", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeReference", null)]
        [NUnit.Framework.TestCaseAttribute("ContractReference", null)]
        [NUnit.Framework.TestCaseAttribute("OriginalPolicyholder", null)]
        [NUnit.Framework.TestCaseAttribute("RiskCountryCode", null)]
        public virtual void _6BrokerDoAPOSTWithoutOPTIONALFieldsShouldReturn201(string field, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("6 Broker do a POST without OPTIONAL fields, should return 201", null, @__tags);
#line 65
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 66
 testRunner.Given(string.Format("\'{0}\' for Submission is null", field), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 67
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 68
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 69
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 70
 testRunner.And("the request feature submission context matches the response submission data", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("8 Broker do a POST with OPTIONAL fields, should return 201")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionVersionDescription", "test change", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeDescription", "test change", null)]
        [NUnit.Framework.TestCaseAttribute("ContractDescription", "test change", null)]
        [NUnit.Framework.TestCaseAttribute("RiskRegionCode", "worldwide", null)]
        public virtual void _8BrokerDoAPOSTWithOPTIONALFieldsShouldReturn201(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("8 Broker do a POST with OPTIONAL fields, should return 201", null, @__tags);
#line 83
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 84
    testRunner.Given(string.Format("for the submission the \'{0}\' is \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 85
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 86
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 87
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 88
 testRunner.And("the request feature submission context matches the response submission data", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("9 Broker does a POST with OriginalPolicyHolder for reinsurance_contract, should r" +
            "eturn 201")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("OriginalPolicyholder", "test change", null)]
        public virtual void _9BrokerDoesAPOSTWithOriginalPolicyHolderForReinsurance_ContractShouldReturn201(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("9 Broker does a POST with OriginalPolicyHolder for reinsurance_contract, should r" +
                    "eturn 201", null, @__tags);
#line 101
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
                TechTalk.SpecFlow.Table table572 = new TechTalk.SpecFlow.Table(new string[] {
                            "BrokerUserEmailAddress",
                            "ContractDescription",
                            "ClassOfBusinessCode",
                            "OriginalPolyholder",
                            "ContractTypeCode"});
                table572.AddRow(new string[] {
                            "aonbroker.ema@limossdidev.onmicrosoft.com",
                            "TS090919A UMR Description",
                            "marine_hull",
                            "test",
                            "reinsurance_contract"});
#line 102
    testRunner.And("I prepare the submission to be posted with the following information", ((string)(null)), table572, "And ");
#line hidden
#line 105
 testRunner.Given(string.Format("for the submission the \'{0}\' is \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 106
 testRunner.And("the feature submission is saved for the scenario", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 107
 testRunner.When("I POST to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 108
 testRunner.Then("the response status code should be \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 109
 testRunner.And("the request feature submission context matches the response submission data", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

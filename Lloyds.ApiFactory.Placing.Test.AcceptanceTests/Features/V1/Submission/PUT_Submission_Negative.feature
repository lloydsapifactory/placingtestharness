#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: PUT_Submission_Negative
	In order to test PUT for a submission resource
	As a user I would like to do a [PUT] a submission and then perform PUT operations on it, look for validation errors and expected outputs.

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the CreatedDateTime from the posted submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [Submission_PUT_Broker_BrokerCodeMismatch] - BELL broker construct a PUT and change the Broker Code
	And I construct a PUT 
	And for the submission the 'BrokerCode' is '1009'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [Submission_PUT_Broker_SubmissionNotFound] BELL broker perform PUT on a submission that does not exist.
	And I construct a PUT 
	And for the submission the 'Id' is 'B1225QR090919AukFnMK_1'
	And for the submission the 'SubmissionReference' is 'B1225QR090919AukFnMK'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "404"
	And the response contains an error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 03 AON broker construct a PUT and change the SubmissionVersionDescription (The test will fail and return a 403, Forbidden, The submission was created by a Bell Broker and an AON broker is trying to update it)
	And I set security token and certificate for the 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the user email address for the feature user 'AONBroker.Tom @LIMOSSDIDEV.onmicrosoft.com' 
	And I set the broker code for the feature user as 1009
	And for the feature submission context add the broker data
	And I construct a PUT 
	And I set the SubmissionUniqueReference 
	And for the  submission the 'SubmissionVersionDescription' is 'QVD - The submission version  runs off insurance duration' 
	And for the submission the PeriodofCover only has an Insurance duration where the duration number is '18' and the duration unit is 'months'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403"

#---------------------------------------------------------------------------
		@CoreAPI
Scenario Outline: 04 Broker does a PUT without mandatory field, returns 400
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
   	And for the submission the '<field>' is ''
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range


	Examples:
	| field                  | 
	| SubmissionReference         | 
	| SubmissionVersionNumber     | 
	| BrokerUserEmailAddress | 
	| BrokerCode             | 
	| ContractTypeCode       | 
	| InsuredOrReinsured     | 
	| CoverTypeCode          | 
	| ClassOfBusinessCode    | 

	
#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 05 [Submission_PUT_Broker_ContractReferenceMismatch] - BELL broker constructs a PUT and change the ContractReference with a value than doesnt start with the structure B{BrokerCode}, returns 4xx
	And I construct a PUT 
	And for the submission the 'ContractReference' is '<value>'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	
	Examples:
	| value           |
	| B6666testChange |
	| C1225testChange |
	| TESTCHANGE      |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 [Submission_PUT_Broker_ProgrammeReferenceMismatch] BELL broker constructs a PUT and change the ProgrammeReference with a value than doesnt start with the structure B{BrokerCode}, returns 4xx
	And I construct a PUT 
	And for the submission the 'ProgrammeReference' is '<value>'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	
	Examples:
	| value           |
	| B6666testChange |
	| C1225testChange |
	| TESTCHANGE      |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 07 [Submission_PUT_Underwriter_UnderwriterNotAllowed] Underwriter construct a PUT, returns 400/403
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVDPUT17092019'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400"


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 08 [Submission_PUT_Broker_BrokerUserNotMemberOfDepartment] - Another broker from the different company does a PUT, returns 403
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'TestForPUT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be '400' or '403'
	And the response contains one of the following error messages
| ErrorMessage                 |
| BrokerDepartmentDoesNotExist |
| ResourceAccessDenied         |
| BrokerNotMemberOfDepartment  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 [Submission_PUT_Broker_BrokerNotAuthorised] - Another broker from the same company BUT different department does a PUT, returns 400/403
	And I retrieve the broker departments for 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And store the CreatedDateTime from the posted submission

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'TestForPUT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 11 [Submission_PUT_Broker_SubmissionNotInDraft] - Broker does a PUT for a Submission not in DRFT, returns 400
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	# MOVE TO NON DRFT
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And for the submission the 'SubmissionVersionDescription' is 'testPUtWhenInDraft'
    And the feature submission is saved for the scenario
	When I make a PUT request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "400"


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 12 [Submission_PUT_Broker_BrokerCodeNotInDepartment] - Broker does a PUT with invalid Broker department, returns 400
	And I construct a PUT 
    Given 	for the submission the 'BrokerCode' is '1250'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 13 [Submission_PUT_Broker_SubmissionReferenceMismatch] - Broker does a PUT with invalid submission reference, returns 400
	And I construct a PUT 
	Given for the feature submission context the 'SubmissionReference' is 'B1001QR090919A' plus a random string
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'InvalidSubmissionReference'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 14 [Submission_PUT_Broker_OriginalPolicyholderPresent] - Broker does a PUT with invalid OriginalPolicyholder, returns 400
	And I construct a PUT 
	Given for the submission the 'OriginalPolicyholder' is 'testValue'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'OriginalPolicyholderNotExpected'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [Submission_PUT_Broker_ClassOfBusinessCodeSupported] - Broker does a PUT with invalid ClassOfBusinessCode, returns 400
	And I construct a PUT 
	Given for the submission the 'ClassOfBusinessCode' is 'wrongValue'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'ClassOfBusinessCodeNotSupported'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 16 [Submission_PUT_Broker_RegionXORCountryPopulated] - Broker does a PUT with invalid RiskCountryCode, returns 400
	And I construct a PUT 
	Given for the submission the 'RiskCountryCode' is 'GB'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'EitherRiskRegionCodeOrRiskCountryCodesMustBePopulated'


	#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 17 [Submission_PUT_Broker_PeriodXORDurationPopulated] - Broker does a PUT with invalid PeriodOfCover, returns 400
	And I construct a PUT 
	Given for the submission the PeriodOfCover is invalid
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'EitherBothPeriodAndDurationPopulatedOrNonePopulated'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 18 [Submission_PUT_Broker_ExpiryBeforeInceptionDate] - Broker does a PUT where expiry date is before Inception date, returns 400
	And I construct a PUT 
	Given for the submission the PeriodofCover has an Insurance period where the inception date is '2019-01-01' and the expiry date is '2018-10-31'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'PeriodExpiryDateisNotGreaterOrEqualPeriodInceptionDate'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 [Submission_PUT_Broker_BrokerNotAuthorised] - Another broker from different company does a PUT, returns 400/403
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the CreatedDateTime from the posted submission

	And the response status code is "201"
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'TestForPUT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 20 Broker does a PUT on OriginalPolicyHolder where the contractType is NOT reinsurance_contract, returns 400
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
   	And for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be '403' or '400'

	Examples:
	| field                        | value           |
	| OriginalPolicyholder         | test change     |
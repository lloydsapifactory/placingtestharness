#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_Submission
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to post(create) and perform get(retrieve) via API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 (GetSubmissionById) BELL broker perform a GET on the submissions resource using the submission Id [The submission is created in the Given]
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should have value

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 (Get ALL) BELL broker perform a GET on the submissions resource
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	When I make a GET request to '/Submissions' resource 
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a link with proper value
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 (Get [contains filter]) BELL broker perform a GET on the submissions resource and add a filter on InsuredOrReinsured field 
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a 'contains' filter on 'InsuredOrReinsured' with a value 'AT01 Limited'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a link with proper value
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 05 (Get [contains pagination]) BELL broker perform a GET on the submissions resource and with pagination
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a filter on '_pageNum' with a value '20'
	And I add a filter on '_pageSize' with a value '5'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 06 (Get [contains suffix filter]) BELL broker perform a GET on the submissions resource and and add a filter on OriginalPolicyholder field 
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I add a 'suffix' filter on 'OriginalPolicyholder' with a random value from the response of submissions
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 07 (Get [contains prefix filter]) BELL broker perform a GET on the submissions resource and add a filter on ProgrammeDescription field 
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a 'prefix' filter on 'ProgrammeDescription' with a value 'AT01'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 08 (Get [uses contains filter]) BELL broker perform a GET on the submissions resource and and add a filter on ProgrammeDescription field 
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I add a 'contains' filter on 'ProgrammeDescription' with a value 'AT01'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 13 (Get [uses contains filter]) BELL broker perform a GET on the submissions resource and filter on ContractReference field 
	And I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 8 characters and save it to the context
	And I POST a set of 3 submissions from the data
	And I store the submission in the context
	And I add a 'contains' filter on 'ContractReference' with a random value from the response context of submission
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have no 'prev' link
	And collection of submissions should have no 'next' link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 14 (GetALL) When broker performs a GET ALL request, mandatory fields are being returned
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/Submissions' resource 
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	Then validate submission results contain the following fields
	| fieldname                  |
	| SubmissionReference        |
	| SubmissionVersionNumber    |
	| TechnicianUserEmailAddress |
	| CreatedDateTime            |
	| SubmissionStatusCode       |
	| BrokerDepartmentId         |
	| BrokerUserEmailAddress     |
	| BrokerCode                 |
	| ContractTypeCode           |
	| InsuredOrReinsured         |
	| CoverTypeCode              |
	| ClassOfBusinessCode        |
	| ProgrammeId                |
	| PeriodOfCover              |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 (GetById) When broker performs a GET ById request, mandatory fields are being returned
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource 
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And I get a random submission
	And I set the SubmissionUniqueReference 
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And validate submission context contain the following fields
	| fieldname                  |
	| SubmissionReference        |
	| SubmissionVersionNumber    |
	| TechnicianUserEmailAddress |
	| CreatedDateTime            |
	| SubmissionStatusCode       |
	| BrokerDepartmentId         |
	| BrokerUserEmailAddress     |
	| BrokerCode                 |
	| ContractTypeCode           |
	| InsuredOrReinsured         |
	| CoverTypeCode              |
	| ClassOfBusinessCode        |
	| ProgrammeId                |
	| PeriodOfCover              |

#---------------------------------------------------------------------------
	@CoreAPI
Scenario Outline: 16 (GetALL [uses contains, prefix filter]) Broker perform a GET on the submissions resource and adds a filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 8 characters and save it to the context
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I add a '<filter>' filter on '<field>' with a random value from the response of submission
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And validate submission results contain the value of <field>

Examples:
| field                        |  filter   |
| SubmissionVersionDescription |  contains |
| SubmissionVersionDescription |  prefix   |
| ContractDescription          |  contains |
| ContractDescription          |  prefix   |
| ContractReference            |  contains |
| ContractReference            |  prefix   |
| InsuredOrReinsured           |  contains |
| InsuredOrReinsured           |  prefix   |
| OriginalPolicyholder         |  contains |
| OriginalPolicyholder         |  prefix   |
| ProgrammeDescription         |  contains |
| ProgrammeDescription         |  prefix   |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 17 (GetALL [exact match on a field]) Broker perform a GET on the submissions resource and add an exact match filter  
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource
	And the response status code should be "200"
	And the response body should contain a collection of submissions
	And I add a '' filter on '<field>' with a random value from the response of submission
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And validate submission results contain the value of <field>

		Examples:
| field                   | 
| BrokerDepartmentId      | 
| SubmissionVersionNumber | 

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 17i (GetALL [exact match on a field ENUMS]) Broker perform a GET on the submissions resource and add an exact match filter  
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response body should contain a collection of submissions
	And I add a 'match' filter on '<field>' with a random value from the response of submissions
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And validate submission results contain the value of <field>

		Examples:
| field                  | value                                        |
| ContractTypeCode       | direct_insurance_contract                    |
| SubmissionStatusCode        | DRFT                                         |
| ClassOfBusinessCode    | marine_hull                                  |
| CoverTypeCode          | facultative_proportional                     |

#---------------------------------------------------------------------------
#Separating BrokerUserEmailAddress into a different scenario as it took a long time and we were havign time-outs
@CoreAPI
Scenario: 17a (GetALL [exact match on BrokerUserEmailAddress]) Broker perform a GET on the submissions resource and add an exact match filter  
	Given I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I make a GET request to '/Submissions' resource 
	When the response status code should be "200"
	Then the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	
	Given I add a filter on 'SubmissionReference' with a value from the response collection
	And I add a filter on 'BrokerUserEmailAddress' with a value from the response collection
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And validate submission results contain the value of BrokerUserEmailAddress

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18 (GetALL [multiple values filter]) Broker perform a GET on the submissions resource and adds a multiple values filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 8 characters and save it to the context
	And I POST a set of 2 submissions from the data
	And I store the submission in the context
	And I add a '' filter on '<field>' with values '<values>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than <number of found>
	And for submissions I validate I am getting the correct results for <field> with values of (<values>)

		Examples:
| field                  | values                                         | number of found |
| ClassOfBusinessCode    | marine_hull,marine_increased_value, livestock  | 2               |
| CoverTypeCode          | facultative_proportional,excess_of_loss        | 2               |
| SubmissionVersionNumber | 1,2    | 1              |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 19 (GetALL [uses orderby filter]) Broker perform a GET on the submissions resource and and orderby datetime
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a filter on '_order' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And the submission response is order by '<value>' 'ascending'


		Examples:
| value           |
| CreatedDateTime |
| _last_modified  |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 20 (GetALL not supported filter) When sending a filter that is not supported, return 404
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a '<filter>' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range

Examples:
| field                      | value                                        |
| ProgrammeId                | 34094                                        |
| RiskRegionCode             | worldwide                                    |
| RiskCountryCode            | worldwide                                    |
| PeriodOfCover              | test                                         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 23 (GetALL [exact match on SubmissionReference]) Broker perform a GET on the submissions resource and add an exact match filter  
	Given I set security token and certificate for the 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I make a GET request to '/Submissions' resource 
	When the response status code should be "200"
	Then the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	
	Given I add a filter on 'SubmissionReference' with a value from the response collection
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0

#---------------------------------------------------------------------------
	@CoreAPI
Scenario Outline: 24 (GetALL [multiple values filter - SubmissionReference]) Broker perform a GET on the submissions resource and adds a multiple values filter for SubmissionReference
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 2 submissions from the data
	And I add a 'match' filter on '<field>' with 2 values from the response collection of submissions
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total equal to 2

		Examples:
| field                  | 
| SubmissionReference         | 


#---------------------------------------------------------------------------
	@CoreAPI
Scenario Outline: 25 (GetALL [uses suffix filter]) Broker perform a GET on the submissions resource and adds a filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response body should contain a collection of submissions
	And I add a 'suffix' filter on '<field>' with a random value from the response of submissions
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a total greater than 0
	And validate submission results contain the value of <field>

Examples:
| field                   | 
| SubmissionVersionDescription | 
| ContractDescription     | 
| ContractReference       | 
| InsuredOrReinsured      | 
| OriginalPolicyholder    | 
| ProgrammeDescription    | 


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 26 (Get By Id) Broker from the same department and company can have access to a submission that another Broker POSTed
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "200"
	And I refresh the Submissioncontext with the response content
	And I store the submission context from the response

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 27 (GetALL) Broker from the same company and same department, returns 200
	And I log in as a broker '<BrokerUserEmailAddress1>'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission context from the response
	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have a total equal to 1

Examples:
| BrokerUserEmailAddress1                      | BrokerDepartmentName |
| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 28 Adding _select parameter will return results with the exact fields
	Given I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,programmeDescription'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submissions
	And collection of submissions should have at least 1 link
	And collection of submissions should have a link with proper value
	And collection of submissions should have a total greater than 0


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 29 (DATE before,after filtering): GETALL Submissions, before or after a datetime for CreatedDateTime, _last_modified
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/Submissions' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submissions collection
	And validate that the submissions results for the field '<field>' are <comparison> the date of <when>
	
	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | before     | one week  |
		| CreatedDateTime | after      | yesterday |
		| _last_modified  | after      | yesterday |
		| _last_modified  | before     | one week  |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 30 (DATE range Filter): GETALL submissions, with a date range 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/Submissions' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submissions collection
	And validate that the submissions results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           |
		| CreatedDateTime |
		| _last_modified  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 31 (DATE range adatetime..bdatetime): GETALL submissions, with a date range in format adatetime..bdatetime
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=yesterday and bdatetime=today
	When I make a GET request to '/Submissions' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submissions collection
	And validate that the submissions date results for the field '<field>' are  for yesterday
	Examples:
		| field           | 
		| CreatedDateTime | 
		| _last_modified  | 








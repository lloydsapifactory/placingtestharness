#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_Submission_Negative
	In order to place and view submissions
	As an onboarded broker or underwriter user
	I need to be able to post(create) and perform get(retrieve) via API

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario Outline: 01 ([InvalidFilterCriterion] [uses xyz filter])  - parameter=xyz(value)
	And I add a 'xyz' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "400"
	And the response contains a validation error 'InvalidFilterCriterion'

		Examples:
	| field                        | value |
	| ProgrammeDescription         | test  |
	| SubmissionVersionDescription | test  |
	| ContractReference            | test  |
	| ContractDescription          | test  |
	| OriginalPolicyholder         | test  |
	| InsuredOrReinsured           | test  |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 02 ([InvalidFilterCriterion] not supported filter with match) - parameter=match(value)
	And I add a 'match' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
| field                        | value |
| SubmissionVersionDescription | test  |
| ProgrammeDescription         | test  |
| ContractReference            | test  |
| ContractDescription          | test  |
| OriginalPolicyholder         | test  |
| InsuredOrReinsured           | test  |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 03 ([InvalidFilterCriterion] not supported filter with contains) - parameter=contains(value)
	And I add a 'contains' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
| field                   | value |
| SubmissionReference     | test  |
| SubmissionStatusCode    | test  |
| BrokerUserEmailAddress  | test  |
| ContractTypeCode        | test  |
| CoverTypeCode           | test  |
| ClassOfBusinessCode     | test  |
| SubmissionVersionNumber | 1     |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 05 ([InvalidFilterCriterion] not supported filter with prefix) - parameter=prefix(value)
	And I add a 'prefix' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
| field                  | value |
| SubmissionReference    | test  |
| SubmissionStatusCode   | test  |
| BrokerUserEmailAddress | test  |
| ContractTypeCode       | test  |
| CoverTypeCode          | test  |
| ClassOfBusinessCode    | test  |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 ([InvalidFilterCriterion] not supported filter with suffix) - parameter=suffix(value)
	And I add a 'suffix' filter on '<field>' with a value '<value>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

Examples:
| field                  | value |
| SubmissionReference    | test  |
| SubmissionStatusCode   | test  |
| BrokerUserEmailAddress | test  |
| ContractTypeCode       | test  |
| CoverTypeCode          | test  |
| ClassOfBusinessCode    | test  |

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 07 Submission_GET_Broker_BrokerNotAuthorised - Another broker from different company performs a GETById, returns 403
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I set security token and certificate for the 'aonbroker.ema@limossdidev.onmicrosoft.com' user
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 08 Submission_GET_Broker_BrokerNotAuthorised - Another broker from the same company but different department performs a GETById, returns 403
	Given I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I retrieve the broker departments for 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com' retrieved multiple departments
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 

	#And I select broker that is in a different broker department with the stored broker
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 09 UnderwriterNotAllowed - An Underwriter does a GetById to a Submission POSTed by a Broker, returns 403/400
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "403" or "400"


#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 10 UnderwriterNotAllowed - An Underwriter does a GetALL, returns 403/400
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a GET request to '/Submissions' resource 
	Then the response status code should be "403" or "400"


#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 11 - Broker does a GetById to a submission that is not valid, returns 400/404
	Given I log in as a the saved broker
	When I make a GET request to '/Submissions/666666' resource 
	Then the response status code should be "400" or "404"

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 12 SubmissionNotFound- Broker does a GetById to a submission that is valid BUT does not exist, returns 404
	Given I log in as a the saved broker
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	And I set a valid but not existing SubmissionUniqueReference 
	When I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 13 GetALL ([InvalidFilterCriterion] exact match INVALID filtering) - parameter=value
	Given I log in as a the saved broker
	And I add a filter on '<field>' with a value '<value>'
	When  I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
	| field                      | value |
	| TechnicianUserEmailAddress | test  |
	| BrokerCode                 | 2     |
	| ProgrammeId                | 2     |
	| ProgrammeReference         | test  |
	| RiskRegionCode             | test  |
	| RiskCountryCode            | test  |
	| PeriodOfCover              | test  |

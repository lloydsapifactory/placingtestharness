// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.Submission
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("GET_Submission_Negative")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class GET_Submission_NegativeFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "GET_Submission_Negative.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "GET_Submission_Negative", "\tIn order to place and view submissions\r\n\tAs an onboarded broker or underwriter u" +
                    "ser\r\n\tI need to be able to post(create) and perform get(retrieve) via API", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 11
#line hidden
#line 12
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 13
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 14
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 15
 testRunner.And("store the broker", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 16
 testRunner.And("I retrieve the broker departments for \'bellbroker.bella@limossdidev.onmicrosoft.c" +
                    "om\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 17
 testRunner.And("I POST a submission in the background on behalf of the broker \'bellbroker.bella@l" +
                    "imossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 ([InvalidFilterCriterion] [uses xyz filter])  - parameter=xyz(value)")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("ProgrammeDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionVersionDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("OriginalPolicyholder", "test", null)]
        [NUnit.Framework.TestCaseAttribute("InsuredOrReinsured", "test", null)]
        public virtual void _01InvalidFilterCriterionUsesXyzFilter_ParameterXyzValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 ([InvalidFilterCriterion] [uses xyz filter])  - parameter=xyz(value)", null, @__tags);
#line 21
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 22
 testRunner.And(string.Format("I add a \'xyz\' filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 23
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 24
 testRunner.Then("the response status code should be \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 25
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("02 ([InvalidFilterCriterion] not supported filter with match) - parameter=match(v" +
            "alue)")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionVersionDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractDescription", "test", null)]
        [NUnit.Framework.TestCaseAttribute("OriginalPolicyholder", "test", null)]
        [NUnit.Framework.TestCaseAttribute("InsuredOrReinsured", "test", null)]
        public virtual void _02InvalidFilterCriterionNotSupportedFilterWithMatch_ParameterMatchValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 ([InvalidFilterCriterion] not supported filter with match) - parameter=match(v" +
                    "alue)", null, @__tags);
#line 38
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 39
 testRunner.And(string.Format("I add a \'match\' filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 40
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 41
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 42
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("03 ([InvalidFilterCriterion] not supported filter with contains) - parameter=cont" +
            "ains(value)")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionStatusCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("BrokerUserEmailAddress", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("CoverTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ClassOfBusinessCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionVersionNumber", "1", null)]
        public virtual void _03InvalidFilterCriterionNotSupportedFilterWithContains_ParameterContainsValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("03 ([InvalidFilterCriterion] not supported filter with contains) - parameter=cont" +
                    "ains(value)", null, @__tags);
#line 55
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 56
 testRunner.And(string.Format("I add a \'contains\' filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 57
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 58
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 59
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("05 ([InvalidFilterCriterion] not supported filter with prefix) - parameter=prefix" +
            "(value)")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionStatusCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("BrokerUserEmailAddress", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("CoverTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ClassOfBusinessCode", "test", null)]
        public virtual void _05InvalidFilterCriterionNotSupportedFilterWithPrefix_ParameterPrefixValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("05 ([InvalidFilterCriterion] not supported filter with prefix) - parameter=prefix" +
                    "(value)", null, @__tags);
#line 73
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 74
 testRunner.And(string.Format("I add a \'prefix\' filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 75
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 76
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 77
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("06 ([InvalidFilterCriterion] not supported filter with suffix) - parameter=suffix" +
            "(value)")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("SubmissionStatusCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("BrokerUserEmailAddress", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ContractTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("CoverTypeCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ClassOfBusinessCode", "test", null)]
        public virtual void _06InvalidFilterCriterionNotSupportedFilterWithSuffix_ParameterSuffixValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("06 ([InvalidFilterCriterion] not supported filter with suffix) - parameter=suffix" +
                    "(value)", null, @__tags);
#line 90
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 91
 testRunner.And(string.Format("I add a \'suffix\' filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 92
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 93
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 94
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("07 Submission_GET_Broker_BrokerNotAuthorised - Another broker from different comp" +
            "any performs a GETById, returns 403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _07Submission_GET_Broker_BrokerNotAuthorised_AnotherBrokerFromDifferentCompanyPerformsAGETByIdReturns403()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("07 Submission_GET_Broker_BrokerNotAuthorised - Another broker from different comp" +
                    "any performs a GETById, returns 403", null, new string[] {
                        "CoreAPI"});
#line 107
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 108
 testRunner.And("the response status code is \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 109
 testRunner.And("I refresh the Submissioncontext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 110
 testRunner.And("I set the SubmissionUniqueReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 111
 testRunner.Given("I set security token and certificate for the \'aonbroker.ema@limossdidev.onmicroso" +
                        "ft.com\' user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 112
 testRunner.When("I make a GET request to \'/Submissions/{submissionUniqueReference}\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 113
 testRunner.Then("the response status code should be \'403\' or \'400\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 114
 testRunner.And("the response contains a validation error \'ResourceAccessDenied\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("08 Submission_GET_Broker_BrokerNotAuthorised - Another broker from the same compa" +
            "ny but different department performs a GETById, returns 403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _08Submission_GET_Broker_BrokerNotAuthorised_AnotherBrokerFromTheSameCompanyButDifferentDepartmentPerformsAGETByIdReturns403()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("08 Submission_GET_Broker_BrokerNotAuthorised - Another broker from the same compa" +
                    "ny but different department performs a GETById, returns 403", null, new string[] {
                        "CoreAPI"});
#line 118
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 119
 testRunner.Given("I log in as a broker \'aonbroker.gar@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 120
 testRunner.And("I retrieve the broker departments for \'aonbroker.gar@limossdidev.onmicrosoft.com\'" +
                        "", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 121
 testRunner.And("I POST a submission in the background on behalf of the broker \'aonbroker.gar@limo" +
                        "ssdidev.onmicrosoft.com\' retrieved multiple departments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 122
 testRunner.And("the response status code is \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 123
 testRunner.And("I refresh the Submissioncontext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 124
 testRunner.And("I set the SubmissionUniqueReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 127
 testRunner.Given("I log in as a broker \'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 128
 testRunner.When("I make a GET request to \'/Submissions/{submissionUniqueReference}\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 129
 testRunner.Then("the response status code should be \'403\' or \'400\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 130
 testRunner.And("the response contains a validation error \'ResourceAccessDenied\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("09 UnderwriterNotAllowed - An Underwriter does a GetById to a Submission POSTed b" +
            "y a Broker, returns 403/400")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _09UnderwriterNotAllowed_AnUnderwriterDoesAGetByIdToASubmissionPOSTedByABrokerReturns403400()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("09 UnderwriterNotAllowed - An Underwriter does a GetById to a Submission POSTed b" +
                    "y a Broker, returns 403/400", null, new string[] {
                        "CoreAPI"});
#line 134
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 135
 testRunner.And("the response status code is \"201\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 136
 testRunner.And("I refresh the Submissioncontext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 137
 testRunner.And("I set the SubmissionUniqueReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 138
 testRunner.Given("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 139
 testRunner.When("I make a GET request to \'/Submissions/{submissionUniqueReference}\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 140
 testRunner.Then("the response status code should be \"403\" or \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("10 UnderwriterNotAllowed - An Underwriter does a GetALL, returns 403/400")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _10UnderwriterNotAllowed_AnUnderwriterDoesAGetALLReturns403400()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("10 UnderwriterNotAllowed - An Underwriter does a GetALL, returns 403/400", null, new string[] {
                        "CoreAPI"});
#line 145
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 146
 testRunner.Given("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 147
 testRunner.When("I make a GET request to \'/Submissions\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 148
 testRunner.Then("the response status code should be \"403\" or \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("11 - Broker does a GetById to a submission that is not valid, returns 400/404")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _11_BrokerDoesAGetByIdToASubmissionThatIsNotValidReturns400404()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("11 - Broker does a GetById to a submission that is not valid, returns 400/404", null, new string[] {
                        "CoreAPI"});
#line 153
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 154
 testRunner.Given("I log in as a the saved broker", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 155
 testRunner.When("I make a GET request to \'/Submissions/666666\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 156
 testRunner.Then("the response status code should be \"400\" or \"404\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("12 SubmissionNotFound- Broker does a GetById to a submission that is valid BUT do" +
            "es not exist, returns 404")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _12SubmissionNotFound_BrokerDoesAGetByIdToASubmissionThatIsValidBUTDoesNotExistReturns404()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("12 SubmissionNotFound- Broker does a GetById to a submission that is valid BUT do" +
                    "es not exist, returns 404", null, new string[] {
                        "CoreAPI"});
#line 160
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 161
 testRunner.Given("I log in as a the saved broker", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 162
 testRunner.And("I refresh the Submissioncontext with the response content", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 163
 testRunner.And("I set the SubmissionUniqueReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 164
 testRunner.And("I set a valid but not existing SubmissionUniqueReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 165
 testRunner.When("I make a GET request to \'/Submissions/{submissionUniqueReference}\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 166
 testRunner.Then("the response status code should be \"404\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 167
 testRunner.And("the response contains a validation error \'ResourceDoesNotExist\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("13 GetALL ([InvalidFilterCriterion] exact match INVALID filtering) - parameter=va" +
            "lue")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("TechnicianUserEmailAddress", "test", null)]
        [NUnit.Framework.TestCaseAttribute("BrokerCode", "2", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeId", "2", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeReference", "test", null)]
        [NUnit.Framework.TestCaseAttribute("RiskRegionCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("RiskCountryCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("PeriodOfCover", "test", null)]
        public virtual void _13GetALLInvalidFilterCriterionExactMatchINVALIDFiltering_ParameterValue(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("13 GetALL ([InvalidFilterCriterion] exact match INVALID filtering) - parameter=va" +
                    "lue", null, @__tags);
#line 171
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 172
 testRunner.Given("I log in as a the saved broker", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 173
 testRunner.And(string.Format("I add a filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 174
 testRunner.When("I make a GET request to \'/Submissions\' resource and append the filter(s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 175
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 176
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: Submission_SubmissionVersionNumber
	In order to test POST for a submission resource
	As a user I would like to create [POST] a number of submissions, look for validation errors and expected outputs.


Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And set up version number Submissions creation

	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I prepare the submission context with the following information
         | BrokerUserEmailAddress                       | ClassOfBusinessCode | CoverTypeCode            | RiskRegionCode|
         | bellbroker.bella@limossdidev.onmicrosoft.com | marine_hull         | facultative_proportional | Test          |
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 1 POST Placing slip inherited - Broker creates submissions with different version number, all subsequent submissions have the same PLACING document 
	And for the feature submission documents context the 'SubmissionVersionNumber' is 1
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And for the submission documents the 'FileName' is 'MRC_Placingslip.docx'
	And for the submission documents the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\MRC_Placingslip.docx to the request
	And the submission document is saved for the scenario
	And I POST to '/SubmissionDocuments' resource with multipart

	#POST Submissions with subsequent version number and the same PLACING Document
	When I POST 3 more submissions with different version for stored Submission
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response body should contain a collection of submissions
    And collection of submissions should have a total equal to 4
	And I reset all the filters

	And I set the SubmissionReference
	And I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	Then submission documents should have 1 document

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 2 POST Placing and supporting document inherited - Broker creates submissions with different version number, all subsequent submissions have the same PLACING and supporting document and submission documents are created for these submissions
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	| sample_clientcorr.docx | 2          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | correspondence_client | Client Correspondence | Data\supporting_documents\sample_clientcorr.docx	| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	#POST Submission with subsequent version number and a supporting document
	When I POST 1 more submissions with different version for stored Submission
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response body should contain a collection of submissions
    And collection of submissions should have a total equal to 2

	And I set the SubmissionReference
	And I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission documents collection
	Then submission documents should have 3 document

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 3 DELETE [SubmissionNotInDraft] - DELETE Submission, when there are Submissions with different version number and one of them is not in DRFT, return 403
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	#POST subsequent version number Submissions with the same Documents
	And I POST 1 more submissions with different version for stored Submission

	# MOVE TO NON DRFT
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And for the submission the 'SubmissionVersionNumber' is '1'
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	# DELETE THE SUBMISSION, WHILE THERE IS ONE NOT IN DRFT
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be in "4xx" range

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 5 [Submission_POST_Broker_Subsequent_BrokerDepartmentDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different broker department than the initial version, returns 400
	And I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I prepare the submission context with the following information
         | BrokerUserEmailAddress                    | ClassOfBusinessCode | CoverTypeCode            | RiskRegionCode|
         | aonbroker.gar@limossdidev.onmicrosoft.com | marine_hull         | facultative_proportional | Test          |
	And I POST a submission in the background on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json

	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submission context the BrokerDepartmentId is different
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'BrokerDepartmentMismatchPreviousSubmissionVersion'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 6 [Submission_POST_Broker_Subsequent_BrokerCodeDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different broker code than the initial version, returns 400
	And I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I prepare the submission context with the following information
         | BrokerUserEmailAddress                    | ClassOfBusinessCode | CoverTypeCode            | RiskRegionCode|
         | aonbroker.gar@limossdidev.onmicrosoft.com | marine_hull         | facultative_proportional | Test          |
	And I POST a submission in the background on behalf of the broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json

	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submission context the BrokerCode is different
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'BrokerCodeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 7 [Submission_POST_Broker_Subsequent_ContractTypeCodeDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different contract type code than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submissions the 'ContractTypeCode' is 'reinsurance_contract'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'ContractTypeMismatchPreviousSubmissionVersion'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 8 [Submission_POST_Broker_Subsequent_InsuredOrReinsuredDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different InsuredOrReinsured than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submissions the 'InsuredOrReinsured' is 'test'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'InsuredOrReinsuredMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 9 [Submission_POST_Broker_Subsequent_OriginalPolicyholderDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different OriginalPolicyHolder for reinsurance_contract than the initial version, returns 400
	And I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And I prepare the submission context with the following information
     | BrokerUserEmailAddress                    | ContractDescription       | ClassOfBusinessCode | OriginalPolyholder | ContractTypeCode  |  
	| aonbroker.ema@limossdidev.onmicrosoft.com | TS090919A UMR Description | marine_hull         | test               | reinsurance_contract |
	And I POST a submission in the background on behalf of the broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And I save the submission response to the submissioncontext
	And I set the SubmissionReference and the SubmissionDocumentId
	And I prepare submission documents creation from Data\1_2_Model\SubmissionDocument_1.json

	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submissions the 'OriginalPolicyholder' is 'SomeTestValue'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'OriginalPolicyholderMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 10 [Submission_POST_Broker_Subsequent_CoverTypeCodeDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different CoverTypeCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submissions the 'CoverTypeCode' is 'excess_of_loss'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'CoverTypeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 [Submission_POST_Broker_Subsequent_RiskRegionCodeNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different RiskRegionCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submission the 'RiskRegionCode' is 'europe'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'RegionCodeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 12 [Submission_POST_Broker_Subsequent_RiskCountryCodeDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different RiskCountryCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submission the 'RiskRegionCode' is ''
	And for the submission the 'RiskCountryCode' is 'GB'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'CountryCodeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 13 [Submission_POST_Broker_Subsequent_ClassOfBusinessDoesNotMatchPreviousVersion] - Broker creates submissions with different version number BUT different ClassOfBusinessCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I prepare 1 more submissions with different version for stored Submission
	And for the submission the 'ClassOfBusinessCode' is 'livestock'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'ClassOfBusinessMismatchPreviousSubmissionVersion'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 14 [Submission_POST_Broker_CreateSubsequentSubmission] Broker creates subsequent submissions with different version number
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	#POST Submission with subsequent version number and a supporting document
	When I POST 1 more submissions with different version for stored Submission
	Then the response status code should be in "2xx" range
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '2'

	#VALIDATE
	And I set the SubmissionUniqueReference 
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '2'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [Submission_PUT_Broker_UpdateSubsequentSubmission] Broker updates a subsequent submission with different version number, returns 201
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	#POST Submission with subsequent version number and a supporting document
	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'testSubqsequentPUT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '2'

	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response HTTP header ContentType should be to application/json type
	And validate submission result contain the value of SubmissionVersionDescription
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '2'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [Submission_PUT_Broker_Subsequent_ContractTypeCodeDoesNotMatchPreviousVersion] - Broker updates submission with different version number BUT different contract type code than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'ContractTypeCode' is 'reinsurance_contract'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'ContractTypeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 17 [Submission_PUT_Broker_Subsequent_InsuredOrReinsuredDoesNotMatchPreviousVersion] - Broker updates submission with different InsuredOrReinsured than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'InsuredOrReinsured' is 'TEST'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'InsuredOrReinsuredMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 18 [Submission_PUT_Broker_Subsequent_OriginalPolicyholderDoesNotMatchPreviousVersion] - Broker updates submission with different OriginalPolicyholder than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submissions the 'OriginalPolicyholder' is 'SomeTestValue'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'OriginalPolicyholderNotExpected'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 [Submission_PUT_Broker_Subsequent_CoverTypeCodeDoesNotMatchPreviousVersion] - Broker updates submission with different CoverTypeCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submissions the 'CoverTypeCode' is 'excess_of_loss'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'CoverTypeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 20 [Submission_PUT_Broker_Subsequent_RiskRegionCodeNotMatchPreviousVersion] - Broker updates submission with different RiskRegionCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'RiskRegionCode' is 'europe'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'RegionCodeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 21 [Submission_PUT_Broker_Subsequent_RiskCountryCodeDoesNotMatchPreviousVersion] - Broker updates submission with different RiskCountryCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'RiskRegionCode' is ''
	And for the submission the 'RiskCountryCode' is 'GB'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'CountryCodeMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 23 [Submission_PUT_Broker_Subsequent_ClassOfBusinessDoesNotMatchPreviousVersion] - Broker updates submission with different ClassOfBusinessCode than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the PeriodOfCover is different
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'PeriodOfCoverMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 24 [Submission_PUT_Broker_Subsequent_ClassOfBusinessDoesNotMatchPreviousVersion] - Broker updates submission with different PeriodOfCover than the initial version, returns 400
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext

	And I POST 1 more submissions with different version for stored Submission
	And store the CreatedDateTime from the posted submission
	And I reset all the filters

	And I construct a PUT 
	And for the submission the 'ClassOfBusinessCode' is 'livestock'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains a validation error 'ClassOfBusinessMismatchPreviousSubmissionVersion'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 25 Broker creates submissions with different version number - Submission_POST_Broker_CreateSubsequentSubmission
	Given I log in as a broker '<BrokerUserEmailAddress>'
	And I retrieve the broker departments for '<BrokerUserEmailAddress>'
	When I POST <Number of Submissions> submissions with different version in the background on behalf of the broker '<BrokerUserEmailAddress>'
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of submissions
	And collection of submissions should have a total equal to <Number of Submissions>
	And collection of submissions should have subsequent version numbers

	Examples:
	| BrokerUserEmailAddress                       | Number of Submissions |
	| aonbroker.tom@limossdidev.onmicrosoft.com    | 3                     |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | 2                     |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 28 Order by last_modified(POST): Creating Submissions with different version number and do a GETALL order by last modified, they are are ordered by the last Submission created
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	#POST Submission with subsequent version number and a supporting document
	And I POST 3 more submissions with different version for stored Submission
	And I reset all the filters

	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submissions


	And the submissions response is ordered by '_last_modified' of the field 'SubmissionVersionNumber' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| 1                      | 4                       |
	| 2                      | 3                       |
	| 3                      | 2                       |
	| 4                      | 1                       |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |



#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 29 Order by last_modified(PUT): Creating Submissions with different version number and UPDATE one, do a GETALL order by last modified, they are are ordered by the last Submission created
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	And I POST 3 more submissions with different version for stored Submission
	And I reset all the filters

	# UPDATE VERSION NUMBER 2
	And select submission with SubmissionVersionNumber 2
	And store the CreatedDateTime from submission context
	And for the submission the 'SubmissionVersionDescription' is 'change to Version2'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	And I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	And I reset all the filters

	And I set the SubmissionUniqueReference from submission update
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I add a filter on '_order' with a value '<fieldValue>'
	When I make a GET request to '/Submissions' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submissions


	And the submissions response is ordered by '_last_modified' of the field 'SubmissionVersionNumber' with the following order as <sortingOrder>
	| valueOfField_ascending | valueOfField_descending |
	| 1                      | 2                       |
	| 3                      | 4                       |
	| 4                      | 3                       |
	| 2                      | 1                       |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 30 [Submission_POST_Broker_CreateSubsequentSubmission] Broker creates subsequent submissions with different version number
	And for the submission document context the 'SubmissionReference' is SubmissionReferenceforSubmissionDocument
	And I POST a set of submission documents from the data
	| FileName               | DocumentId | FileMIMEType															| DocumentType          | DocumentDescription   | FilePath											| ExpectedOutput |
 	| MRC_Placingslip.docx	 | 1          | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Placing Slip          | Data\mrc_documents\sample_mrc.docx				| 201            |
	And I set the SubmissionReference
	And I clear down test context
	And I save the submission document id response to the submissiondocumentidecontext
	
	#POST Submission with subsequent version number and a supporting document
	When I POST 1 more submissions with different version for stored Submission
	Then the response status code should be in "2xx" range
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '2'

	#POST Submission with subsequent version number and a supporting document
	When I POST 1 more submissions with different version for stored Submission
	Then the response status code should be in "2xx" range
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '3'

	#POST Submission with subsequent version number and a supporting document
	When I POST 1 more submissions with different version for stored Submission
	Then the response status code should be in "2xx" range
	And I refresh the Submissioncontext with the response content
	And validate submission result contain item added on 'SubmissionVersionNumber' with a value '4'


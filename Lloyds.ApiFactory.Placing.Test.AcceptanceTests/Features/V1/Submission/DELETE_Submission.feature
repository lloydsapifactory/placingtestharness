#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: DELETE_Submission
	In order to be able to remove any Submissions
	As an onboarded broker or underwriter user
	I need to be able to do a DELETE

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 1 DeleteSubmission - Delete a Submission, returns 201
	Given I set security token and certificate for the 'bellbroker.bella@limossdidev.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                       | ProgrammeDescription | ContractDescription       | ClassOfBusinessCode |
	| bellbroker.bella@limossdidev.onmicrosoft.com | B1009PR001QR090      | TS090919A UMR Description | marine_hull         |
	And I POST to '/Submissions' resource
	And I store the submission context from the response
	And the request feature submission context matches the response submission data
	And I set the SubmissionUniqueReference 
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be in "2xx" range
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response status code should be "404"


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 2 Submission_DELETE_Broker_SubmissionNotFound - Delete a Submission that doesnt exist, returns 400
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When I make a DELETE request to '/Submissions/66666666' resource 
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 3 Delete a Submission with no authorisation, returns 401
	When I make a DELETE request to '/Submissions/66666666' resource 
	And the response status code should be "401"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 4 Submission_DELETE_Broker_BrokerNotAuthorised - Delete a Submission as another unauthorised broker from another company, returns 400
	Given I set security token and certificate for the 'bellbroker.bella@limossdidev.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                       | ProgrammeDescription | ContractDescription       | ClassOfBusinessCode |
	| bellbroker.bella@limossdidev.onmicrosoft.com | B1009PR001QR090      | TS090919A UMR Description | marine_hull         |
	And I POST to '/Submissions' resource
	And I store the submission context from the response
	And the request feature submission context matches the response submission data
	And I set the SubmissionUniqueReference 
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 4a Submission_DELETE_Broker_BrokerNotAuthorised - Delete a Submission as another unauthorised broker from same company but different department, returns 400
	Given I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                       | ProgrammeDescription | ContractDescription       | ClassOfBusinessCode |
	| AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com | B1009PR001QR090      | TS090919A UMR Description | marine_hull         |
	And I POST to '/Submissions' resource
	And I store the submission context from the response
	And the request feature submission context matches the response submission data
	And I set the SubmissionUniqueReference 
	And I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be '403' or '400'
	And the response contains a validation error 'ResourceAccessDenied'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 5 Submission_DELETE_Broker_SubmissionNotInDraft - Delete a Submission thats is not in DRFT, returns 400
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	#POST Submission, SubmissionDocuments
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	#POST SubmissionUnderwriter
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 

	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionNotInDraft'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 6 UnderwriterNotAllowed - Delete a Submission as an underwriter, returns 403
	Given I set security token and certificate for the 'bellbroker.bella@limossdidev.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                       | ProgrammeDescription | ContractDescription       | ClassOfBusinessCode |
	| bellbroker.bella@limossdidev.onmicrosoft.com | B1009PR001QR090      | TS090919A UMR Description | marine_hull         |
	And I POST to '/Submissions' resource
	And I store the submission context from the response
	And the request feature submission context matches the response submission data
	And I set the SubmissionUniqueReference 

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a DELETE request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "403" or "400"
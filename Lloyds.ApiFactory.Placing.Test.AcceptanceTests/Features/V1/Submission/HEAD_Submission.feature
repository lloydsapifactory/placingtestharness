#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_Submission
	In order to be able to test getting any Submissions
	As an onboarded broker or underwriter user
	I need to be able to do a HEAD

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
#---------------------------------------------------------------------------
@CoreAPI
Scenario: 1 [Submission_HEAD_Broker_HeadSubmissionById] - For a valid submission, I want to be able to do a HEAD returning 2xx
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I make a GET request to '/Submissions' resource 
	And the response body should contain a collection of submissions
	And I get a random submission
	And I set the SubmissionUniqueReference 
	When I make a HEAD request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be in "2xx" range

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 2 Submission_HEAD_Broker_SubmissionNotFound - For a submission that is valid BUT does not exist, returns 404
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I log in as a the saved broker
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	And I set a valid but not existing SubmissionUniqueReference 
	When I make a HEAD request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "404"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 3 Submission_HEAD_Broker_BrokerNotAuthorised - Another broker from different company performs a GETById, returns 403/400
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I set security token and certificate for the 'aonbroker.ema@limossdidev.onmicrosoft.com' user
	When I make a HEAD request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be '403' or '400' or '404'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 4 Submission_HEAD_Underwriter_UnderwriterNotAllowed - For a Submission POSTed by a Broker, returns 400/403
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I make a HEAD request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "403" or "400"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 5 Submission_HEAD_Broker_BrokerNotAuthorised - Another broker from same company BUT different department performs a GETById, returns 400/403
	And I retrieve the broker departments for 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And the response status code is "201"
	And I refresh the Submissioncontext with the response content
	And I set the SubmissionUniqueReference 
	Given I set security token and certificate for the 'aonbroker.ema@limossdidev.onmicrosoft.com' user
	When I make a HEAD request to '/Submissions/{submissionUniqueReference}' resource 
	Then the response status code should be "403" or "400" or "404"
#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity 
Feature: PUT_Submission
	In order to test PUT for a submission resource
	As a user I would like to create [POST] a submission and then perform PUT operations on it, look for validation errors and expected outputs.

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the CreatedDateTime from the posted submission

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 01 [Submission_PUT_Broker_UpdateSubmission] - PUT and change the SubmissionVersionDescription
	Given I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'update description from specflow'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response HTTP header ContentType should be to application/json type
	And validate submission result contain item added on 'SubmissionVersionDescription' with a value 'update description from specflow'
#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [Submission_PUT_Broker_UpdateSubmission] PUT and change the ContractTypeCode
	Given I construct a PUT 
	And for the submission the 'ContractTypeCode' is 'reinsurance_contract'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [Submission_PUT_Broker_UpdateSubmission] PUT and change the PeriodofCover
	Given I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
	And for the submission the PeriodofCover only has an Insurance duration where the duration number is '18' and the duration unit is 'months'
    And the feature submission  is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 technician construct a PUT and change the SubmissionVersionDescription
	Given I set security token and certificate for the 'BellTechnician.hulk@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the user email address for the feature user 'BellTechnician.hulk@LIMOSSDIDEV.onmicrosoft.com' 
	And I set the broker code for the feature user as 1225
	And for the feature submission context add the broker data
    And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 Broker does a PUT on mandatory fields, updates the field and returns 200
	Given I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
   	And for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And validate submission result with the field <field> with value equal to <value>

	Examples:
	| field                  | value                                         |
	| BrokerUserEmailAddress | BellBroker.Morgan@LIMOSSDIDEV.onmicrosoft.com |
	| ContractTypeCode       | direct_insurance_contract                     |
	| InsuredOrReinsured     | AT01pan Limited                               |
	| CoverTypeCode          | facultative_proportional                      |
	| ClassOfBusinessCode    | marine_hull                                   |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 06 Broker does a PUT on alterable fields, updates the field and returns 200
	Given I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
   	And for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And validate submission result with the field <field> with value equal to <value>

	Examples:
	| field                        | value           |
	| SubmissionVersionDescription | test change     |
	| ProgrammeReference           | B1225testChange |
	| ProgrammeDescription         | test change     |
	| ContractReference            | B1225testChange |
	| ContractDescription          | test change     |
	| RiskRegionCode               | worldwide       |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 07 Broker does a PUT on OriginalPolicyHolder where reinsurance_contract, updates the field and returns 200
	Given I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                    | ContractDescription       | ClassOfBusinessCode | OriginalPolyholder | ContractTypeCode  |  
	| aonbroker.ema@limossdidev.onmicrosoft.com | TS090919A UMR Description | marine_hull         | test               | reinsurance_contract |
	And I POST to '/Submissions' resource
	And store the CreatedDateTime from the posted submission
	And I construct a PUT 
	And for the submission the 'SubmissionVersionDescription' is 'QVD - The submission version runs off insurance duration'
   	And for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I make a GET request to '/Submissions/{submissionUniqueReference}' resource 
	And the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And validate submission result with the field <field> with value equal to <value>

	Examples:
	| field                        | value           |
	| OriginalPolicyholder         | test change     |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 08 Broker construct a PUT and change the SubmissionDialogueId
	Given I construct a PUT 
	And for the submission the 'SubmissionStatusCode' is 'SENT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 Broker updates submissions with different version number - UpdateSubsequentSubmission
	And I log in as a broker 'aonbroker.tom@limossdidev.onmicrosoft.com'
	And I retrieve the broker departments for 'aonbroker.tom@limossdidev.onmicrosoft.com'
	And I POST 2 submissions with different version in the background on behalf of the broker 'aonbroker.tom@limossdidev.onmicrosoft.com'
	And for submissions I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I make a GET request to '/Submissions' resource and append the filter(s)
	And the response body should contain a collection of submissions

	Given get random submission from the submission collection
	And store the CreatedDateTime from submission context
	And for the submission the 'SubmissionVersionDescription' is 'testSubsequentVersionNumberPUT'
    And the feature submission is saved for the scenario
	And I set the SubmissionUniqueReference from submission update
	When I PUT to '/Submissions/{submissionUniqueReference}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "200"
	And I refresh the Submissioncontext with the response content
	And validate submission result with the field SubmissionVersionDescription with value equal to testSubsequentVersionNumberPUT

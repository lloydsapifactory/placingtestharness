#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_Submission_Negative
	In order to test POST for a submission resource
	As a user I would like to do a [POST] a number of submissions, look for validation errors and expected outputs.


Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                    |  ContractDescription       | ClassOfBusinessCode | 
	| aonbroker.ema@limossdidev.onmicrosoft.com |  TS090919A UMR Description | marine_hull         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 1 Broker creates a submission where the submission reference has an underscore in it. The request will fail [BadRequest] 
	Given for the submission the 'SubmissionReference' is 'ACB_11'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                                                                 |
	| [ContainsSpecialChar] |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 2 Broker creates a submission where the submission reference does not include the broker code 1009. The request will fail [BadRequest]
	Given for the feature submission context the 'SubmissionReference' is 'B1001QR090919A' plus a random string
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages	
	| ErrorMessage                                                  |
	| The value must match the following regular expression pattern |
	| [InvalidSubmissionReference]                                  |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 3 Broker creates a submission where the SubmissionReference is not alphanumeric (has a whitespace)
	Given for the submission the 'SubmissionReference' is 'B1009 QR090919A'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
    And the response contains one of the following error messages for the field 'SubmissionReference'
	| ErrorMessage         |
	| [ContainsWhitespace] |

#---------------------------------------------------------------------------
@CoreAPI   
Scenario: 4 Submission_POST_Broker_SubmissionReferenceMismatch - Broker creates a submission where the submission reference does not include the broker code 2009
	Given for the feature submission context the 'SubmissionReference' is 'B1001QR090919A' plus a random string
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
| ErrorMessage                                                             |
| The value must match the following regular expression pattern - ^B1009.* |
| [InvalidSubmissionReference]                                             |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 5 Broker creates a submission where the SubmissionReference has a length greater than 50 characters
	Given for the submission the 'SubmissionReference' is 'B1009Q123456789123456789123456789123456789123456ABCDEFGH'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
   And the response contains one of the following error messages for the field 'SubmissionReference'
	| ErrorMessage                                                                     |
	| StringTooLong |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 7 Broker create a submission with the insurance duration set and the period duration set
	Given for the submission the PeriodofCover has an Insurance period where the inception date is '2019-01-01' and the expiry date is '2019-12-31'
	And for the submission the PeriodofCover has an Insurance duration where the duration number is '6' and the duration unit is 'months'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 8 Broker creates a submission where the programme reference is more than 50 chars
	Given for the submission the 'ProgrammeReference' is 'B1009PR001QR090919ABQR090919ABQR090919ABQR090919ABQR090919ABBQR090919ABBQR090919ABBQR090919ABCDEAA'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'StringTooLong' for the field "ProgrammeReference"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 9 Broker creates a submission where the programme description is more than 100 chars
	Given for the submission the 'ProgrammeDescription' is 'B1009PR001QR090919ABQR090919B1009PR001QR090919ABQR090919B1009PR001QR090919ABQR090919B1009PR001QR090919ABQR090919'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'StringTooLong' for the field "ProgrammeDescription"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 10 Submission_POST_Broker_BrokerCodeNotInDepartment - Broker creates a submission where the broker code belongs to another broker 
	Given for the submission the 'BrokerCode' is '1250'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 Broker creates a submission where the broker email address is incorrect 
	Given for the submission the 'BrokerUserEmailAddress' is 'AONBroker.EmaTest@LIMOSSDIDEV.onmicrosoft.com'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                                                    |
	| Broker and Technician do not have access to the same department |
	| BrokerNotMemberOfDepartment                                     |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 12 When broker creates a submission there are mandatory fields need to be provided, otherwise return 400
	Given for the feature submission context the '<mandatoryfield>' is 'null'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	
	Examples:
		| inputFile                   | mandatoryfield             |
		| Data\1_4_Model\Submission_Create_1.json | SubmissionReference             |
		| Data\1_4_Model\Submission_Create_1.json | BrokerDepartmentId         |
		| Data\1_4_Model\Submission_Create_1.json | BrokerUserEmailAddress     |
		| Data\1_4_Model\Submission_Create_1.json | BrokerCode                 |
		| Data\1_4_Model\Submission_Create_1.json | ContractTypeCode           |
		| Data\1_4_Model\Submission_Create_1.json | InsuredOrReinsured         |
		| Data\1_4_Model\Submission_Create_1.json | CoverTypeCode              |
		| Data\1_4_Model\Submission_Create_1.json | ClassOfBusinessCode        |
		| Data\1_4_Model\Submission_Create_1.json | PeriodOfCover              |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 13 Min,Max Chars - Broker cannot create a submission with fields having chars more than the specs
	Given for the feature submission context the '<field>' with a random value <assert> <Characters>
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be in "4xx" range
	
	Examples:
		| field                   | assert    | rule | Characters |
		| SubmissionReference          | more than | max  | 50         |
		| SubmissionReference          | less than | min  | 1          |
		| SubmissionVersionDescription | more than | max  | 100        |
		| SubmissionVersionDescription | less than | min  | 1          |
		| BrokerUserEmailAddress  | more than | max  | 100        |
		| BrokerCode              | more than | max  | 4          |
		| BrokerCode              | less than | min  | 4          |
		| ProgrammeReference      | more than | max  | 50         |
		| ProgrammeDescription    | more than | max  | 100        |
		| ContractReference       | more than | max  | 17         |
		| ContractDescription     | more than | max  | 100        |
		| InsuredOrReinsured      | less than | min  | 2          |
		| InsuredOrReinsured      | more than | max  | 250        |
		| OriginalPolicyholder    | more than | max  | 105        |

#---------------------------------------------------------------------------		
@CoreAPI 
Scenario Outline: 14 - String character rules - When broker creates a submission where the string value has invalid chars, 400 is being returned
	Given for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be in "4xx" range

	Examples:
		| field                   | rule                         | value  |
		| SubmissionReference          | No whitespace                | t t g  |
		| SubmissionReference          | No special characters        | t!�st  |
		| SubmissionVersionDescription | No curly open/close brackets | t{es}t |
		| BrokerDepartmentId      | No whitespace                | 5 5 8  |
		| ProgrammeReference      | No whitespace                | t t g  |
		| ContractReference       | No whitespace                | t t g  |
		| ContractReference       | No special characters        | t!�st  |
		| ProgrammeDescription    | No curly open/close brackets | t{es}t |
		| InsuredOrReinsured      | No curly open/close brackets | t{es}t |
		| OriginalPolicyholder    | No curly open/close brackets | t{es}t |

#---------------------------------------------------------------------------
	@CoreAPI 
Scenario Outline: 15 Submission_POST_Broker_ContractReferenceMismatch - Broker creates a submission with ContractReference with a value than doesnt start with the structure B{BrokerCode}, returns 4xx
	Given for the submission the 'ContractReference' is '<value>'
	And the submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be in "4xx" range

	Examples:
	| value           |
	| B6666testChange |
	| C1225testChange |
	| TESTCHANGE      |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 16 Submission_POST_Broker_ProgrammeReferenceMismatch - Broker creates a submission with ProgrammeReference with a value than doesnt start with the structure B{BrokerCode}, returns 4xx
	Given for the submission the 'ProgrammeReference' is '<value>'
	And for the submission the 'RiskRegionCode' is 'worldwide'
	And for the submission the PeriodofCover has an Insurance period where the inception date is '2019-01-01' and the expiry date is '2019-12-31'
	And the submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be in "4xx" range

	Examples:
	| value           |
	| B6666testChange |
	| C1225testChange |
	| TESTCHANGE      |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 17 [Submission_POST_Underwriter_UnderwriterNotAllowed] - Underwriter POST submission, returns 400/403
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When I POST to '/Submissions' resource
	Then the response status code should be "403" or "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 18 Submission_POST_Broker_BrokerDepartmentDoesNotExist - Broker creates a submission where the broker code belongs to another broker 
	Given for the submission the 'BrokerDepartmentId' is '6666'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 Submission_POST_Broker_BrokerRequesterNotMemberOfDepartment - Broker from another department POST Submission, returns 401
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When I POST to '/Submissions' resource
	Then the response status code should be '403' or '400'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 20 Submission_POST_Broker_OriginalPolicyholderPresent - POST Submission with OriginalPolicyHolder, returns 400
	Given for the submission the 'OriginalPolicyholder' is 'testValue'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'OriginalPolicyholderNotExpected'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 21 Submission_POST_Broker_RegionXORCountryPopulated - POST Submission with RiskCountryCode, returns 400
	Given for the submission the 'RiskCountryCode' is 'GB'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'EitherRiskRegionCodeOrRiskCountryCodesMustBePopulated'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 22 Submission_POST_Broker_ExpiryBeforeInceptionDate - POST Submission when Expiry date is before Inception date, returns 400
	Given for the submission the PeriodofCover has an Insurance period where the inception date is '2019-01-01' and the expiry date is '2018-10-31'
	And the submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'PeriodExpiryDateisNotGreaterOrEqualPeriodInceptionDate'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 23 Submission_POST_Broker_ClassOfBusinessCodeSupported - POST Submission with incalid ClassOfBusinessCode, returns 400
	Given for the submission the 'ClassOfBusinessCode' is 'wrongValue'
	When I POST to '/Submissions' resource
	Then the response status code should be "400"
	And the response contains a validation error 'ClassOfBusinessCodeNotSupported'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 24 Broker does a POST with OriginalPolicyHolder where direct_insurance_contract, should return 201
   	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                    | ContractDescription       | ClassOfBusinessCode | OriginalPolyholder |  
	| aonbroker.ema@limossdidev.onmicrosoft.com | TS090919A UMR Description | marine_hull         | test               |
	Given for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be '403' or '400'
	

	Examples:
	| field                | value       |
	| OriginalPolicyholder | test change |


	
#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_Submission
	In order to test POST for a submission resource
	As a user I would like to create [POST] a number of submissions, look for validation errors and expected outputs.


Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I set security token and certificate for the 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                    | ContractDescription       | ClassOfBusinessCode | 
	| aonbroker.ema@limossdidev.onmicrosoft.com | TS090919A UMR Description | marine_hull         |

#---------------------------------------------------------------------------
@CoreAPI @SmokeTests
Scenario: 1 Submission_POST_Broker_CreateSubmission - Broker creates a submission and verify the response
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And the request feature submission context matches the response submission data
	And validate the POST submission context

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 2 Broker creates a submission, the submission reponse has a value for the field SubmissionVersionDescription 
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And the request feature submission context matches the response submission data
	And the request feature submission context contains a value for 'SubmissionVersionDescription' 
	And validate the POST submission context

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 3 Broker creates a submission with the insurance duration set.
	Given for the submission the PeriodofCover only has an Insurance duration where the duration number is '6' and the duration unit is 'months'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 4 Broker creates a submission where the broker email address is in  uppercase 
	Given for the submission the 'BrokerUserEmailAddress' is 'AONBROKER.EMA@LIMOSSDIDEV.ONMICROSOFT.COM'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 5 Broker creates a submission where the broker email address is in lowercase 
	Given for the submission the 'BrokerUserEmailAddress' is 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 6 Broker do a POST without OPTIONAL fields, should return 201
	Given '<field>' for Submission is null
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And the request feature submission context matches the response submission data

	Examples: 
		| field                   |
		| ProgrammeDescription    |
		| SubmissionVersionDescription |
		| ProgrammeReference      |
		| ContractReference       |
		| OriginalPolicyholder    |
		| RiskCountryCode         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 8 Broker do a POST with OPTIONAL fields, should return 201
   	Given for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And the request feature submission context matches the response submission data
	

	Examples:
	| field                        | value           |
	| SubmissionVersionDescription | test change     |
	| ProgrammeDescription         | test change     |
	| ContractDescription          | test change     |
	| RiskRegionCode               | worldwide       |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 9 Broker does a POST with OriginalPolicyHolder for reinsurance_contract, should return 201
   	And I prepare the submission to be posted with the following information
	| BrokerUserEmailAddress                    | ContractDescription       | ClassOfBusinessCode | OriginalPolyholder | ContractTypeCode  |  
	| aonbroker.ema@limossdidev.onmicrosoft.com | TS090919A UMR Description | marine_hull         | test               | reinsurance_contract |
	Given for the submission the '<field>' is '<value>'
	And the feature submission is saved for the scenario
	When I POST to '/Submissions' resource
	Then the response status code should be "201"
	And the request feature submission context matches the response submission data
	

	Examples:
	| field                | value       |
	| OriginalPolicyholder | test change |
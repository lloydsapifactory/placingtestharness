#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@Quote @PostQuoteDocument @AcceptanceTest @ApiGwSecurity 
Feature: POSTQuoteDocument
	In order to test POST for a quote document resource
	As a user I would like to create [POST] a quote and then create quote documents [POST] for the quote

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I POST a quote in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with broker code '1225' and departmentid '558'
	And I save the quote response to the quotecontext
	And I set the QuoteUniqueReference

@Tranche02_UserStory226,@Standard_positive_scenario
Scenario Outline: 01 BELL broker Bella creates and adds a Placing slip (Quote Document) 
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'FileName' is 'MRC_Placingslip.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "201"
	##And the request quote document context matches the response quote data

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json | 

@Tranche02_UserStory226 @Placing_Slip_txtfiles_not_allowed
Scenario Outline: 02 BELL broker Bella creates and adds a Placing slip (Quote Document) as a txt file where the file mime type is a valid one.
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'FileName' is 'sample_mrc.txt'
	And for the quote document context the 'FileMIMEType' is 'application/txt'
	And I attach the file Data\mrc_documents\sample_mrc.txt to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |
		

@Tranche02_UserStory226, @DocumentType_is_mandatory
Scenario Outline: 03 BELL broker Bella creates a Quote Document and sends an empty string for DocumentType 
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'client_correspondence'
	And for the quote document context the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the quote document context the 'FileName' is 'sample_clientcorr.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |
		
@Tranche02_UserStory226, @FileName_is_mandatory
Scenario Outline: 04 BELL broker Bella creates a Quote Document and sends an empty FileName 
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'client_correspondence'
	And for the quote document context the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the quote document context the 'FileName' is ''
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226, @Filemimetype_is_mandatory
Scenario Outline: 05 BELL broker Bella creates a Quote Document and sends an empty FileMIMEType
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'DocumentDescription' is 'MRC Placing slip'
	And for the quote document context the 'FileName' is 'MRC_Placingslip.docx'
	And for the quote document context the 'FileMIMEType' is ''
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226, @FileSizeInBytes_should_be_greater_than_zero
Scenario Outline: 06 BELL broker Bella creates a Quote Document and sends 0 for FileSizeInBytes
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'FileSizeInBytes' is 0
	And for the quote document context the 'DocumentDescription' is 'Sample MRC'
	And for the quote document context the 'FileName' is 'Sample_MRC.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "201"

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226 @ShareableToAllMarketsFlag_is_null @document_placing_slip_is_always_shareable
Scenario Outline: 07 BELL broker Bella creates a Quote Document and sends no value for ShareabletoAllMarketsFlag
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'client_correspondence'
	And for the quote document context the 'DocumentDescription' is 'Client corr'
	And for the quote document context the 'ShareableToAllMarketsFlag' is 'null'
	And for the quote document context the 'FileName' is 'sample_clientcorr.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226 @ShareableToAllMarketsFlag_is_false @document_placing_slip_is_always_shareable
Scenario Outline: 08 BELL broker Bella creates a a document_placing_slip [Quote Document] and sends false value for ShareabletoAllMarketsFlag
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'DocumentDescription' is 'Client Correspondence'
	And for the quote document context the 'ShareableToAllMarketsFlag' is 'false'
	And for the quote document context the 'FileName' is 'Sample_Clientcorr.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226 @ShareableToAllMarketsFlag_is_true @document_placing_slip_is_always_shareable
Scenario Outline: 09 BELL broker Bella creates a a document_palcing_slip [Quote Document] and sends true value for ShareabletoAllMarketsFlag
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'DocumentDescription' is 'Placing Slip'
	And for the quote document context the 'ShareableToAllMarketsFlag' is 'true'
	And for the quote document context the 'FileName' is 'MRC_Placingslip.pdf'
	And for the quote document context the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\MRC_Placingslip.pdf to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226, @QuoteReference_is_mandatory
Scenario Outline: 10 BELL broker Bella creates a Quote Document and sends an empty value for QuoteReference
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is ''
	And for the quote document context the 'DocumentType' is 'client_correspondence'
	And for the quote document context the 'DocumentDescription' is 'Correspondence Client [z]'
	And for the quote document context the 'FileName' is 'sample_clientcorr.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_clientcorr.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226, @QuoteVersionNumber_is_zero
Scenario Outline: 11 BELL broker Bella creates a Quote Document and sends an empty value for QuoteVersionNumber
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 0
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'DocumentDescription' is 'MRC Plaicng Slip'
	And for the quote document context the 'FileName' is 'MRC_Placingslip.docx'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\mrc_documents\sample_mrc.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226 @Incorrect_DocumentType
Scenario Outline: 12 BELL broker Bella adds a Quote Document and sends an incorrect document type[clc_correspondence_client which does not exist]  
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'clc_correspondence_client'
	And for the quote document context the 'DocumentDescription' is 'Correspondence Client [x]'
	And for the quote document context the 'FileName' is 'Sample Client Corr'
	And for the quote document context the 'FileMIMEType' is 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	And I attach the file Data\supporting_documents\sample_clientcorr.docx to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window


	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226
Scenario Outline: 13 BELL broker Bella creates a quote and AON Tom tries to add the Placing slip to the quote(Quote Document)
	And I set security token and certificate for the 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentType' is 'document_placing_slip'
	And for the quote document context the 'DocumentDescription' is 'Placing slip'
	And for the quote document context the 'FileName' is 'MRC_Placingslip.pdf'
	And for the quote document context the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\sample_mrc.pdf to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "403"
	And print the error message to the output window


	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226
Scenario Outline: 14 BELL broker Bella creates a quote and Bell technician Hulk Tom to add the Placing slip to the quote(Quote Document)
	And I set security token and certificate for the 'BellTechnician.hulk@LIMOSSDIDEV.onmicrosoft.com' user
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'DocumentDescription' is 'Placing slip'
	And for the quote document context the 'FileName' is 'MRC_Placingslip.pdf'
	And for the quote document context the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\sample_mrc.pdf to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "201"

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |


@Tranche02_UserStory226
Scenario Outline: 15 BELL broker Bella creates a quote and adds 2 documents [Placing slip & Client Corr].
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And I POST a set of quote documents from the data
	| FileName              | DocumentId | FileMIMEType    | DocumentType          | DocumentDescription  | FilePath                                        |
	| MRC_Placingslip.pdf   | 1          | application/pdf | document_placing_slip | Placing Slip         | Data\mrc_documents\sample_mrc.pdf               |
	| sample_clientcorr.pdf | 2          | application/pdf | client_correspondence | Client Corrspondence | Data\supporting_documents\sample_clientcorr.pdf |
	And I add a GET filter on 'quoteReference' with a value '$#quoteUniqueReference'
	When I make a GET request to '/quotedocuments' resource and append the filter(s)
	Then the response status code should be "200"
	And the response HTTP header ContentType should be to application/json type
	And the response body should contain a collection of quote documents
	And quote documents should have at least 2 document

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |





@Tranche02_UserStory226, @txt_file_is_not_allowed_for_Document_Placing_slip
Scenario Outline: 16 BELL broker Bella creates and adds a Placing slip (Quote Document) as a txt file with a File Mime type = application/jpeg.
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'FileName' is 'MRC_Placingslip.txt'
	And for the quote document context the 'FileMIMEType' is 'application/txt'
	And I attach the file Data\mrc_documents\sample_mrc.txt to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226, @a_file_with_no_extension_is_not_allowed_for_Document_Placing_slip
Scenario Outline: 17 BELL broker Bella creates and adds a Placing slip (Quote Document) as a txt file with a File Mime type = application/jpeg.
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'FileName' is 'MRC_Placingslip_NoExtension'
	And for the quote document context the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\MRC_Placingslip_NoExtension to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

@Tranche02_UserStory226, @upload_a_file_with_no_extension_is_not_allowed_for_Document_Placing_slip 
Scenario Outline: 18 BELL broker Bella creates and adds a Placing slip (Quote Document) as a txt file with a File Mime type = application/jpeg.
	And I set the quote document context from the json payload <inputFile>
	And for the quote document context the 'QuoteVersionNumber' is 1
	And for the quote document context the 'QuoteReference' is QuoteUniqueReference
	And for the quote document context the 'FileName' is 'MRC_Placingslip_NoExtension'
	And for the quote document context the 'FileMIMEType' is 'application/pdf'
	And I attach the file Data\mrc_documents\MRC_Placingslip_NoExtension to the request
	And the quote document is saved for the scenario
	When I POST to '/quotedocuments' resource with multipart
	Then the response status code should be "400"
	And print the error message to the output window

	Examples:
		| inputFile                           |
		| Data\1_2_Model\QuoteDocument_1.json |

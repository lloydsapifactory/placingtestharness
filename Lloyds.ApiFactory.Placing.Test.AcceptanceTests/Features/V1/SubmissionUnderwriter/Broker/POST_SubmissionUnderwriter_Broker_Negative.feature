#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionUnderwriter_Negative
	In order to create a submission underwriter
	As a broker 
	I want to be able to do a POST to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 01 Mandatory fields should be provided, if not return 4xx
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the '<field>' is ''
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'Required property'

	Examples:
	| field                       |
	| SubmissionReference         |
	| SubmissionVersionNumber     |
	| UnderwriterOrganisationId   |
	| UnderwriterUserEmailAddress |
	| CommunicationsMethodCode    |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 02 Min,Max Chars - Authorised broker does POST with fields having chars more or less than the value specs rules
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the feature submission underwriters context the '<field>' with a random value <assess> than <Characters>
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<ValidationError>'
	
	Examples:
		| field                   | assess | rule | Characters | ValidationError |
		| SubmissionReference     | more   | max  | 50         | StringTooLong   |
		| SubmissionReference     | less   | min  | 6          | StringTooShort    |
		| SubmissionVersionNumber | less   | min  | 1          | InvalidNumber   |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 03 - String character rules - Authorised broker does a POST with input string value having invalid chars, 4xx is being returned
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the '<field>' is '<value>'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error '<error>' for the field "<field>"


	Examples:
		| field               | rule                  | value | error               |
		| SubmissionReference | No whitespace         | t t f | ContainsWhitespace  |
		| SubmissionReference | No special characters | t!@st | ContainsSpecialChar |

#---------------------------------------------------------------------------
@CoreAPI
#This makes sense to be tested in a unit-test level > unit test functionality, quicker, more efficient
Scenario Outline: 04 - Valid email format - Authorised broker does a POST where the email is not an invalid email address, 4xx is being returned
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is '<value>'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range

	#the only reason we are having new scenarios for a single invalid email is that we cannot do a POST twice!
	Examples:
		| value                                   |
		| fdsa@fdsa.                              |
		| plainaddress                            |
		| @example.com                            |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionUnderwriter_POST_Broker_BrokerNotAuthorised] Unauthorised broker from another company CANNOT do a POST, returns 400/403
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And I make a GET request to '/brokerdepartments' resource
	And I store broker departments
	And broker departments do not include related submission broker department
	And I set a valid UnderwriterOrganisationId for 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'UnderwriterUserFullName' is 'Underwriter Lee'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| ResourceAccessDenied                 |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 07 [SubmissionUnderwriter_POST_Broker_NoLinkedSubmission] POST with an invalid submission reference, returns 4xx
	And for the submission underwriters the 'SubmissionReference' is '666666'
	And I set a valid UnderwriterOrganisationId for 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'UnderwriterUserFullName' is 'Underwriter Lee'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 08 [SubmissionUnderwriter_POST_Broker_NoLinkedSubmission] POST with an invalid submission version number, returns 4xx
	And for the submission underwriters the 'SubmissionVersionNumber' is '999'
	And I set a valid UnderwriterOrganisationId for 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'UnderwriterUserFullName' is 'Underwriter Lee'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 09 [[SubmissionUnderwriter_POST_Broker_BrokerNotAuthorised]] The requester broker is not an authorised broker for the related Submission, returns 400/403
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 10 [SubmissionUnderwriter_POST_Broker_UnderwriterMismatch] POST with an underwriter that has an invalid underwriter email address
	Given I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'testUnderwriter.BruceLee@LIMOSSDIDEV.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterEmailDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 11 Authorised broker CANNOT do a POST to duplicate underwriters for the same organisation - Lee and Sydney belong to Amlin organisation, returns 4xx
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	And I POST to '/SubmissionUnderwriters' resource
	And the response status code should be "201"

	And I clear down test context

	And the underwriter organisation for 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterOrganisationAlreadyExists'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 12 [SubmissionUnderwriter_POST_Broker_NoLinkedSubmission] POST with a NON existing submission, returns 4xx
	And I store the submission in the context with a non existng submission
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 13 [SubmissionUnderwriter_POST_Broker_InvalidCommunicationMethod] POST with invalid communication method code, returns 4xx
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is '<methodcodeVALUE>'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'CommunicationMethodCodePlatformExpected'

	Examples:
		| methodcodeVALUE |
		| PAPER           |
		| OTHER           |
		| EMAIL           |

#---------------------------------------------------------------------------	
@CoreAPI
Scenario: 14 [SubmissionUnderwriter_POST_Broker_UnderwriterMismatch] POST with an underwriter that the underwriter email address is not VALID for this organisation
	Given I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterEmailDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 15 [SubmissionUnderwriter_POST_Broker_NoLinkedUnderwriterOrganisation] POST with an underwriter organisation id that does not EXIST, returns 4xx
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'UnderwriterOrganisationId' is '666666'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterOrganisationDoesNotExist'

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 16 [SubmissionUnderwriter_POST_Broker_BrokerNotAuthorised] - Unauthorised broker from another company does a POST for a valid underwriter user email address, return 400/403
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation

	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| ResourceAccessDenied                 |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 17 [SubmissionUnderwriter_POST_Broker_BrokerNotAuthorised] - Unauthorised broker from the same company but different department does a POST for a valid underwriter user email address, return 400/403
	And I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	
	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| ResourceAccessDenied                 |
	| AccessToReferencedResourceNotAllowed |



#---------------------------------------------------------------------------
@CoreAPI
Scenario: 18 [SubmissionUnderwriter_POST_Broker_UnderwriterMismatch] POST with an underwriter that has an invalid underwriter email address
	Given I store a random underwriter organisations details 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| UnderwriterEmailDoesNotExist    |
	| UnderwriterNotAuthorisedToQuote |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 19 [SubmissionUnderwriter_POST_Broker_UnderwriterNotAuthorised] POST with an underwriter that has an invalid underwriter email address
	Given I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| UnderwriterEmailDoesNotExist    |
	| UnderwriterNotAuthorisedToQuote |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 20 [SubmissionUnderwriter_POST_Broker_DuplicateUnderwriter] POST with an underwriterorganisation that a SubmissionUnderwriter record that has the same combination of SubmissionReference, SubmissionVersionNumber and UnderwritingOrganisationId, returns 400
	Given I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "201"

	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| UnderwriterOrganisationAlreadyExists |



#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: DELETE_SubmissionUnderiter
	In order to delete a submission underwriter
	As a broker
	I want to be able to do a DELETE to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_DELETE_Broker_DeleteSubmissionUnderwriter] - DELETE a SubmissionUnderwriter that I have access to
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	And I store the submission underwriter in the context
	And I set the SubmissionUnderwriterId

	When  I make a DELETE request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be in "2xx" range
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	And the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_DELETE_Broker_BrokerNotAuthorised] Unauthorised broker from another company CANNOT DELETE a SubmissionUnderwriter, returns 400/403
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	And I store the submission underwriter in the context
	And I set the SubmissionUnderwriterId

	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "200" 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionUnderwriter_DELETE_Broker_SubmissionUnderwriterNotFound] - When try to DELETE a Submission nderwriter that does not exist, returns 404
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionUnderwriters/6666' resource
	Then the response status code should be "404"
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 04 [SubmissionUnderwriter_DELETE_Broker_BrokerNotAuthorised] - Unauthorised broker from the same company but diffrent department CANNOT DELETE to a SubmissionUnderwriter, returns 403
	And I clear down test context
	Given I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	And I store the submission underwriter in the context
	And I set the SubmissionUnderwriterId

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceAccessDenied'


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 [SubmissionUnderwriter_DELETE_Broker_SubmissionUnderwriterNotInDraft] - Delete a SubmissionUnderwriter thats is not in DRFT, returns 400
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	#POST Submission, SubmissionDocuments
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	#POST SubmissionUnderwriter
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I set the SubmissionUniqueReference 

	When  I make a DELETE request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "400"
	And the response contains a validation error 'SubmissionUnderwriterNotInDraft'



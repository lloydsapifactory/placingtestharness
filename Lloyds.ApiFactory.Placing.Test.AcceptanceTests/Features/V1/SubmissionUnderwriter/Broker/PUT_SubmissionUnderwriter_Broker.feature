#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionUnderwriter_Broker
	In order to update a submission underwriter
	As a broker
	I want to be able to do a PUT to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 |
	And store the CreatedDateTime from submission underwriters context
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 01 [SubmissionUnderwriter_PUT_Broker_UpdateSubmissionUnderwriter] PUT for the UnderwriterUserEmailAddress 
	And I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	And store the LastModifiedDateTime from the response
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header using LastModifiedDate
	Then the response status code should be "200"

	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "200" 
	And I store the submission underwriter in the context
	And validate submission underwriters result contain the value of UnderwriterUserEmailAddress
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And validate submission underwriters result contain item added on 'CommunicationsMethodCode' with a value 'PLATFORM'
	And Submission fields copied to SubmissionUnderwriter




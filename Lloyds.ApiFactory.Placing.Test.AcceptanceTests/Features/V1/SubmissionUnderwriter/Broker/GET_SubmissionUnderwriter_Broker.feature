#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionUnderwriter_Broker
	In order to retrieve submission underwriters information that belong to my organisation
	As a broker
	I want to be able to do a GET to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

	@CoreAPI
Scenario: 01 [SubmissionUnderwriter_GET_ALL_Broker_GetSubmissionUnderwriters] - GETALL submission underwriters that I have access
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |
	And I reset all the filters

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And collection of submission underwriters should have at least 1 link
	And collection of submission underwriters should have a link with proper value

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_GET_ALL_Broker_GetSubmissionUnderwriters] - GETALL submission underwriters that I have access and validate they belong to the correct broker
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And collection of submission underwriters should have at least 1 link
	And collection of submission underwriters should have a link with proper value
	And submission underwriters belong to broker 'bellbroker.bella@limossdidev.onmicrosoft.com' 


#---------------------------------------------------------------------------	
@CoreAPI
Scenario: 03 GETALL submission underwriters AND return all MANDATORY fields
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |
	
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the following fields
	| field                           |
	| SubmissionUnderwriterId         |
	| SubmissionUnderwriterStatusCode |
	| SubmissionReference             |
	| SubmissionVersionNumber         |
	| BrokerOrganisationName          |
	| BrokerUserEmailAddress          |
	| BrokerUserFullName              |
	| UnderwriterOrganisationId       |
	| UnderwriterOrganisationName     |
	| UnderwriterUserEmailAddress     |
	| CommunicationsMethodCode        |
	| ProgrammeId                     |
	| ContractTypeCode                |
	| InsuredOrReinsured              |
	| CoverTypeCode                   |
	| ClassOfBusinessCode             |
	| PeriodOfCover                   |
	| CreatedDateTime                 |
	| UnderwriterUserFullName         |



#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 06 GetALL ('contains' partial match filtering):  GETALL submission underwriter with CONTAINS filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
	| field                        |
	| SubmissionVersionDescription |
	| BrokerUserFullName           |
	| ProgrammeDescription         |
	| ContractReference            |
	| InsuredOrReinsured           |
	| OriginalPolicyholder         |
	| UnderwriterUserFullName      |
	| BrokerOrganisationName       |
	| UnderwriterOrganisationName  |
	| ProgrammeReference           |
	| ContractDescription          |
	| BrokerDepartmentName         |


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 07 GetALL ('prefix' partial match filtering):  GETALL submission underwriter, with PREFIX filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'prefix' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
				| field                        |
				| SubmissionVersionDescription |
				| BrokerUserFullName           |
				| ProgrammeDescription         |
				| ContractReference            |
				| InsuredOrReinsured           |
				| OriginalPolicyholder         |
				| UnderwriterUserFullName      |
				| BrokerOrganisationName       |
				| UnderwriterOrganisationName  |
				| ProgrammeReference           |
				| ContractDescription          |
				| BrokerDepartmentName         |

#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 08 GetALL ('suffix' partial match filtering):  GETALL submission underwriter, with SUFFIX filter 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
				| field                        |
		| SubmissionVersionDescription |
		| BrokerUserFullName           |
		| ProgrammeDescription         |
		| ContractReference            |
		| InsuredOrReinsured           |
		| OriginalPolicyholder         |
		| UnderwriterUserFullName      |
		| BrokerOrganisationName       |
		| UnderwriterOrganisationName  |
		| ProgrammeReference           |
		| ContractDescription          |
		| BrokerDepartmentName         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 09 GetALL (exact match filtering):  GETALL submission underwriter, with single EXACT MATCH field filtering 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 3 characters and save it to the context
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I add a filter on '<field>' with a value '<value>'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I reset all the filters
	And I add a 'match' filter on '<field>' with a value '<value>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
	| field                 | value                     |
	| ContractTypeCode      | direct_insurance_contract |
	| CoverTypeCode         | facultative_proportional  |
	| ClassOfBusinessCode   | marine_hull               |


	#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 09a GetALL (exact match filtering SubmissionVerionNumber):  GETALL submission underwriter, with single EXACT MATCH field filtering 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 3 characters and save it to the context
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I add a filter on '<field>' with a value '<value>'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I reset all the filters
	And I add a filter on '<field>' with a value '<value>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
	| field                 | value                     |
	| SubmissionVersionNumber    | 1                         |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 10 GETALL submission underwriters ordered by 
	And I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a filter on '_order' with a value '<field>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the submission underwriters response is order by '<field>' 'ascending'

	Examples:
		| field                       |
		| SubmissionReference         |
		| SubmissionVersionNumber     |
		| BrokerOrganisationName      |
		| BrokerDepartmentName        |
		| BrokerUserEmailAddress      |
		| UnderwriterOrganisationName |
		| UnderwriterUserEmailAddress |
		| CreatedDateTime             |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 13 GETALL with pagination 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                          | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com     | PLATFORM                 |
	| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 |
	And I reset all the filters

	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a filter on '_pageNum' with a value '1'
	And I add a filter on '_pageSize' with a value '2'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the count of submission underwriters returned will be = 2

	And I clear down test context

	And I add a filter on '_pageNum' with a value '2'
	And I add a filter on '_pageSize' with a value '1'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the count of submission underwriters returned will be = 1

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 15 [SubmissionUnderwriter_GET_Broker_GetSubmissionUnderwriterById] - GETById to a submission underwriter that my organisation has access to, returns 200
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 
	And submission underwriters context should have at least 1 link

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 19 GetById to a submission underwriter and check that the Id has the correct structure 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And for the submission underwriters and the returned result the field 'Id' has the structure of 'SubmissionReference_SubmissionVersionNumber_Guid'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 21 GetALL (exact match filtering SubmissionReference):  GETALL submission underwriter as a broker, with single field filtering 
		Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a '' filter on 'SubmissionReference' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of SubmissionReference

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 23 GetALL (multiple field AND multiple value-filtering): GETALL submission underwriter, multiple value filtering 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode | SubmissionVersionNumber | ContractTypeCode     | ClassOfBusinessCode    |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    | 1                  | reinsurance_contract | marine_increased_value |
	
	And I reset all the filters
	And I add a filter for each one of the '<fields>' with values from the context of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the values of <fields>

	Examples:
	| fields                                                  |
	| SubmissionVersionNumber,CreatedDateTime                      |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 24 GetALL ('match' single value filtering):  GETALL submission underwriter with MATCH single value filtering 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'match' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
		| field                 |
		| ContractTypeCode      |
		| SubmissionUnderwriterStatusCode |
		| CoverTypeCode         |
		| ClassOfBusinessCode   |
		

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 25 GetALL (multiple value filtering Filter=A,B,C):  GETALL submission underwriter as a broker, with multiple value filtering 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	And I retrieve the broker departments for 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And generate a random string of 6 characters and save it to the context
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I prepare the submission context with the following information
         | BrokerUserEmailAddress                       | ClassOfBusinessCode | CoverTypeCode            | RiskRegionCode|
         | bellbroker.bella@limossdidev.onmicrosoft.com | marine_hull         | facultative_proportional | Test          |
	And I POST a submission in the background on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode | ContractTypeCode     | ClassOfBusinessCode    |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | reinsurance_contract | marine_increased_value |
	
	# POST SUBMISSION + SUBMISSION UNDERWRITER FOR VERSION 2
	And I POST 1 more submissions with different version for stored Submission
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode | ContractTypeCode     | ClassOfBusinessCode    |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | reinsurance_contract | marine_increased_value |

	And I reset all the filters
	And I add a filter on '<field>' with a value '<values>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And for submission underwriters I validate I am getting the correct results for <field> with values of (<values>)

	Examples:
	| field                   | values |
	| SubmissionVersionNumber | 1,2    |

#---------------------------------------------------------------------------
@CoreAPI  
Scenario Outline: 26 GetALL (multiple value filtering MATCH Filter=A,B,C):  GETALL submission underwriter as a broker, with multiple value filtering 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                          | CommunicationsMethodCode | SubmissionVersionNumber | ContractTypeCode     | ClassOfBusinessCode    |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com     | PLATFORM                 | 1                  | direct_insurance_contract | marine_increased_value |
	| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 | 5                  | reinsurance_contract | marine_hull |

	And I reset all the filters
	And I add a 'match' filter on '<field>' with values '<values>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And for submission underwriters I validate I am getting the correct results for <field> with values of (<values>)

	Examples:
	| field                 | values                                         |
	| SubmissionUnderwriterStatusCode | DRFT, SENT                                     |
	| ContractTypeCode      | direct_insurance_contract,reinsurance_contract |
	| ClassOfBusinessCode  | marine_increased_value,marine_hull             |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 27 GetALL (datetime range filtering):  GETALL submission underwriter as a broker, with datetime range filtering 
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate that the submission underwriters results for the field '<field>' are between the date of yesterday and today

	Examples:
	| field           | 
	| CreatedDateTime | 

			

#---------------------------------------------------------------------------
@CoreAPI  @TestNotImplemented @BackendNotImplemented-Ebix @TestNotImplemented
Scenario: 28 GetALL (exact match createddatetime filtering):  GETALL submission underwriter as a broker, with single field filtering 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 3 characters and save it to the context
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And for the submission underwriters I add a date filter on 'CreatedDateTime' with a datetime value of today
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a filter on '<field>' with a value '<value>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 29 (DATETIME before,after filtering): GETALL submission underwriters, before or after a datetime 
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate that the submission underwriters results for the field '<field>' are <comparison> the date of <when>
	
	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | after      | yesterday |
		| _last_modified  | after      | yesterday |
		| _last_modified  | before     | one week  |


#---------------------------------------------------------------------------
		@CoreAPI
Scenario Outline: 30 GETALL submission underwriters from another broker in the same company and same department
	And I log in as a broker '<BrokerUserEmailAddress>'
	And store the broker
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |
	And I reset all the filters

	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And collection of submission underwriters should have a total equal to 1

	Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |
	| AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com    | AON                  |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 31 GetById: As a broker from same company and same department, returns 200
	And I log in as a broker '<BrokerUserEmailAddress>'
	And store the broker
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I select broker that is in the same department with the stored broker
	And I log in as a the saved broker
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200"

	Examples:
	| BrokerUserEmailAddress                       | BrokerDepartmentName |
	| BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com | BELL                 |
	| AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com    | AON                  |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 32 Order by last_modified(POST): Creating Submission Underwriters and do a GETALL order by last modified, they are are ordered by the last Submission created
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                          | CommunicationsMethodCode | ContractTypeCode          | 
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com     | PLATFORM                 | direct_insurance_contract | 
	| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 | reinsurance_contract      | 
	And I reset all the filters

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And for submissions underwriters I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I add a filter on '_order' with a value '<fieldValue>'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection

	And the submission underwriters response is ordered by '_last_modified' of the field 'UnderwriterUserEmailAddress' with the following order as <sortingOrder>
	| valueOfField_ascending                               | valueOfField_descending                              |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com     | beazleyunderwriter.fiona@limossdidev.onmicrosoft.com |
	| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | amlinunderwriter.lee@limossdidev.onmicrosoft.com     |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 33 Order by last_modified(PUT): Updating Submission Underwriters and do a GETALL order by last modified, they are are ordered by the last Submission created
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                          | CommunicationsMethodCode | ContractTypeCode          | 
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com     | PLATFORM                 | direct_insurance_contract | 
	| beazleyunderwriter.fiona@limossdidev.onmicrosoft.com | PLATFORM                 | reinsurance_contract      | 
	And I reset all the filters

	And store the CreatedDateTime from submission underwriters context
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And for submissions underwriters I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission
	And I add a filter on '_order' with a value '<fieldValue>'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection

	And the submission underwriters response is ordered by '_last_modified' of the field 'UnderwriterUserEmailAddress' with the following order as <sortingOrder>
	| valueOfField_ascending                                | valueOfField_descending                               |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com      | BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com |
	| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | amlinunderwriter.lee@limossdidev.onmicrosoft.com      |

Examples:
| fieldValue      | sortingOrder |
| _last_modified  | ascending    |
| -_last_modified | descending   |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 34 Adding _select parameter will return results with the exact fields
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,UnderwriterUserFullName,underwriterUserEmailAddress'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submission underwriters
	And collection of submission underwriters should have a link with proper value


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 35 (DATE range Filter): GETALL Submission underwriters, with a date range 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission underwriters
	And validate that the submission underwriters results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 
		| _last_modified | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 36 GetALL (range adatetime..bdatetime):  GETALL submission underwriter as a broker, with datetime range filtering 
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=yesterday and bdatetime=today
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate that the submission underwriters date results for the field '<field>' are  for yesterday

	Examples:
	| field           | 
	| CreatedDateTime | 

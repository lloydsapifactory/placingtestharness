#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionUnderwriter_Negative
	In order to update a submission underwriter
	As a broker
	I want to be able to do a PUT to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 |
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission
	And store the CreatedDateTime from submission underwriters context


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_PUT_Broker_BrokerNotAuthorised] Unauthorised broker from another company CANNOT do a PUT to SubmissionUnderwriter, returns 400/403
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_PUT_Broker_NoLinkedSubmission] PUT for a submission that has invalid submission reference, returns 4xx
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And for the submission underwriters the SubmissionReference is invalid
	And the submission underwriter is saved for the scenario PUT
	And I set the SubmissionUnderwriterId
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "400"
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| SubmissionDoesNotExist          |
	| SubmissionInPayloadDoesNotExist |
	| ResourceKeysDoNotMatch          |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionUnderwriter_PUT_Broker_UnderwriterMismatch] PUT for a submission that has invalid UnderwriterUserEmailAddress, returns 4xx
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'TESTINVALIDEMAIL.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterEmailDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario Outline: 05 [SubmissionUnderwriter_PUT_Broker_InvalidCommunicationMethod] PUT with an invalid CommunicationsMethodCode, returns 4xx
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And for the submission underwriters the 'CommunicationsMethodCode' is '<methodcodeVALUE>'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'CommunicationMethodCodePlatformExpected'

	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "200" 
	And I store the submission underwriter in the context
	And validate submission underwriters result DOESNT contain the value of CommunicationsMethodCode

	Examples:
		| methodcodeVALUE |
		| PAPER           |
		| OTHER           |
		| EMAIL           |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [[SubmissionUnderwriter_PUT_Broker_UnderwriterMismatch]] PUT for the UnderwriterUserEmailAddress that belong in differen organisation, return 4xx
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'UnderwriterEmailDoesNotExist'

	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "200" 
	And I store the submission underwriter in the context
	And validate submission underwriters result DOESNT contain the value of UnderwriterUserEmailAddress


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 07 [SubmissionUnderwriter_PUT_Broker_SubmissionUnderwriterNotFound] PUT where SubmissionUnderwriterId is NOT VALID, return 404
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And for the submission underwriters the SubmissionUnderwriterId is invalid
	And the submission underwriter is saved for the scenario PUT
	And I set the SubmissionUnderwriterId
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 08 [SubmissionUnderwriter_PUT_Broker_BrokerNotAuthorised] - As an unauthorised broker from another company I CANNOT do a PUT for the UnderwriterUserEmailAddress, returns 400/403 
	Given I log in as a broker 'AONBroker.Ema@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 09 [SubmissionUnderwriter_PUT_Broker_BrokerNotAuthorised] - As an unauthorised broker from another company I CANNOT do a PUT for the UnderwriterUserEmailAddress, returns 403/400
	Given I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And store the CreatedDateTime from submission underwriters context
	And I clear down test context
	And I save the submission underwriter context from submission

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400"
	And the response contains a validation error 'ResourceAccessDenied'	

#---------------------------------------------------------------------------
@CoreAPI @TestNotImplemented
#marked as TestNotImplemented until we agree whether this validation should we going forward
Scenario: 10 [SubmissionUnderwriter_PUT_Broker_ResourceKeysMismatch] PUT for a submission that has invalid submission reference, returns 4xx
	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And for the submission underwriters the 'SubmissionVersionNumber' is '3'
	And the submission underwriter is saved for the scenario PUT
	And I set the SubmissionUnderwriterId
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be in "4xx" range
	And the response contains one of the following error messages
	| ErrorMessage                    |
	| ResourceKeysDoNotMatch          |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 11 [SubmissionUnderwriter_PUT_Broker_SubmissionUnderwriterNotDraft] PUT SubmissionUnderwrter when not in DRFT, returns 4xx
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And store the broker
	#POST Submission, SubmissionDocuments
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                              | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	#POST SubmissionUnderwriter
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I set the SubmissionUnderwriterId
	And I reset all the filters
	#PREPARE FOR QMD as a BROKER
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |
	And I clear down test context

	And for the submission underwriters the 'UnderwriterUserEmailAddress' is 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When I PUT to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource with 'If-Unmodified-Since' header
	Then the response status code should be "403" or "400"
	And the response contains a validation error 'SubmissionUnderwriterNotInDraft'	

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionUnderwriter_Broker_Negative
	In order to retrieve submission underwriters information that belong to my organisation
	As a broker
	I want to be able to do a GET to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I clear down test context
	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 GetALL NO auth: GETALL submission underwriters without authorisation returns 401
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_GET_Broker_SubmissionUnderwriterNotFound] GetById to a submission underwriter that does not exists returns 404
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/SubmissionUnderwriters/6i6i6' resource 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionUnderwriter_GET_Broker_BrokerNotAuthorised] As a broker from another company, GETById to a submission underwriter that I have no access to returns 400/403
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'aonbroker.ema@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 04 GetALL (exact match INVALID filtering):  GETALL with INVALID single field filtering, returns 4xx 
	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And generate a random string of 3 characters and save it to the context
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I add a filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
	| field                     | value |
	| SubmissionUnderwriterId   | test  |
	| BrokerUserContactNumber   | test  |
	| UnderwriterOrganisationId | test  |
	| CommunicationsMethodCode  | test  |
	| ProgrammeId               | test  |
	| RiskRegionCode            | test  |
	| RiskCountryCode           | test  |
	| PeriodOfCover             | test  |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 05 [SubmissionUnderwriter_GET_Broker_BrokerNotAuthorised] - As a broker from another company, GETById to a submission underwriter that I have no access to returns 400/403
	Given I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'BellBroker.Bella@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 06 [SubmissionUnderwriter_GET_Broker_BrokerNotAuthorised] - As a broker from the same company and different departmet, GETById to a submission underwriter that I have no access to returns 400/403
	Given I log in as a broker 'aonbroker.gar@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains one of the following error messages
	| ErrorMessage                         |
	| AccessToReferencedResourceNotAllowed |
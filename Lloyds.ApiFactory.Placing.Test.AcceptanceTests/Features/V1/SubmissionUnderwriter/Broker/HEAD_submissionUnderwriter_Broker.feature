#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_SubmissionUnderwriter
	In order to retrieve a submission underwriter
	As a broker 
	I want to be able to do a HEAD request to SubmissionUnderwriters

	Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionUnderwriter_HEAD_Broker_HeadSubmissionUnderwriterById] As a Broker I want to be able to do a HEAD SubmissionUnderwriter resource for a valid submission underwriter, returning 2xx
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be in "2xx" range

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionUnderwriter_HEAD_Broker_BrokerNotAuthorised] - As an unauthorised Broker from another company HEAD to a valid SubmissionUnderwriter, returning 400/403/404
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "403" or "400" or "404"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionUnderwriter_HEAD_Broker_BrokerNotAuthorised] - As an unauthorised Broker from the same company but different department HEAD to a valid SubmissionUnderwriter, returning 400/403/404
	Given I log in as a broker 'AONBroker.Tom@LIMOSSDIDEV.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |

	Given I log in as a broker 'AonBroker.David@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "403" or "400" or "404"

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 04 [SubmissionUnderwriter_HEAD_Broker_SubmissionUnderwriterNotFound] HEAD to an invalid id, returns 404
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionUnderwriters/666666' resource
	Then the response status code should be "404"

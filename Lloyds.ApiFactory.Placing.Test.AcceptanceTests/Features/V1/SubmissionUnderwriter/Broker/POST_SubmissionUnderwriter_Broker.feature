#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionUnderwriter_Broker
	In order to create a submission underwriter
	As a broker 
	I want to be able to do a POST to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 01 [SubmissionUnderwriter_POST_Broker_AssignSubmissionUnderwriter] POST for a valid underwriter user email address, return 201
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	And I store the submission underwriter in the context
	And I set the SubmissionUnderwriterId

	Given  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 
	And I store the submission underwriter in the context
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And validate submission underwriters result contain item added on 'CommunicationsMethodCode' with a value 'PLATFORM'
	And Submission fields copied to SubmissionUnderwriter

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 Authorised broker CAN do a POST to more than one underwriters that belong to different organisations - Lee belongs to Amlin organisation, while Taylor belongs to Beazley
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	And I POST to '/SubmissionUnderwriters' resource
	And the response status code should be "201"
	And I clear down test context
		
	And the underwriter organisation for 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'beazleyunderwriter.taylor@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	And the response status code should be "201"




#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionUnderwriter_Underwriter_Negative
	In order to retrieve submission underwriters information that belong to my organisation
	As an underwriter
	I want to be able to do a GET to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_GET_Underwriter_SubmissionUnderwriterNotFound] GetById to a submission underwriter that does not exists returns 404
	Given I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	When I make a GET request to '/SubmissionUnderwriters/6i6i6' resource 
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'ResourceDoesNotExist'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById to a submission that my organisation has no access to returns 400/403/404
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403" or "404"
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from an UNAUTHORISED UW(BeazleyTaylor) with the correct QMDID found from the assigned UW (AMLINLEE), returns 400/403
	#POST QMD BROKER->UW AS A PREREQUISITE, IN ORDER TO DO GETALL QM AS AN UW
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| SubmissionVersionNumber | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| 1                  | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And store the submission underwriter id by finding the submission reference quoted

	Given I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "403" or "400"
	And the response contains a validation error 'ResourceAccessDenied'


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 04 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from an underwriter in a different company, returns 400/403 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	
	Given I log in as an underwriter '<UnderwriterUserEmailAddress>'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "403" or "400"

	Examples:
	| UnderwriterUserEmailAddress                          | UnderwriterOrganisationname |
	| BeazleyUnderwriter.Fiona@LIMOSSDIDEV.onmicrosoft.com | BEAZLEY                     |
	| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com |BEAZLEY                     |


#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 05 GetALL (exact match INVALID filtering):  GETALL with INVALID single field filtering, returns 4xx 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '<field>' with a value '<value>'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be in "4xx" range
	And the response contains a validation error 'InvalidFilterCriterion'

	Examples:
	| field                     | value |
	| SubmissionUnderwriterId   | test  |
	| BrokerUserContactNumber   | test  |
	| UnderwriterOrganisationId | test  |
	| CommunicationsMethodCode  | test  |
	| ProgrammeId               | test  |
	| RiskRegionCode            | test  |
	| RiskCountryCode           | test  |
	| PeriodOfCover             | test  |


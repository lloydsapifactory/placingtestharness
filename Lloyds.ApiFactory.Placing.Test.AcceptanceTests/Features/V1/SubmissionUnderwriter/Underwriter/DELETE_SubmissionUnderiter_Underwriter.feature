#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: DELETE_SubmissionUnderiter
	In order to delete a submission underwriter
	As an underwriter
	I want to be able to test that I CANNOT DELETE a SubmissionUnderwriter

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_DELETE_Underwriter_UnderwriterNotAllowed] As an underwriter of the Submission I CANNOT do a DELETE to a SubmissionUnderwriter, returns 400/403
	And I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "201"
	And I store the submission underwriter in the context
	And I set the SubmissionUnderwriterId

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a DELETE request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'ResourceAccessDenied'

	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource
	Then the response status code should be "200"




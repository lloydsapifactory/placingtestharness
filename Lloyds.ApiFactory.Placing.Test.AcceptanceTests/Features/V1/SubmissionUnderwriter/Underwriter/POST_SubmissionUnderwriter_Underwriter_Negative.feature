#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: POST_SubmissionUnderwriter_Negative
	In order to create a submission underwriter
	As an underwriter 
	I want to be able to test that I CANNOT POST to SubmissionUnderwriter

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	And I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_POST_Underwriter_UnderwriterNotAllowed] As an underwriter I CANNOT do a POST for a submission, returns 400/403
	Given I store a random underwriter organisations details 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' for submission underwriter
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	And I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'UnderwriterNotAuthorised'

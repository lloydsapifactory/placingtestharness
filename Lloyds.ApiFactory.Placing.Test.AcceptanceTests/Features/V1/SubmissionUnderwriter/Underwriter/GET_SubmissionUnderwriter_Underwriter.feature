#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: GET_SubmissionUnderwriter_Underwriter
	In order to retrieve submission underwriters information that belong to my organisation
	As an underwriter
	I want to be able to do a GET to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 1 [SubmissionUnderwriter_GET_ALL_Underwriter_GetSubmissionUnderwriters] GETALL submission underwriters that I have been assigned
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission
	And the underwriter organisation for 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is allowed to be approached for the related submission
	And the underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' is valid for the underwriter organisation
	And for the submission underwriters the 'CommunicationsMethodCode' is 'PLATFORM'
	And the submission underwriter is saved for the scenario
	When I POST to '/SubmissionUnderwriters' resource
	Then the response status code should be "201"

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 2 GetALL: GETALL submission underwriters that I have access and validate that they belong to the correct underwriter
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And submission underwriters belong to underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com' 

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 3 GetALL: GETALL submission underwriters that I have been assigned to AND return all MANDATORY fields
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a GET request to '/SubmissionUnderwriters' resource 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the following fields
		| field                       |
	| SubmissionUnderwriterId               |
	| SubmissionUnderwriterStatusCode       |
	| SubmissionReference              |
	| SubmissionVersionNumber          |
	| BrokerOrganisationName      |
	| BrokerUserEmailAddress      |
	| BrokerUserFullName          |
	| UnderwriterOrganisationId   |
	| UnderwriterOrganisationName |
	| UnderwriterUserEmailAddress |
	| CommunicationsMethodCode    |
	| ProgrammeId                 |
	| ContractTypeCode            |
	| InsuredOrReinsured          |
	| CoverTypeCode               |
	| ClassOfBusinessCode         |
	| PeriodOfCover               |
	| CreatedDateTime             |

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 6 [SubmissionUnderwriter_GET_Underwriter_GetSubmissionUnderwriterById] GETById to a submission underwriter that my organisation has access to, returns 200 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 
	And I store the submission underwriter in the context
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 7 GetALL: GETALL and retrieve the submission underwriters assigned to me and validate them
	#POST QMD BROKER->UW AS A PREREQUISITE, IN ORDER TO DO GETALL QM AS AN UW
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'contains' filter on 'ProgrammeDescription' with the value from the referenced submission
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And validate submission underwriters result contain item added on 'SubmissionUnderwriterStatusCode' with a value 'DRFT'
	And validate submission underwriters result contain item added on 'CommunicationsMethodCode' with a value 'PLATFORM'
	And validate submission underwriters result contain item added on 'ProgrammeDescription' with a value from the referenced submission
	And validate submission underwriters result contain item added on 'ProgrammeReference' with a value from the referenced submission

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 8 GETALL without a POST to SubmissionDialogue from the Broker, the submission underwriter is NOT included in the collection returned
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription  |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test      |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And searching by the SubmissionReference validate that the submission underwriter is NOT included in the collection


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 9 GetALL ('contains' partial match filtering):  GETALL submission underwriter with CONTAINS filter 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
		| field                        |
		| SubmissionVersionDescription |
		| BrokerUserFullName           |
		| ProgrammeDescription         |
		| ContractReference            |
		| InsuredOrReinsured           |
		| OriginalPolicyholder         |
		| UnderwriterUserFullName      |
		| BrokerOrganisationName       |
		| UnderwriterOrganisationName  |
		| ProgrammeReference           |
		| ContractDescription          |
		| BrokerDepartmentName         |



#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 11 GetALL ('prefix' partial match filtering):  GETALL submission underwriter, with PREFIX filter 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'prefix' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
		| field                        |
		| SubmissionVersionDescription |
		| BrokerUserFullName           |
		| ProgrammeDescription         |
		| ContractReference            |
		| InsuredOrReinsured           |
		| OriginalPolicyholder         |
		| UnderwriterUserFullName      |
		| BrokerOrganisationName       |
		| UnderwriterOrganisationName  |
		| ProgrammeReference           |
		| ContractDescription          |
		| BrokerDepartmentName         |


#---------------------------------------------------------------------------
@CoreAPI @BackendNotImplemented-Ebix
Scenario Outline: 12 GetALL ('suffix' partial match filtering):  GETALL submission underwriter, with SUFFIX filter 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'contains' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
				| field                        |
				| SubmissionVersionDescription |
				| BrokerUserFullName           |
				| ProgrammeDescription         |
				| ContractReference            |
				| InsuredOrReinsured           |
				| OriginalPolicyholder         |
				| UnderwriterUserFullName      |
				| BrokerOrganisationName       |
				| UnderwriterOrganisationName  |
				| ProgrammeReference           |
				| ContractDescription          |
				| BrokerDepartmentName         |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 14 GetALL (exact match filtering):  GETALL submission underwriter, with single EXACT MATCH field filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '<field>' with a value '<value>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
	| field                 | value                     |
	| SubmissionVersionNumber    | 1                         |

	#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 14a GetALL (exact match filtering ENUMS):  GETALL submission underwriter, with single EXACT MATCH field filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on '<field>' with a value '<value>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
	| field                 | value                     |
	| ContractTypeCode      | direct_insurance_contract |
	| SubmissionUnderwriterStatusCode | DRFT                      |
	| CoverTypeCode         | facultative_proportional  |
	| ClassOfBusinessCode   | marine_hull               |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 15 GETALL submission underwriters ordered by 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_order' with a value '<field>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the submission underwriters response is order by '<field>' 'ascending'

	Examples:
		| field                       |
		| SubmissionReference         |
		| SubmissionVersionNumber     |
		| BrokerOrganisationName      |
		| BrokerUserEmailAddress      |
		| UnderwriterOrganisationName |
		| UnderwriterUserEmailAddress |
		| CreatedDateTime             |
		| BrokerDepartmentName        |
		| _last_modified              |



#---------------------------------------------------------------------------
@CoreAPI
Scenario: 16 GETALL with pagination 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '_pageNum' with a value '1'
	And I add a filter on '_pageSize' with a value '2'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the count of submission underwriters returned will be = 2

	And I clear down test context

	And I add a filter on '_pageNum' with a value '2'
	And I add a filter on '_pageSize' with a value '1'
	When  I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And the count of submission underwriters returned will be = 1

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 17 GetALL (exact match filtering SubmissionReference):  GETALL submission underwriter as a broker, with single field filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a '' filter on 'SubmissionReference' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of SubmissionReference


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18 GetALL (multiple field AND multiple value-filtering): GETALL submission underwriter, multiple value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200" 
	And I store the submission underwriter in the context
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter for each one of the '<fields>' with values from the context of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the values of <fields>

	Examples:
	| fields                                                  |
	| SubmissionVersionNumber,CreatedDateTime                      |

	#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18a GetALL (multiple field AND multiple value-filtering): GETALL submission underwriter, multiple value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200" 
	And I store the submission underwriter in the context
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a filter on '<field1>' with a random value from the response of submission underwriters
	And I add a 'match' filter on '<field2>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the values of <fields>

	Examples:
	| field1             | field2              | fields                                 |
	| SubmissionVersionNumber | ClassOfBusinessCode | SubmissionVersionNumber,ClassOfBusinessCode |
	| SubmissionVersionNumber | ContractTypeCode    | SubmissionVersionNumber,ContractTypeCode    |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 18b GetALL (multiple field AND multiple value-filtering): GETALL submission underwriter, multiple value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200" 
	And I store the submission underwriter in the context
	And I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on '<field1>' with a random value from the response of submission underwriters
	And I add a 'match' filter on '<field2>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the values of <fields>

	Examples:
	| field1           | field2              | fields                               |
	| ContractTypeCode | ClassOfBusinessCode | ContractTypeCode,ClassOfBusinessCode |

#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 19 GetALL ('match' single value filtering):  GETALL submission underwriter with MATCH single value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a 'match' filter on '<field>' with a random value from the response of submission underwriters
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate submission underwriters results contain the value of <field>

	Examples:
		| field                 |
		| ContractTypeCode      |
		| SubmissionUnderwriterStatusCode |
		| CoverTypeCode         |
		| ClassOfBusinessCode   |

#---------------------------------------------------------------------------
@CoreAPI  
Scenario Outline: 20 GetALL (multiple value filtering Filter=A,B,C):  GETALL submission underwriter as a broker, with multiple value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a '' filter on '<field>' with values '<values>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And for submission underwriters I validate I am getting the correct results for <field> with values of (<values>)

	Examples:
	| field                 | values                                         |
	| SubmissionVersionNumber    | 1,5                                            |


#---------------------------------------------------------------------------
@CoreAPI  
Scenario Outline: 21 GetALL (multiple value filtering MATCH Filter=A,B,C):  GETALL submission underwriter as a broker, with multiple value filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on '<field>' with values '<values>'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And for submission underwriters I validate I am getting the correct results for <field> with values of (<values>)

	Examples:
	| field                 | values                                         |
	| SubmissionUnderwriterStatusCode | DRFT, SENT                                     |
	| ContractTypeCode      | direct_insurance_contract,reinsurance_contract |
	| ClassOfBusinessCode  | marine_increased_value,marine_hull             |

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 22 GetALL (datetime range filtering):  GETALL submission underwriter as a broker, with datetime range filtering 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate that the submission underwriters results for the field '<field>' are between the date of yesterday and today

	Examples:
	| field           | 
	| CreatedDateTime | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 23 (DATE before,after filtering): GETALL submission underwriters, before or after a date for CreatedDateTime, _last_modified
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I add a date filter on the '<field>' <comparison> the date of <when>
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And validate that the submission underwriters results for the field '<field>' are <comparison> the date of <when>
	
	Examples:
		| field           | comparison | when      |
		| CreatedDateTime | before     | yesterday |
		| CreatedDateTime | after      | yesterday |
		#| _last_modified  | before      | yesterday |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 24 GETALL submission underwriter once broker POST QMD, QM is returned and validate 
	#POST QMD TO AMLIN-LEE
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	And the response status code should be "200"
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"	
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I store the submission underwriter in the context from the collection
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And validate submission underwriter context matching the posted submission underwriter

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 25 GETById submission underwriter once broker POST QMD and validate QM returned
	#POST QMD TO AMLIN-LEE
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		| Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And I set the SubmissionDialogue context with the following documents for broker
	| FileName|
	| MRC_Placingslip.docx |
	| sample_mrc.docx      |
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And store the submission underwriter id by finding the submission reference quoted
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I store the submission underwriter in the context
	And validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And validate submission underwriter context matching the posted submission underwriter


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 26 GetById: GETById from an underwriter of the same company, returns 200 
	And I log in as an underwriter '<UnderwriterUserEmailAddress1>'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	Given I log in as an underwriter '<UnderwriterUserEmailAddress2>'
	When  I make a GET request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "200" 

	Examples:
	| UnderwriterUserEmailAddress1                          | UnderwriterUserEmailAddress2                         | UnderwriterOrganisationname |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com      | AmlinUnderwriter.Sidney@LIMOSSDIDEV.onmicrosoft.com  | AMLIN                       |
	| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | BeazleyUnderwriter.Fiona@LIMOSSDIDEV.onmicrosoft.com | BEAZLEY                     |


#---------------------------------------------------------------------------
@CoreAPI
Scenario Outline: 27 GETALL: GETALL from an underwriter of the same company, returns 200 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		 | JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		 | Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
		 | Data\1_2_Model\SubmissionDocument_1.json | sample_mrc.docx      | application/vnd.openxmlformats-officedocument.wordprocessingml.document | advice_premium        | Data\mrc_documents\sample_mrc.docx | Client Corrspondence test |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress    | CommunicationsMethodCode |
		| <UnderwriterUserEmailAddress1> | PLATFORM                 |
	And I reset all the filters

	And I log in as an underwriter '<UnderwriterUserEmailAddress1>'
	And  I make a GET request to '/SubmissionUnderwriters' resource 
	And the response status code should be "200"
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And I pick a random submission underwriter
	Given I log in as an underwriter '<UnderwriterUserEmailAddress2>'
	And I add a '' filter on 'SubmissionReference' with a random value from the response of submission underwriters
	When  I make a GET request to '/SubmissionUnderwriters/' resource and append the filter(s)
	Then the response status code should be "200" 
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And collection of submission underwriters should have a total greater than 0

	Examples:
	| UnderwriterUserEmailAddress1                          | UnderwriterUserEmailAddress2                         | UnderwriterOrganisationname |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com      | AmlinUnderwriter.Sidney@LIMOSSDIDEV.onmicrosoft.com  | AMLIN                       |
	| BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com | BeazleyUnderwriter.Fiona@LIMOSSDIDEV.onmicrosoft.com | BEAZLEY                     |


#---------------------------------------------------------------------------
@CoreAPI
Scenario: 29 Adding _select parameter will return results with the exact fields
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a '' filter on '_select' with a value 'submissionReference,submissionVersionNumber,UnderwriterUserFullName,underwriterUserEmailAddress'
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	Then the response status code should be "200"
	And the response body should contain a collection of submission underwriters
	And collection of submission underwriters should have a link with proper value

	
#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 30 (DATE range Filter): GETALL Submission underwriters, with a date range 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a date ranging filter on the '<field>' from yesterday to tomorrow
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission underwriters
	And validate that the submission underwriters results for the field '<field>' are between the date of yesterday and today

	Examples:
		| field           | 
		| CreatedDateTime | 

#---------------------------------------------------------------------------
@CoreAPI 
Scenario Outline: 31 (range adatetime..bdatetime): GETALL Submission underwriters, with a date range 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add date range filter on the '<field>' as adatetime..bdatetime where adatetime=yesterday and bdatetime=today
	When I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s) 
	Then the response status code should be "200"
	And the response body should contain a collection of submission underwriters
	And validate that the submission underwriters date results for the field '<field>' are  for yesterday

	Examples:
		| field           | 
		| CreatedDateTime | 

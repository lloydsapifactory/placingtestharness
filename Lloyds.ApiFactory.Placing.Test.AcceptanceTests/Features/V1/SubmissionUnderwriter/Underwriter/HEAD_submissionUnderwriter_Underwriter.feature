#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: HEAD_SubmissionUnderwriter_Underwriter
	In order to retrieve a submission underwriter
	As a broker
	I want to be able to do a HEAD request to SubmissionUnderwriters

	Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 01 [SubmissionUnderwriter_HEAD_Underwriter_HeadSubmissionUnderwriterById] As an underwriter I want to be able to do a HEAD SubmissionUnderwriter resource for a valid submission underwriter, returning 2xx
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And store the submission underwriter id by finding the submission reference quoted
	When I make a HEAD request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be in "2xx" range

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 02 [SubmissionUnderwriter_HEAD_Underwriter_SubmissionUnderwriterNotFound] 
	Given I log in as an underwriter 'amlinunderwriter.lee@limossdidev.onmicrosoft.com'
	When  I make a HEAD request to '/SubmissionUnderwriters/7777777777' resource
	Then the response status code should be "404"

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 03 [SubmissionUnderwriter_HEAD_Underwriter_UnderwriterNotAuthorised] 
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I POST a submission on behalf of the broker 'bellbroker.bella@limossdidev.onmicrosoft.com' with the following submission documents
		| JsonPayload                         | FileName             | FileMimeType                                                            | DocumentType          | File                               | DocumentDescription       |
		| Data\1_2_Model\SubmissionDocument_1.json | MRC_Placingslip.docx | application/vnd.openxmlformats-officedocument.wordprocessingml.document | document_placing_slip | Data\mrc_documents\sample_mrc.docx | Placing Slip test         |
	And I set the SubmissionReference
	And I POST SubmissionUnderwriter from the data provided submission documents have been posted
		| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |     
		| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                 | 
	And I reset all the filters
	And POST as a broker to SubmissionDialogues with following info
	| IsDraftFlag | SenderActionCode | SenderNote             |
	| False       | RFQ              | a note from the broker |

	Given I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference
	And I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)
	And the response is a valid application/json; charset=utf-8 type of submission underwriters collection
	And store the submission underwriter id by finding the submission reference quoted

	Given I log in as an underwriter 'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com'
	When I make a HEAD request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be '400' or '404' or '403'
		

#----------------------------------------------------------------------- 
# Copyright © 2018-2019, The Society of Lloyd’s
#-----------------------------------------------------------------------

@ApiGwSecurity
Feature: PUT_SubmissionUnderwriter_Underwriter_Negative
	In order to update a submission underwriter
	As an underwriter
	I want to be able to test that I CANNOT PUT to SubmissionUnderwriters

Background:
	Given I have placing V1 base Uri
	And I set Accept header equal to application/json type
	Given I log in as a broker 'bellbroker.bella@limossdidev.onmicrosoft.com'
	And I set the feature submission context from the json payload Data\1_4_Model\Submission_1.json
	And I POST a set of 1 submissions from the data
	And I POST SubmissionUnderwriter from the data
	| UnderwriterUserEmailAddress                      | CommunicationsMethodCode |
	| amlinunderwriter.lee@limossdidev.onmicrosoft.com | PLATFORM                    |
	And I store the submission in the context
	And I set the SubmissionUnderwriterId
	And I clear down test context
	And I save the submission underwriter context from submission

#---------------------------------------------------------------------------
@CoreAPI  
Scenario: 01 [SubmissionUnderwriter_DELETE_Underwriter_UnderwriterNotAllowed] Unauthorised underwriter CANNOT do a PUT, returns 400/403
	And I log in as an underwriter 'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When  I make a PUT request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI 
Scenario: 02 [SubmissionUnderwriter_DELETE_Underwriter_UnderwriterNotAllowed] Underwriter from the same company CANNOT do a PUT, returns 400/403
	And I log in as an underwriter 'amlinunderwriter.sidney@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When  I make a PUT request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'ResourceAccessDenied'

#---------------------------------------------------------------------------
@CoreAPI
Scenario: 03 [SubmissionUnderwriter_DELETE_Underwriter_UnderwriterNotAllowed] Underwriter from another company CANNOT do a PUT, returns 400/403
	And I log in as an underwriter 'beazleyunderwriter.fiona@limossdidev.onmicrosoft.com'
	And the submission underwriter is saved for the scenario PUT
	When  I make a PUT request to '/SubmissionUnderwriters/{SubmissionUnderwriterId}' resource 
	Then the response status code should be "400" or "403"
	And the response contains a validation error 'ResourceAccessDenied'

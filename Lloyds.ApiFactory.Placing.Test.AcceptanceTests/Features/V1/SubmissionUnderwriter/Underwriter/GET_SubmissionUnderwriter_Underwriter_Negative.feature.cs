// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Features.V1.SubmissionUnderwriter.Underwriter
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("GET_SubmissionUnderwriter_Underwriter_Negative")]
    [NUnit.Framework.CategoryAttribute("ApiGwSecurity")]
    public partial class GET_SubmissionUnderwriter_Underwriter_NegativeFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "ApiGwSecurity"};
        
#line 1 "GET_SubmissionUnderwriter_Underwriter_Negative.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "GET_SubmissionUnderwriter_Underwriter_Negative", "\tIn order to retrieve submission underwriters information that belong to my organ" +
                    "isation\r\n\tAs an underwriter\r\n\tI want to be able to do a GET to SubmissionUnderwr" +
                    "iters", ProgrammingLanguage.CSharp, new string[] {
                        "ApiGwSecurity"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 11
#line hidden
#line 12
 testRunner.Given("I have placing V1 base Uri", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 13
 testRunner.And("I set Accept header equal to application/json type", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("01 [SubmissionUnderwriter_GET_Underwriter_SubmissionUnderwriterNotFound] GetById " +
            "to a submission underwriter that does not exists returns 404")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _01SubmissionUnderwriter_GET_Underwriter_SubmissionUnderwriterNotFoundGetByIdToASubmissionUnderwriterThatDoesNotExistsReturns404()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 [SubmissionUnderwriter_GET_Underwriter_SubmissionUnderwriterNotFound] GetById " +
                    "to a submission underwriter that does not exists returns 404", null, new string[] {
                        "CoreAPI"});
#line 17
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 18
 testRunner.Given("I log in as an underwriter \'AmlinUnderwriter.Lee@LIMOSSDIDEV.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 19
 testRunner.When("I make a GET request to \'/SubmissionUnderwriters/6i6i6\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 20
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 21
 testRunner.And("the response contains a validation error \'ResourceDoesNotExist\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("02 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById to a " +
            "submission that my organisation has no access to returns 400/403/404")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _02SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorisedGETByIdToASubmissionThatMyOrganisationHasNoAccessToReturns400403404()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById to a " +
                    "submission that my organisation has no access to returns 400/403/404", null, new string[] {
                        "CoreAPI"});
#line 25
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 26
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table549 = new TechTalk.SpecFlow.Table(new string[] {
                            "JsonPayload",
                            "FileName",
                            "FileMimeType",
                            "DocumentType",
                            "File",
                            "DocumentDescription"});
                table549.AddRow(new string[] {
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "MRC_Placingslip.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "document_placing_slip",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Placing Slip test"});
                table549.AddRow(new string[] {
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "sample_mrc.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Client Corrspondence test"});
#line 27
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                        "soft.com\' with the following submission documents", ((string)(null)), table549, "And ");
#line hidden
#line 31
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table550 = new TechTalk.SpecFlow.Table(new string[] {
                            "UnderwriterUserEmailAddress",
                            "CommunicationsMethodCode"});
                table550.AddRow(new string[] {
                            "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                            "PLATFORM"});
#line 32
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                        "n posted", ((string)(null)), table550, "And ");
#line hidden
#line 35
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table551 = new TechTalk.SpecFlow.Table(new string[] {
                            "IsDraftFlag",
                            "SenderActionCode",
                            "SenderNote"});
                table551.AddRow(new string[] {
                            "False",
                            "RFQ",
                            "a note from the broker"});
#line 36
 testRunner.And("POST as a broker to SubmissionDialogues with following info", ((string)(null)), table551, "And ");
#line hidden
#line 40
 testRunner.Given("I log in as an underwriter \'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com" +
                        "\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 41
 testRunner.When("I make a GET request to \'/SubmissionUnderwriters/{SubmissionUnderwriterId}\' resou" +
                        "rce", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 42
 testRunner.Then("the response status code should be \"400\" or \"403\" or \"404\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 43
 testRunner.And("the response contains a validation error \'ResourceAccessDenied\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("03 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from " +
            "an UNAUTHORISED UW(BeazleyTaylor) with the correct QMDID found from the assigned" +
            " UW (AMLINLEE), returns 400/403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        public virtual void _03SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorisedGETByIdFromAnUNAUTHORISEDUWBeazleyTaylorWithTheCorrectQMDIDFoundFromTheAssignedUWAMLINLEEReturns400403()
        {
            string[] tagsOfScenario = new string[] {
                    "CoreAPI"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("03 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from " +
                    "an UNAUTHORISED UW(BeazleyTaylor) with the correct QMDID found from the assigned" +
                    " UW (AMLINLEE), returns 400/403", null, new string[] {
                        "CoreAPI"});
#line 47
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 49
 testRunner.And("I log in as a broker \'bellbroker.bella@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table552 = new TechTalk.SpecFlow.Table(new string[] {
                            "SubmissionVersionNumber",
                            "JsonPayload",
                            "FileName",
                            "FileMimeType",
                            "DocumentType",
                            "File",
                            "DocumentDescription"});
                table552.AddRow(new string[] {
                            "1",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "MRC_Placingslip.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "document_placing_slip",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Placing Slip test"});
                table552.AddRow(new string[] {
                            "1",
                            "Data\\1_2_Model\\SubmissionDocument_1.json",
                            "sample_mrc.docx",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "advice_premium",
                            "Data\\mrc_documents\\sample_mrc.docx",
                            "Client Corrspondence test"});
#line 50
 testRunner.And("I POST a submission on behalf of the broker \'bellbroker.bella@limossdidev.onmicro" +
                        "soft.com\' with the following submission documents", ((string)(null)), table552, "And ");
#line hidden
#line 54
 testRunner.And("I set the SubmissionReference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table553 = new TechTalk.SpecFlow.Table(new string[] {
                            "UnderwriterUserEmailAddress",
                            "CommunicationsMethodCode"});
                table553.AddRow(new string[] {
                            "amlinunderwriter.lee@limossdidev.onmicrosoft.com",
                            "PLATFORM"});
#line 55
 testRunner.And("I POST SubmissionUnderwriter from the data provided submission documents have bee" +
                        "n posted", ((string)(null)), table553, "And ");
#line hidden
#line 58
 testRunner.And("I reset all the filters", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table554 = new TechTalk.SpecFlow.Table(new string[] {
                            "IsDraftFlag",
                            "SenderActionCode",
                            "SenderNote"});
                table554.AddRow(new string[] {
                            "False",
                            "RFQ",
                            "a note from the broker"});
#line 59
 testRunner.And("POST as a broker to SubmissionDialogues with following info", ((string)(null)), table554, "And ");
#line hidden
#line 63
 testRunner.And("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 64
 testRunner.And("I add a \'match\' filter on \'SubmissionReference\' with the value from the reference" +
                        "d submission as submission reference", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 65
 testRunner.And("I make a GET request to \'/SubmissionUnderwriters\' resource and append the filter(" +
                        "s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 66
 testRunner.And("the response is a valid application/json; charset=utf-8 type of submission underw" +
                        "riters collection", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 67
 testRunner.And("store the submission underwriter id by finding the submission reference quoted", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 69
 testRunner.Given("I log in as an underwriter \'BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com" +
                        "\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 70
 testRunner.When("I make a GET request to \'/SubmissionUnderwriters/{SubmissionUnderwriterId}\' resou" +
                        "rce", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 71
 testRunner.Then("the response status code should be \"403\" or \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 72
 testRunner.And("the response contains a validation error \'ResourceAccessDenied\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("04 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from " +
            "an underwriter in a different company, returns 400/403")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("BeazleyUnderwriter.Fiona@LIMOSSDIDEV.onmicrosoft.com", "BEAZLEY", null)]
        [NUnit.Framework.TestCaseAttribute("BeazleyUnderwriter.Taylor@LIMOSSDIDEV.onmicrosoft.com", "BEAZLEY", null)]
        public virtual void _04SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorisedGETByIdFromAnUnderwriterInADifferentCompanyReturns400403(string underwriterUserEmailAddress, string underwriterOrganisationname, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("04 [SubmissionUnderwriter_GET_Underwriter_UnderwriterNotAuthorised] GETById from " +
                    "an underwriter in a different company, returns 400/403", null, @__tags);
#line 77
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 78
 testRunner.Given("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 79
 testRunner.And("I make a GET request to \'/SubmissionUnderwriters\' resource", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 80
 testRunner.And("the response status code should be \"200\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 81
 testRunner.And("the response is a valid application/json; charset=utf-8 type of submission underw" +
                        "riters collection", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 82
 testRunner.And("I pick a random submission underwriter", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 84
 testRunner.Given(string.Format("I log in as an underwriter \'{0}\'", underwriterUserEmailAddress), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 85
 testRunner.When("I make a GET request to \'/SubmissionUnderwriters/{SubmissionUnderwriterId}\' resou" +
                        "rce", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 86
 testRunner.Then("the response status code should be \"403\" or \"400\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("05 GetALL (exact match INVALID filtering):  GETALL with INVALID single field filt" +
            "ering, returns 4xx")]
        [NUnit.Framework.CategoryAttribute("CoreAPI")]
        [NUnit.Framework.TestCaseAttribute("SubmissionUnderwriterId", "test", null)]
        [NUnit.Framework.TestCaseAttribute("BrokerUserContactNumber", "test", null)]
        [NUnit.Framework.TestCaseAttribute("UnderwriterOrganisationId", "test", null)]
        [NUnit.Framework.TestCaseAttribute("CommunicationsMethodCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("ProgrammeId", "test", null)]
        [NUnit.Framework.TestCaseAttribute("RiskRegionCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("RiskCountryCode", "test", null)]
        [NUnit.Framework.TestCaseAttribute("PeriodOfCover", "test", null)]
        public virtual void _05GetALLExactMatchINVALIDFilteringGETALLWithINVALIDSingleFieldFilteringReturns4Xx(string field, string value, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "CoreAPI"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("05 GetALL (exact match INVALID filtering):  GETALL with INVALID single field filt" +
                    "ering, returns 4xx", null, @__tags);
#line 96
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 11
this.FeatureBackground();
#line hidden
#line 97
 testRunner.Given("I log in as an underwriter \'amlinunderwriter.lee@limossdidev.onmicrosoft.com\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 98
 testRunner.And(string.Format("I add a filter on \'{0}\' with a value \'{1}\'", field, value), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 99
 testRunner.When("I make a GET request to \'/SubmissionUnderwriters\' resource and append the filter(" +
                        "s)", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 100
 testRunner.Then("the response status code should be in \"4xx\" range", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 101
 testRunner.And("the response contains a validation error \'InvalidFilterCriterion\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    public abstract class SubmissionApiBaseSteps : ApiBaseSteps
    {
        protected readonly User _userContext;

        public SubmissionApiBaseSteps()
        {
            _userContext = new User();
            fContext.Add(Constants.UserContext, _userContext);

            _submissionContext = new Submission();
            fContext.Add(Constants.SubmissionContext, _submissionContext);
        }

        protected SubmissionApiBaseSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext, ScenarioContext scenarioContext, FeatureContext featureContext) : this()
        {
            fContext = featureContext;
        }


    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds = 90)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

            wait.Until(_driver =>
            {
                try
                {
                    var el = _driver.FindElement(by);
                    return el;
                }
                catch
                {
                    return null;
                }

            });

            return driver.FindElement(by);
        }


        public static void TryClick(this IWebElement element, IWebDriver driver = null, By by = null)
        {
            DefaultWait<IWebElement> wait = new DefaultWait<IWebElement>(element);

            bool foundStaleElement = false;
            for (int i = 0; i < 5000; i++)
            {
                try
                {
                    if (foundStaleElement && driver != null && by != null)
                    {
                        var el = driver.FindElement(by, 20);
                        el.Click();
                        break;
                    }

                    element.Click();
                    break;
                }
                catch (StaleElementReferenceException)
                {
                    if (driver != null)
                        Thread.Sleep(TimeSpan.FromSeconds(1));

                    foundStaleElement = true;
                    continue;

                }
                catch (InvalidOperationException)
                {
                    if (driver != null)
                        Thread.Sleep(TimeSpan.FromSeconds(1));

                    continue;
                }
                catch (ElementNotVisibleException)
                {
                    if (driver != null)
                        Thread.Sleep(TimeSpan.FromSeconds(1));

                    continue;
                }
                catch (System.Reflection.TargetInvocationException)
                {
                    if (driver != null)
                        Thread.Sleep(TimeSpan.FromSeconds(1));

                    continue;
                }
                catch (Exception ex)
                {
                    var test = ex;
                }
            }


        }


        public static IWebElement WaitElementToBeNotNull(this IWebDriver driver, IWebElement element, int timeoutsec = 90)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutsec));
                wait.Until(d => element != null);
                return element;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }

        public static IWebElement WaitElementToBeClickable(this IWebDriver driver, IWebElement element, int timeoutsec = 90)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutsec));

            return wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void WaitForElementToBeClickableAndClick(this IWebDriver driver, IWebElement element, int timeoutssec = 90)
        {
            driver.WaitElementToBeClickable(element, timeoutssec).TryClick();
        }

        public static void WaitForElementToHaveTextNotNull(this IWebDriver driver, IWebElement element, int timeoutsec = 90)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutsec));

            wait.Until(d => element.Text != null);
        }

        public static IWebElement FindElementWhenNotNull(this IWebDriver driver, By selector, int timeoutsec = 90)
        {
            for (int i = 0; i < 50000; i++)
            {
                try
                {
                    var element = driver.FindElement(selector, timeoutsec);
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutsec));
                    wait.Until(d => element != null);
                    return element;
                }
                catch (NoSuchElementException)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
                catch (StaleElementReferenceException)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
            }
            return null;

        }

        public static IWebElement FindElementReturningNull(this IWebDriver driver, By by)
        {
            IWebElement element = null;

            try
            {
                element = driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                return null;
            }
            catch (StaleElementReferenceException)
            {
                return null;
            }

            return element;
        }


    }
}

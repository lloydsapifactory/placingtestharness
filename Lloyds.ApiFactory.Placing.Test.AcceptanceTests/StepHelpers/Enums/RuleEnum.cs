//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.ComponentModel;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums
{
    public class RuleEnum
    {
        public enum Rule
        {
            [Description("No leading or trailing whitespace")]
            no_leading_or_trailing_whitespace,

            [Description("No whitespace")]
            no_whitespace,

            [Description("Special characters")]
            no_special_chars,

            [Description("No curly open/close brackets")]
            no_curly_open_close_brackets,

            [Description("No more than 2 consecutive whitespace characters")]
            no_more_than2_consecutive_whitepsace_chars,

            [Description("ISO-3166-2 Format")]
            iso_3166_2_format,

            [Description("No valid Email")]
            no_valid_email,

            [Description("No rule")]
            no_rule,
        }
    }
}

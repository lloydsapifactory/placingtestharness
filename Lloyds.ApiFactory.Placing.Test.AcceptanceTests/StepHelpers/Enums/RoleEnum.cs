//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.ComponentModel;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums
{
    public class RoleEnum
    {
        public enum Role
        {
            [Description("broker")]
            Broker,

            [Description("underwriter")]
            Underwriter,


        }
    }
}

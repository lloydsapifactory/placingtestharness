//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.ComponentModel;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums
{
    public class EntityEnum
    {
        public enum Entity
        {
            [Description("submission")]
            submission,

            [Description("submissions")]
            submissions,

            [Description("submission documents")]
            submission_documents,

            [Description("submission underwriters")]
            submission_underwriters,

            [Description("submission dialogue")]
            submission_dialogue,

            [Description("underwriter organisations")]
            underwriter_organisations,
        }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Core;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using ExpectedConditions = OpenQA.Selenium.Support.UI.ExpectedConditions;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class UICommonSteps : ApiBaseSteps, IDisposable
    {
        private readonly User _userContext;

        private ChromeDriver _chromeDriver;
        private WebDriverWait _wait;

        public UICommonSteps(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext
            //WebDriverContext webDriverContext
            //LoginPage loginpage
            ) 
            : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            //_loginPage = loginpage;
            //_webDriverContext = webDriverContext;

            if (!sContext.ContainsKey(Constants.SubmissionContext)) sContext.Add(Constants.SubmissionContext, new Submission());
            if (!sContext.ContainsKey(Constants.UserContext)) sContext.Add(Constants.UserContext, new User());

            _userContext = sContext[Constants.UserContext] as User;
            _submissionContext = sContext[Constants.SubmissionContext] as Submission;

            RegisterChromeDriver();
        }

        [Given(@"I login to PPL UI as '(.*)' user")]
        [When(@"I login to PPL UI as '(.*)' user")]
        [Then(@"I login to PPL UI as '(.*)' user")]
        public void ThenILoginToThePPLUIAsUser(string userEmailId)
        {
            var userCredential = _objectContainer.Resolve<UserCredential>(userEmailId);

            //POM -- needs to be refactored for every instance
            //_loginPage.NavigateTo(_apiTestContext.PPLUIBaseUrl);

            _chromeDriver.Navigate().GoToUrl(_apiTestContext.PPLUIBaseUrl);
            _chromeDriver.Manage().Window.Maximize();
           
            //_loginPage.EnterLoginName(userCredential.PPLUsername);
            var userNameInput = _chromeDriver.FindElementById("userName");
            _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("userName")));
            userNameInput.SendKeys(userCredential.PPLUsername);

            var passwordInput = _chromeDriver.FindElementById("password");
            _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("password")));
            passwordInput.SendKeys(userCredential.PPLPassword);

            var loginButton = _chromeDriver.FindElementByCssSelector("img.loginGo");
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("img.loginGo")));
            loginButton.Click();

            _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("current-user")));
            Assert.NotNull(_chromeDriver.FindElementById("current-user"));
        }

        [Given(@"I navigate to the draft placements page")]
        [When(@"I navigate to the draft placements page")]
        [Then(@"I navigate to the draft placements page")]
        public void ThenINavigateToTheDraftPlacementsPage()
        {
            var draftLinks = _chromeDriver.FindElementsByClassName("UserCountMoreThanZero");
            Assert.IsTrue(draftLinks.Count >= 2 );
            
            /*
            var draftLink = draftLinks[1].FindElement(By.CssSelector("a"));
            Assert.NotNull(draftLink);
            draftLink.Click();
            */

            var allPlacementsIcon = _chromeDriver.FindElementByCssSelector("img.DashboardIconCol");
            Assert.NotNull(allPlacementsIcon);
            allPlacementsIcon.Click();

            var wait = new WebDriverWait(_chromeDriver, TimeSpan.FromSeconds(2));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("input.FilterSearch")));
            Assert.NotNull(_chromeDriver.FindElementByCssSelector("input.FilterSearch"));
        }

        [Given(@"I search for the submission reference in the ReInsured field")]
        [When(@"I search for the submission reference in the ReInsured field")]
        [Then(@"I search for the submission reference in the ReInsured field")]
        public void ThenISearchForTheSubmissionInTheReInsuredField()
        {
            var submissionPosted = _submissionContext;

            var submission = (Submission)submissionPosted;

            Assert.NotNull(submission);

            var advancedSearchImgButton = _chromeDriver.FindElementByCssSelector("img.OpenAdvancedJsSearch");
            advancedSearchImgButton.Click();

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("input[column=\"CedantInsured\"]")));

            var searchInput = _chromeDriver.FindElementByCssSelector("input[column=\"CedantInsured\"]");
            searchInput.SendKeys(submission.SubmissionReference);
            searchInput.SendKeys(Keys.Enter);

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("input.FilterSearch")));
            Assert.NotNull(_chromeDriver.FindElementByCssSelector("input.FilterSearch"));
        }

        [Given(@"I click on the first search result")]
        [When(@"I click on the first search result")]
        [Then(@"I click on the first search result")]
        public void ThenIClickOnTheSearchResult()
        {
            _wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector("div.ui-widget-overlay")));
            var submissionPanel = _chromeDriver.FindElementByCssSelector("table#project-list");
            Assert.NotNull(submissionPanel);

            IWebElement submissionRow = null;
            try
            {
                submissionRow = _chromeDriver.FindElement(By.CssSelector("tbody.rowBody"));
            }
            catch (NoSuchElementException)
            {
                submissionRow = null;
            }
            catch (StaleElementReferenceException)
            {
                submissionRow = null;
            }

            if (submissionRow == null)
                Assert.Fail("Couldn't find any search results.");

            var submissionLink = submissionRow.FindElement(By.TagName("a"));
            submissionLink.Click();
            _wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("OverviewHeading")));
            //_wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ovPlacement")));

        }



        [Then(@"the (.*) submission details in the UI are correct")]
        public void ThenTheSubmissionDetailsInTheUIAreCorrect(string submissionStatus)
        {

            //  Expand the box
            var overviewColumn = _chromeDriver.FindElementByCssSelector("td.overviewColumn");
            var expandLink = overviewColumn.FindElements(By.TagName("a")).FirstOrDefault(i => i.Displayed);
            expandLink.Click();
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("td.placingDetails")));
            
            var placingDetails = _chromeDriver.FindElementByCssSelector("td.placingDetails");
            var labels = placingDetails.FindElements(By.CssSelector("th.th-largeCOBName")).Select(i => i.Text).ToList();
            var values = placingDetails.FindElements(By.CssSelector("div.dv-largeCOBName")).Select(i => i.Text).ToList();

            Assert.AreEqual(labels.Count(), values.Count());

            var submission = _submissionContext;

            //  Validate the submission status
            var status = overviewColumn.FindElement(By.CssSelector("label.displayRiskStatus")).Text;
            Assert.AreEqual(submissionStatus.ToLowerInvariant(), status.ToLowerInvariant());

            //  Validate the table data
            Assert.AreEqual(values[labels.IndexOf("Contract")], $"{submission.ProgrammeReference}");
            Assert.AreEqual(values[labels.IndexOf("Reference")], $"{submission.SubmissionReference}");
            Assert.AreEqual(values[labels.IndexOf("Business Type")], $"{EnumHelper.GetReferenceValue(EnumHelper.ContractTypeMappings, submission.ContractTypeCode)}");
            Assert.AreEqual(values[labels.IndexOf("(Re)Insured")], $"{submission.InsuredOrReinsured}");
            Assert.AreEqual(values[labels.IndexOf("Class of Business")], $"{EnumHelper.GetReferenceValue(EnumHelper.ClassOfBusinessMappings, submission.ClassOfBusinessCode)}");
            Assert.AreEqual(values[labels.IndexOf("Original Policy Holder")], $"{submission.OriginalPolicyholder}");
            Assert.AreEqual(values[labels.IndexOf("Contract Type")], $"{EnumHelper.GetReferenceValue(EnumHelper.CoverTypeCodeMappings, submission.CoverTypeCode)}");
            Assert.AreEqual(values[labels.IndexOf("Region")], $"{EnumHelper.GetReferenceValue(EnumHelper.RiskRegionCodeMappings, submission.RiskRegionCode)}");

            Assert.True(IsCorrectPeriodOfCover(submission.PeriodOfCover, values[labels.IndexOf("Period")]));
        }

        [Given(@"I navigate to the create submission screen")]
        public void GivenINavigateToTheCreateSubmissionScreen()
        {
            // Find the 'New' button on the page.
            var newNavTab = _chromeDriver.FindElementByCssSelector("li.New-nav");
            Assert.NotNull(newNavTab);

            // If no 'New' button is shown, it most likley means that an Organisation must first be selected.
            if (!newNavTab.Displayed)
            {
                // Click on 'Organisation'.
                var orgNavTab = _chromeDriver.FindElementByCssSelector("li.organisation-nav");
                Assert.NotNull(orgNavTab);
                orgNavTab.Click();

                // Click on the first shown organisation (*ALL appears first though which we don't want).
                _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("li.organisation-nav ul")));
                var orgLink = _chromeDriver.FindElementsByCssSelector("li.organisation-nav ul a")[1];
                Assert.NotNull(orgLink);
                orgLink.Click();

                // Now the New link should be displayed / interactable.
                _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("li.New-nav")));
                newNavTab = _chromeDriver.FindElementByCssSelector("li.New-nav");
            }

            // Click the 'New' button to bring up the list of new things you can do.
            newNavTab.Click();
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("li.New-nav ul")));

            // Find and click the 'Submission' option.
            var submissionLink = _chromeDriver.FindElementByCssSelector("li.New-nav ul a");
            Assert.NotNull(submissionLink);
            submissionLink.Click();

            // Wait until the expected file upload popup appears.
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog input#fileupload")));
        }

        [When(@"I create and (.*) the submission in the UI using the base template (.*) and MRC (.*) for underwriter (.*)")]
        [Given(@"I create and (.*) the submission in the UI using the base template (.*) and MRC (.*) for underwriter (.*)")]
        public void ICreateTheSubmissionInTheUI(string action, string baseTemplatePath, string mrcFilePath, string underwriter)
        {
            if ((_submissionContext == null) || !(_submissionContext is Submission))
                throw new AssertionException("The scenario cannot find the submission object.");

            var submission = base.GetSubmissionFromScenarioContext();

            ThenIUploadAnMRCDocument(mrcFilePath);

            var newSubmissionForm = _chromeDriver.FindElementByCssSelector("form#NewQuoteForm");

            //  Choose broker
            var brokerSelect = newSubmissionForm.FindElement(By.Id("brokerName"));
            SelectOptionByValue(brokerSelect, submission.BrokerUserEmailAddress);

            //  Set submission reference
            var submissionReferenceInput = newSubmissionForm.FindElement(By.Id("quoteReference"));
            WriteToTextbox(submissionReferenceInput, submission.SubmissionReference);

            //  Change focus
            var versionCountLabel = newSubmissionForm.FindElement(By.Id("versionCount"));
            var versionNumber = int.Parse(versionCountLabel.Text);
            submission.SubmissionVersionNumber = versionNumber;
            versionCountLabel.Click();

            //Set version description
            var versionDescriptionInput = newSubmissionForm.FindElement(By.Id("versionDescription"));
            _wait.Until(ExpectedConditions.ElementToBeClickable(versionDescriptionInput));
            Thread.Sleep(500);
            WriteToTextbox(versionDescriptionInput, submission.SubmissionVersionDescription);

            //Set programme reference
            var programmeReferenceInput = newSubmissionForm.FindElement(By.Id("programRef"));
            WriteToTextbox(programmeReferenceInput, submission.ProgrammeReference);

            //Set programme description
            var programmeDescriptionInput = newSubmissionForm.FindElement(By.Id("riskName"));
            WriteToTextbox(programmeDescriptionInput, submission.ProgrammeDescription);

            //Set contract reference
            var contractReferenceInput = newSubmissionForm.FindElement(By.Id("contractUMR"));
            var contractReferenceWithoutBrokerCode = submission.ContractReference.Substring(5);
            WriteToTextbox(contractReferenceInput, contractReferenceWithoutBrokerCode);

            //Set contract description
            var contractDescriptionInput = newSubmissionForm.FindElement(By.Id("riskDesc"));
            WriteToTextbox(contractDescriptionInput, submission.ContractDescription);

            //Set contract type code
            var contractTypeCodeSelect = newSubmissionForm.FindElement(By.Id("businessType"));
            SelectOptionByValue(contractTypeCodeSelect, submission.ContractTypeCode.ToString());

            //Set insuredOrReinsured
            var reinsuredInput = newSubmissionForm.FindElement(By.Id("reInsured"));
            WriteToTextbox(reinsuredInput, submission.InsuredOrReinsured);

            if (submission.ContractTypeCode == ContractType.reinsurance_contract)
            {
                //Set original policy holder
                var originalPolicyHolderInput = newSubmissionForm.FindElement(By.Id("originalInsured"));
                WriteToTextbox(originalPolicyHolderInput, submission.OriginalPolicyholder);
            }

            //Set contract type code
            var coverTypeCodeSelect = newSubmissionForm.FindElement(By.Id("contractType"));
            SelectOptionByValue(coverTypeCodeSelect, submission.CoverTypeCode);

            //Set class of business code
            var classOfBusinessCodeSelect = newSubmissionForm.FindElement(By.Id("classOfBusiness"));
            SelectOptionByValue(classOfBusinessCodeSelect, submission.ClassOfBusinessCode);

            //  Set RiskRegionCode
            if (!string.IsNullOrWhiteSpace(submission.RiskRegionCode))
            {
                var riskRegionCodeSelect = newSubmissionForm.FindElement(By.Id("location"));
                SelectOptionByValue(riskRegionCodeSelect, submission.RiskRegionCode);
            }

            //  Set RiskCountryCode
            if (!string.IsNullOrWhiteSpace(submission.RiskCountryCode))
            {
                var riskCountryCodeSelect = newSubmissionForm.FindElement(By.Id("riskCountry"));
                SelectOptionByValue(riskCountryCodeSelect, submission.RiskCountryCode);
            }

            //  Set Period
            if (submission.PeriodOfCover.InsurancePeriod != null)
            {
                var inceptionDate = $"{submission.PeriodOfCover.InsurancePeriod.InceptionDate.UtcDateTime:ddMMyyyy}";
                var expiryDate = $"{submission.PeriodOfCover.InsurancePeriod.ExpiryDate.UtcDateTime:ddMMyyyy}";

                if (!string.IsNullOrWhiteSpace(inceptionDate))
                {
                    var inceptionDateTextbox = newSubmissionForm.FindElement(By.Id("startDate"));
                    WriteToTextbox(inceptionDateTextbox, inceptionDate);
                    versionCountLabel.Click();
                }

                if (!string.IsNullOrWhiteSpace(expiryDate))
                {
                    var expiryDateTextbox = newSubmissionForm.FindElement(By.Id("endDate"));
                    WriteToTextbox(expiryDateTextbox, expiryDate, true);
                    versionCountLabel.Click();
                }
            }

            //  Set Duration
            if (submission.PeriodOfCover.InsuranceDuration != null)
            {
                var periodTextbox = newSubmissionForm.FindElement(By.Id("period"));
                WriteToTextbox(periodTextbox, submission.PeriodOfCover.InsuranceDuration.DurationNumber.ToString());
            
                var periodUnitSelect = newSubmissionForm.FindElement(By.Id("periodType"));
                SelectOptionByValue(periodUnitSelect, submission.PeriodOfCover.InsuranceDuration.DurationUnit.ToString());
                
            }

            //  Add an underwriter
            ThenIAddAnUnderwriter(underwriter);

            //  Save or send the submission
            ThenISaveOrSendTheSubmission(action);
            
            //  Expand the box
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("td.overviewColumn")));
            var overviewColumn = _chromeDriver.FindElementByCssSelector("td.overviewColumn");
            var expandLink = overviewColumn.FindElements(By.TagName("a")).FirstOrDefault(i => i.Displayed);
            expandLink.Click();
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("td.placingDetails")));

            var placingDetails = _chromeDriver.FindElementByCssSelector("td.placingDetails");
            var labels = placingDetails.FindElements(By.CssSelector("th.th-largeCOBName")).Select(i => i.Text).ToList();
            var values = placingDetails.FindElements(By.CssSelector("div.dv-largeCOBName")).Select(i => i.Text).ToList();
            var computedReference = values[labels.IndexOf("Reference")];

            //  Persist the submission reference in the scenario context
            submission.SubmissionReference = computedReference;
            sContext[Constants.SubmissionContext] = submission;

            var submissionUniqueReference = $"{computedReference}_{submission.SubmissionVersionNumber}";
            sContext[Constants.SubmissionUniqueReference] = submissionUniqueReference;
        }

        [Given(@"I click on the add/remove underwriter link")]
        [When(@"I click on the add/remove underwriter link")]
        [Then(@"I click on the add/remove underwriter link")]
        public void ThenIClickOnTheAddRemoveUnderwriterLink()
        {
            var actionsTab = _chromeDriver.FindElement(By.CssSelector("div.overviewActionsLabel"));
            actionsTab.Click();
            //  .poActionItem:nth-child(1) > a
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".poActionItem:nth-child(1)")));
            var addLink = _chromeDriver.FindElementByCssSelector(".poActionItem:nth-child(1) > a");
            addLink.Click();
        }

        [Given(@"I upload an MRC document (.*)")]
        [When(@"I upload an MRC document (.*)")]
        [Then(@"I upload an MRC document (.*)")]
        public void ThenIUploadAnMRCDocument(string mrcFilePath)
        {
            //  Attach form
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog input#fileupload")));
            var fileUploadButton = _chromeDriver.FindElementByCssSelector("div.ui-dialog input#fileupload");
            var absoluteMrcFilePath =
                Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), mrcFilePath);
            fileUploadButton.SendKeys(absoluteMrcFilePath);

            /*
            // Dismiss the overlay
            var fileUploadOverlay = fileUploadButton.FindElement(By.XPath("ancestor::div"));
            var closeButton = fileUploadOverlay.FindElement(By.CssSelector("div.ui-dialog button.ui-dialog-titlebar-close"));
            closeButton.Click();
            */
            var addButton = _chromeDriver.FindElementById("Add");
            addButton.Click();

            _wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector("div.ui-dialog input#fileupload")));
        }

        [Then(@"I withdraw the submission for the underwriter")]
        public void ThenIWithdrawTheSubmissionForTheUnderwriter()
        {
            var actionsTab = _chromeDriver.FindElement(By.CssSelector("div.overviewActionsLabel"));
            actionsTab.Click();
            //  .poActionItem:nth-child(1) > a
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".poActionItem:nth-child(2)")));
            var addLink = _chromeDriver.FindElementByCssSelector(".poActionItem:nth-child(2) > a");
            addLink.Click();

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.quotePlacingDetails")));
            var reasonTextbox = _chromeDriver.FindElementByCssSelector("div.quotePlacingDetails #transactionReason");
            WriteToTextbox(reasonTextbox, "Withdrawing submission");

            var checkbox = _chromeDriver.FindElement(By.CssSelector("input.panelSendMarket"));
            var className = checkbox.GetAttribute("class");
            _chromeDriver.ExecuteScript($"document.getElementsByClassName('{className}')[0].click();");

            var sendButton = _chromeDriver.FindElementByCssSelector("button#sendQuoteWithdraw");
            sendButton.Click();

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("button#draftButton")));
            var confirmButton = _chromeDriver.FindElementByCssSelector("button#draftButton");
            confirmButton.Click();
        }


        [Given(@"I add an underwriter (.*)")]
        [When(@"I add an underwriter (.*)")]
        [Then(@"I add an underwriter (.*)")]
        public void ThenIAddAnUnderwriter(string underwriter)
        {
            //  Add underwriter
            var newSubmissionForm = _chromeDriver.FindElementByCssSelector("form#NewQuoteForm");
            var underwriterInput = newSubmissionForm.FindElement(By.Id("MarketUnderwriters"));
            WriteToTextbox(underwriterInput, underwriter);
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("ul.ui-autocomplete li.ui-menu-item a")));
            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("ul.ui-autocomplete li.ui-menu-item a")));
            Thread.Sleep(200);
            //  Find underwriter from list
            var selectedUnderwriter = _chromeDriver.FindElementByCssSelector("ul.ui-autocomplete li.ui-menu-item a");
            selectedUnderwriter.Click();

            _wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector("ul.ui-autocomplete")));
        }

        private void IncludeUnderwriter()
        {
            //  Needs to supply a message to underwriter before sending
            var newSubmissionForm = _chromeDriver.FindElementByCssSelector("form#NewQuoteForm");
            _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("reason")));
            var reasonField = newSubmissionForm.FindElement(By.Id("reason"));
            WriteToTextbox(reasonField, "reason");

            //  Check the 'include' underwriter checkbox
            var includeCheckbox = newSubmissionForm.FindElement(By.CssSelector("table.quoteMarketLists.openMarket input[type='checkbox']"));
            var className = includeCheckbox.GetAttribute("class");
            _wait.Until(ExpectedConditions.ElementToBeClickable(includeCheckbox));
            _chromeDriver.ExecuteScript($"document.getElementsByClassName('{className}')[0].click();");
        }

        [Given(@"I (.*) the submission")]
        [When(@"I (.*) the submission")]
        [Then(@"I (.*) the submission")]
        public void ThenISaveOrSendTheSubmission(string action)
        {
            var newSubmissionForm = _chromeDriver.FindElementByCssSelector("form#NewQuoteForm");
            //  Click the Save & Quit or Send button
            var submitButtons = newSubmissionForm.FindElements(By.CssSelector("button#quote"));
            if (action.Equals("save", StringComparison.OrdinalIgnoreCase))
            {
                var saveQuitButton = submitButtons.First(i => i.Text.Equals("Save & Quit", StringComparison.OrdinalIgnoreCase));
                _wait.Until(ExpectedConditions.ElementToBeClickable(saveQuitButton));
                saveQuitButton.Click();
            }
            else if (action.Equals("send", StringComparison.OrdinalIgnoreCase))
            {
                IncludeUnderwriter();

                var saveQuitButton = submitButtons.First(i => i.Text.Equals("Send", StringComparison.OrdinalIgnoreCase));
                _wait.Until(ExpectedConditions.ElementToBeClickable(saveQuitButton));
                saveQuitButton.Click();
            }

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog #draftButton")));
            var confirmButton = _chromeDriver.FindElement(By.CssSelector("div.ui-dialog #draftButton"));
            confirmButton.Click();

            _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div#desktop-project-list")));

        }

        public void Dispose()
        {
            if (ObjectRepository.Driver != null)
            {
                ObjectRepository.Driver.Quit();
                ObjectRepository.Driver.Dispose();
            }

        }

        private void RegisterChromeDriver()
        {
            var chromeOptions = new ChromeOptions();
            var seleniumOptions = _apiTestContext.SeleniumOptions;
            if (!string.IsNullOrWhiteSpace(seleniumOptions))
            {
                chromeOptions.AddArgument(seleniumOptions);
            }
            _chromeDriver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            _wait = new WebDriverWait(_chromeDriver, TimeSpan.FromSeconds(30));
            ObjectRepository.Driver = _chromeDriver;
            _objectContainer.RegisterInstanceAs(ObjectRepository.Driver);
        }

        private void SelectOptionByValue(IWebElement element, string value)
        {
            Assert.NotNull(element);
            var selectOption = new SelectElement(element);
            selectOption.SelectByValue(value);
        }

        private void WriteToTextbox(IWebElement element, string text, bool replaceAllText = false)
        {
            Assert.NotNull(element);

            if (replaceAllText)
            {
                element.Clear();
            }

            element.SendKeys(text);
        }
        
        private bool IsCorrectPeriodOfCover(PeriodChoice periodChoice, string actual)
        {
            var expected = string.Empty;
            if (periodChoice.InsurancePeriod != null)
            {
                expected = $"{periodChoice.InsurancePeriod.InceptionDate.UtcDateTime:dd MMM yyyy} - {periodChoice.InsurancePeriod.ExpiryDate.UtcDateTime:dd MMM yyyy}";
            }
            else
            {
                expected =
                    $"{periodChoice.InsuranceDuration.DurationNumber} {periodChoice.InsuranceDuration.DurationUnit}";
            };

            return string.Equals(expected, actual);
        }

        
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using static Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums.EntityEnum;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class AbstractStepsResults : ApiBaseSteps
    {
        private StateHolder _stateHolder;

        public AbstractStepsResults(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext, 
            ScenarioContext scenarioContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;
        }


        [Then(@"validate (.*) results contain the field (.*)")]
        public void ThenValidateResultsContainSubmissionDocumentAddedOn(EntityEnum.Entity entity, string fieldname)
        {
            if (entity == EntityEnum.Entity.submission)
            {
                if (_submissionCollectionContext.Items != null)
                {
                    foreach (var item in _submissionCollectionContext.Items)
                        AssertPropertyAndValue(item, fieldname);
                }
                else
                    throw new AssertionException("The submission collection data is empty.");
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentCollectionContext.Items != null)
                {
                    foreach (var item in _submissionDocumentCollectionContext.Items)
                        AssertPropertyAndValue(item, fieldname);
                }
                else
                    throw new AssertionException("The submission documents collection data is empty.");
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext.Items != null)
                {
                    foreach (var item in _submissionUnderwriterCollectionContext.Items)
                        AssertPropertyAndValue(item, fieldname);
                }
                else
                    throw new AssertionException("The submission documents collection data is empty.");
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext.Items != null)
                {
                    foreach (var item in _submissionDialogueCollectionContext.Items)
                        AssertPropertyAndValue(item, fieldname);
                }
                else
                    throw new AssertionException("The submission dialogue collection data is empty.");
            }
        }

        [Then(@"validate (.*) result contain the field (.*)")]
        public void ValidateResultContainSubmissionDocumentAddedOn(EntityEnum.Entity entity, string fieldname)
        {
            if (entity == EntityEnum.Entity.submission)
            {
                if (_submissionContext != null)
                    AssertPropertyAndValue(_submissionContext, fieldname);
                else
                    throw new AssertionException("The submission document data is empty.");
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentContext != null)
                    AssertPropertyAndValue(_submissionDocumentContext, fieldname);
                else
                    throw new AssertionException("The submission document data is empty.");
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_SubmissionUnderwriterContext != null)
                    AssertPropertyAndValue(_SubmissionUnderwriterContext, fieldname);
                else
                    throw new AssertionException("The submission underwriter data is empty.");
            }
        }

        [Then(@"validate (.*) results contain the following fields")]
        public void ThenValidateResultsContainSubmissionDocumentAddedOn(EntityEnum.Entity entity, Table mandatoryFieldsTable)
        {
            object[] array = SetArrayCollection(entity);

            for (var i = 0; i < mandatoryFieldsTable.Rows.Count; i++)
            {
                var field = mandatoryFieldsTable.Rows[i].FirstOrDefault().Value;

                foreach (var item in array)
                {
                    AssertPropertyAndValue(item, field);
                }
            }
        }

        [Then(@"validate (.*) context contain the following fields")]
        public void ThenValidateContextContainSubmissionDocumentAddedOn(EntityEnum.Entity entity, Table mandatoryFieldsTable)
        {
            var currentObject = SetObject(entity);

            for (var i = 0; i < mandatoryFieldsTable.Rows.Count; i++)
            {
                var field = mandatoryFieldsTable.Rows[i].FirstOrDefault().Value;
                AssertPropertyAndValue(currentObject, field);
            }
        }

        [Then(@"for the (.*) and the returned results the field '(.*)' has the structure of '(.*)'")]
        public void ThenTheFieldHasTheStructureOfResults(EntityEnum.Entity entity, string field, string fieldStructure)
        {
            object[] array = SetArrayCollection(entity);

            var fields = fieldStructure.Split('_');

            if (array != null)
            {
                foreach (var item in array)
                {
                    StepsExension.BuildStructure(fields, item, out string structuredString);

                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field);

                    if (valueAssert == null)
                        Assert.Fail($"The value of the field {field} is null.");

                    //// check the structure
                    Assert.IsTrue(valueAssert.ToString().Contains(structuredString));
                }

            }

        }

        [Then(@"for the (.*) and the returned result the field '(.*)' has the structure of '(.*)'")]
        public void ThenTheFieldHasTheStructureOfResult(EntityEnum.Entity entity, string field, string fieldStructure)
        {
            var currentObject = SetObject(entity);

            var fields = fieldStructure.Split('_');

            if (currentObject != null)
            {
                StepsExension.BuildStructure(fields, currentObject, out string structuredString);

                var valueAssert = ReflectionHelper.GetPropertyValue(currentObject, field);

                if (valueAssert == null)
                    Assert.Fail($"The value of the field {field} is null.");

                Assert.True(valueAssert.ToString().Contains(structuredString, 
                    StringComparison.OrdinalIgnoreCase),
                    $"Failing value {valueAssert} doesnt contain {structuredString}");
            }
            else
                Assert.Fail($"Current object {currentObject} is null.");
        }
        

        [Then(@"validate (.*) results contain the value of (.*)")]
        public void ThenValidateTheResultsOfDocumentIdWith(EntityEnum.Entity entity, string field)
        {
            object[] array = SetArrayCollection(entity);

            var notfound = false;

            if (array != null)
            {
                foreach (var item in array)
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

                    if (valueAssert != null)
                    {
                        if (!valueAssert.ToString().Contains(_stateHolder.ParamsBuilderValueToValidate, StringComparison.OrdinalIgnoreCase))
                        {
                            notfound = true;
                            break;
                        }
                    }
                }

                if (array.Length > 0)
                    Assert.IsFalse(notfound, $"Couldn't find the value of the field {field}");
            }
            else
                throw new AssertionException("The submission collection data is empty.");

        }

        [Then(@"validate (.*) results contain values of the fields '(.*)'")]
        public void ThenValidateTheResultsOfFields(EntityEnum.Entity entity, string fieldsConcatString)
        {
            object[] array = SetArrayCollection(entity);
            var fields = fieldsConcatString.Split(",").ToList();

            var found = false;

            if (array != null)
            {
                foreach (var item in array)
                {
                    foreach (var field in fields)
                    {
                        var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

                        if (valueAssert.ToString().Contains(_stateHolder.ParamsBuilderValueToValidate, StringComparison.OrdinalIgnoreCase))
                        {
                            found = true;
                            break;
                        }
                    }
                }

               
            }
            else
                throw new AssertionException("The submission collection data is empty.");

        }

        [Then(@"validate (.*) result contain the value of (.*)")]
        public void ThenValidateTheResultValue(EntityEnum.Entity entity, string field)
        {
            object SubmissionEntity = SetObject(entity);
            var found = false;

            var valueAssert = ReflectionHelper.GetPropertyValue(SubmissionEntity, field.Trim());
            var expectedValue = _stateHolder._sTestParameters[field];
            if (valueAssert.ToString().Contains(expectedValue, StringComparison.OrdinalIgnoreCase))
            {
                found = true;
            }

            Assert.IsTrue(found, $"Couldn't find the value of the field {field}");
        }

        [Then(@"validate (.*) result DOESNT contain the value of (.*)")]
        public void ThenValidateTheResultdoesntValue(EntityEnum.Entity entity, string field)
        {
            object objectUsed = SetObject(entity);

            var valueAssert = ReflectionHelper.GetPropertyValue(objectUsed, field.Trim());
            var expectedValue = _stateHolder._sTestParameters[field];

            if (valueAssert.ToString().Contains(expectedValue, StringComparison.OrdinalIgnoreCase))
            {
                Assert.Fail($"Found the value of the field {field}.");
            }

        }


        [Then(@"validate (.*) results contain item added on '(.*)' with a value '(.*)'")]
        public void ThenValidateResultsContainSubmissionDocumentAdded(EntityEnum.Entity entity, string fieldname, string fieldvalue)
        {
            object[] array = SetArrayCollection(entity);

            var found = false;

            if (array != null)
            {
                foreach (var item in array)
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, fieldname.Trim());

                    if (Convert.ToString(valueAssert) == fieldvalue)
                    {
                        found = true;
                        break;
                    }
                }
            }
            else
                throw new AssertionException("The submission documents collection data is empty.");

            if (!found)
                Assert.Fail($"Cannot find the value of {fieldvalue} of the field {fieldname}.");
        }

        [Given(@"validate (.*) result contain item added on '(.*)' with a value '(.*)'")]
        [When(@"validate (.*) result contain item added on '(.*)' with a value '(.*)'")]
        [Then(@"validate (.*) result contain item added on '(.*)' with a value '(.*)'")]
        public void ThenValidateResultContainAdded(EntityEnum.Entity entity, string fieldname, string fieldvalue)
        {
            object usedObject = SetObject(entity);

            if (usedObject != null)
            {
                var valueAssert = ReflectionHelper.GetPropertyValue(usedObject, fieldname.Trim());

                if (!valueAssert.ToString().Equals(fieldvalue, StringComparison.OrdinalIgnoreCase))
                    Assert.Fail($"Cannot find the value of {fieldvalue} of the field {fieldname}.");
            }
            else
                throw new AssertionException($"The {usedObject} data is empty.");
        }

        [Then(@"validate submission underwriters result contain item added on '(.*)' with a value from the referenced submission")]
        public void ThenValidateSubmissionUnderwritersResultContainItemAddedOnWithAValueFromTheReferencedSubmission(string fieldname)
        {
            var usedObject = SetObject(EntityEnum.Entity.submission_underwriters);
            var referenceSubmission = SetObject(EntityEnum.Entity.submissions);
            var valueAssert = ReflectionHelper.GetPropertyValue(usedObject, fieldname.Trim()).ToString();
            var fieldvalue = ReflectionHelper.GetPropertyValue(referenceSubmission, fieldname.Trim()).ToString();

            if (!valueAssert.Equals(fieldvalue, StringComparison.OrdinalIgnoreCase))
                Assert.Fail($"Cannot find the value of {fieldvalue} of the field {fieldname}.");
        }

        [Given(@"the response body should contain a collection of (.*)")]
        [When(@"the response body should contain a collection of (.*)")]
        [Then(@"the response body should contain a collection of (.*)")]
        public void ResponseBodyShouldContainACollectionOf(EntityEnum.Entity input)
        {
            if (input == EntityEnum.Entity.submissions)
            {
                _submissionCollectionContext = JsonConvert.DeserializeObject<SubmissionCollectionWrapper>(_apiTestContext.Response.Content);
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.IsTrue(_submissionCollectionContext is SubmissionCollectionWrapper);
            }

            if (input == EntityEnum.Entity.submission_underwriters)
            {
                _submissionUnderwriterCollectionContext = JsonConvert.DeserializeObject<SubmissionUnderwriterCollectionWrapper>(_apiTestContext.Response.Content);
                if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission underwriters collection data is empty.");
                Assert.IsTrue(_submissionUnderwriterCollectionContext is SubmissionUnderwriterCollectionWrapper);
            }

            if (input == EntityEnum.Entity.submission_documents)
            {
                _submissionDocumentCollectionContext = JsonConvert.DeserializeObject<SubmissionDocumentCollectionWrapper>(_apiTestContext.Response.Content);
                if (_submissionDocumentCollectionContext is null) throw new AssertionException("The submission documents collection data is empty.");

                Assert.IsTrue(_submissionDocumentCollectionContext is SubmissionDocumentCollectionWrapper);
            }

            if (input == EntityEnum.Entity.submission_dialogue)
            {
                _submissionDialogueCollectionContext = JsonConvert.DeserializeObject<SubmissionDialogueCollectionWrapper>(_apiTestContext.Response.Content);
                if (_submissionDialogueCollectionContext is null) throw new AssertionException("The submission dialogue collection data is empty.");

                Assert.IsTrue(_submissionDialogueCollectionContext is SubmissionDialogueCollectionWrapper);
            }

            And("the response HTTP header ContentType should be to application/json type");
            And("the response body should have value");
        }


        [Then(@"for (.*) I validate I am getting the correct results for (.*) with (.*)")]
        public void ThenValidateIAmGettingTheCorrectResultsForClassOfBusinessCodeWithValuesOf(EntityEnum.Entity entity, string field, List<string> values)
        {
            object[] array = SetArrayCollection(entity);

            if (array != null)
            {
                foreach (var item in array)
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

                    if (!values.Contains(valueAssert.ToString()))
                          Assert.Fail($"Couldn't find the value of {valueAssert} item.");
                }
            }
            else
                throw new AssertionException($"The {entity} collection data is empty.");
        }


        [Then(@"validate (.*) results contain the values of (.*)")]
        public void ThenValidateTheResultsOfDocumentIdWithValues(EntityEnum.Entity entity, string fieldArray)
        {
            object[] array = SetArrayCollection(entity);
            string fieldTested = "";

            var notfound = false;

            if (array != null)
            {
                foreach (var item in array)
                {
                    List<string> fields = fieldArray.Split(',').Select(x=>x.ToString()).ToList();

                    foreach (var field in fields)
                    {
                        var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

                        if (!_stateHolder.ParamsBuilderValuesToValidate.Contains(valueAssert.ToString()))
                        {
                            notfound = true;
                            fieldTested = field;
                            break;
                        }
                    }

                     Assert.IsFalse(notfound, $"Couldn't find the value of the field {fieldTested}");
                }
            }
            else
                throw new AssertionException("The submission collection data is empty.");
        }

        [Then(@"validate the collection of (.*) I am getting the correct results for '(.*)'")]
        public void ThenValidateIAmGettingTheCorrectResultsForClassOfBusinessCodeWithValuesOf(EntityEnum.Entity entity, string field)
        {
            object[] array = SetArrayCollection(entity);

            if (array != null)
            {
                foreach (var item in array)
                {
                    ValueAssertionWithStoredValues(item, field);
                }
            }
            else
                throw new AssertionException("The collection data is empty.");
        }

        [Then(@"validate the collection of (.*) I am getting contain the values of '(.*)'")]
        public void ThenValidateIAmGettingTheCorrectResultsForClassOfBusinessCodeWithValuesOfArray(EntityEnum.Entity entity, string fieldsArayString)
        {
            var listFields = fieldsArayString.Split(",").ToList();
            object[] array = SetArrayCollection(entity);

            if (array != null)
            {
                foreach (var item in array)
                {
                    foreach(var field in listFields)
                    {
                        ValueAssertionWithStoredValues(item, field);
                    }
                }
            }
            else
                throw new AssertionException("The collection data is empty.");
        }

        [Then(@"validate that the (.*) date results for the field '(.*)' are (.*) for (.*)")]
        [Then(@"validate that the (.*) results for the field '(.*)' are (.*) the date of (.*)")]
        public void ThenValidateThatTheSubmissionMarketDialogueResultsAreBeforeToday(Entity entity,
            string field, 
            string comparison, 
            string when)
        {
            object[] array = SetArrayCollection(entity);
            DateTime dtToCompareWith = new DateTime();

            dtToCompareWith = when switch
            {
                "yesterday" => DateTime.Now.AddDays(-1),
                "one week" => DateTime.Now.AddDays(-7),
                "today" => DateTime.Now,
                "yesterday and today" => DateTime.Now, // this is for between, needs to be implemented differently to enable more options
                "tomorrow" => DateTime.Now.AddDays(1),
                _ => DateTime.Now,
            };

            var count = 0;

            //if (array.Length == 0)
            //    Assert.Fail($"Couldn't find any data in order to validate sort for {field}.");

            foreach (var item in array)
            {
                if (field == "_last_modified")
                {
                    if (count > 15) break;  //as this takes a lot of time, doing an extra GetBydId, will limit to 15 results

                    DateTime valueAssert = new DateTime();
                    var id = ReflectionHelper.GetPropertyValue(item, "Id").ToString();

                    if (entity == Entity.submissions)
                        valueAssert = GetLastModifiedValueFromSubmission(id);

                    if (entity == Entity.submission_dialogue)
                        valueAssert = GetLastModifiedValueFromSubmissionDialogue(id);

                    if (entity == Entity.submission_underwriters)
                        valueAssert = GetLastModifiedValueFromSubmissionUnderwriter(id);

                    var currentValue = DateTime.SpecifyKind(valueAssert, DateTimeKind.Utc);
                    StepsExension.CompareAndValidateDateTime(comparison, currentValue, dtToCompareWith);
                    count++;
                }
                else
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim()).ToString();
                    var currentValue = DateTimeOffset.Parse(valueAssert);
                    StepsExension.CompareAndValidateDateTime(comparison, currentValue, dtToCompareWith);
                }
            }
        }

        [Then(@"the (.*) response is ordered by '(.*)' of the field '(.*)' with the following order as (.*)")]
        public void ThenTheSubmissionDialogueResponseIsOrderedByOfTheFieldWithTheFollowingOrder(Entity entity, 
            string fieldOrderBy,
            string fieldToValidate,
            string order,
            Table table)
        {
            object[] array = SetArrayCollection(entity);

            for (int i = 0; i < array.Length; i++)
            {
                var record = array[i];

                var valueAssert = ReflectionHelper.GetPropertyValue(record, fieldToValidate);
                var rowValue = $"valueOfField_{order}";
                var valueOfRecordToValidate = table.Rows[i][rowValue].ToString();

                if (!valueOfRecordToValidate.Equals(valueAssert.ToString(),
                    StringComparison.OrdinalIgnoreCase))
                {
                    Assert.Fail($"When validating the ordered results," +
                        $"found {valueAssert} while expecting for {valueOfRecordToValidate}.");
                }
            }
        }

        private void ValueAssertionWithStoredValues(object item, string field)
        {
            var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

            if (!_stateHolder.ParamsBuilderValuesToValidate.Any(x => x.Equals(valueAssert.ToString(),
                StringComparison.OrdinalIgnoreCase)))
            {
                Assert.Fail($"Couldn't find the value of {valueAssert} item.");
            }
        }       

        private DateTime GetLastModifiedValueFromSubmission(string id)
        {
            GetSubmissionBySubmissionReference(id); //GetById + store response
            And("store the LastModifiedDateTime from the response");
            return _stateHolder.LastModifiedDateTime;
        }

        private DateTime GetLastModifiedValueFromSubmissionDialogue(string id)
        {
            GetSubmissionDialoguesById(id); //GetById + store response
            And("store the LastModifiedDateTime from the response");
            return _stateHolder.LastModifiedDateTime;
        }

        private DateTime GetLastModifiedValueFromSubmissionUnderwriter(string id)
        {
            GetSubmissionUnderwriterById(id); //GetById + store response
            And("store the LastModifiedDateTime from the response");
            return _stateHolder.LastModifiedDateTime;
        }
    }
}

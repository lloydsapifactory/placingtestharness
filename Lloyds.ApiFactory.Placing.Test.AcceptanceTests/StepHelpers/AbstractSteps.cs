//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums.EntityEnum;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class AbstractSteps : ApiBaseSteps
    {
        private StateHolder _stateHolder;

        public AbstractSteps(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
        }

        [Given(@"for the (.*) context the '(.*)' is appended with current value of '(.*)'")]
        public void GivenForTheFeatureSubmissionContextTheIsAppendedWithCurrentValueOf(EntityEnum.Entity entity, string propertyName, string propertyToAppend)
        {
            var SubmissionEntity = GetProperty(entity, propertyName, out var submissionproperty);

            SubmissionEntity = GetProperty(entity, propertyToAppend, out var submissionpropertyToAppend);

            if (submissionproperty.PropertyType != typeof(string))
                throw new AssertionException(
                    $"Cannot append property {propertyName} with current value of {propertyToAppend}, the property is not of type string");

            if (submissionpropertyToAppend.PropertyType != typeof(string))
                throw new AssertionException(
                    $"Cannot append property {propertyName} with current value of {propertyToAppend} (the appender), the appender is not of type string");

            string value = submissionproperty.GetValue(SubmissionEntity).ToString();

            string appendValue = submissionpropertyToAppend.GetValue(SubmissionEntity).ToString();

            string newValue = value + appendValue;

            submissionproperty.SetValue(SubmissionEntity, newValue);
            _stateHolder.ParamsBuilderValueToValidate = newValue;
        }


        [Given(@"for the feature (.*) context the '(.*)' is (.*)")]
        public void TheSubmissionDocumentContextTheIsInteger(EntityEnum.Entity entity, string propertyName, int propertyValue)
        {
            object SubmissionEntity = null;

            if (entity == EntityEnum.Entity.submission)
            {
                SubmissionEntity = _submissionContext;

                if (!(SubmissionEntity is Submission))
                    throw new AssertionException("The scenario cannot find the submission object.");
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                SubmissionEntity = _submissionDocumentContext;

                if ((_submissionDocumentContext == null) || !(_submissionDocumentContext is SubmissionDocument))
                    throw new AssertionException("The scenario cannot find the submission document object.");
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                SubmissionEntity = _SubmissionUnderwriterContext;

                if ((_SubmissionUnderwriterContext == null) || !(_SubmissionUnderwriterContext is SubmissionUnderwriter))
                    throw new AssertionException("The scenario cannot find the submission underwriter object.");
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                SubmissionEntity = _submissionDialogueContext;

                if ((_submissionDialogueContext == null) || !(_submissionDialogueContext is SubmissionDialogue))
                    throw new AssertionException("The scenario cannot find the submission dialogue object.");
            }

            var submissionproperty = SubmissionEntity.GetType().GetProperty(propertyName);

            if (submissionproperty is null)
                throw new AssertionException($"The submission scenario cannot find the property {propertyName}");

            submissionproperty.SetValue(SubmissionEntity, propertyValue);
        }

        [Given(@"the feature (.*) is saved for the scenario")]
        public void GivenTheFeatureSubmissionIsSavedForTheScenario(EntityEnum.Entity entity)
        {
            object usedObject = SetObject(entity);

            var submissionRequestData = JsonConvert.SerializeObject(usedObject);
            _apiTestContext.AddContent(usedObject, MimeEnumType.Json);
            _stateHolder.JsonRequestBody = submissionRequestData;
        }

        [Given(@"I attach the file (.*) to the request")]
        public void GivenIAttachTheFileToTheRequest(string filePath)
        {
            var path = Path.Combine(Environment.CurrentDirectory, filePath);
            _apiTestContext.AddContent<string>(path, MimeEnumType.File);
            var fInfo = new FileInfo(path);
            _submissionDocumentContext.FileSizeInBytes = int.Parse(fInfo.Length.ToString());
        }

        [Given(@"for the (.*) the '(.*)' is '(.*)'")]
        [When(@"for the (.*) the '(.*)' is '(.*)'")]
        [Then(@"for the (.*) the '(.*)' is '(.*)'")]
        [Given(@"for the (.*) context the '(.*)' is '(.*)'")]
        [When(@"for the (.*) context the '(.*)' is '(.*)'")]
        [Then(@"for the (.*) context the '(.*)' is '(.*)'")]
        public void ForTheFeatureContextTheIsString(EntityEnum.Entity entityEnum, string propertyName, string propertyValue)
        {
            var entity = GetProperty(entityEnum, propertyName, out var property);

            _stateHolder._sTestParameters[propertyName] = propertyValue;

            if ((string.IsNullOrEmpty(propertyValue)) || (string.Compare(propertyValue, "null") == 0) && ((property.PropertyType.IsGenericType) && (property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))))
            {
                property.SetValue(entity, null);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(bool)) || (property.PropertyType == typeof(bool?)))
            {
                var boolvalue = bool.Parse(propertyValue);
                property.SetValue(entity, boolvalue);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(int)) || (property.PropertyType == typeof(int?)))
            {
                var intvalue = Int32.Parse(propertyValue);
                property.SetValue(entity, intvalue);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(DateTimeOffset)) || (property.PropertyType == typeof(DateTimeOffset?)))
            {
                var dtoffsetvalue = DateTimeOffset.Parse(propertyValue);
                property.SetValue(entity, dtoffsetvalue);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(ParticipantRole)) || (property.PropertyType == typeof(ParticipantRole?)))
            {
                var role = (ParticipantRole)Enum.Parse(typeof(ParticipantRole), propertyValue.ToString());

                property.SetValue(entity, role);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(ContractType)) || (property.PropertyType == typeof(ContractType?)))
            {
                var role = (ContractType)Enum.Parse(typeof(ContractType), propertyValue.ToString());

                property.SetValue(entity, role);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(SubmissionStatus)) || (property.PropertyType == typeof(SubmissionStatus?)))
            {
                var role = (SubmissionStatus)Enum.Parse(typeof(SubmissionStatus), propertyValue.ToString());

                property.SetValue(entity, role);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(CommunicationMethod)) || (property.PropertyType == typeof(CommunicationMethod?)))
            {
                var role = (CommunicationMethod)Enum.Parse(typeof(CommunicationMethod), propertyValue.ToString());

                property.SetValue(entity, role);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(WebApi.Dto.SubmissionDialogueFilterModel)) || (property.PropertyType == typeof(WebApi.Dto.SubmissionDialogueFilterModel?)))
            {
                var role = (WebApi.Dto.SubmissionDialogueFilterModel)Enum.Parse(typeof(WebApi.Dto.SubmissionDialogueFilterModel), propertyValue.ToString());

                property.SetValue(entity, role);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(List<string>)) || (property.PropertyType == typeof(List<string>)))
            {
                var strOnevalue = new List<string> { propertyValue.ToString() }; //TODO: if we pass more than one value, need to change the code
                property.SetValue(entity, strOnevalue);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue;
                return;
            }

            if ((property.PropertyType == typeof(PeriodChoice)))
            {
                // todo: not sure how to set this property
                return;
            }

            property.SetValue(entity, propertyValue);
        }

        [Given(@"for the (.*) the '(.*)' is '(.*)' string enhanced")]
        [When(@"for the (.*) the '(.*)' is '(.*)' string enhanced")]
        [Then(@"for the (.*) the '(.*)' is '(.*)' string enhanced")]
        [Given(@"for the (.*) context the '(.*)' is '(.*)' string enhanced")]
        [When(@"for the (.*) context the '(.*)' is '(.*)' string enhanced")]
        [Then(@"for the (.*) context the '(.*)' is '(.*)' string enhanced")]
        public void ForTheFeatureContextTheIsStringEnhanced(EntityEnum.Entity entity, string propertyName, string propertyValue)
        {
            var randomStringAppend = Extension.GenerateRandomString(4);
            var SubmissionEntity = GetProperty(entity, propertyName, out var submissionproperty);

            _stateHolder._sTestParameters[propertyName] = propertyValue + randomStringAppend;

            if ((string.IsNullOrEmpty(propertyValue)) || (string.Compare(propertyValue, "null") == 0) && ((submissionproperty.PropertyType.IsGenericType) && (submissionproperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))))
            {
                submissionproperty.SetValue(SubmissionEntity, null);
                _stateHolder.ParamsBuilderValueToValidate = propertyValue + randomStringAppend;
                return;
            }

            submissionproperty.SetValue(SubmissionEntity, propertyValue + randomStringAppend);
        }


        [Given(@"I save the (.*) added")]
        [When(@"I save the (.*) added")]
        [Then(@"I save the (.*) added")]
        public void SaveTheEntityAdded(EntityEnum.Entity input)
        {
            if (input == EntityEnum.Entity.submission_documents)
                _stateHolder.CurrentSubmissionDocuments.Add(_submissionDocumentContext);

            if (input == EntityEnum.Entity.submission_underwriters)
            {
                _stateHolder.CurrentSubmissionUnderwriters.Add(_SubmissionUnderwriterContext);
            }
        }


        [Then(@"the count of (.*) returned will be = (.*)")]
        public void CountOfSubmissionUnderwritersReturnedWillBe(EntityEnum.Entity entity, int submissionUnderwriters)
        {
            object[] array = SetArrayCollection(entity);

            Assert.AreEqual(array.Length, submissionUnderwriters);
        }

        [Given(@"for the (.*) I add a date filter on '(.*)' with a (.*)")]
        [When(@"for the (.*) I add a date filter on '(.*)' with a (.*)")]
        [Then(@"for the (.*) I add a date filter on '(.*)' with a (.*)")]
        [Given(@"for the (.*) I set '(.*)' with (.*)")]
        public void AddAFilterOnWithAValueOfToday(EntityEnum.Entity entity, string dateTimeFilter, DateTime dt)
        {
            var localDate = dt;
            ForTheFeatureContextTheIsString(entity, dateTimeFilter, localDate.ToString("yyyy-MM-dd"));
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with a random value from the response context of (.*)")]
        public void GivenIAddAFilterOnRandomContext(string filtertype, string fieldname, EntityEnum.Entity entity)
        {
            object objectUsed = null;

            if (entity == EntityEnum.Entity.submission_documents)
                objectUsed = _submissionDocumentContext;

            if (entity == EntityEnum.Entity.submission)
                objectUsed = _submissionContext;

            var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            var valueSet = ReflectionHelper.GetPropertyValue(objectUsed, fieldname.Trim());

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? valueSet.ToString() : $"({valueSet.ToString()})";
            _stateHolder.ParamsBuilderValueToValidate = valueSet.ToString();
        }

        [Then(@"collection of (.*) should have at least (.*) link")]
        public void ThenCollectionShouldHaveAtLeastLink(EntityEnum.Entity entity, int linkCount)
        {
            if (entity == EntityEnum.Entity.submission)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.IsTrue(_submissionCollectionContext.Links.Count >= linkCount);
            }

            if (entity == EntityEnum.Entity.submissions)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.IsTrue(_submissionCollectionContext.Links.Count >= linkCount);
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission collection market data is empty.");
                Assert.IsTrue(_submissionUnderwriterCollectionContext.Links.Count >= linkCount);
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentCollectionContext is null) throw new AssertionException("The submission collection documents data is empty.");
                Assert.IsTrue(_submissionDocumentCollectionContext.Links.Count >= linkCount);
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext is null) throw new AssertionException("The submission dialogue collection data is empty.");
                Assert.IsTrue(_submissionDialogueCollectionContext.Links.Count >= linkCount);
            }
        }

        [Then(@"collection of (.*) should have a link with proper value")]
        [Then(@"collection of (.*) should have a canonical link with proper value")]
        public void ValidateLinksForCollection(EntityEnum.Entity entity)
        {
            Link canonicalLink = null;

            if (entity == EntityEnum.Entity.submission || entity == EntityEnum.Entity.submissions)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");

                if (_submissionCollectionContext.Links == null)
                    Assert.IsNotNull(_submissionCollectionContext.Links, "No links found.");

                canonicalLink = _submissionCollectionContext.Links.FirstOrDefault(x => x.rel.Equals("canonical",
                    StringComparison.OrdinalIgnoreCase));
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission collection market data is empty.");

                if (_submissionUnderwriterCollectionContext.Links == null)
                    Assert.IsNotNull(_submissionUnderwriterCollectionContext.Links, "No links found.");

                canonicalLink = _submissionUnderwriterCollectionContext.Links.FirstOrDefault(x => x.rel.Equals("canonical",
                    StringComparison.OrdinalIgnoreCase));
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentCollectionContext is null) throw new AssertionException("The submission collection data is empty.");

                if (_submissionDocumentCollectionContext.Links == null)
                    Assert.IsNotNull(_submissionDocumentCollectionContext.Links, "No links found.");

                canonicalLink = _submissionDocumentCollectionContext.Links.FirstOrDefault(x => x.rel.Equals("canonical",
                    StringComparison.OrdinalIgnoreCase));
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext is null) throw new AssertionException("The submission collection market data is empty.");

                if (_submissionDialogueCollectionContext.Links == null)
                    Assert.IsNotNull(_submissionDialogueCollectionContext.Links, "No links found.");

                canonicalLink = _submissionDialogueCollectionContext.Links.FirstOrDefault(x => x.rel.Equals("canonical",
                    StringComparison.OrdinalIgnoreCase));
            }


            Assert.IsNotNull(canonicalLink.rel, "No rel found.");
            Assert.GreaterOrEqual(canonicalLink.href.Length, 1, "Href is empty.");
            Assert.AreEqual(canonicalLink.rel, "canonical", "Rel is not 'canonical'");

            foreach (var key in _stateHolder._sParamsBuilder.Keys.ToList())
            {
                Assert.IsTrue(canonicalLink.href.Contains(key));
            }

            foreach (var value in _stateHolder._sParamsBuilder.Values.ToList())
            {
                Assert.IsTrue(canonicalLink.href.Contains(value,
                    StringComparison.OrdinalIgnoreCase));
            }
        }

        [Then(@"(.*) context should have a link with proper value")]
        public void ValidateLinksForContext(EntityEnum.Entity entity)
        {
            Link associatedLink = null;

            if (entity == EntityEnum.Entity.submission || entity == EntityEnum.Entity.submissions)
            {
                if (_submissionContext is null) throw new AssertionException("The submission is empty.");

                if (_submissionContext.Links == null)
                    Assert.IsNotNull(_submissionContext.Links, "No links found.");

                associatedLink = Enumerable.FirstOrDefault<Link>(_submissionContext.Links);
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_SubmissionUnderwriterContext is null) throw new AssertionException("The submission underwriter is empty.");

                if (_SubmissionUnderwriterContext.Links == null)
                    Assert.IsNotNull(_SubmissionUnderwriterContext.Links, "No links found.");

                associatedLink = Enumerable.FirstOrDefault<Link>(_SubmissionUnderwriterContext.Links);
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentContext is null) throw new AssertionException("The submission document is empty.");

                if (_submissionDocumentContext.Links == null)
                    Assert.IsNotNull(_submissionDocumentContext.Links, "No links found.");

                associatedLink = Enumerable.FirstOrDefault<Link>(_submissionDocumentContext.Links);
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueContext is null) throw new AssertionException("The submission dialogue is empty.");

                if (_submissionDialogueContext.Links == null)
                    Assert.IsNotNull(_submissionDialogueContext.Links, "No links found.");

                associatedLink = Enumerable.FirstOrDefault<Link>(_submissionDialogueContext.Links);
            }

            Assert.IsNotNull(associatedLink.rel, "No rel found.");
            Assert.GreaterOrEqual(associatedLink.href.Length, 1, "Href is empty.");
            Assert.AreEqual(associatedLink.rel, "canonical", "Rel is not 'canonical'");

            foreach (var key in _stateHolder._sParamsBuilder.Keys.ToList())
            {
                Assert.IsTrue(associatedLink.href.Contains(key));
            }

            foreach (var value in _stateHolder._sParamsBuilder.Values.ToList())
            {
                Assert.IsTrue(associatedLink.href.Contains(value));
            }
        }

        [Then(@"(.*) context should have at least (.*) link")]
        public void ThenShouldHaveAtLeastLink(EntityEnum.Entity entity, int linkCount)
        {
            if (entity == Entity.submission)
            {
                if (_submissionContext is null) throw new AssertionException("The submission context is empty.");
                Assert.IsTrue(_submissionContext.Links.Count >= linkCount);
            }

            if (entity == Entity.submissions)
            {
                if (_submissionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.IsTrue(_submissionContext.Links.Count >= linkCount);
            }

            if (entity == Entity.submission_underwriters)
            {
                if (_SubmissionUnderwriterContext is null) throw new AssertionException("The submission collection market data is empty.");
                Assert.IsTrue(_SubmissionUnderwriterContext.Links.Count >= linkCount);
            }

            if (entity == Entity.submission_documents)
            {
                if (_submissionDocumentContext is null) throw new AssertionException("The submission collection documents data is empty.");
                Assert.IsTrue(_submissionDocumentContext.Links.Count >= linkCount);
            }

            if (entity == Entity.submission_dialogue)
            {
                if (_submissionDialogueContext is null) throw new AssertionException("The submission dialogue data is empty.");
                Assert.IsTrue(_submissionDialogueContext.Links.Count >= linkCount);
            }
        }

        [Given(@"get random (.*) from the (.*) collection")]
        [When(@"get random (.*) from the (.*) collection")]
        [Then(@"get random (.*) from the (.*) collection")]
        public void GetARandomFromTheCollection(string objectUsed, EntityEnum.Entity entity)
        {
            object[] array = SetArrayCollection(entity);

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                var qmd = SubmissionDialogueExtension.ReturnRandomSubmissionDialogue(array);
                _submissionDialogueContext = qmd;
                _stateHolder.CurrentSubmissionDialogue = qmd;
                _stateHolder.CurrentSenderDocumentIds = qmd.SenderDocumentIds;
            }

            if (entity == EntityEnum.Entity.submission)
            {
                var submission = SubmissionExtension.ReturnRandomSubmission(array);
                _submissionContext = submission;
                _stateHolder.CurrentSubmission = submission;
            }
        }

        [Given(@"I clear down test context")]
        [When(@"I clear down test context")]
        [Then(@"I clear down test context")]
        public void TestContextClearDown()
        {
            _apiTestContext.ClearDown();
        }


        [Given(@"store the CreatedDateTime from the posted (.*)")]
        public void StoreCreatedDateTimeFromPosted(Entity entity)
        {
            if (entity == Entity.submission)
                _submissionContext = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);

            if (entity == Entity.submission_dialogue)
                _submissionDialogueContext = JsonConvert.DeserializeObject<SubmissionDialogue>(_apiTestContext.Response.Content);

            if (entity == Entity.submission_underwriters)
                _SubmissionUnderwriterContext = JsonConvert.DeserializeObject<SubmissionUnderwriter>(_apiTestContext.Response.Content);

            if (entity == Entity.submission_documents)
                _submissionDocumentContext = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);

            StoreCreatedDateTime(entity);
        }


        [Given(@"store the CreatedDateTime from (.*) context")]
        public void StoreCreatedDateTime(Entity entity)
        {
            if (entity == Entity.submission)
            {
                if (_submissionContext.CreatedDateTime.HasValue)
                    _stateHolder.CreatedDateTime = _submissionContext.CreatedDateTime.Value.DateTime;
                else
                    Assert.Fail($"CreatedDateTime doest have a value. Need to store the CreatedDateTime from the {entity}");
            }

            if (entity == Entity.submission_dialogue)
            {
                if (_submissionDialogueContext.CreatedDateTime.HasValue)
                    _stateHolder.CreatedDateTime = _submissionDialogueContext.CreatedDateTime.Value.DateTime;
                else
                    Assert.Fail($"CreatedDateTime doest have a value. Need to store the CreatedDateTime from the {entity}");
            }

            if (entity == Entity.submission_underwriters)
            {
                if (_SubmissionUnderwriterContext.CreatedDateTime.HasValue)
                    _stateHolder.CreatedDateTime = _SubmissionUnderwriterContext.CreatedDateTime.Value.DateTime;
                else
                    Assert.Fail($"CreatedDateTime doest have a value. Need to store the CreatedDateTime from the {entity}");
            }

            if (entity == Entity.submission_documents)
            {
                if (_submissionDocumentContext.CreatedDateTime.HasValue)
                    _stateHolder.CreatedDateTime = _submissionDocumentContext.CreatedDateTime.Value.UtcDateTime;
                else
                    Assert.Fail($"CreatedDateTime doest have a value. Need to store the CreatedDateTime from the {entity}");
            }
        }

        [Given(@"store the LastModifiedDateTime from the response")]
        [When(@"store the LastModifiedDateTime from the response")]
        [Then(@"store the LastModifiedDateTime from the response")]
        public void StoreLastModifiedDateTime()
        {
            if (_apiTestContext.Response.Headers.Any(a => a.Name == "Last-Modified"))
            {
                DateTime.TryParse(_apiTestContext.Response.Headers.FirstOrDefault(a => a.Name == "Last-Modified").Value.ToString(), out DateTime lastModified);
                _stateHolder.LastModifiedDateTime = lastModified;
            }
        }
    }
}

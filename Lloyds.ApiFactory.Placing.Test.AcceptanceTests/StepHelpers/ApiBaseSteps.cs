//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public abstract class ApiBaseSteps : Steps
    {
        protected readonly IObjectContainer _objectContainer;
        protected readonly ApiTestContext _apiTestContext;
        protected ScenarioContext sContext;
        protected FeatureContext fContext;

        protected ApiBaseSteps()
        {

        }

        protected ApiBaseSteps(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext, 
            FeatureContext featureContext) : this(objectContainer, apiTestContext, scenarioContext)
        {
            sContext = scenarioContext;
            fContext = featureContext;
        }

        protected ApiBaseSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext, ScenarioContext scenarioContext) : this(objectContainer, apiTestContext)
        {
            sContext = scenarioContext;
        }
        protected ApiBaseSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext)
        {
            _objectContainer = objectContainer;
            _apiTestContext = apiTestContext;
        }


        #region Contexts

        protected SubmissionCollectionWrapper _submissionCollectionContext
        {
            get { return sContext[Constants.SubmissionCollectionWrapper] as SubmissionCollectionWrapper; }
            set { sContext[Constants.SubmissionCollectionWrapper] = value; }
        }

        protected Submission _submissionContext
        {
            get => sContext[Constants.SubmissionContext] as Submission;
            set => sContext[Constants.SubmissionContext] = value;
        }

        protected SubmissionDocument _submissionDocumentContext
        {
            get
            {
                return sContext[Constants.SubmissionDocumentContext] as SubmissionDocument;
            }
            set
            {
                sContext[Constants.SubmissionDocumentContext] = value;
            }
        }

        protected SubmissionDocumentCollectionWrapper _submissionDocumentCollectionContext
        {
            get { return sContext[Constants.SubmissionDocumentCollectionWrapper] as SubmissionDocumentCollectionWrapper; }
            set { sContext[Constants.SubmissionDocumentCollectionWrapper] = value; }
        }

        protected SubmissionUnderwriter _SubmissionUnderwriterContext
        {
            get => sContext.FirstOrDefault(x => x.Key.Equals(Constants.SubmissionUnderwriterContext,
                                                      StringComparison.OrdinalIgnoreCase)).Value != null ?
                   sContext[Constants.SubmissionUnderwriterContext] as SubmissionUnderwriter : null;

            set
            {
                sContext[Constants.SubmissionUnderwriterContext] = value;
            }
        }

        protected SubmissionUnderwriterCollectionWrapper _submissionUnderwriterCollectionContext
        {
            get { return sContext[Constants.SubmissionUnderwriterCollectionWrapper] as SubmissionUnderwriterCollectionWrapper; }
            set { sContext[Constants.SubmissionUnderwriterCollectionWrapper] = value; }
        }

        protected SubmissionDialogue _submissionDialogueContext
        {
            get => sContext.FirstOrDefault(x => x.Key.Equals(Constants.SubmissionDialogueContext,
                                                      StringComparison.OrdinalIgnoreCase)).Value != null ?
                   sContext[Constants.SubmissionDialogueContext] as SubmissionDialogue : null;

            set => sContext[Constants.SubmissionDialogueContext] = value;
        }

        protected SubmissionDialogueCollectionWrapper _submissionDialogueCollectionContext
        {
            get { return sContext[Constants.SubmissionDialogueCollectionWrapper] as SubmissionDialogueCollectionWrapper; }
            set { sContext[Constants.SubmissionDialogueCollectionWrapper] = value; }
        }

        protected BrokerDepartmentCollectionWrapper _brokerDepartmentContext
        {
            get { return sContext[Constants.BrokerDepartmentContext] as BrokerDepartmentCollectionWrapper; }
            set { sContext[Constants.BrokerDepartmentContext] = value; }
        }

        protected UnderwriterOrganisationCollectionWrapper _underwriterOrganisationContext
        {
            get { return sContext[Constants.UnderwriterOrganisationContext] as UnderwriterOrganisationCollectionWrapper; }
            set { sContext[Constants.UnderwriterOrganisationContext] = value; }
        }

        #endregion

        protected void SaveSubmissionToScenarioContext(Submission submission)
        {
            sContext[Constants.SubmissionContext] = submission;
        }

        protected Submission GetSubmissionFromScenarioContext()
        {
            return sContext.ContainsKey(Constants.SubmissionContext) ? sContext[Constants.SubmissionContext] as Submission : new Submission();
        }

        protected void AssertPropertyAndValue(Object obj, string fieldname)
        {
            var propAssert = ReflectionHelper.GetPropertyCc(obj, fieldname.Trim().ToLower());

            if (propAssert == null)
                Assert.Fail($"Couldn't find the field {fieldname}.");

            var valueAssert = ReflectionHelper.GetPropertyValue(obj, fieldname);

            if (valueAssert == null)
                Assert.Fail($"The value of the field {fieldname} is null.");

            var submissionproperty = obj.GetType().GetProperty(fieldname);

            if ((submissionproperty.PropertyType == typeof(DateTimeOffset)) || (submissionproperty.PropertyType == typeof(DateTimeOffset?)))
            {
                var dto = (DateTimeOffset)valueAssert;

                if (dto.UtcDateTime == DateTime.MinValue)
                    Assert.Fail($"{fieldname} datetime is {dto.UtcDateTime} minimum datetime value.");
            }
        }

        protected object[] SetArrayCollection(EntityEnum.Entity entity)
        {
            if (entity == EntityEnum.Entity.submission)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");

                return _submissionCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submissions)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");

                return _submissionCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission collection market data is empty.");

                return _submissionUnderwriterCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentCollectionContext is null) throw new AssertionException("The submission collection documents data is empty.");

                return _submissionDocumentCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext is null) throw new AssertionException("The submission dialogue collection data is empty.");

                return _submissionDialogueCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.underwriter_organisations)
            {
                if (_underwriterOrganisationContext is null) throw new AssertionException("The underwriter organisations collection data is empty.");

                return _underwriterOrganisationContext.Items;
            }

            return null;
        }

        protected object SetObject(EntityEnum.Entity entity)
        {
            if (entity == EntityEnum.Entity.submission)
                return _submissionContext;

            if (entity == EntityEnum.Entity.submissions)
                return _submissionContext;

            if (entity == EntityEnum.Entity.submission_documents)
                return _submissionDocumentContext;

            if (entity == EntityEnum.Entity.submission_underwriters)
                return _SubmissionUnderwriterContext;

            if (entity == EntityEnum.Entity.submission_dialogue)
                return _submissionDialogueContext;

            if (entity == EntityEnum.Entity.underwriter_organisations)
                return _underwriterOrganisationContext;

            return null;
        }

        protected object GetProperty(EntityEnum.Entity entity1, string propertyName, out PropertyInfo propertyInfo)
        {
            object submissionEntity = null;

            if (entity1 == EntityEnum.Entity.submission)
            {
                submissionEntity = _submissionContext;

                if (!(submissionEntity is Submission))
                    throw new AssertionException("The scenario cannot find the submission object.");
            }

            if (entity1 == EntityEnum.Entity.submissions)
            {
                submissionEntity = _submissionContext;

                if (!(submissionEntity is Submission))
                    throw new AssertionException("The scenario cannot find the submission object.");
            }

            if (entity1 == EntityEnum.Entity.submission_documents)
            {
                submissionEntity = _submissionDocumentContext;

                if ((_submissionDocumentContext == null) || !(_submissionDocumentContext is SubmissionDocument))
                    throw new AssertionException("The scenario cannot find the submission document object.");

            }

            if (entity1 == EntityEnum.Entity.submission_underwriters)
            {
                submissionEntity = _SubmissionUnderwriterContext;

                if ((_SubmissionUnderwriterContext == null) || !(_SubmissionUnderwriterContext is SubmissionUnderwriter))
                    throw new AssertionException("The scenario cannot find the submission underwriter object.");

            }

            if (entity1 == EntityEnum.Entity.submission_dialogue)
            {
                if ((_submissionDialogueContext == null) || !(_submissionDialogueContext is SubmissionDialogue))
                    throw new AssertionException("The scenario cannot find the submission dialogue object.");

                submissionEntity = _submissionDialogueContext;
            }

            propertyInfo = submissionEntity.GetType().GetProperty(propertyName);

            if (propertyInfo is null)
                throw new AssertionException($"The submission scenario cannot find the property {propertyName}");
            return submissionEntity;
        }

        protected Submission PrepareSubmissionData(Submission submission, string brokerEmailAddress)
        {
            var mappedSubmission = MapBrokerCodeAndDepartment(brokerEmailAddress, submission);
            SubmissionExtension.SetSubmissionValues(mappedSubmission, _submissionContext);

            return mappedSubmission;
        }

        protected Submission MapBrokerCodeAndDepartment(string brokerEmailAddress, Submission submission)
        {
            var brokerDepartments = _brokerDepartmentContext.Items.Where(bd => bd.BrokerUsers.Any(bu => bu.BrokerUserEmailAddress.Equals(brokerEmailAddress, StringComparison.OrdinalIgnoreCase)));

            if (brokerDepartments == null) throw new Exception("Unable to find a matching Broker Department");

            var brokerDepartment = brokerDepartments.FirstOrDefault();
            submission.BrokerCode = brokerDepartment.BrokerCodes.FirstOrDefault();

            submission.BrokerDepartmentId = brokerDepartment.BrokerDepartmentId;

            return submission;
        }

        protected void GetSubmissionBySubmissionReferenceAndQVN(string submissionreference, string submissionversionnumber)
        {
            And($"I add a 'match' filter on 'SubmissionReference' with a value '{submissionreference}'");
            And($"I add a '' filter on 'SubmissionVersionNumber' with a value '{submissionversionnumber}'");
            When("I make a GET request to '/submissions' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            And("the response body should contain a collection of submissions");

            _submissionContext = _submissionCollectionContext.Items.FirstOrDefault();
        }

        protected void GetSubmissionBySubmissionReferenceAndQVN()
        {
            And("I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference");
            And("for submission I add a '' filter on 'SubmissionVersionNumber' with the value from the referenced submission");
            When("I make a GET request to '/submissions' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            And("the response body should contain a collection of submissions");

            _submissionContext = _submissionCollectionContext.Items.FirstOrDefault();
        }

        protected void GetQMBySubmissionReferenceAndQVN(string userEmailAddress)
        {
            LoginAs(userEmailAddress);
            And("I reset all the filters");
            And("I add a 'match' filter on 'SubmissionReference' with the value from the referenced submission as submission reference");
            And("for submission I add a '' filter on 'SubmissionVersionNumber' with the value from the referenced submission");
            When("I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            And("the response is a valid application/json; charset=utf-8 type of submission underwriters collection");
        }

        protected IEnumerable<SubmissionDialogue> GetQMDsBySubmissionReferenceAndQVNAs(string userEmailAddress, string postedBy)
        {
            LoginAs(userEmailAddress);
            And("I reset all the filters");
            And($"I add a filter on 'SubmissionReference' with a value '{_submissionDialogueContext.SubmissionReference}'");
            And("for submission I add a '' filter on 'SubmissionVersionNumber' with the value from the referenced submission");
            When("I make a GET request to '/SubmissionDialogues' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            Then("the response is a valid application/json; charset=utf-8 type of submission dialogue collection");

            Assert.IsNotNull(_submissionDialogueCollectionContext.Items.FirstOrDefault());
            Assert.GreaterOrEqual(_submissionDialogueCollectionContext.Items.Count(), 1);

            if (postedBy == "broker")
                _submissionDialogueCollectionContext.Items = _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.B).ToArray();

            if (postedBy == "underwriter")
                _submissionDialogueCollectionContext.Items = _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.U).ToArray();

            return _submissionDialogueCollectionContext.Items;
        }

        protected IEnumerable<SubmissionDialogue> GetQMDsBySubmissionReferenceAndQVNAs(string userEmailAddress, 
            string submissionReference, 
            string submissionversionnumber)
        {
            LoginAs(userEmailAddress);
            And("I reset all the filters");
            And($"I add a filter on 'SubmissionReference' with a value '{submissionReference}'");
            And($"I add a filter on 'SubmissionVersionNumber' with a value '{submissionversionnumber}'");
            When("I make a GET request to '/SubmissionDialogues' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            Then("the response is a valid application/json; charset=utf-8 type of submission dialogue collection");

            Assert.IsNotNull(_submissionDialogueCollectionContext.Items.FirstOrDefault());
            Assert.GreaterOrEqual(_submissionDialogueCollectionContext.Items.Count(), 1);

            return _submissionDialogueCollectionContext.Items;
        }

        protected void GetQMDById(string userEmailAddress)
        {
            //get QMDId from the QM
            var qmd = _submissionDialogueContext;

            LoginAs(userEmailAddress);
            GetSubmissionDialoguesById(qmd.Id);
        }

        protected void GetSubmissionUnderwriterById(string id)
        {
            When($"I make a GET request to '/SubmissionUnderwriters/{id}' resource");
            Then("the response status code should be \"200\"");
            And("I store the submission underwriter in the context");
        }

        protected void GetSubmissionDialoguesById(string id)
        {
            When($"I make a GET request to '/SubmissionDialogues/{id}' resource");
            Then("the response status code should be \"200\"");

            Then("I store the submission dialogue response in the context");
        }

        protected void GetBrokerDepartmentAs(string userEmailAddress)
        {
            LoginAs(userEmailAddress);
            When("I make a GET request to '/brokerdepartments' resource");
            And("I store broker departments");
        }

        protected void ValidateSubmissionUnderwritersAsUnderwriterUserEmailAddress(string emailAddress)
        {
            Then($"validate submission underwriters result contain item added on 'UnderwriterUserEmailAddress' with a value '{emailAddress}'");
        }
        protected void ValidateSubmissionUnderwritersWithOrganisationId(string underwriterOrganisationId)
        {
            Then($"validate submission underwriters result contain item added on 'UnderwriterOrganisationid' with a value '{underwriterOrganisationId}'");
        }

        protected void LoginAs(string userEmailAddress)
        {
            Given($"I log in as a broker '{userEmailAddress}'");
        }

        protected void GetRandomSubmissionDocumentAs(string email)
        {
            Given($"I log in as a broker '{email}'");
            And("I make a GET request to '/submissions' resource");
            And("the response status code should be \"200\"");
            And("the response body should contain a collection of submissions");
            And("I get a random submission that contains submission documents");
        }


        protected void GetSubmissionDocumentsBySubmissionReferenceAs(string email, string submissionReference)
        {
            Given($"I log in as a broker '{email}'");
            When($"I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionReference}' resource");
            And("the response is a valid application/json; charset=utf-8 type of submission documents collection");
        }

        protected void GetSubmissionDocumentsForTheSameSubmission()
        {
            Given("I make a GET request to '/submissions' resource");
            Given("the response body should contain a collection of submissions");
            Given("I get a random submission");
            When("I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource");
            And("the response is a valid application/json; charset=utf-8 type of submission documents collection");
        }

        protected SubmissionDialogue GetRandomQMDAs(string userEmailAddress, string postedBy)
        {
            var qmds = new List<SubmissionDialogue>();
            var random = new Random();

            LoginAs(userEmailAddress);
            And("I reset all the filters");
            When("I make a GET request to '/SubmissionDialogues' resource and append the filter(s)");
            Then("the response status code should be \"200\"");
            Then("the response is a valid application/json; charset=utf-8 type of submission dialogue collection");

            Assert.IsNotNull(_submissionDialogueCollectionContext.Items.FirstOrDefault());

            if (postedBy == "broker")
                qmds = _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.B).ToList();

            if (postedBy == "underwriter")
                qmds = _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.U).ToList();

            int index = random.Next(qmds.Count());

            return qmds[index];
        }

        protected void GetRandomSubmissionwithSubmissionDocumentAs(string email)
        {
            Given($"I log in as a broker '{email}'");
            And("I make a GET request to '/submissions' resource");
            And("the response status code should be \"200\"");
            And("the response body should contain a collection of submissions");
            And("I get a random submission that contains submission documents");
        }

        protected void ValidateCollectionResponse(string type, string entity)
        {
            And($"the response HTTP header ContentType should be to {type} type");
            And("the response body should have value");
            Then($"the response body should contain a collection of {entity}");
        }

        protected void GetUnderwriterOrganisationFor(string email)
        {
            Given("I reset all the filters");

            Given("I add a filter on 'BrokerDepartmentId' with the first broker department");
            Given($"I add a 'contains' filter on 'NameEmailOrg' with  '{email}'");
            When("I make a GET request to '/UnderwriterOrganisations' resource with filter(s)");
            Then("collection of underwriter organistations should have a total greater than 0");
        }

        protected void ValidateSubmissionUnderwriterByIdWithOrganisation(string submissionUnderwriterId)
        {
            GetSubmissionUnderwriterById(submissionUnderwriterId);
            var underwriterOrganisationId = _SubmissionUnderwriterContext.UnderwriterOrganisationId;
            ValidateSubmissionUnderwritersWithOrganisationId(underwriterOrganisationId);
        }

        protected void GetSubmissionBySubmissionReference(string submissionReference)
        {
            When($"I make a GET request to '/Submissions/{submissionReference}' resource");
            Then("I refresh the Submissioncontext with the response content");            
        }

        protected void GetSubmissionDocumentsBySubmissionReference(string submissionReference)
        {
            When($"I make a GET request to '/SubmissionDocuments/{submissionReference}' resource");
            Then("the response is a valid application/json; charset=utf-8 type of submission documents collection");
        }
    }
}

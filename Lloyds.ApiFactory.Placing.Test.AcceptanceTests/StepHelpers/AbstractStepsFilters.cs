//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class AbstractStepsFilters : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private readonly CommonSteps _commonSteps;

        public AbstractStepsFilters(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
            _commonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
        }


        [Given(@"I add a '(.*)' filter on '(.*)' with a value '(.*)'")]
        public void GivenIAddAFilterOn(string filtertype, string fieldname, string fieldvalue)
        {
            var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? fieldvalue : $"({fieldvalue})";
            _stateHolder.ParamsBuilderValueToValidate = fieldvalue;
        }

        [Given(@"I add a GET filter on '(.*)' with a value '(.*)'")]
        [Given(@"I add a filter on '(.*)' with a value '(.*)'")]
        [When(@"I add a filter on '(.*)' with a value '(.*)'")]
        [Then(@"I add a filter on '(.*)' with a value '(.*)'")]
        public void GivenIAddAFilterOnWithAValue(string fieldname, string fieldvalue)
        {
            // when you inject the item stored within the scenariocontext
            if (fieldvalue.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalue.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalue}");
                fieldvalue = sContext[sContextFieldName].ToString();
            }

            GivenIAddAFilterOn(string.Empty, fieldname, fieldvalue);
            _stateHolder.ParamsBuilderValueToValidate = fieldvalue;
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with values '(.*)'")]
        [When(@"I add a '(.*)' filter on '(.*)' with values '(.*)'")]
        [Then(@"I add a '(.*)' filter on '(.*)' with values '(.*)'")]
        public void GivenIAddAFilterOnWithValues(string filtertype, string fieldname, string fieldvalue)
        {
            var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? fieldvalue : $"({fieldvalue})";

            // when you inject the item stored within the scenariocontext
            if (fieldvalue.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalue.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalue}");
                fieldvalue = sContext[sContextFieldName].ToString();
            }

            GivenIAddAFilterOn(string.Empty, fieldname, fieldvalue);

            _stateHolder.ParamsBuilderValuesToValidate = fieldvalue.Split(',').ToList();
        }



        [Given(@"I add a '(.*)' filter on '(.*)' with a value which is the random string")]
        public void GivenIAddAFilterOnWithAValueWhichIsTheRandomString(string filtertype, string fieldname)
        {
            GivenIAddAFilterOn(filtertype, fieldname, sContext[Constants.SubmissionRandomStr].ToString());
        }


        [Given(@"I make a GET request to '(.*)' resource and append the filter\(s\)")]
        [When(@"I make a GET request to '(.*)' resource and append the filter\(s\)")]
        [Then(@"I make a GET request to '(.*)' resource and append the filter\(s\)")]
        public void WhenIMakeAGETRequestToResourceAndAppendTheFilterS(string resource)
        {
            var sBuilder = new StringBuilder();

            if (_stateHolder._sParamsBuilder.Count() <= 0)
            {
                if (_stateHolder._sParamsBuilder.Count() <= 0)
                {
                    //throw new Exception("No parameters found to add.");
                }
                else
                    _stateHolder._sParamsBuilder = _stateHolder._sParamsBuilder;
            }

            foreach (var param in _stateHolder._sParamsBuilder)
            {
                sBuilder.Append("&").Append(param.Key).Append(param.Value);
            }

            if (sBuilder.Length > 0)
                sBuilder.Remove(0, 1);

            if (!resource.Contains("?"))
                sBuilder.Insert(0, $"{resource}?");
            else
                sBuilder.Insert(0, $"{resource}&");

            _commonSteps.WhenIMakeRequestToResource(Method.GET, sBuilder.ToString());
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with a value from the (.*) context")]
        [When(@"I add a '(.*)' filter on '(.*)' with a value from the (.*) context")]
        [Then(@"I add a '(.*)' filter on '(.*)' with a value from the (.*) context")]
        public void GivenIAddAFilterOnWitValueFromtheContext(string filtertype, string fieldname, EntityEnum.Entity entity)
        {
            object objectused = SetObject(entity);
            var fieldvalues = "";
            List<string> values = new List<string>();

            if (objectused != null)
            {
                var submissionproperty = ReflectionHelper.GetPropertyInfo(objectused, fieldname);

                if ((submissionproperty.PropertyType == typeof(DateTimeOffset)) || (submissionproperty.PropertyType == typeof(DateTimeOffset?)))
                {
                    var stringValue = ReflectionHelper.GetPropertyValue(objectused,
                                        fieldname).ToString();

                    var dtoffsetvalue = DateTimeOffset.Parse(stringValue);

                    values.Add(dtoffsetvalue.ToString("yyyy-MM-dd"));
                }
                else
                {
                    values.Add(ReflectionHelper.GetPropertyValue(objectused,
                                        fieldname).ToString());
                }
            }
            else
                Assert.Fail($"The context returned is empty.");

            fieldvalues = string.Join(",", values);

            var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? fieldvalues : $"({fieldvalues})";

            // when you inject the item stored within the scenariocontext
            if (fieldvalues.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalues.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalues}");
                fieldvalues = sContext[sContextFieldName].ToString();
            }

            _stateHolder.ParamsBuilderValuesToValidate.Add(values.First());
        }


        [Given(@"I add a filter for each one of the '(.*)' with values from the context of (.*)")]
        public void GivenIAddAFiltersValue(string fieldsCommaConcat, EntityEnum.Entity entity)
        {
            var fields = fieldsCommaConcat.Split(",").ToList();

            foreach (var field in fields)
            {
                GivenIAddAFilterOnWitValueFromtheContext("", field, entity);
            }
        }

        [When(@"I make a GET request to '(.*)' resource and append the filter\(s\) for (.*)")]
        public void WhenIMakeAGETRequestToResourceAndAppendTheFilterS(string resource, EntityEnum.Entity entity)
        {
            var sBuilder = new StringBuilder();

            if (_stateHolder._sParamsBuilder.Count() <= 0)
            {
                //throw new Exception("No parameters found to add.");
            }
            else
                _stateHolder._sParamsBuilder = _stateHolder._sParamsBuilder;

            foreach (var param in _stateHolder._sParamsBuilder)
            {
                sBuilder.Append("&").Append(param.Key).Append(param.Value);
            }

            if (sBuilder.Length > 0)
                sBuilder.Remove(0, 1);

            if (!resource.Contains("?"))
                sBuilder.Insert(0, $"{resource}?");
            else
                sBuilder.Insert(0, $"{resource}&");

            _commonSteps.WhenIMakeRequestToResource(Method.GET, sBuilder.ToString());
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with the value from the referenced submission")]
        public void GivenIAddAFilterOnWithTheValueFromTheReferencedSubmission(string filtertype, string fieldname)
        {
            var fieldvalues = "";
            List<string> values = new List<string>();
            var valueFromSubmission = _submissionContext.ProgrammeDescription;
            values.Add(valueFromSubmission);
            fieldvalues = string.Join(",", values);

            GivenIAddAFilterOn(filtertype, fieldname, fieldvalues); 
            _stateHolder.ParamsBuilderValuesToValidate.Add(values.First());
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with the value from the referenced submission as submission reference")]
        [When(@"I add a '(.*)' filter on '(.*)' with the value from the referenced submission as submission reference")]
        [Then(@"I add a '(.*)' filter on '(.*)' with the value from the referenced submission as submission reference")]
        public void GivenIAddAFilterOnWithTheValueFromTheReferencedSubmissionReferenceSubmission(string filtertype, string fieldname)
        {
            List<string> values = new List<string>();
            var valueFromSubmission = _submissionContext.SubmissionReference;
            values.Add(valueFromSubmission);
            string fieldvalues = string.Join(",", values);

            GivenIAddAFilterOn(filtertype, fieldname, fieldvalues); 
            _stateHolder.ParamsBuilderValuesToValidate.Add(values.First());
        }

        [Given(@"for (.*) I add a '(.*)' filter on '(.*)' with the value from the referenced submission")]
        [When(@"for (.*) I add a '(.*)' filter on '(.*)' with the value from the referenced submission")]
        [Then(@"for (.*) I add a '(.*)' filter on '(.*)' with the value from the referenced submission")]
        public void GivenIAddAFilterOnWithTheValueFromTheReferencedSubmissionReferenceSubmissionId(EntityEnum.Entity entity, 
            string filtertype, 
            string fieldname)
        {
            object objectused = SetObject(entity);
            List<string> values = new List<string>();
            var valueFromSubmission = ReflectionHelper.GetPropertyValue(objectused, fieldname).ToString();
            values.Add(valueFromSubmission);
            string fieldvalues = string.Join(",", values);

            GivenIAddAFilterOn(filtertype, fieldname, fieldvalues);
            _stateHolder.ParamsBuilderValuesToValidate.Add(values.First());
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with value equals '(.*)'")]
        public void GivenIAddAFilterOnTESTINGTHISONE(string filtertype, string fieldname, string value)
        {
            List<string> values = new List<string>{value};
            string fieldvalues = string.Join(",", values);

            GivenIAddAFilterOn(filtertype, fieldname, fieldvalues); 
            _stateHolder.ParamsBuilderValuesToValidate.Add(values.First());
        }


        [Given(@"I add a '(.*)' filter on '(.*)' with (.*) values from the response collection of (.*)")]
        [When(@"I add a '(.*)' filter on '(.*)' with (.*) values from the response collection of (.*)")]
        [Then(@"I add a '(.*)' filter on '(.*)' with (.*) values from the response collection of (.*)")]
        public void GivenIAddAFilterOnWithAValuesFromTheResponseCollection(string filtertype, 
            string fieldname, 
            int count, 
            EntityEnum.Entity entity)
        {
            if (entity == EntityEnum.Entity.submissions)
            {
                List<string> values = new List<string>();

                if (_stateHolder.CurrentSubmissions.Any())
                {
                    for (int i = 0; i < count; i++)
                    {
                        values.Add(ReflectionHelper.GetPropertyValue(_stateHolder.CurrentSubmissions[i],
                            fieldname).ToString());
                    }
                }
                else
                    Assert.Fail($"The collection returned is empty.");

                string fieldvalues = string.Join(",", values);

                GivenIAddAFilterOn(filtertype, fieldname, fieldvalues);
                _stateHolder.ParamsBuilderValueToValidate = fieldvalues;
                _stateHolder.ParamsBuilderValuesToValidate = values.ToList();
            }
        }


        [Given(@"I reset all the filters")]
        [When(@"I reset all the filters")]
        [Then(@"I reset all the filters")]
        public void RemoveFilters()
        {
            _stateHolder._sParamsBuilder = new Dictionary<string, string>();
        }

        [Given(@"I add a filter on '(.*)' with different random values from the response of (.*)")]
        [When(@"I add a filter on '(.*)' with different random values from the response of (.*)")]
        [Then(@"I add a filter on '(.*)' with different random values from the response of (.*)")]
        public void GivenIAddAFilterOnWithRandomValuesFromTheRsponse(string fieldname, EntityEnum.Entity entity)
        {
            object[] array = SetArrayCollection(entity);

            var key = $"{fieldname}=";
            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            if (array.Length > 0)
            {
                var valuesArray = new List<string>();
                var previousValue = "";

                StringHelper.FindRandomFieldValues(array, fieldname, ref valuesArray, ref previousValue);

                foreach (var val in valuesArray)
                    _stateHolder.ParamsBuilderValuesToValidate.Add(val);

                var commaSeparatedString = string.Join(",", valuesArray);

                GivenIAddAFilterOn(string.Empty, fieldname, commaSeparatedString);
            }
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with different random values from the response of (.*)")]
        [When(@"I add a '(.*)' filter on '(.*)' with different random values from the response of (.*)")]
        [Then(@"I add a '(.*)' filter on '(.*)' with different random values from the response of (.*)")]
        public void GivenIAddAParameterFilterOnWithRandomValuesFromTheRsponse(string filtertype, string fieldname, EntityEnum.Entity entity)
        {
            object[] array = SetArrayCollection(entity);
            if (array.Length > 0)
            {
                var valuesArray = new List<string>();
                var previousValue = "";

                StringHelper.FindRandomFieldValues(array, fieldname, ref valuesArray, ref previousValue);

                string fieldvalues = string.Join(",", valuesArray);
                var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

                if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

                _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? fieldvalues : $"({fieldvalues})";

                // when you inject the item stored within the scenariocontext
                if (fieldvalues.StartsWith("$#"))
                {
                    var sContextFieldName = fieldvalues.Substring(2);
                    if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalues}");
                    fieldvalues = sContext[sContextFieldName].ToString();
                }

                foreach (var val in valuesArray)
                    _stateHolder.ParamsBuilderValuesToValidate.Add(val);
                var commaSeparatedString = string.Join(",", valuesArray);

                GivenIAddAFilterOn(string.Empty, fieldname, commaSeparatedString);
            }
        }

        [Given(@"I add a date ranging filter on the '(.*)' from yesterday to tomorrow")]
        public void GivenIAddADatetimeFilterOnTheFromYesterdayToToday(string field)
        {
            var dateFrom = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            var dateTo = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            var strDateTimeValue = $"range({dateFrom},incl,{dateTo},excl)";

            Given($"I add a '' filter on '{field}' with a value '{strDateTimeValue}'");
        }

        [Given(@"I add date range filter on the '(.*)' as adatetime..bdatetime where (.*) and (.*)")]
        public void AddDaterangeFilterFromTo(string field,
           DateTime from,
           DateTime to)
        {
            var dateFrom = from.ToString("yyyy-MM-dd");
            var dateTo = to.ToString("yyyy-MM-dd");
            var strDateTimeValue = $"{dateFrom}..{dateTo}";

            Given($"I add a '' filter on '{field}' with a value '{strDateTimeValue}'");
        }

        [Given(@"I add a date filter on the '(.*)' (.*) the date of (.*)")]
        [When(@"I add a date filter on the '(.*)' (.*)  the date of (.*)")]
        [Then(@"I add a date filter on the '(.*)' (.*)  the date of (.*)")]
        public void GivenIAddADatetimeFilterOnTheFromYesterdayToToday(string field, 
            string timecomparison,
            string when)
        {
            string dateToday = when switch
            {
                "yesterday" => DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"),
                "2 days" => DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd"),
                "one week" => DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd"),
                _ => DateTime.Now.ToString("yyyy-MM-dd"),
            };

            string rangeDateTimeParam;
            if (timecomparison == "before")
                rangeDateTimeParam = $"...{dateToday}";
            else
                rangeDateTimeParam = $"{dateToday}...";

            Given($"I add a '' filter on '{field}' with a value '{rangeDateTimeParam}'");
        }

        [Given(@"I add a filter on '(.*)' with a random value from the response of (.*)")]
        [When(@"I add a filter on '(.*)' with a random value from the response of (.*)")]
        [Then(@"I add a filter on '(.*)' with a random value from the response of (.*)")]
        public void GivenIAddAFilterOnRandom(string fieldname, EntityEnum.Entity entity)
        {
            object[] array = SetArrayCollection(entity);
            var valueToCheck = string.Empty;

            var key = $"{fieldname}=";
            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            if (array.Length > 0)
            {
                StringHelper.ReturnRandomValueFromCollectionWithValue(array, fieldname, ref valueToCheck);

                if (valueToCheck.StartsWith("$#"))
                {
                    var sContextFieldName = valueToCheck.Substring(2);
                    if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {valueToCheck}");
                    valueToCheck = sContext[sContextFieldName].ToString();
                }

                GivenIAddAFilterOn(string.Empty, fieldname, valueToCheck);
                _stateHolder._sParamsBuilder[key] = valueToCheck;
                _stateHolder.ParamsBuilderValueToValidate = valueToCheck;
                _stateHolder.ParamsBuilderValuesToValidate.Add(valueToCheck);
            }
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with a random value from the response of (.*)")]
        public void GivenIAddAFilterOnRandom(string filtertype, string fieldname, EntityEnum.Entity entity)
        {
            object[] array = SetArrayCollection(entity);

            var key = string.IsNullOrEmpty(filtertype) ? $"{fieldname}=" : $"{fieldname}={filtertype}";

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            var valueToCheck = string.Empty;

            if (array.Length > 0)
            {
                StringHelper.ReturnRandomValueFromCollectionWithValue(array, fieldname, ref valueToCheck);

                _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filtertype) ? valueToCheck : $"({valueToCheck})";
                _stateHolder.ParamsBuilderValueToValidate = valueToCheck;
                _stateHolder.ParamsBuilderValuesToValidate.Add(valueToCheck);
            }
        }

    }
}

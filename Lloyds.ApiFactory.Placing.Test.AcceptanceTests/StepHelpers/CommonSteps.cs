//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;
using Constants = Lloyds.ApiFactory.Placing.Test.Common.Constants;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class CommonSteps : ApiBaseSteps
    {
        private StateHolder _stateHolder;

        public CommonSteps(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext, 
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
        }

        [Given(@"I have placing (.*) base Uri")]
        public void GivenIHavePlacingVBaseUri(string endpointUrlPath)
        {
            _apiTestContext.AddEndpointUrlPath(endpointUrlPath);
        }

        [Given(@"I set Accept header equal to (.*) type")]
        public void GivenISetAcceptHeaderEqualToType(string mimeType)
        {
            _apiTestContext.AddAcceptType(mimeType);
        }

        [Given(@"I set Content Type header equal to (.*) type")]
        [When(@"I set Content Type header equal to (.*) type")]
        [Then(@"I set Content Type header equal to (.*) type")]
        public void GivenISetContentTypeHeaderEqualToType(string contentType)
        {
            _apiTestContext.AddContentType(contentType);
        }


        [Given(@"I log in as a broker '(.*)'")]
        [When(@"I log in as a broker '(.*)'")]
        [Then(@"I log in as a broker '(.*)'")]
        [Given(@"I log in as an underwriter '(.*)'")]
        [When(@"I log in as an underwriter '(.*)'")]
        [Then(@"I log in as an underwriter '(.*)'")]
        [Given(@"I set security token and certificate for the '(.*)' user")]
        [When(@"I set security token and certificate for the '(.*)' user")]
        public void GivenISetSecurityTokenAndCertificateForTheUser(string userEmailId)
        {
            SetSecurityCertificate(userEmailId);
        }

        //[Given(@"I log in (.*) '(.*)'")]
        //[When(@"I log in (.*) '(.*)'")]
        //[Then(@"I log in (.*) '(.*)'")]
        //[Given(@"I log in (.*) '(.*)'")]
        //[When(@"I log in (.*) '(.*)'")]
        //[Then(@"I log in (.*) '(.*)'")]
        //public void LoginAs(RoleEnum.Role role, string userEmailAddress)
        //{
        //    SetSecurityCertificate(userEmailAddress);

        //    if (role == RoleEnum.Role.Broker)
        //        _stateHolder.BrokerUserEmailAddress = userEmailAddress;

        //    if (role == RoleEnum.Role.Underwriter)
        //        _stateHolder.UnderwriterUserEmailAddress = userEmailAddress;
        //}

        [Given(@"I log in as a the saved broker")]
        public void GivenILogInAsATheSavedBroker()
        {
            var userEmail = _stateHolder.BrokerUserEmailAddress;
            GivenISetSecurityTokenAndCertificateForTheUser(userEmail);
        }

        [Given(@"store the broker")]
        public void GivenStoreTheBroker()
        {
            _stateHolder.BrokerUserEmailAddress = _stateHolder.LoginUser;
        }


        [Given(@"I set query parameter (.*) to (.*)")]
        public void GivenISetQueryParameterAs(string parameter, string value)
        {
            if (value.StartsWith("|") && value.EndsWith("|"))
            {
                //  It needs to be evaluated
                var expression = value.Substring(1, value.Length - 2);
                value = ScenarioContext.Current[expression].ToString();
            }

            _apiTestContext.AddParameters(new Dictionary<string, object>
            {
                { parameter, value }
            });
        }

        [Given(@"I (.*) to '(.*)' resource")]
        [When(@"I (.*) to '(.*)' resource")]
        [Then(@"I (.*) to '(.*)' resource")]
        [Given(@"I make a (.*) request to '(.*)' resource")]
        [When(@"I make a (.*) request to '(.*)' resource")]
        [Then(@"I make a (.*) request to '(.*)' resource")]
        public void WhenIMakeRequestToResource(Method method, string resource)
        {
            resource = TransformScenarioVariables(resource);

            _apiTestContext.AddHTTPVerb(method);
            _apiTestContext.AddResource(resource);
            _apiTestContext.Response = _apiTestContext.Execute();
            _stateHolder.ResourceRequest = resource;
      }

        [Given(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header")]
        [When(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header")]
        [Then(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header")]
        public void RequestWithIfUnmodifiedSince(Method method, string resource)
        {
            resource = TransformScenarioVariables(resource);

            if ((_stateHolder.CreatedDateTime == null) || (_stateHolder.CreatedDateTime == DateTime.MinValue))
                Assert.Fail("Need to store CreatedDateTime for If-Modified-Since header in order to do a PUT request.");
            var createdDt = _stateHolder.CreatedDateTime;

            _apiTestContext.AddHTTPVerb(method);
            _apiTestContext.AddResource(resource);
            _apiTestContext.Response = _apiTestContext.ExecutePutIfUnmodifiedSince(createdDt, true);
            _stateHolder.ResourceRequest = resource;
        }

        [Given(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header using LastModifiedDate")]
        [When(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header using LastModifiedDate")]
        [Then(@"I (.*) to '(.*)' resource with 'If-Unmodified-Since' header using LastModifiedDate")]
        public void RequestWithIfUnmodifiedSinceUsingLastModifiedDateTime(Method method, string resource)
        {
            resource = TransformScenarioVariables(resource);

            if ((_stateHolder.LastModifiedDateTime == null) || (_stateHolder.LastModifiedDateTime == DateTime.MinValue))
                Assert.Fail("Need to store LastModifiedDateTime for If-Modified-Since header in order to do a PUT request.");
            var lastModifiedDate = _stateHolder.LastModifiedDateTime;

            _apiTestContext.AddHTTPVerb(method);
            _apiTestContext.AddResource(resource);
            _apiTestContext.Response = _apiTestContext.ExecutePutIfUnmodifiedSince(lastModifiedDate, false);
            _stateHolder.ResourceRequest = resource;
        }

        [When(@"make a GET_BINARY request to '(.*)' resource")]
        public void WhenIMakeRequestGetBinaryToResource(string resource)
        {
            resource = TransformScenarioVariables(resource);

            _apiTestContext.AddHTTPVerb(Method.GET);
            _apiTestContext.AddResource(resource);
            _apiTestContext.Response = _apiTestContext.ExecuteGetBinary();
            _stateHolder.ResourceRequest = resource;
        }

        [Given(@"I make a GET request to '(.*)' resource fetching all the results")]
        [When(@"I make a GET request to '(.*)' resource fetching all the results")]
        [Then(@"I make a GET request to '(.*)' resource fetching all the results")]
        public void WhenIMakeAGETRequestToResourceWithAllResults(string resource)
        {
            resource = TransformScenarioVariables(resource);

            //  Get the results
            var submissions = new List<Submission>();
            var nextLink = new Link
            {
                href = resource
            };

            while (nextLink != null)
            {
                WhenIMakeRequestToResource(Method.GET, nextLink.href);

                Assert.True(_apiTestContext.Response.IsSuccessful, $"Call to GET {nextLink.href} failed with status code {_apiTestContext.Response.StatusCode}");

                var pagedResult = JsonConvert.DeserializeObject<SubmissionCollectionWrapper>(_apiTestContext.Response.Content);
                submissions.AddRange(pagedResult.Items);
                nextLink = pagedResult.Links.FirstOrDefault(i => i.rel.Equals("next"));
            }

            //  Persist in scenario context
            sContext[Constants.SubmissionListContext] = submissions;

        }

        [Given(@"I POST to '(.*)' resource with multipart")]
        [When(@"I POST to '(.*)' resource with multipart")]
        [Then(@"I POST to '(.*)' resource with multipart")]
        public void WhenIPOSTToResourceWithMultipart(string resource)
        {
            _apiTestContext.AddHTTPVerb(Method.POST);
            _apiTestContext.AddResource(resource);
            _apiTestContext.Response = _apiTestContext.ExecuteMultipartAsync().Result;
        }


        [Given(@"the response status code should be ""(.*)""")]
        [When(@"the response status code should be ""(.*)""")]
        [Then(@"the response status code should be ""(.*)""")]
        [Given(@"the response status code is ""(.*)""")]
        [When(@"the response status code is ""(.*)""")]
        [Then(@"the response status code is ""(.*)""")]
        public void ThenTheResponseStatusCodeShouldBe(int expectedHttpStatusCode)
        {
            int actualHttpStatusCode = (int)_apiTestContext.Response.StatusCode;
            if (actualHttpStatusCode != expectedHttpStatusCode)
            {
                Console.WriteLine($"Response JSON is : {_apiTestContext.Response.Content.JsonPrettify()}");
            }

            
            Assert.AreEqual(expectedHttpStatusCode, actualHttpStatusCode, $"The status code received from the operation is {_apiTestContext.Response.StatusCode}");
        }

        [Given(@"the response status code should be '(.*)' or '(.*)'")]
        [When(@"the response status code should be '(.*)' or '(.*)'")]
        [Then(@"the response status code should be '(.*)' or '(.*)'")]
        [Then(@"the response status code should be ""(.*)"" or ""(.*)""")]
        [Then(@"the response status code should be ""(.*)"" or ""(.*)""")]
        [Then(@"the response status code should be ""(.*)"" or ""(.*)""")]
        public void ResponseStatusCodeShouldBe(int statusCode1, int statusCode2)
        {
            int actualHttpStatusCode = (int)_apiTestContext.Response.StatusCode;
            if (actualHttpStatusCode != statusCode1 && actualHttpStatusCode != statusCode2)
            {
                var message = $"Response JSON is : {_apiTestContext.Response.Content.JsonPrettify()} \n" +
                    $"and the status code {_apiTestContext.Response.StatusCode} \n " +
                    $"when expected {statusCode1} or {statusCode2}.";
                Console.WriteLine(message);

                Assert.Fail(message);
            }
        }

        [Given(@"the response status code should be '(.*)' or '(.*)' or '(.*)'")]
        [When(@"the response status code should be '(.*)' or '(.*)' or '(.*)'")]
        [Then(@"the response status code should be '(.*)' or '(.*)' or '(.*)'")]
        [Given(@"the response status code should be ""(.*)"" or ""(.*)"" or ""(.*)""")]
        [When(@"the response status code should be ""(.*)"" or ""(.*)"" or ""(.*)""")]
        [Then(@"the response status code should be ""(.*)"" or ""(.*)"" or ""(.*)""")]
        public void ResponseStatusCodeShouldBeForThreeStatusCodes(int statusCode1, 
            int statusCode2,
            int statusCode3)
        {
            int actualHttpStatusCode = (int)_apiTestContext.Response.StatusCode;
            if (actualHttpStatusCode != statusCode1
                && actualHttpStatusCode != statusCode2
                && actualHttpStatusCode != statusCode3)
            {
                var message = $"Response JSON is : {_apiTestContext.Response.Content.JsonPrettify()} \n" +
                    $"and the status code {_apiTestContext.Response.StatusCode} \n " +
                    $"when expected {statusCode1} or {statusCode2}.";
                Console.WriteLine(message);

                Assert.Fail(message);
            }
        }

        [Given(@"the response status code should be in ""(.*)"" range")]
        [When(@"the response status code should be in ""(.*)"" range")]
        [Then(@"the response status code should be in ""(.*)"" range")]
        public void ThenTheResponseStatusCodeShouldBeInRange(string expectedRange)
        {
            var x = expectedRange?[0];
            if (int.TryParse(x.ToString(), out var startRange))
            {
                var minRange = startRange * 100;
                var maxRange = minRange + 99;

                var actualHttpStatusCode = (int)_apiTestContext.Response.StatusCode;
                Assert.True(actualHttpStatusCode >= minRange && actualHttpStatusCode <= maxRange, $"The status code received from the operation {_apiTestContext.Response.StatusCode} is not in range {expectedRange}");
            }
            else
            {
                throw new Exception($"Invalid status code range specified: {expectedRange}. Examples should be 2xx, 4xx, etc");
            }
        }

        [Given(@"the submission with reference '(.*)' is (.*) in the results")]
        [When(@"the submission with reference '(.*)' is (.*) in the results")]
        [Then(@"the submission with reference '(.*)' is (.*) in the results")]
        public void ThenTheSubmissionIsPresentInTheResults(string submissionReference, string notPresent)
        {
            submissionReference = TransformScenarioVariables(submissionReference);

            var submissionList = sContext[Constants.SubmissionListContext] as List<Submission>;
            var submissionIsPresent = submissionList.Any(i => i.Id.Equals(submissionReference, StringComparison.OrdinalIgnoreCase));

            if (notPresent.Equals("absent", StringComparison.OrdinalIgnoreCase))
            {
                Assert.False(submissionIsPresent);
            }
            else
            {
                Assert.True(submissionIsPresent);
            }
        }


        [Given(@"the response HTTP header ContentType should be to (.*) type")]
        [When(@"the response HTTP header ContentType should be to (.*) type")]
        [Then(@"the response HTTP header ContentType should be to (.*) type")]
        public void ThenTheResponseHTTPHeaderContentTypeShouldBeToType(string mimeType)
        {
            Assert.IsTrue(_apiTestContext.Response.ContentType.Contains(mimeType, StringComparison.OrdinalIgnoreCase));
        }


        [Then(@"the response submission should match the original submission")]
        public void ThenTheResponseSubmissionShouldMatchTheOriginalSubmission()
        {
            var submissionFromResponse = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
            var expectedSubmission = sContext[Constants.SubmissionContext] as Submission;
            Assert.NotNull(expectedSubmission);
            Assert.AreEqual(submissionFromResponse.BrokerCode, expectedSubmission.BrokerCode);
            Assert.AreEqual(submissionFromResponse.ClassOfBusinessCode, expectedSubmission.ClassOfBusinessCode);
            Assert.AreEqual(submissionFromResponse.CoverTypeCode, expectedSubmission.CoverTypeCode);

            if (expectedSubmission.ContractTypeCode != null && expectedSubmission.ContractTypeCode == ContractType.reinsurance_contract)
            {
                Assert.AreEqual(submissionFromResponse.OriginalPolicyholder, expectedSubmission.OriginalPolicyholder);
            }

            Assert.AreEqual(submissionFromResponse.SubmissionReference, expectedSubmission.SubmissionReference);
            Assert.AreEqual(submissionFromResponse.SubmissionVersionNumber, expectedSubmission.SubmissionVersionNumber);
            Assert.AreEqual(submissionFromResponse.SubmissionVersionDescription, expectedSubmission.SubmissionVersionDescription);
            Assert.AreEqual(submissionFromResponse.RiskCountryCode, expectedSubmission.RiskCountryCode);
            Assert.AreEqual(submissionFromResponse.RiskRegionCode, expectedSubmission.RiskRegionCode);
            Assert.AreEqual(submissionFromResponse.BrokerUserEmailAddress, expectedSubmission.BrokerUserEmailAddress);
            Assert.AreEqual(submissionFromResponse.ContractDescription, expectedSubmission.ContractDescription);
            Assert.AreEqual(submissionFromResponse.ContractReference, $"{expectedSubmission.ContractReference}");
            Assert.AreEqual(submissionFromResponse.ContractTypeCode, expectedSubmission.ContractTypeCode);
            Assert.AreEqual(submissionFromResponse.InsuredOrReinsured, expectedSubmission.InsuredOrReinsured);
            Assert.AreEqual(submissionFromResponse.ProgrammeDescription, expectedSubmission.ProgrammeDescription);
            Assert.AreEqual(submissionFromResponse.ProgrammeReference, $"B{ expectedSubmission.BrokerCode}{ expectedSubmission.ProgrammeReference}");
        }

        public string TransformScenarioVariables(string input)
        {
            var regex = @"{(.*?)}";
            var matches = Regex.Matches(input, regex);

            foreach (var match in matches.ToList())
            {
                var matchValue = match.Groups[1].Value;

                var computedString = sContext[matchValue].ToString();

                input = input.Replace(match.Value, computedString);
            }

            return input;
        }

        [Then(@"the submission status should be '(.*)'")]
        public void ThenTheSubmissionStatusShouldBe(string expectedStatus)
        {
            var submissionFromResponse = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
            var statusCode = submissionFromResponse.SubmissionStatusCode.Value.ToString();
            var statusDescription = EnumHelper.GetReferenceValue(EnumHelper.SubmissionStatusMappings, statusCode);
            Assert.AreEqual(expectedStatus, statusDescription);

        }

        [Then(@"print the error message to the output window")]
        public void ThenPrintTheErrorMessageToTheOutputWindow()
        {
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest) return;

            var submissionresponsedata = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);
            if (submissionresponsedata == null) return;

            if (submissionresponsedata.ValidationErrors == null) return;

            foreach (var errormessage in submissionresponsedata.ValidationErrors)
            {
                Console.WriteLine($"The field {errormessage.Field} has a validation error {errormessage.Error}.");
            }
        }

        [Given(@"I add a content-level header '(.*)' for the request")]
        [When(@"I add a content-level header '(.*)' for the request")]
        [Then(@"I add a content-level header '(.*)' for the request")]
        public void GivenIAddAForTheRequest(string header)
        {
            IDictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("If-None-Match", "etag");
            _apiTestContext.AddHeaders(headers);
        }

        [Given(@"the response is a valid (.*) type of (.*) collection")]
        [When(@"the response is a valid (.*) type of (.*) collection")]
        [Then(@"the response is a valid (.*) type of (.*) collection")]
        public void IsValidResponse(string type, string entity)
        {
            ValidateCollectionResponse(type, entity);
        }

        [Given(@"store the underwriter")]
        [When(@"store the underwriter")]
        [Then(@"store the underwriter")]
        public void GivenIStoreTheunderwriter()
        {
            _stateHolder.UnderwriterUserEmailAddress = _stateHolder.LoginUser;
        }

        [Given(@"store the underwriter as '(.*)'")]
        [When(@"store the underwriter as '(.*)'")]
        [Then(@"store the underwriter as '(.*)'")]
        public void GivenIStoreTheunderwriter(string underwriterUserEmailAddress)
        {
            _stateHolder.UnderwriterUserEmailAddress = underwriterUserEmailAddress;
        }

        [Then(@"the (.*) response is order by '(.*)' '(.*)'")]
        public void ThenTheResponseIsOrderBy(EntityEnum.Entity entity, string fieldName, string ordering = "ascending")
        {
            object[] items = null;

            if (entity == EntityEnum.Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext is null)
                    throw new AssertionException("The submission underwriters collection data is empty.");
                items = _submissionUnderwriterCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission)
            {
                if (_submissionCollectionContext is null)
                    throw new AssertionException("The submissions collection data is empty.");
                items = _submissionCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission_documents)
            {
                if (_submissionDocumentCollectionContext is null)
                    throw new AssertionException("The submission documents collection data is empty.");
                items = _submissionDocumentCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext is null)
                    throw new AssertionException("The submission dialogue collection data is empty.");
                items = _submissionDialogueCollectionContext.Items;
            }

            if (entity == EntityEnum.Entity.underwriter_organisations)
            {
                if (_underwriterOrganisationContext is null)
                    throw new AssertionException("The underwriter organisation collection data is empty.");
                items = _underwriterOrganisationContext.Items;
            }

            var previousDatetime = new DateTime();
            var lastStringFound = string.Empty;

            foreach (var i in items)
            {
                if (fieldName == "_last_modified")
                    return;

                var propertyfound = ReflectionHelper.GetPropertyValue(i, fieldName);

                if (propertyfound == null) return; //not sure if we should fail when found null. there are lots of old-data dailures here
                    //Assert.Fail($"Couldn't found field of {fieldName}.");

                // datetime ordering
                if (propertyfound is DateTimeOffset dto)
                {
                    var prop = ReflectionHelper.GetProperty(i, fieldName);
                    var currentDt = dto.UtcDateTime;

                    StepsExension.ValidateDateTimeSort(currentDt, previousDatetime, ordering);
                    previousDatetime = dto.UtcDateTime;
                }

                //string ordering
                if (propertyfound is string)
                {
                    StepsExension.ValidateStringSort(lastStringFound, propertyfound, ordering);
                    lastStringFound = Convert.ToString(propertyfound);
                }
            }
        }

        private void SetSecurityCertificate(string userEmailId)
        {
            var email = userEmailId.Replace("'", "");
            var authToken = _objectContainer.Resolve<UserBearerToken>(email).AuthToken;
            _apiTestContext.AddAuthorization(authToken);
            var orgRoot = _objectContainer.Resolve<OrganisationRoot>();

            var orgName = orgRoot.Organisations.GetOrganisationNameByUserEmailId(email);
            if (string.IsNullOrEmpty(orgName)) throw new Exception($"Unable to retrieve the organisation details for the email address {userEmailId}.");

            var clientCert = _objectContainer.Resolve<OrganisationClientCertificate>(orgName).ClientCertificate;
            if (clientCert is null) throw new Exception($"Unable to resolve organisation client certificate for the organisation name {orgName}.");

            _apiTestContext.AddClientCertificate(clientCert);
            _stateHolder.LoginUser = email;
        }
       


    }
}

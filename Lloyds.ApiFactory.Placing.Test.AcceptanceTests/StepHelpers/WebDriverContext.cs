//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    public class WebDriverContext
    {
        public ChromeDriver driver;
        private WebDriverWait _wait;

        public WebDriverContext()
        {
            driver = new ChromeDriver();
        }

        
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using RestSharp;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers
{
    [Binding]
    public class Transforms1
    {
        [StepArgumentTransformation(@"(.*)")]
        public double? ToNullableDouble(string value)
        {
            return value == null ? default(double?) : double.Parse(value);
        }

        [StepArgumentTransformation(@"null")]
        public string ToNull()
        {
            return null;
        }

        [StepArgumentTransformation(@"string action")]
        public Method ActionEnumFromString(string action)
        {
            if (action == "GET")
                return Method.GET;

            if (action == "POST")
                return Method.POST;

            if (action == "PUT")
                return Method.PUT;

            if (action == "DELETE")
                return Method.DELETE;

            if (action == "HEAD")
                return Method.HEAD;

            return Method.GET;
        }

        [StepArgumentTransformation(@"(.*)")]
        public RuleEnum.Rule RuleEnumFromString(string action)
        {
            switch (action)
            {
                case "No leading or trailing whitespace":
                    return RuleEnum.Rule.no_leading_or_trailing_whitespace;

                case "No curly open/close brackets":
                    return RuleEnum.Rule.no_curly_open_close_brackets;

                case "No whitespace":
                    return RuleEnum.Rule.no_whitespace;

                case "No special characters":
                    return RuleEnum.Rule.no_special_chars;

                default:
                    return RuleEnum.Rule.no_rule;

            }
        }

        [StepArgumentTransformation(@"values of \((.*)\)")]
        public List<string> ToListString(string input)
        {
            if (input.Contains(","))
            {
                return input.Split(',').Select(x => x).ToList();
            }

            return new List<String>();
        }

        [StepArgumentTransformation(@"string date")]
        public DateTime StringToDateTime(string input)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            DateTime.TryParseExact(input,
            new string[] { "yyyy/MM/dd", "yyyy-MM-dd" },
                provider, DateTimeStyles.None, out DateTime dateTime1);

            return dateTime1;
        }

        [StepArgumentTransformation(@"(.*)")]
        public EntityEnum.Entity EntityStringToEnum(string input)
        {
            if (input == "submission")
                return EntityEnum.Entity.submission;

            if (input == "submissions")
                return EntityEnum.Entity.submissions;

            if (input == "submission underwriters")
                return EntityEnum.Entity.submission_underwriters;

            if (input == "submission documents")
                return EntityEnum.Entity.submission_documents;

            if (input == "submission dialogue")
                return EntityEnum.Entity.submission_dialogue;

            if (input == "underwriter organisations")
                return EntityEnum.Entity.underwriter_organisations;

            return EntityEnum.Entity.submission;
        }

        //having post submission document
        [StepArgumentTransformation(@"input has (.*)")]
        public bool HasSubmissionDocumentsToBool(string input)
        {
            if (input == "submission documents")
                return true;

            if (input == "no submission documents")
                return false;

            return false;
        }

        [StepArgumentTransformation(@"can(.*)")]
        public bool ReturnCanCannot(string mode)
        {
            if (mode == "not")
                return false;

            return true;
        }

        [StepArgumentTransformation(@"datetime value of (.*)")]
        public DateTime ReturnDateTimeFromString(string time)
        {
            if (time == "today")
                return DateTime.Now;

            if (time == "a year from now")
                return DateTime.Now.AddYears(1);

            if (time == "a month from now")
                return DateTime.Now.AddMonths(1);

            if (time == "yesterday")
                return DateTime.Now.AddDays(-1);

            if (time == "last year")
                return DateTime.Now.AddYears(-1);

            return DateTime.Now;
        }

        [StepArgumentTransformation(@"IsDraft=(.*)")]
        public bool ReturnIsDrfatBoolean(string stringBoolean)
        {
            if (stringBoolean.ToLower() == "true")
                return true;

            if (stringBoolean.ToLower() == "false")
                return false;

            return false;
        }

        [StepArgumentTransformation(@"SenderActionCode=(.*)")]
        public SubmissionDialogueFilterModel ReturnActionCodeToSubmissionDialogueActionType(string type)
        {
            if (type.ToLower() == "quote")
                return SubmissionDialogueFilterModel.QUOTE;

            if (type.ToLower() == "eoi")
                return SubmissionDialogueFilterModel.EOI;

            if (type.ToLower() == "rfi")
                return SubmissionDialogueFilterModel.RFI;

            if (type.ToLower() == "decl")
                return SubmissionDialogueFilterModel.DECL;

            return SubmissionDialogueFilterModel.QUOTE;
        }
		
		[StepArgumentTransformation(@"(.*) the submission")]
        public bool ReturnBoolFromString(string withOrWithout)
        {
            if (withOrWithout == "with")
                return true;

            if (withOrWithout == "without")
                return false;

            return true;
        }

        [StepArgumentTransformation(@"(.*)")]
        public IEnumerable<Model.SubmissionDocumentCreateModel> TableToListDocumentCreateModel(Table table)
        {
            return table.CreateSet<Model.SubmissionDocumentCreateModel>();
        }

        [StepArgumentTransformation(@"(.*)")]
        public IEnumerable<Submission> TableToSubmissions(Table submissiontable)
        {
           return submissiontable.CreateSet<Submission>();
        }


        [StepArgumentTransformation(@"is (.*)")]
        public bool ReturnToBoolnNot(string exists)
        {
            if (exists == "NOT")
                return false;

            return true;
        }

        [StepArgumentTransformation(@"as (.*)")]
        public RoleEnum.Role StringRole(string role)
        {
            if (role.Equals("a broker", StringComparison.OrdinalIgnoreCase))
                return RoleEnum.Role.Broker;

            if (role.Equals("an underwriter", StringComparison.OrdinalIgnoreCase))
                return RoleEnum.Role.Broker;

            return RoleEnum.Role.Underwriter;
        }


        [StepArgumentTransformation(@"adatetime=(.*)")]
        [StepArgumentTransformation(@"bdatetime=(.*)")]
        public DateTime RangeStringToDateTime(string input)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;

            if (input == "a week ago")
                return DateTime.Now.AddDays(-7);

            if (input == "yesterday")
                return DateTime.Now.AddDays(-1);

            if (input == "today")
                return DateTime.Now;

            if (input == "tomorrow")
                return DateTime.Now.AddDays(1);

            DateTime.TryParseExact(input,
            new string[] { "yyyy/MM/dd", "yyyy-MM-dd" },
                provider, DateTimeStyles.None, out DateTime dateTime1);

            return dateTime1;
        }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using BoDi;
using FluentAssertions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class VersionSteps : ApiBaseSteps
    {
        public VersionSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext) : base(objectContainer, apiTestContext)
        {
        }

        [Given(@"the response body should have value")]
        [When(@"the response body should have value")]
        [Then(@"the response body should have value")]
        public void ThenTheResponseBodyShouldHaveValue()
        {
            Assert.IsTrue(!string.IsNullOrEmpty(_apiTestContext.Response.Content));
        }

        [Then(@"the version resource has (.*) property")]
        public void ThenTheVersionResourceHasProperty(string expectedApiVersion)
        {
            //Todo: PV this needs to be refactor to support XML and JSON format
            var jToken = JToken.Parse(_apiTestContext.Response.Content);
            var actualApiVersion = jToken.ToObject<VersionInformation>();

            actualApiVersion.APIVersionNumber.Should().Contain(expectedApiVersion);
        }

    }
}

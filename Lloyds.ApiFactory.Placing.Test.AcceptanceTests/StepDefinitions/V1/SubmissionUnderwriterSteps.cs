
//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Linq;
using BoDi;
using FluentAssertions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionUnderwriterSteps : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private CommonSteps CommonSteps;
        private SubmissionGET SubmissionGetSteps;
        private AbstractSteps AbstractSteps;
        private AbstractStepsFilters _filterSteps;

        public SubmissionUnderwriterSteps(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
                : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;
            CommonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionGetSteps = new SubmissionGET(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            _filterSteps = new AbstractStepsFilters(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
        }


        [Then(@"submission underwriters belong to (.*) '(.*)'")]
        public void ThenSubmissionUnderwritersBelongToBroker(RoleEnum.Role role, string emailAddress)
        {
            if (role == RoleEnum.Role.Broker)
            {
                Given($"I log in as a broker '{emailAddress}'");
                When("I make a GET request to '/brokerdepartments' resource");
                And("I store broker departments");

                SubmissionUnderwritersMatchDepartment(role);
            }

            if (role == RoleEnum.Role.Underwriter)
            {
                var brokerUserEmailAddress = _submissionUnderwriterCollectionContext.Items.FirstOrDefault().BrokerUserEmailAddress;

                Given("I reset all the filters");

                GetBrokerDepartmentAs(brokerUserEmailAddress);

                GetUnderwriterOrganisationFor(emailAddress);

                var uwOrgItems = _underwriterOrganisationContext.Items;

                foreach (var uworg in uwOrgItems)
                {
                    if (!_submissionUnderwriterCollectionContext.Items.Any(x => x.UnderwriterOrganisationId == uworg.UnderwriterOrganisationId))
                        Assert.Fail($"The underwriter organisation {uworg.UnderwriterOrganisationName} with id {uworg.Id} is not been included in the return submission underwriters.");
                }
            }

        }

        private void SubmissionUnderwritersMatchDepartment(RoleEnum.Role role)
        {
            if (role == RoleEnum.Role.Broker)
            {
                var brokerDepartmentNames = _brokerDepartmentContext.Items.Select(x => x.BrokerDepartmentName);
                foreach (var item in _submissionUnderwriterCollectionContext.Items)
                {
                    var departmentName = item.BrokerDepartmentName;

                    Assert.IsTrue(brokerDepartmentNames.Any(x => x.Equals(departmentName,
                        StringComparison.OrdinalIgnoreCase)));
                }

            }
        }

        private void SubmissionUnderwritersMatchToEmailAddress(RoleEnum.Role role, string emailAddress)
        {
            if (role == RoleEnum.Role.Broker)
            {
                // BrokerUsers.BrokerUserEmailAddress = [Requester User Email Address] RULE 
                foreach (var item in _submissionUnderwriterCollectionContext.Items)
                {
                    if (!emailAddress.Equals(item.BrokerUserEmailAddress, StringComparison.OrdinalIgnoreCase))
                        Assert.Fail($"The BrokerUserEmailAddress of the submission underwriter {item.BrokerUserEmailAddress} doesn't match with" +
                            $"with the correct broker user email address {emailAddress}.");
                }
            }

            if (role == RoleEnum.Role.Underwriter)
            {
               // no further checks needed for the underwriter
            }
        }

        [Given(@"I pick a random submission underwriter")]
        public void GivenIPickARandomSubmissionUnderwriter()
        {
            var random = new Random();
            if (_submissionUnderwriterCollectionContext.Items.Any())
            {
                for (int i = 0; i < _submissionUnderwriterCollectionContext.Items.Count(); i++)
                {
                    int index = random.Next(_submissionUnderwriterCollectionContext.Items.Count());

                    _SubmissionUnderwriterContext = _submissionUnderwriterCollectionContext.Items[index];
                    _SubmissionUnderwriterContext.SubmissionUnderwriterId = _submissionUnderwriterCollectionContext.Items[index].Id;
                    SetTheSubmissionUnderwriterId();
                }

            }
            else
                Assert.Fail("There are no submission underwriters returned.");
        }

        [Given(@"I set the SubmissionUnderwriterId")]
        [When(@"I set the SubmissionUnderwriterId")]
        [Then(@"I set the SubmissionUnderwriterId")]
        public void SetTheSubmissionUnderwriterId()
        {
            if (!sContext.ContainsKey(Constants.SubmissionUnderwriterContext))
                sContext.Add(Constants.SubmissionUnderwriterContext, "");

            if (_SubmissionUnderwriterContext == null)
                _SubmissionUnderwriterContext = new SubmissionUnderwriter();

            sContext[Constants.SubmissionUnderwriterId] = _SubmissionUnderwriterContext != null ? _SubmissionUnderwriterContext.Id : null;
        }


        [Given(@"I store a random underwriter organisations details '(.*)' for submission underwriter")]
        public void StoreTheUnderwriterOrganisationsDetailsFor(string uwOrgEmailAddress)
        {
            GivenUnderwriterOrganisationAllowed(uwOrgEmailAddress);
            UnderwriterIsValidForOrganisaiton(uwOrgEmailAddress);
        }


        [Given(@"the underwriter organisation for '(.*)' is allowed to be approached for the related submission")]
        public void GivenUnderwriterOrganisationAllowed(string uwOrgEmailAddress)
        {
            var searchPrefix = uwOrgEmailAddress.Substring(0, uwOrgEmailAddress.IndexOf("@")+5);

            _filterSteps.GivenIAddAFilterOn("contains", "NameEmailOrg", searchPrefix);
            _filterSteps.GivenIAddAFilterOn($"", "BrokerDepartmentId", _submissionContext.BrokerDepartmentId);
            _filterSteps.WhenIMakeAGETRequestToResourceAndAppendTheFilterS("/UnderwriterOrganisations");
            Then("save the collection of underwriter organistations");
        }


        [Given(@"the underwriter '(.*)' is valid for the underwriter organisation")]
        public void UnderwriterIsValidForOrganisaiton(string uwOrgEmailAddress)
        {
            var rand = new Random();

            if (_underwriterOrganisationContext.Items.Any())
            {
                int index = rand.Next(_underwriterOrganisationContext.Items.Count());
                _stateHolder.UnderwriterOrganisation = _underwriterOrganisationContext.Items[index];

                var uwUser = _stateHolder.UnderwriterOrganisation.UnderwriterUsers
                    .FirstOrDefault(x => x.UnderwriterUserEmailAddress.Equals(uwOrgEmailAddress, 
                    StringComparison.OrdinalIgnoreCase));

                if (uwUser != null)
                {
                    _SubmissionUnderwriterContext.UnderwriterOrganisationId = _stateHolder.UnderwriterOrganisation.UnderwriterOrganisationId;
                    _SubmissionUnderwriterContext.UnderwriterUserFullName = uwUser.UnderwriterUserFullName; ;
                    _SubmissionUnderwriterContext.UnderwriterUserEmailAddress = uwUser.UnderwriterUserEmailAddress;
                }
                else
                    Assert.Fail($"Couldnt find any underwriter user in the underwriter organisations for {uwOrgEmailAddress}");
            }
            else
                Assert.Fail($"Couldnt find any UnderwriterOrganisations for {uwOrgEmailAddress}");
        }


        [Given(@"the submission underwriter is saved for the scenario")]
        public void GivenTheSubmissionUnderwriterIsSavedForTheScenario()
        {
            var submissionrequest = JsonConvert.SerializeObject(_SubmissionUnderwriterContext, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            Console.WriteLine(submissionrequest.JsonPrettify());
            _apiTestContext.AddParameterToRequestBody("submissionUnderwriter", submissionrequest);
            _stateHolder.JsonRequestBody = submissionrequest;

            And("I set Content Type header equal to application/json type");
        }

        [Given(@"the submission underwriter is saved for the scenario PUT")]
        public void GivenTheSubmissionUnderwriterIsSavedForTheScenarioPUT()
        {
            var submissionPUTrequest = new SubmissionUnderwriterPUT() {
                SubmissionUnderwriterId = _SubmissionUnderwriterContext.SubmissionUnderwriterId,
                CommunicationsMethodCode = _SubmissionUnderwriterContext.CommunicationsMethodCode,
                SubmissionReference = _SubmissionUnderwriterContext.SubmissionReference,
                SubmissionVersionNumber = _SubmissionUnderwriterContext.SubmissionVersionNumber,
                UnderwriterUserEmailAddress = _SubmissionUnderwriterContext.UnderwriterUserEmailAddress,
                UnderwriterUserFullName = _SubmissionUnderwriterContext.UnderwriterUserFullName
            };

            var submissionrequest = JsonConvert.SerializeObject(submissionPUTrequest, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            Console.WriteLine(submissionrequest.JsonPrettify());
            _apiTestContext.AddParameterToRequestBody("submissionUnderwriter", submissionrequest);
            _stateHolder.JsonRequestBody = submissionrequest;
            And("I set Content Type header equal to application/json type");
        }


        [Given(@"I save the submission underwriter context from submission")]
        public void SaveTheSubmissionUnderwriterContextFromSubmission()
        {
            _SubmissionUnderwriterContext.SubmissionReference = _submissionContext.SubmissionReference;
            _SubmissionUnderwriterContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber ?? 1;
        }

        [Given(@"for the submission underwriters the '(.*)' is different than the default submission version number")]
        public void GivenForTheSubmissionUnderwriterContextTheIsDifferentThanTheSubmissionrVersionNumber(string fieldname)
        {
            var newSubmissionVersionNumber = _submissionContext.SubmissionVersionNumber + 1;
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_underwriters, fieldname, newSubmissionVersionNumber.ToString());
        }

        //todo: move to Abstract Steps
        [Given(@"I store the submission underwriter in the context")]
        [When(@"I store the submission underwriter in the context")]
        [Then(@"I store the submission underwriter in the context")]
        public void GivenIStoreTheSubmissionUnderwriter()
        {
            _SubmissionUnderwriterContext = JsonConvert.DeserializeObject<SubmissionUnderwriter>(_apiTestContext.Response.Content);
            if (_SubmissionUnderwriterContext is null) throw new AssertionException("The submission underwriter is empty.");

            Assert.IsTrue(_SubmissionUnderwriterContext is SubmissionUnderwriter);
        }

        [Given(@"I store the submission underwriter in the context from the collection")]
        [When(@"I store the submission underwriter in the context from the collection")]
        [Then(@"I store the submission underwriter in the context from the collection")]
        public void GivenIStoreTheSubmissionUnderwritersFromTheCollection()
        {
            Assert.IsTrue(_submissionUnderwriterCollectionContext.Items.Length > 0);

            _SubmissionUnderwriterContext = _submissionUnderwriterCollectionContext.Items.FirstOrDefault();
            if (_SubmissionUnderwriterContext is null) throw new AssertionException("The submission underwriter is empty.");

            Assert.IsTrue(_SubmissionUnderwriterContext is SubmissionUnderwriter);
        }


        [Given(@"I POST SubmissionUnderwriter from the data")]
        [When(@"I POST SubmissionUnderwriter from the data")]
        [Then(@"I POST SubmissionUnderwriter from the data")]
        public void POSTASubmissionUnderwriterWithTheFollowingDetails(Table table)
        {
            var submissionUnderwriters = table.CreateSet<SubmissionUnderwriter>();

            Given("I store the submission in the context");
            Given("I set the SubmissionUnderwriterId");
            Given("I clear down test context");
            Given("I save the submission underwriter context from submission");

            foreach (var qm in submissionUnderwriters)
            {
                StoreTheUnderwriterOrganisationsDetailsFor(qm.UnderwriterUserEmailAddress);

                if (qm.CommunicationsMethodCode != null)
                    Given($"for the submission underwriters the 'CommunicationsMethodCode' is '{qm.CommunicationsMethodCode}'");

                if (qm.ContractTypeCode != null)
                    Given($"for the submission underwriters the 'ContractTypeCode' is '{qm.ContractTypeCode}'");

                if (qm.ClassOfBusinessCode != null)
                    Given($"for the submission underwriters the 'ClassOfBusinessCode' is '{qm.ClassOfBusinessCode}'");

                Given("the submission underwriter is saved for the scenario");
                When("I POST to '/SubmissionUnderwriters' resource");

                Then("I store the submission underwriter in the context");
                And("I save the submission underwriters added");
                Then("I set the SubmissionUnderwriterId");

                Given("I clear down test context"); //this is necessary if we POST more than one QMs
            }
        }

        [Given(@"select submission underwriter of '(.*)'")]
        public void GivenSelectSubmissionUnderwriterOf(string underwriterEmailAddress)
        {
            When("I make a GET request to '/SubmissionUnderwriters' resource and append the filter(s)");
            And("the response is a valid application/json; charset=utf-8 type of submission underwriters collection");

            var subissionUnderwriter = _submissionUnderwriterCollectionContext.Items.FirstOrDefault(x => x.UnderwriterUserEmailAddress.Equals(underwriterEmailAddress,
                StringComparison.OrdinalIgnoreCase));


            _SubmissionUnderwriterContext = subissionUnderwriter;
        }


        [Given(@"I POST SubmissionUnderwriter from the data provided submission documents have been posted")]
        [When(@"I POST SubmissionUnderwriter from the data provided submission documents have been posted")]
        [Then(@"I POST SubmissionUnderwriter from the data provided submission documents have been posted")]
        public void POSTASubmissionUnderwritersWithTheFollowingDetailsWithQD(Table table)
        {
            var submissionUnderwriters = table.CreateSet<SubmissionUnderwriter>();

            SetTheSubmissionUnderwriterId();
            AbstractSteps.TestContextClearDown();
            SaveTheSubmissionUnderwriterContextFromSubmission();

            foreach (var qm in submissionUnderwriters)
            {
                StoreTheUnderwriterOrganisationsDetailsFor(qm.UnderwriterUserEmailAddress);

                if (qm.CommunicationsMethodCode != null)
                    Given($"for the submission underwriters the 'CommunicationsMethodCode' is '{qm.CommunicationsMethodCode}'");

                if (qm.ContractTypeCode != null)
                    Given($"for the submission underwriters the 'ContractTypeCode' is '{qm.ContractTypeCode}'");

                if (qm.ClassOfBusinessCode != null)
                    Given($"for the submission underwriters the 'ClassOfBusinessCode' is '{qm.ClassOfBusinessCode}'");

                Given("the submission underwriter is saved for the scenario");
                When("I POST to '/SubmissionUnderwriters' resource");
                Then("I store the submission underwriter in the context");
                And("I save the submission underwriters added");
                Then("I set the SubmissionUnderwriterId");

                Given("I clear down test context"); //this is necessary if we POST more than one QMs
            }

        }



        [Given(@"I set the submission broker department id to '(.*)'")]
        public void GivenISetTheSubmissionBrokerDepartmentIdTo(string brokerDepartmentId)
        {
            _submissionContext.BrokerDepartmentId = brokerDepartmentId;
        }



        [Then(@"validate the submission underwriter response")]
        public void ThenValidateTheSubmissionUnderwritersResponse()
        {
            if (_SubmissionUnderwriterContext != null)
            {
                foreach (var qm in _stateHolder.CurrentSubmissionUnderwriters)
                {
                    if (qm.SubmissionUnderwriterId != _SubmissionUnderwriterContext.SubmissionUnderwriterId)
                        Assert.Fail($"SubmissionUnderwriter is not included on the scenario.");
                }
            }
        }


        [Given(@"broker departments do not include related submission broker department")]
        public void GivenBrokerDepartmentsDoNotIncludeRelatedSubmissionBrokerDepartment()
        {
            var submissionBrokerDepartmentId = _submissionContext.BrokerDepartmentId;

            if (_brokerDepartmentContext.Items.Any(x => x.BrokerDepartmentId == submissionBrokerDepartmentId))
                Assert.Fail("This broker is on the same broker department. Choose another broker for the scenario!");
        }


        [When(@"I delete all submission underwriters with createddatetime is '(.*)'")]
        public void WhenIDeleteAllSubmissionUnderwritersWithCreateddatetimeIs(string p0)
        {
            foreach (var item in _submissionUnderwriterCollectionContext.Items)
            {
                if (item.CreatedDateTime == DateTime.MinValue)
                {
                    When($"I make a DELETE request to '/SubmissionUnderwriters/{item.SubmissionUnderwriterId}' resource");
                }
            }
        }

        [Then(@"I delete all the submission underwriters with minimum createddatetime value")]
        public void ThenIDeleteAllTheSubmissionUnderwritersWithMinimumCreateddatetimeValue()
        {
            for (int i = 0; i < 500; i++)
            {
                And("I make a GET request to '/SubmissionUnderwriters' resource");
                And("the response is a valid application/json; charset=utf-8 type of submission underwriters collection");

                if (_submissionUnderwriterCollectionContext.Items.Any(x => x.CreatedDateTime == DateTime.MinValue))
                    When("I delete all submission underwriters with createddatetime is 'null'");
                else
                    break;
            }
        }

        [Then(@"validate that the submission underwriters context created will be matching the POST model")]
        [Then(@"Submission fields copied to SubmissionUnderwriter")]
        public void ThenValidateThatTheSubmissionUnderwritersContextCreatedWillBeMatchingThePOSTModel()
        {
            // validate _SubmissionUnderwriterContext WITH _submissionContext
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionReference, _submissionContext.SubmissionReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionDescription, _submissionContext.SubmissionVersionDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, _submissionContext.SubmissionVersionNumber);
            Assert.AreEqual(_SubmissionUnderwriterContext.BrokerUserEmailAddress, _submissionContext.BrokerUserEmailAddress);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeId, _submissionContext.ProgrammeId);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeReference, _submissionContext.ProgrammeReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeDescription, _submissionContext.ProgrammeDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractReference, _submissionContext.ContractReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractDescription, _submissionContext.ContractDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractTypeCode, _submissionContext.ContractTypeCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.InsuredOrReinsured, _submissionContext.InsuredOrReinsured);
            Assert.AreEqual(_SubmissionUnderwriterContext.OriginalPolicyholder, _submissionContext.OriginalPolicyholder);
            Assert.AreEqual(_SubmissionUnderwriterContext.CoverTypeCode, _submissionContext.CoverTypeCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.ClassOfBusinessCode, _submissionContext.ClassOfBusinessCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.RiskRegionCode, _submissionContext.RiskRegionCode);

            if (_submissionContext.PeriodOfCover.InsuranceDuration != null)
            {
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsuranceDuration.DurationNumber, _submissionContext.PeriodOfCover.InsuranceDuration.DurationNumber);
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsuranceDuration.DurationUnit, _submissionContext.PeriodOfCover.InsuranceDuration.DurationUnit);
            }

            if (_submissionContext.PeriodOfCover.InsurancePeriod != null)
            {
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.ExpiryDate, _submissionContext.PeriodOfCover.InsurancePeriod.ExpiryDate);
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.InceptionDate, _submissionContext.PeriodOfCover.InsurancePeriod.InceptionDate);

                Assert.AreNotEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.ExpiryDate, DateTime.MinValue);
                Assert.AreNotEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.InceptionDate, DateTime.MinValue);
            }

            Assert.AreNotEqual(_SubmissionUnderwriterContext.CreatedDateTime, DateTime.MinValue);
        }


        [Given(@"for the submission underwriters the SubmissionReference is invalid")]
        public void TheSubmissionUnderwritersTheSubmissionReferenceIsInvalid()
        {
            var newId = "";

            var invalidSubmissionReference = Extension.GenerateRandomString(10);

            var parts = _SubmissionUnderwriterContext.Id.Split("_");
            var last = Enumerable.Last<string>(parts);
            var first = Enumerable.First<string>(parts);
            foreach (var p in parts)
            {
                if (p.Equals(first))
                {
                    newId += invalidSubmissionReference + "_";
                }
                else if (p.Equals(last))
                {
                    newId += p;
                }
                else
                {
                    newId += p + "_";
                }
            }

            _SubmissionUnderwriterContext.Id = newId;
            _SubmissionUnderwriterContext.SubmissionReference = invalidSubmissionReference;
        }


        [Given(@"for the submission underwriters the SubmissionUnderwriterId is invalid")]
        public void TheSubmissionUnderwritersTheSubmissionUdnerwriterIdIsInvalid()
        {
            var newId = "";

            var invalidQMId = Extension.GenerateRandomStringNumbers(4);

            var parts = _SubmissionUnderwriterContext.Id.Split("_");
            var last = Enumerable.Last<string>(parts);
            var first = Enumerable.First<string>(parts);
            foreach (var p in parts)
            {
                if (p.Equals(first))
                {
                    newId += p + "_";
                }
                else if (p.Equals(last))
                {
                    newId += "B" + invalidQMId;
                }
                else
                {
                    newId += p + "_";
                }
            }

            _SubmissionUnderwriterContext.Id = newId;
            _SubmissionUnderwriterContext.SubmissionUnderwriterId = "B" + invalidQMId;
        }


        [Given(@"store the submission underwriter id by finding the submission reference quoted")]
        public void FindTheSubmissionUnderwriterByTheSubmissionReferenceQuoted()
        {
            foreach(var qm in _submissionUnderwriterCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qm.SubmissionReference == submissionReferenceQuoted)
                {
                    _SubmissionUnderwriterContext = qm;
                    break;
                }
            }

            Then("I set the SubmissionUnderwriterId");
        }

        [Then(@"validate that the submission underwriter is NOT included in the collection")]
        [Then(@"searching by the SubmissionReference validate that the submission underwriter is NOT included in the collection")]
        public void ThenValidateThatTheSubmissionUnderwriterIsNOTIncludedInTheCollection()
        {
            var found = false;

            foreach (var qmd in _submissionUnderwriterCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qmd.SubmissionReference == submissionReferenceQuoted)
                {
                    found = true;
                    break;
                }
            }

            Assert.IsFalse(found);
        }

        [Given(@"store the submission underwriter id by submission reference related")]
        public void GivenIFindTheSubmissionDialogueIdByTheSubmissionReferenceQuoted()
        {
            foreach (var qm in _submissionUnderwriterCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qm.SubmissionReference == submissionReferenceQuoted)
                {
                    _SubmissionUnderwriterContext = qm;
                    break;
                }
            }

            Then("I set the SubmissionUnderwriterId");
        }

        [Then(@"validate submission underwriter context matching the posted submission underwriter")]
        public void ValidateSubmissionUnderwriterContextMatchingThePostedSubmissionUnderwriter()
        {
            var returnedQMD = _SubmissionUnderwriterContext;
            var currentSubmissionUw = _stateHolder.CurrentSubmissionUnderwriters.FirstOrDefault();

            // validate against posted submission
            ThenValidateThatTheSubmissionUnderwritersContextCreatedWillBeMatchingThePOSTModel();

            //validate against posted qm
            AssertSubmissionUnderwriter(currentSubmissionUw);
        }

        [Then(@"collection of submission underwriters should have a total greater than (.*)")]
        public void ThenCollectionOfSubmissionsShouldHaveATotalGreaterThan(int total)
        {
            if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission underwriter collection data is empty.");
            _submissionUnderwriterCollectionContext.Total.Should().BeGreaterThan(total, "The expected and actual totals do not match");
        }      

        private void AssertSubmissionUnderwriter(SubmissionUnderwriter current)
        {
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, current.SubmissionVersionNumber);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionReference, current.SubmissionReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionDescription, current.SubmissionVersionDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, current.SubmissionVersionNumber);
            Assert.AreEqual(_SubmissionUnderwriterContext.BrokerUserEmailAddress, current.BrokerUserEmailAddress);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeId, current.ProgrammeId);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeReference, current.ProgrammeReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.ProgrammeDescription, current.ProgrammeDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractReference, current.ContractReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractDescription, current.ContractDescription);
            Assert.AreEqual(_SubmissionUnderwriterContext.ContractTypeCode, current.ContractTypeCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.InsuredOrReinsured, current.InsuredOrReinsured);
            Assert.AreEqual(_SubmissionUnderwriterContext.OriginalPolicyholder, current.OriginalPolicyholder);
            Assert.AreEqual(_SubmissionUnderwriterContext.CoverTypeCode, current.CoverTypeCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.ClassOfBusinessCode, current.ClassOfBusinessCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.RiskRegionCode, current.RiskRegionCode);
            Assert.AreEqual(_SubmissionUnderwriterContext.BrokerOrganisationName, current.BrokerOrganisationName);
            Assert.AreEqual(_SubmissionUnderwriterContext.UnderwriterOrganisationId, current.UnderwriterOrganisationId);
            Assert.AreEqual(_SubmissionUnderwriterContext.UnderwriterOrganisationName, current.UnderwriterOrganisationName);
            Assert.AreEqual(_SubmissionUnderwriterContext.UnderwriterUserEmailAddress, current.UnderwriterUserEmailAddress);
            Assert.AreEqual(_SubmissionUnderwriterContext.CommunicationsMethodCode, current.CommunicationsMethodCode);

            if (_SubmissionUnderwriterContext.PeriodOfCover.InsuranceDuration != null)
            {
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsuranceDuration.DurationNumber, _submissionContext.PeriodOfCover.InsuranceDuration.DurationNumber);
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsuranceDuration.DurationUnit, _submissionContext.PeriodOfCover.InsuranceDuration.DurationUnit);
            }

            if (_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod != null)
            {
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.ExpiryDate, _submissionContext.PeriodOfCover.InsurancePeriod.ExpiryDate);
                Assert.AreEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.InceptionDate, _submissionContext.PeriodOfCover.InsurancePeriod.InceptionDate);

                Assert.AreNotEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.ExpiryDate, DateTime.MinValue);
                Assert.AreNotEqual(_SubmissionUnderwriterContext.PeriodOfCover.InsurancePeriod.InceptionDate, DateTime.MinValue);
            }

            Assert.AreNotEqual(_SubmissionUnderwriterContext.CreatedDateTime, DateTime.MinValue);
        }

 		public void SubmissionUnderwriterIdExistsForRelatedSubmissionWhenQMD(string submissionUnderwriterId)
        {
            CommonSteps.WhenIMakeRequestToResource(RestSharp.Method.GET, $"/SubmissionUnderwriters/{submissionUnderwriterId}");
            GivenIStoreTheSubmissionUnderwriter();

            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionUnderwriterId, submissionUnderwriterId);

            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionReference, _submissionDialogueContext.SubmissionReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, _submissionDialogueContext.SubmissionVersionNumber);
        }
    }
}

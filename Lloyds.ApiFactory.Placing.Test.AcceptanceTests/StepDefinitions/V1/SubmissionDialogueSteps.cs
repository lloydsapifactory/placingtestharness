//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using AutoMapper;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionDialogueSteps : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private FeatureContext _featureContext;
        private SubmissionDocumentsSteps _submissionDocumentSteps;

        public SubmissionDialogueSteps(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
            : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
            _featureContext = featureContext;
            _submissionDocumentSteps = new SubmissionDocumentsSteps(_objectContainer, _apiTestContext, sContext, fContext, _stateHolder);
        }

        [Given(@"add the submission dialogue request")]
        [Then(@"add the submission dialogue request")]
        [When(@"add the submission dialogue request")]
        public void AddSubmissionDialogueRequest()
        {
            var submissionrequest = JsonConvert.SerializeObject(_submissionDialogueContext,
                new JsonSerializerSettings() {ContractResolver = new NullToEmptyStringResolver()});

            _apiTestContext.AddParameterToRequestBody("submissionDialogue", submissionrequest);
            _stateHolder.JsonRequestBody = submissionrequest;

            And("I set Content Type header equal to application/json type");
        }

        //[Given(@"select the current submission dialogue")]
        //public void GivenSelectSubmissionDialogueWithSenderNote()
        //{
        //    _submissionDialogueContext = (SubmissionDialogue)_featureContext["SubmissionDialogueForScenario"];
        //}

        [Given(@"add the submission dialogue request adding the field")]
        public void AddSubmissionDialogueRequestAddingTheField()
        {
            // mapping the field into an empty string included in the json
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SubmissionDialogue, SubmissionDialogueViewModel>();
            });

            IMapper iMapper = config.CreateMapper();
            var destination = iMapper.Map<SubmissionDialogue, SubmissionDialogueViewModel>(_submissionDialogueContext);

            var submissionrequest = JsonConvert.SerializeObject(destination,
                new JsonSerializerSettings() {ContractResolver = new NullToEmptyStringResolver()});

            _apiTestContext.AddParameterToRequestBody("submissionDialogue", submissionrequest);
            _stateHolder.JsonRequestBody = submissionrequest;

            And("I set Content Type header equal to application/json type");
        }

        [Given(@"I save the current submission dialogue")]
        [Given(@"the current submission dialogue is saved for the scenario")]
        [When(@"the current submission dialogue is saved for the scenario")]
        [Then(@"the current submission dialogue is saved for the scenario")]
        public void SubmissionDialogueIsSavedForTheScenario()
        {
            _stateHolder.CurrentSubmissionDialogue = _submissionDialogueContext;
        }

        [Given(@"set the SubmissionUnderwriterId")]
        [Then(@"set the SubmissionUnderwriterId")]
        public void GivenSetTheSubmissionUnderwriterId()
        {
            _submissionDialogueContext.SubmissionUnderwriterId = _submissionUnderwriterCollectionContext.Items.First().SubmissionUnderwriterId;
        }

        [Given(
            @"the submission dialogue is saved for the scenario - updating json request value of '(.*)' with '(.*)'")]
        public void SubmissionDialogueIsSavedForTheScenario(string field, string valueToReplace)
        {
            var submissionrequest = JsonConvert.SerializeObject(_submissionDialogueContext, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            var updatedJsonRequest = JSONExtension.UpdateJsonRequestField(submissionrequest, field, valueToReplace, _stateHolder);

            _apiTestContext.AddParameterToRequestBody("submissionDialogue", updatedJsonRequest);
            _stateHolder.JsonRequestBody = updatedJsonRequest;

            And("I set Content Type header equal to application/json type");

            _stateHolder.CurrentSubmissionDialogue = _submissionDialogueContext;
        }

        [Given(@"I set the SubmissionDialogue context (.*) for (.*)")]
        [When(@"I set the SubmissionDialogue context (.*) for (.*)")]
        [Then(@"I set the SubmissionDialogue context (.*) for (.*)")]
        public void SetTheSubmissionMarketUnderwriter(bool withSubmission, string role)
        {
            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            _submissionDialogueContext.SenderDocumentIds = new List<string>();
            _submissionDialogueContext.SenderParticipantCode = null;
            _submissionDialogueContext.SenderUserEmailAddress = string.Empty;
            _submissionDialogueContext.SenderUserFullName = string.Empty;

            if (role == "underwriter")
            {
                if (withSubmission)
                    StoreTheSubmissionUnderwriterId();
            }
            else
                _submissionDialogueContext.SubmissionUnderwriterId = _SubmissionUnderwriterContext.SubmissionUnderwriterId;
        }

        [Given(@"I set the SubmissionDialogue context for (.*)")]
        [When(@"I set the SubmissionDialogue context for (.*)")]
        [Then(@"I set the SubmissionDialogue context for (.*)")]
        public void SetTheSubmissionUnderwriterIdForSubmissionDialogue(string role)
        {
            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            if (role == "broker")
            {
                _submissionDialogueContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber.Value;
                _submissionDialogueContext.SubmissionUnderwriterId = _SubmissionUnderwriterContext.SubmissionUnderwriterId;
                _submissionDialogueContext.SubmissionReference = _submissionContext.SubmissionReference;
                _submissionDialogueContext.SenderDocumentIds = _stateHolder.CurrentSubmissionDocuments.Select(x => x.DocumentId).ToList();
            }

            if (role == "underwriter")
            {
                _submissionDialogueContext.SenderDocumentIds = new List<string>();
                _submissionDialogueContext.SenderParticipantCode = ParticipantRole.U;
                _submissionDialogueContext.SenderUserEmailAddress = _stateHolder.LoginUser;
                _submissionDialogueContext.SenderUserFullName = string.Empty;

                Assert.IsNotNull(_submissionDialogueContext.SubmissionVersionNumber);
                Assert.IsNotNull(_submissionDialogueContext.SubmissionReference);
            }
        }

        [Given(@"I store the submission underwriter id")]
        public void StoreTheSubmissionUnderwriterId()
        {
            GetQMBySubmissionReferenceAndQVN(_stateHolder.UnderwriterUserEmailAddress);

            var qm = _submissionUnderwriterCollectionContext.Items.FirstOrDefault();
            _submissionDialogueContext.SubmissionUnderwriterId = qm.SubmissionUnderwriterId;
        }


        #region SubmissionDocumentswithQMD

        [Given(@"documents uploaded")]
        [When(@"documents uploaded")]
        [Then(@"documents uploaded")]
        public void GivenDocumentsUploaded(Table table)
        {
            GetSubmissionDocumentsBySubmissionReferenceAs(_stateHolder.UnderwriterUserEmailAddress, _submissionDialogueContext.SubmissionReference);

            List<string> documentNames = new List<string>();
            for (var i = 0; i < table.Rows.Count(); i++)
            {
                var document = table.Rows[i]["documentname"].ToString();
                documentNames.Add(document);
            }

            SubmissionDialogueExtension.ValidateDocumentUploadAndAttached(_submissionDialogueContext,
                _submissionDocumentCollectionContext.Items,
                documentNames);
        }

        [Given(@"validate submission documents attached")]
        [When(@"validate submission documents attached")]
        [Then(@"validate submission documents attached")]
        public void ValidateSubmissionDocumentsAttached()
        {
            SubmissionDialogueExtension.ValidateSubmissionDocumentAttached(_submissionDialogueContext, _stateHolder);
        }

        [Then(@"validate submission documents attached are the following")]
        public void ValidateSubmissionDocumentsAttachedAreTheFollowing(Table table)
        {
            // number of documents match
            Assert.AreEqual(_submissionDialogueContext.SenderDocumentIds.Count, table.Rows.Count());

            for (var i = 0; i < table.Rows.Count(); i++)
            {
                var file = SubmissionDialogueExtension.FindFileInSubmissionDocuments(_stateHolder, table.Rows[i]["FileName"].ToString());
                var documentId = file.DocumentId;

                var documentExists =
                    _submissionDialogueContext.SenderDocumentIds.Any(x =>
                        x.Equals(documentId, StringComparison.OrdinalIgnoreCase));

                Assert.IsTrue(documentExists,
                    $"The document {table.Rows[i]["FileName"]} doesn't seem to be attached to the submission dialogue.");
            }
        }

        [Given(@"provide the same documents")]
        public void SubmissionDialogue_SavesToCurrentSenderDocumentIds()
        {
            Assert.True(_submissionDialogueContext.SenderDocumentIds.Count() > 0,
                "cannot find SenderDocumentIds");

            _stateHolder.CurrentSenderDocumentIds = _submissionDialogueContext.SenderDocumentIds;
        }

        [Given(@"provide broker initial submission documents")]
        public void ProvideBrokerSubmissionDocumentsFromStateholder()
        {
            Assert.True(_stateHolder.CurrentSenderDocumentIds.Count() > 0,
                "cannot find stored DocumentIds that have been posted by the Broker.");

            _submissionDialogueContext.SenderDocumentIds = _stateHolder.CurrentSenderDocumentIds;
        }

        [Given(@"attach the initial submission dialogue SenderDocments")]
        public void GivenAttachTheInitialSubmissionDialogueSenderDocments()
        {
            GetQMDsBySubmissionReferenceAndQVNAs(_submissionDialogueContext.SenderUserEmailAddress, "broker");

            var qmdBroker = _submissionDialogueCollectionContext.Items.FirstOrDefault();
            _stateHolder.CurrentSenderDocumentIds = qmdBroker.SenderDocumentIds;
        }

        [Given(@"provide the same documents for SenderDocumentIds")]
        public void ForTheSubmissionDialogueContextAsUnderwriterTheDocsAreTheSame()
        {
            Assert.True(_stateHolder.CurrentSenderDocumentIds.Count() > 0,
                "cannot find SenderDocumentIds - have not been saved");
            _submissionDialogueContext.SenderDocumentIds = _stateHolder.CurrentSenderDocumentIds;
        }

        [Given(@"I set the SubmissionDialogue context with the following documents for (.*)")]
        [When(@"I set the SubmissionDialogue context with the following documents for (.*)")]
        [Then(@"I set the SubmissionDialogue context with the following documents for (.*)")]
        [Given(@"replace the SubmissionDialogue context with the following documents for (.*)")]
        [When(@"replace the SubmissionDialogue context with the following documents for (.*)")]
        [Then(@"replace the SubmissionDialogue context with the following documents for (.*)")]
        public void SetTheSubmissionDialogueWithDocuments(string role, Table table)
        {
            var documentIds = new List<string>();
            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            _submissionDialogueContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber.Value;
            _submissionDialogueContext.SubmissionReference = _submissionContext.SubmissionReference;

            if (role == "broker")
            {
                for (var i = 0; i < table.Rows.Count(); i++)
                {
                    var document = SubmissionDialogueExtension.FindFileInSubmissionDocuments(_stateHolder, table.Rows[i]["FileName"].ToString());
                    documentIds.Add(document.DocumentId);
                }

                _submissionDialogueContext.SenderDocumentIds = documentIds;
                _stateHolder.CurrentSenderDocumentIds = documentIds;
                _submissionDialogueContext.SubmissionUnderwriterId = _SubmissionUnderwriterContext.SubmissionUnderwriterId;
            }

            if (role == "underwriter")
            {
                var qdSteps = new SubmissionDocumentsSteps(_objectContainer, _apiTestContext, sContext, fContext, _stateHolder);
                foreach (var docToReplaceRow in table.Rows)
                {
                    var documentToReplace = SubmissionDialogueExtension.FindFileInSubmissionDocuments(_stateHolder, docToReplaceRow["FileName"].ToString());

                    if (documentToReplace is null) throw new AssertionException("");

                    documentToReplace.ShareableToAllMarketsFlag = false;

                    var replaceSubmissionDoc = SubmissionDialogueExtension.SetSubmissionDocumentModel(documentToReplace,
                        docToReplaceRow);

                    replaceSubmissionDoc.ShareableToAllMarketsFlag = false;
                    if (docToReplaceRow.ContainsKey("ShareableToAllMarketsFlag"))
                    {   replaceSubmissionDoc.ShareableToAllMarketsFlag = (docToReplaceRow["ShareableToAllMarketsFlag"] ?? "False").ToString().Equals("False", StringComparison.OrdinalIgnoreCase) ? false : true;
                    }

                    qdSteps.PostSubmissionDocument(replaceSubmissionDoc);
                    qdSteps.RefreshTheSubmissioncontext();

                    SubmissionDocument submissionDocumentFound = null;
                    foreach (SubmissionDocument submissionDoc in _stateHolder.CurrentSubmissionDocuments)
                    {
                        if (submissionDoc.ReplacingDocumentId != null)
                        {
                            if (submissionDoc.ReplacingDocumentId.Equals(documentToReplace.DocumentId,
                                StringComparison.OrdinalIgnoreCase))
                                submissionDocumentFound = submissionDoc;
                        }
                    }

                    if (submissionDocumentFound != null)
                    {
                        var documentId = submissionDocumentFound.DocumentId;
                        _submissionDialogueContext.SenderDocumentIds.Add(documentId);
                        _stateHolder.CurrentSenderDocumentIds = _submissionDialogueContext.SenderDocumentIds;
                    }
                    else
                        Assert.Fail("Couldnt find submission document to replace.");
                }
            }
        }


        [Given(@"set for replace the SubmissionDialogue context with the following documents")]
        [When(@"set for replace the SubmissionDialogue context with the following documents")]
        [Then(@"set for replace the SubmissionDialogue context with the following documents")]
        public void SetForReplaceTheSubmissionDialogueWithDocuments(Table table)
        {
            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            foreach (var docToReplaceRow in table.Rows)
            {
                var documentToReplace = SubmissionDialogueExtension.FindFileInSubmissionDocuments(_stateHolder, docToReplaceRow["FileName"].ToString());

                if (documentToReplace is null) throw new AssertionException("");

                var replaceSubmissionDoc = SubmissionDialogueExtension.SetSubmissionDocumentModel(documentToReplace,
                    docToReplaceRow);

                replaceSubmissionDoc.ShareableToAllMarketsFlag = false;

                if (docToReplaceRow.ContainsKey("DocumentType"))
                    replaceSubmissionDoc.DocumentType = docToReplaceRow["DocumentType"].ToString();

                _submissionDocumentSteps.PrepareSubmissionDocument(replaceSubmissionDoc);
            }
        }

        [Given(@"add to the SubmissionDialogue context previous random submission documents")]
        [When(@"add to the SubmissionDialogue context previous random submission documents")]
        [Then(@"add to the SubmissionDialogue context previous random submission documents")]
        public void SubmissiontDialogueWithPreviousDocuments(string role)
        {
            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            _submissionDialogueContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber.Value;
            _submissionDialogueContext.SubmissionReference = _submissionContext.SubmissionReference;

            GetRandomSubmissionwithSubmissionDocumentAs(_stateHolder.BrokerUserEmailAddress);

            _submissionDialogueContext.SenderDocumentIds.Add(_submissionDocumentCollectionContext.Items.FirstOrDefault().DocumentId);
            _stateHolder.CurrentSenderDocumentIds = _submissionDialogueContext.SenderDocumentIds;
        }

        [Given(@"there are no document ids that will be identified as a placing slip")]
        public void GivenThereIsNoDocumentIdThatWillBeIdentifiedAsAPlacementSlip()
        {
            var qmdDocumentIds = _submissionDialogueContext.SenderDocumentIds;

            // iterate through the submission documents added and find the placing slip one
            foreach (var qd in _stateHolder.CurrentSubmissionDocuments)
            {
                if (qd.DocumentType == "document_placing_slip")
                {
                    var documentIdPlacingSlip = qd.DocumentId;

                    //remove from the list
                    var documentIdToRemove = qmdDocumentIds.FirstOrDefault(r => r == documentIdPlacingSlip);

                    if (documentIdToRemove == null)
                        Assert.Fail("Couldn't find the document Id of the placing slip from the list");

                    qmdDocumentIds.Remove(documentIdToRemove);
                }
            }
        }
        [Given(@"duplicate sender document ids")]
        public void DuplicateSenderDocumentIds()
        {
            _submissionDialogueContext.SenderDocumentIds = _submissionDialogueContext.SenderDocumentIds.Concat(_submissionDialogueContext.SenderDocumentIds).ToList();
        }

        [Given(@"attach (.*) document from the current sender documents as (.*)")]
        public void GivenAttachSupportingDocumentFromTheCurrentSenderDocuments(string documentType, 
            string role)
        {
            var allCurrentDocuments = role == "underwriter" ?
                _stateHolder.CurrentSubmissionDocuments.Where(x => x.SuppliedBy == ParticipantRole.U).ToList()
                :
                _stateHolder.CurrentSubmissionDocuments.Where(x => x.SuppliedBy == ParticipantRole.B).ToList();

            foreach (var document in allCurrentDocuments)
            {
                if (!_submissionDialogueContext.SenderDocumentIds.Any(x=> x == document.DocumentType))
                {
                    if (document.DocumentType == "document_placing_slip" && documentType == "placing slip")
                    {
                        _submissionDialogueContext.SenderDocumentIds.Add(document.DocumentId);
                        break;
                    }
                    else if (document.DocumentType != "document_placing_slip" && documentType != "placing slip")
                    {
                        _submissionDialogueContext.SenderDocumentIds.Add(document.DocumentId);
                        break;
                    }


                    }
            }
        }


        [Given(@"a supporting document referenced does not exist for the related SubmissionDocument")]
        public void GivenASupportingDocumentReferencedDoesNotExistForTheRelatedSubmissionDocument()
        {
            // adding an id to the document ids that does not exist in the existing submission documents
            Guid g = Guid.NewGuid();
            _submissionDialogueContext.SenderDocumentIds.Add(g.ToString());
        }

        [Given(@"a supporting document referenced is not referenced for the related Submission")]
        public void GivenASupportingDocumentReferencedIsNotReferencedForTheRelatedSubmission()
        {
            // remove one document and find another document id and link it with the existing submission documents 
            if (_submissionDialogueContext.SenderDocumentIds.Count > 1)
                _submissionDialogueContext.SenderDocumentIds.RemoveAt(1);

            for (int i = 0; i < 30; i++) // loop until we get a submission with submission documents included
            {
                GetSubmissionDocumentsForTheSameSubmission();

                if (_submissionDocumentCollectionContext.Items.Length > 0)
                {
                    var randomQDId = _submissionDocumentCollectionContext.Items.FirstOrDefault().DocumentId;
                    _submissionDialogueContext.SenderDocumentIds.Add(randomQDId);
                    break;
                }
            }
        }

        [Given(@"there are more than one document ids that will be identified as a placement slip")]
        public void GivenThereAreMoreThanOneDocumentIdsThatWillBeIdentifiedAsAPlacementSlip()
        {
            var randomQDIdPlacingSlip = _stateHolder.CurrentSubmissionDocuments.
                FirstOrDefault(x => x.DocumentType == "document_placing_slip").DocumentId;

            _submissionDialogueContext.SenderDocumentIds.Add(randomQDIdPlacingSlip);
        }

        [Then(
            @"for the collection of submission dialogues validate document ids matching the correct submission documents")]
        public void ValidateSubmissionDialogueDocuments()
        {
            //for a number of 3 random ones, otherwise it will take too much time
            var testCount = 3;
            var random = new Random();
            var total = _submissionDialogueCollectionContext.Items.Count();

            for (var i = 0; i < total; i++)
            {
                if (testCount < 0)
                    break;

                int index = random.Next(total);
                var qmd = _submissionDialogueCollectionContext.Items[index];
                var submissionDocumentsIdsReturned = qmd.SenderDocumentIds;

                if (submissionDocumentsIdsReturned != null)
                {
                    foreach (var submissionDocumentId in submissionDocumentsIdsReturned)
                    {
                        var submissionDocumentIdReference =
                            $"{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{submissionDocumentId}";
                        GetSubmissionDocumentsBySubmissionReference(submissionDocumentIdReference);
                    }
                }

                testCount--;
            }
        }

        #endregion

        [Given(@"I refresh the SubmissionDialogeContext with the response content")]
        [When(@"I refresh the SubmissionDialogeContext with the response content")]
        [Then(@"I refresh the SubmissionDialogeContext with the response content")]
        public void RefreshTheSubmissionDialoguecontext()
        {
            StepsExension.ValidateOkCreatedResponse(_apiTestContext.Response);

            _submissionDialogueContext =
                JsonConvert.DeserializeObject<SubmissionDialogue>(_apiTestContext.Response.Content);
        }

        [Given(@"I refresh the SubmissionDialogeContext with the first response content")]
        [When(@"I refresh the SubmissionDialogeContext with the first response content")]
        [Then(@"I refresh the SubmissionDialogeContext with the first response content")]
        public void RefreshSubmisionDialogueWithFisrResponses()
        {
            var latestSubmissionDialogue = JsonConvert.DeserializeObject<SubmissionDialogue>(_apiTestContext.Response.Content);

            _submissionDialogueContext = _stateHolder.CurrentSubmisionDialogues.First(x => !x.Id.Equals(latestSubmissionDialogue.Id,
                StringComparison.OrdinalIgnoreCase));
        }

        [Given(@"store the current submission dialogue")]
        public void GivenStoreTheCurrentSubmissionDialogue()
        {
            _stateHolder.CurrentSubmisionDialogues.Add(_submissionDialogueContext);
        }

        [Given(@"I set the SubmissionDialogueId")]
        [When(@"I set the SubmissionDialogueId")]
        [Then(@"I set the SubmissionDialogueId")]
        public void SetTheSubmissionDialogueId()
        {
            if (!sContext.ContainsKey(Constants.SubmissionDialogueId))
                sContext.Add(Constants.SubmissionDialogueId, "");

            sContext[Constants.SubmissionDialogueId] = _submissionDialogueContext != null
                ? _submissionDialogueContext.Id
                : _stateHolder.CurrentSubmissionDialogue.Id;
        }

        [Given(@"I set the SubmissionDialogueId on the contexts")]
        public void GivenISetTheSubmissionDialogueIdOnTheContexts()
        {
            _submissionDialogueContext.SubmissionDialogueId = sContext[Constants.SubmissionDialogueId].ToString();
        }


        [Then(@"collection of submission dialogue include all broker department resources for (.*)")]
        public void CollectionOfSubmissionDialogueIncludeAllBrokerDepartmentResources(string email)
        {
            GetBrokerDepartmentAs(email);

            var brokerDepartmentNames = _brokerDepartmentContext.Items.Select(x => x.BrokerDepartmentName);

            var brokerQMDItems = _submissionDialogueCollectionContext.Items
                .Where(x => x.SenderParticipantCode == ParticipantRole.B)
                .ToList(); //based on L2 behavioural Rules condition

            //for a number of 3 random ones
            var testCount = 3;
            var random = new Random();
            var total = brokerQMDItems.Count();

            for (var i = 0; i < total; i++)
            {
                if (testCount < 0)
                    break;

                int index = random.Next(total);
                var qmd = brokerQMDItems[index];
                var id = $"{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{qmd.SubmissionUnderwriterId}";
                GetSubmissionUnderwriterById(id);

                var brokerDepartmentName = _SubmissionUnderwriterContext.BrokerDepartmentName;

                // L2 behavioural Rules Outcomes 
                Assert.IsTrue(brokerDepartmentNames.Any(x => x.Equals(brokerDepartmentName,
                    StringComparison.OrdinalIgnoreCase)));
                Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionReference, qmd.SubmissionReference);
                Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, qmd.SubmissionVersionNumber);
                testCount--;
            }
        }

        [Then(@"collection of submission dialogues belong to (.*)")]
        public void CollectionOfSubmissionDialogueAsUnderwriterBelongTo(string email)
        {
            // random one
            var random = new Random();
            var total = _submissionDialogueCollectionContext.Items.Count();

            int index = random.Next(total);
            var qmd = _submissionDialogueCollectionContext.Items[index];
            var qmdId = $"{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{qmd.SubmissionUnderwriterId}";

            ValidateSubmissionUnderwriterByIdWithOrganisation(qmdId);
        }

        [Then(@"context of submission dialogue includes all broker department resources for (.*)")]
        public void ContextOfSubmissionDialogueIncludeAllBrokerDepartmentResources(string email)
        {
            GetBrokerDepartmentAs(email);

            var brokerDepartmentNames = _brokerDepartmentContext.Items.Select(x => x.BrokerDepartmentName);

            var qm = _SubmissionUnderwriterContext;

            GetSubmissionUnderwriterById(qm.Id);

            var QMBrokerDepartmentName = _SubmissionUnderwriterContext.BrokerDepartmentName;

            // L2 behavioural Rules Outcomes 
            Assert.IsTrue(brokerDepartmentNames.Any(x => x.Equals(QMBrokerDepartmentName,
                StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionReference, qm.SubmissionReference);
            Assert.AreEqual(_SubmissionUnderwriterContext.SubmissionVersionNumber, qm.SubmissionVersionNumber);
        }


        [Then(@"submission dialogue context belong to (.*)")]
        public void SubmissionDialogueContextAsUnderwriterBelongTo(string email)
        {
            var qmd = _submissionDialogueContext;
            var qmId = $"{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{qmd.SubmissionUnderwriterId}";

            GetSubmissionUnderwriterById(qmId);
            var underwriterOrganisationId = _SubmissionUnderwriterContext.UnderwriterOrganisationId;
            ValidateSubmissionUnderwritersWithOrganisationId(underwriterOrganisationId);
        }

        [Then(@"collection of submission dialogue include all NON DRAFT underwriter resources")]
        public void CollectionOfSubmissionDialogueIncludeAllNONDRAFTUnderwriterResourcesFor()
        {
            var underwriterQMDItems = _submissionDialogueCollectionContext.Items
                .Where(x => x.SenderParticipantCode ==
                            ParticipantRole.U); //based on L2 behavioural Rules condition        }

            // validate they are NON draft
            foreach (var qmd in underwriterQMDItems)
            {
                Assert.IsFalse(qmd.IsDraftFlag);
            }
        }

        [Given(@"I store the submission dialogue response in the context")]
        [When(@"I store the submission dialogue response in the context")]
        [Then(@"I store the submission dialogue response in the context")]
        public void StoreSubmissionDialogue()
        {
            _submissionDialogueContext =
                JsonConvert.DeserializeObject<SubmissionDialogue>(_apiTestContext.Response.Content);
            if (_submissionDialogueContext is null)
                throw new AssertionException("The submission dialogue is empty.");

            Assert.IsTrue(_submissionDialogueContext is SubmissionDialogue);
        }
    
        [Given(@"validate new submission dialogue created")]
        [Then(@"validate new submission dialogue created")]
        public void ValidateNewSubmissionDialogueCreated()
        {
            GetSubmissionDialoguesById(_submissionDialogueContext.Id); // GetById for the new QMD returned

            // assert that QMDId will be different from the current posted one
            Assert.IsFalse((bool)_stateHolder.CurrentSubmissionDialogue.Id.Equals(_submissionDialogueContext.Id, StringComparison.OrdinalIgnoreCase));
        }

        [Then(@"validate submission dialogue SenderUserEmailAddress with the requester")]
        public void ValidateSubmissionDialogueWithTheRequester()
        {
            // SenderUserEmailAddress matches the requester email address
            Assert.IsTrue(_submissionDialogueContext.SenderUserEmailAddress
                .Equals(_stateHolder.UnderwriterUserEmailAddress,
                StringComparison.OrdinalIgnoreCase));
        }

        [Then(@"validate submission dialogue context for the (.*)")]
        public void ValidateThatTheSubmissionDialogueContextForTheBroker(string role)
        {
            SubmissionDialogueExtension.ValidateMandatoryFields(_submissionDialogueContext);

            if (role == "broker")
                SubmissionDialogueExtension.ValidateDateTimeAndStatusCode(_submissionDialogueContext);
        }

        [Then(@"validate mandatory fields for the collection of submission dialogue as a (.*)")]
        public void ValidateMandatoryFieldsForTheCollectionOfSubmissionDialogueAsABroker(string role)
        {
            foreach (var qmd in _submissionDialogueCollectionContext.Items)
                SubmissionDialogueExtension.ValidateMandatoryFields(qmd);
        }

        [Then(@"validate mandatory fields for the context of submission dialogue")]
        public void ThenValidateMandatoryFieldsForTheCollectionOfSubmissionDialogue()
        {
            var qmd = _submissionDialogueContext;
            SubmissionDialogueExtension.ValidateMandatoryFields(qmd);
        }

        [Then(@"validate collection of submission dialogue as a (.*)")]
        public void ValidateCollectionOfSubmissionDialogueAsABroker(string role)
        {
            foreach (var qmd in _submissionDialogueCollectionContext.Items)
                SubmissionDialogueExtension.ValidateDateTimeAndStatusCode(qmd);
        }

        [Given(@"submission underwriter id is set from a different submission underwriter that exists")]
        public void SubmissionUnderwriterIdIsSetFromADifferentSubmissionUnderwriterThatExists()
        {
            // find an existing submission market
            When("I make a GET request to '/SubmissionUnderwriters' resource");
            And("the response is a valid application/json; charset=utf-8 type of submission underwriters collection");

            // replace the SubmissionUnderwriterId with a random submission underwriter
            var rnd = new Random();
            int index = rnd.Next(_submissionUnderwriterCollectionContext.Items.Count());
            var randomQmId = _submissionUnderwriterCollectionContext.Items[index].SubmissionUnderwriterId;

            _submissionDialogueContext.SubmissionUnderwriterId = randomQmId;
        }

        [Given(@"submission is set from a different submission underwriter that exists")]
        public void GivenSubmissionIsSetFromADifferentSubmissionUnderwritersThatExists()
        {
            // find an existing submission underwriter
            Given("I add a 'match' filter on 'SubmissionStatusCode' with a value 'DRFT'");
            When("I make a GET request to '/submissions' resource and append the filter(s)");
            And("the response body should contain a collection of submissions");

            var rnd = new Random();
            int index = rnd.Next(_submissionCollectionContext.Items.Count());
            var randomSubmission = _submissionCollectionContext.Items[index];

            _submissionDialogueContext.SubmissionReference = randomSubmission.SubmissionReference;
            _submissionDialogueContext.SubmissionVersionNumber = randomSubmission.SubmissionVersionNumber.Value;
        }

        [Then(@"validate that the submission dialogues is included in the collection")]
        public void ValidateThatTheSubmissionDialoguesIsIncludedInTheCollection()
        {
            var found = false;

            foreach (var qmd in _submissionDialogueCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qmd.SubmissionReference == submissionReferenceQuoted)
                {
                    found = true;
                    break;
                }
            }

            Assert.IsTrue(found);
        }

        [Then(@"validate submission dialogue posted by the (.*) is included in the submission dialogue collection")]
        public void ValidateSubmissionDialoguePostedIsIncludedInTheSubmissionDialogueCollection(string postedBy)
        {
            var postedQMD = _stateHolder.CurrentSubmissionDialogue;
            var found = false;

            var qmdsToCheck = postedBy == "broker"
                ? _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.B)
                    .ToList()
                : _submissionDialogueCollectionContext.Items.Where(x => x.SenderParticipantCode == ParticipantRole.U)
                    .ToList();

            foreach (var qmd in qmdsToCheck)
            {
                if (postedQMD.SubmissionReference.Equals(qmd.SubmissionReference, StringComparison.OrdinalIgnoreCase))
                {
                    found = true;
                    SubmissionDialogueExtension.AssertSubmissionDialogue(qmd, postedBy, _stateHolder);
                }
            }

            Assert.IsTrue(found);
        }

        [Then(@"validate that the submission dialogues (.*) included in the collection")]
        [Then(@"searching by the SubmissionReference validate that the submission dialogue (.*) included in the collection")]
        public void ValidateThatTheSubmissionDialogueIsNOTIncludedInTheCollection(bool not)
        {
            var found = false;

            foreach (var qmd in _submissionDialogueCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qmd.SubmissionReference == submissionReferenceQuoted)
                {
                    found = true;
                    break;
                }
            }

            //not found
            if (!not)
                Assert.IsFalse(found);

            //found
            if (not)
                Assert.IsTrue(found);
        }

        [Then(@"validate submission dialogue context as an (.*) matching the submission dialogue posted by (.*)")]
        public void ValidateSubmissionDialogueContextMatchingThePostedSubmissionDialogue(string role,
            string postedBy)
        {
            var returnedQMD = _submissionDialogueContext;

            Assert.IsNotNull(returnedQMD, "No submission dialogues found to validate.");

            SubmissionDialogueExtension.ValidateDateTimeAndStatusCode(returnedQMD);
            SubmissionDialogueExtension.ValidateSubmissionDialogueWhenPostedAs(returnedQMD, postedBy, _stateHolder);

            // check posted Submission
            var submission = _stateHolder.CurrentSubmissions.FirstOrDefault();
            Assert.IsTrue(returnedQMD.SubmissionReference.Equals(submission.SubmissionReference, StringComparison.OrdinalIgnoreCase));

            if (submission.SubmissionVersionNumber.HasValue)
                Assert.IsTrue(returnedQMD.SubmissionVersionNumber == submission.SubmissionVersionNumber.Value);
        }

        [Given(@"validate submission dialogue context as an (.*) matching the submission dialogue updated by (.*)")]
        [Then(@"validate submission dialogue context as an (.*) matching the submission dialogue updated by (.*)")]
        public void ValidateSubmissionDialogueContextMatchingTheUpdatedSubmissionDialogue(string role,
            string postedBy)
        {
            var returnedQMD = _submissionDialogueContext;

            Assert.IsNotNull(returnedQMD, "No submission dialogues found to validate.");

            SubmissionDialogueExtension.ValidateDateTimeAndStatusCode(returnedQMD);
            SubmissionDialogueExtension.ValidateSubmissionDialogueWhenPostedAs(returnedQMD, postedBy, _stateHolder);

            // check posted Submission
            var submission = _stateHolder.CurrentSubmissions.FirstOrDefault();
            Assert.IsTrue(returnedQMD.SubmissionReference.Equals(submission.SubmissionReference, StringComparison.OrdinalIgnoreCase));

            if (submission.SubmissionVersionNumber.HasValue)
                Assert.IsTrue(returnedQMD.SubmissionVersionNumber == submission.SubmissionVersionNumber.Value);
        }

        [Given(@"store the submission dialogue id by submission reference related")]
        public void FindTheSubmissionDialogueIdByTheSubmissionReferenceQuoted()
        {
            foreach (var qmd in _submissionDialogueCollectionContext.Items)
            {
                var submissionReferenceQuoted = _submissionContext.SubmissionReference;

                if (qmd.SubmissionReference == submissionReferenceQuoted)
                {
                    _submissionDialogueContext = qmd;
                    break;
                }
            }

            Then("I set the SubmissionDialogueId");
        }

        [Given(@"store the submission from the submission dialogue")]
        public void StoreTheSubmissionReferenceFromTheSubmissionDialogue()
        {
            Assert.IsNotNull(_submissionDialogueContext,
                "SubmissionDialogue is null. Cannot retrieve qoure reference" +
                "and submission version number.");

            var submissionReference = _submissionDialogueContext.SubmissionReference;
            var submissionVersionNumber = _submissionDialogueContext.SubmissionVersionNumber;

            GetSubmissionBySubmissionReferenceAndQVN(submissionReference, submissionVersionNumber.ToString());

            _stateHolder.CurrentSubmissions = new List<Submission>() {_submissionContext};
        }

        [Given(@"store the submission using the submission reference and Submission version")]
        public void StoreTheSubmissionUsingQuotrRefAndVersion()
        {
            GetSubmissionBySubmissionReferenceAndQVN();
            _stateHolder.CurrentSubmissions = new List<Submission>() {_submissionContext};
        }

        [Given(@"store the submission underwriter from the submission dialogue")]
        public void StoreTheSubmissionUnderwritersFromTheSubmissionDialogue()
        {
            var id = "{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{qmd.SubmissionUnderwriterId}";
            GetSubmissionUnderwriterById(id);
        }


        [Then(@"validate SubmissionUnderwriterStatusCode is updated")]
        public void ThenValidateSubmissionUnderwriterStatusCodeIsUpdated()
        {
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;

            GetQMBySubmissionReferenceAndQVN(brokerUserEmailAddress);

            var returnedQM = _submissionUnderwriterCollectionContext.Items.FirstOrDefault();

            // validate that this now has changed
            var submissionUnderwriterStatus = _SubmissionUnderwriterContext.SubmissionUnderwriterStatusCode;

            Assert.AreNotEqual(returnedQM.SubmissionUnderwriterStatusCode, submissionUnderwriterStatus);
        }

        [Then(@"validate Submission Status is updated")]
        public void ValidateSubmissionStatusIsUpdated()
        {
            GetSubmissionBySubmissionReferenceAndQVN();

            Assert.AreNotEqual(_submissionContext.SubmissionStatusCode,
                _stateHolder.CurrentSubmissions.FirstOrDefault().SubmissionStatusCode);
        }

        [Given(@"get the submission dialogues as a (.*) posted by the (.*)")]
        [When(@"get the submission dialogues as a (.*) posted by the (.*)")]
        [Then(@"get the submission dialogues as a (.*) posted by the (.*)")]
        public void RetriveTheSubmissionDialogueAsABroker(string role, string postedBy)
        {
            string userEmailAddress = string.Empty;

            if (role == "broker")
                userEmailAddress = _stateHolder.BrokerUserEmailAddress;

            if (role == "underwriter")
                userEmailAddress = _stateHolder.UnderwriterUserEmailAddress;

            //store QMDs by Submissionreference/SubmissionVersionNumber and based on who posted
            GetQMDsBySubmissionReferenceAndQVNAs(userEmailAddress, postedBy).ToArray();
        }


        [Then(@"validate the submission dialogues are (.*)")]
        public void RetriveTheSubmissionDialogueAsABroker(int count)
        {
            Assert.AreEqual(count, _submissionDialogueCollectionContext.Items.Count());
        }

        [Then(@"store the submission dialogue context from the submission dialogues")]
        public void ThenStoreTheSubmissionDialogueContextFromTheSubmissionDialogues()
        {
            _submissionDialogueContext = _submissionDialogueCollectionContext.Items.FirstOrDefault();
        }

        [Then(@"validate submission dialogue context with the submission dialogue posted by (.*)")]
        public void ValidateThatTheBrokerCanSeeTheSubmissionDialoguePosted(string postedBy)
        {
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;

            _submissionDialogueContext = _submissionDialogueCollectionContext.Items.FirstOrDefault();
            var qmd = _submissionDialogueContext;

            var qmdId = $"{qmd.SubmissionReference}_{qmd.SubmissionVersionNumber}_{qmd.SubmissionDialogueId}";

            Given($"I log in as a broker '{brokerUserEmailAddress}'");
            When($"I make a GET request to '/SubmissionDialogues/{qmdId}' resource");
            StoreSubmissionDialogue();

            //validate against _stateHolder.CurrentSubmissionDialogue
            SubmissionDialogueExtension.ValidateSubmissionDialogueWhenPostedAs(_submissionDialogueContext, postedBy,
                _stateHolder);
        }

        [Then(@"the submission dialogue contains ""(.*)"" SenderDocumentId")]
        public void SubmissionDialogueContainsSenderDocumentId(int p0)
        {
            Assert.AreEqual(_submissionDialogueContext.SenderDocumentIds.Count, p0, "The created submission dialogue contains {0} sender documents, the actual should be {1}", _submissionDialogueContext.SenderDocumentIds.Count, p0);
        }

        [Given(@"Add the SubmissionDialogue context with the following documents for (.*)")]
        [When(@"Add the SubmissionDialogue context with the following documents for (.*)")]
        public void AddTheSubmissionDialogueContextWithTheFollowingDocumentsForUnderwriter(string role, Table table)
        {
            if (role != "underwriter") return;

            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            _submissionDialogueContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber.Value;
            _submissionDialogueContext.SubmissionReference = _submissionContext.SubmissionReference;

            foreach (var docRow in table.Rows)
            {
                var addSubmissionDoc = SubmissionDialogueExtension.SetSubmissionDocumentModel(docRow, _submissionDialogueContext);

                _submissionDocumentSteps.PostSubmissionDocument(addSubmissionDoc);
                _submissionDocumentSteps.RefreshTheSubmissioncontext(); // refreshes the submission document context
                _submissionDialogueContext.SenderDocumentIds.Add(_submissionDocumentContext.DocumentId);
            }

        }

        [Given(@"reset the SenderDocumentIds")]
        public void GivenResetTheSenderDocumentIds()
        {
            _submissionDialogueContext.SenderDocumentIds = new List<string>();
        }


        [Given(@"Set the SubmissionDialogue context with the following documents for (.*)")]
        [When(@"Set the SubmissionDialogue context with the following documents for (.*)")]
        public void SetTheSubmissionDialogueContextWithTheFollowingDocumentsForUnderwriter(string role, Table table)
        {
            if (role != "underwriter") return;

            if (!sContext.ContainsKey(Constants.SubmissionDialogueContext))
                sContext.Add(Constants.SubmissionDialogueContext, "");

            if (_submissionDialogueContext == null)
                _submissionDialogueContext = new SubmissionDialogue();

            foreach (var docRow in table.Rows)
            {

                var hasRepl = docRow.Any(x => x.Key == "ReplacingDocumentId");

                var addSubmissionDoc = SubmissionDialogueExtension.SetSubmissionDocumentModel(docRow, _submissionDialogueContext);

                _submissionDocumentSteps.PrepareSubmissionDocument(addSubmissionDoc);
            }
        }

       
        [Given(@"I pick another random submission dialogue Id")]
        public void PickAnotherRandomSubmissionDialogueId()
        {
            //pick another one in order to receive another tandom QMDId
            And("get random submission dialogue from the submission dialogue collection");
        }

        [Given(@"I set the submission dialogue Id from a different submission dialogue")]
        public void SetTheSubmissionDialogueIdFromADifferentSubmissionDialogue()
        {
            object[] array = SetArrayCollection(EntityEnum.Entity.submission_dialogue);
            var qmd = SubmissionDialogueExtension.ReturnRandomSubmissionDialogue(array);

            _submissionDialogueContext.Id = qmd.Id;
            var parts = _submissionDialogueContext.Id.Split("_");

            _submissionDialogueContext.SubmissionDialogueId = parts[2].ToString();
        }

        [Given(@"POST as a broker to SubmissionDialogues with following info")]
        [When(@"POST as a broker to SubmissionDialogues with following info")]
        public void POSTSubmissionDialogesProcessAsBroker(Table submissionDialogueTable)
        {
            SetTheSubmissionUnderwriterIdForSubmissionDialogue("broker");

            PrepareSubmissionDialogueAsBroker(submissionDialogueTable);

            SubmissionDialogueIsSavedForTheScenario();
            AddSubmissionDialogueRequest();
            SubmissionDialogueIsSavedForTheScenario();

            And("I POST to '/SubmissionDialogues' resource");
        }

        [Given(@"preparing to POST as a broker to SubmissionDialogues with following info")]
        [When(@"preparing to POST as a broker to SubmissionDialogues with following info")]
        public void PrepareSubmissionDialogueAsBroker(Table table)
        {
            var submssionDialogues = table.CreateSet<SubmissionDialogue>();
            var sd = submssionDialogues.First();

            And($"for the submission dialogue the 'SenderActionCode' is '{sd.SenderActionCode}'");
            And($"for the submission dialogue the 'IsDraftFlag' is '{sd.IsDraftFlag}'");
            And($"for the submission dialogue the 'SenderNote' is '{sd.SenderNote}'");
        }

        [Given(@"POST as an underwriter to SubmissionDialogues with following info")]
        [When(@"POST as an underwriter to SubmissionDialogues with following info")]
        public void POSTSubmissionDialogesProcessAsUnderwriter(Table submissionDialogueTable)
        {
            SetTheSubmissionMarketUnderwriter(true, "underwriter");
            PrepareSubmissionDialogueAsUnderwriter(submissionDialogueTable);

            AddSubmissionDialogueRequest();
            SubmissionDialogueIsSavedForTheScenario();

            And("I POST to '/SubmissionDialogues' resource");
        }


        [Given(@"preparing to POST as an underwriter to SubmissionDialogues with following info")]
        [When(@"preparing to POST as an underwriter to SubmissionDialogues with following info")]
        public void PrepareSubmissionDialogueAsUnderwriter(Table table)
        {
            var submssionDialogues = table.CreateSet<SubmissionDialogue>();
            var sd = submssionDialogues.First();

            And($"for the submission dialogue the 'IsDraftFlag' is '{sd.IsDraftFlag}'");
            And($"for the submission dialogue the 'UnderwriterQuoteReference' is '{sd.UnderwriterQuoteReference}' string enhanced");

            if (sd.SenderActionCode == WebApi.Dto.SubmissionDialogueFilterModel.QUOTE)
                And("for the submission dialogue I add a date filter on 'UnderwriterQuoteValidUntilDate' with a datetime value of a year from now");
            else
                And($"for the submission dialogue the 'UnderwriterQuoteValidUntilDate' is ''");

            And($"for the submission dialogue the 'SenderActionCode' is '{sd.SenderActionCode}'");
            And($"for the submission dialogue the 'SenderNote' is '{sd.SenderNote}'");
        }        

        [Given(@"restore feature context")]
        [Given(@"restore submission dialogue information from previous scenario")]
        [When(@"restore submission dialogue information from previous scenario")]
        [Then(@"restore submission dialogue information from previous scenario")]
        public void RestoreSubmissionDialogueInformationToBeUsedNext()
        {
            if (_featureContext.Any())
            {
                if (_featureContext["stateholder"] != null)
                    _stateHolder = (StateHolder)_featureContext["stateholder"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("submissiondialoguecontext",
                    StringComparison.OrdinalIgnoreCase)).Value != null)
                    _submissionDialogueContext = (SubmissionDialogue)_featureContext["submissiondialoguecontext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("submissioncontext",
                     StringComparison.OrdinalIgnoreCase)).Value != null)
                    _submissionContext = (Submission)_featureContext["submissioncontext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("SubmissionUnderwriterContext",
                   StringComparison.OrdinalIgnoreCase)).Value != null)
                    _SubmissionUnderwriterContext = (SubmissionUnderwriter)_featureContext["SubmissionUnderwriterContext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("submissiondocumentcontext",
                    StringComparison.OrdinalIgnoreCase)).Value != null)
                    _submissionDocumentContext = (SubmissionDocument)_featureContext["submissiondocumentcontext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("SubmissionDocumentCollectionContext",
                    StringComparison.OrdinalIgnoreCase)).Value != null)
                    _submissionDocumentCollectionContext = (SubmissionDocumentCollectionWrapper)_featureContext["SubmissionDocumentCollectionContext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("sContext",
                    StringComparison.OrdinalIgnoreCase)).Value != null)
                    sContext = (ScenarioContext)_featureContext["sContext"];

                if (_featureContext.FirstOrDefault(x => x.Key.Equals("submissiondialoguecollectioncontext",
                 StringComparison.OrdinalIgnoreCase)).Value != null)
                    _submissionDialogueCollectionContext = (SubmissionDialogueCollectionWrapper)_featureContext["submissiondialoguecollectioncontext"];

            }
            else
            {
                Assert.Fail("This Scenario cannot be run separately, " +
                    "as it invovles specflow feature dependant prerequisite data. " +
                    "Please run together with Scenario 1.");
            }

        }

        [Given(@"Clear feature context and get ready for the next scenario")]
        [When(@"Clear feature context and get ready for the next scenario")]
        [Then(@"Clear feature context and get ready for the next scenario")]
        public void ClearFeatureContext()
        {
            _featureContext.Clear();
        }

        [Then(@"store submission dialogue information to be used next")]
        [Then(@"store feature context")]
        public void StoreSubmissionDialogueInformationToBeUsedNext()
        {
            if (_featureContext.Any(x => x.Key == "stateholder"))
                _featureContext["stateholder"] = _stateHolder;
            else
                _featureContext.Add("stateholder", _stateHolder);

            if (_featureContext.Any(x => x.Key == "submissiondialoguecontext"))
                _featureContext["submissiondialoguecontext"] = _submissionDialogueContext;
            else
            {
                if (_submissionDialogueContext != null)
                    _featureContext.Add("submissiondialoguecontext", _submissionDialogueContext);
            }

            if (_featureContext.Any(x => x.Key == "submissiondialoguecollectioncontext"))
                _featureContext["submissiondialoguecollectioncontext"] = _submissionDialogueCollectionContext;
            else
            {
                if (sContext.ContainsKey(Constants.SubmissionDialogueCollectionWrapper))
                    _featureContext.Add("submissiondialoguecollectioncontext", _submissionDialogueCollectionContext);
            }

            if (_featureContext.Any(x => x.Key == "submissiondocumentcontext"))
                _featureContext["submissiondocumentcontext"] = _submissionDocumentContext;
            else
            {
                if (_submissionDocumentContext != null)
                    _featureContext.Add("submissiondocumentcontext", _submissionDocumentContext);
            }

            if (_featureContext.Any(x => x.Key == "submissioncontext"))
                _featureContext["submissioncontext"] = _submissionContext;
            else
            {
                if (_submissionContext != null)
                    _featureContext.Add("submissioncontext", _submissionContext);
            }

            if (_featureContext.Any(x => x.Key == "SubmissionUnderwriterContext"))
                _featureContext["SubmissionUnderwriterContext"] = _SubmissionUnderwriterContext;
            else
            {
                if (_SubmissionUnderwriterContext != null)
                    _featureContext.Add("SubmissionUnderwriterContext", _SubmissionUnderwriterContext);
            }

            if (_featureContext.Any(x => x.Key == "SubmissionDocumentCollectionContext"))
                _featureContext["SubmissionDocumentCollectionContext"] = _submissionDocumentCollectionContext;
            else
            {
                if (sContext.ContainsKey(Constants.SubmissionDocumentCollectionWrapper))
                    _featureContext.Add("SubmissionDocumentCollectionContext", _submissionDocumentCollectionContext);
            }

            if (_featureContext.Any(x => x.Key == "sContext"))
                _featureContext["sContext"] = sContext;
            else
            {
                if (sContext != null)
                    _featureContext.Add("sContext", sContext);
            }

            if (_featureContext.Any(x => x.Key == "SubmissionDocumentId"))
                _featureContext["SubmissionDocumentId"] = sContext;
            else
            {
                if (sContext != null)
                    _featureContext.Add("SubmissionDocumentId", sContext);
            }
        }

        [Given(@"set new submission dialogue context")]
        public void GivenSetNewSubmissionDialogueContext()
        {
            _submissionDialogueContext = new SubmissionDialogue();
        }

        [Given(@"get random (.*) from the (.*) collection that doesnt have any QMD in draft")]
        [When(@"get random (.*) from the (.*) collection that doesnt have any QMD in draft")]
        [Then(@"get random (.*) from the (.*) collection that doesnt have any QMD in draft")]
        public void GetARandomFromTheCollectionWithoutDraft(string objectUsed, EntityEnum.Entity entity)
        {
            SubmissionDialogue qmd = null;

            object[] array = SetArrayCollection(entity);

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                for (int i = 0; i < 100; i++)
                {
                    qmd = SubmissionDialogueExtension.ReturnRandomSubmissionDialogue(array);

                    var allQmds = GetQMDsBySubmissionReferenceAndQVNAs(_stateHolder.UnderwriterUserEmailAddress,
                        qmd.SubmissionReference,
                        qmd.SubmissionVersionNumber.ToString());

                    if (!allQmds.Any(x => x.IsDraftFlag == true))
                        break;
                }

                if (qmd != null)
                {
                    _submissionDialogueContext = qmd;
                    _stateHolder.CurrentSubmissionDialogue = qmd;
                    _stateHolder.CurrentSenderDocumentIds = qmd.SenderDocumentIds;
                }
                else
                    Assert.Fail("Couldnt find QMDs for test.");
            }
        }

        [Given(@"get random (.*) from the (.*) collection that does have SenderDocumentIds")]
        [When(@"get random (.*) from the (.*) collection that does have SenderDocumentIds")]
        [Then(@"get random (.*) from the (.*) collection that does have SenderDocumentIds")]
        public void GetARandomFromTheCollectionWithSenderDocumentIds(string objectUsed, EntityEnum.Entity entity)
        {
            SubmissionDialogue qmd = null;

            object[] array = SetArrayCollection(entity);

            if (entity == EntityEnum.Entity.submission_dialogue)
            {
                qmd = SubmissionDialogueExtension.ReturnRandomSubmissionDialogueWithSenderDocIds(array);

                if (qmd != null)
                {
                    _submissionDialogueContext = qmd;
                    _stateHolder.CurrentSubmissionDialogue = qmd;
                    _stateHolder.CurrentSenderDocumentIds = qmd.SenderDocumentIds;
                }
                else
                    Assert.Fail("Couldnt find QMDs for test.");
            }
        }

        [Given(@"I set the submission reference from the submission dialogue")]
        public void GivenISetTheSubmissionReferenceFromTheSubmissionDialogue()
        {
            sContext[Constants.SubmissionReferenceforSubmissionDocument] = _submissionDialogueContext.SubmissionReference;
        }

        [Given(@"I save the documents of the submission dialogue into the current documents")]
        public void GivenISaveTheDocumentsOfTheSubmissionDialogueIntoTheCurrentDocuments()
        {
            _stateHolder.CurrentSubmissionDocuments = _submissionDocumentCollectionContext.Items.ToList();
        }


        [Given(@"I get a random submission document that has not been sent to the underwriter")]
        public void GivenIGetARandomSubmissionDocumentThatHasNotBeenSentToTheUnderwriter()
        {
            if (_submissionDocumentCollectionContext.Items != null)
            {
                var initialSubmissionDocuments = _stateHolder.CurrentSubmissionDocuments;

                if (initialSubmissionDocuments.Count == 0)
                    Assert.Fail("Couldnt find any initial submission documents.");

                foreach (var sd in initialSubmissionDocuments)
                {
                    if (!_stateHolder.CurrentSenderDocumentIds.Any(x => x.Equals(sd.DocumentId,
                        StringComparison.OrdinalIgnoreCase)))
                    {

                        _submissionDocumentContext = sd;
                        _submissionDocumentContext.Id = sd.Id;
                    }
                }

                _submissionDocumentSteps.SetTheSubmissionDocumentId();
                _submissionDocumentSteps.SubmissionDocumentIsSavedForTheRequest();
            }
            else
                Assert.Fail("Couldnt find any submission documents.");
        }

        [Given(@"I set the SubmissionReference from the SubmissionDialogue context")]
        [When(@"I set the SubmissionReference from the SubmissionDialogue context")]
        [Then(@"I set the SubmissionReference from the SubmissionDialogue context")]
        public void SetTheSubmissionReference()
        {
            if (!sContext.ContainsKey(Constants.SubmissionUniqueReference))
                sContext.Add(Constants.SubmissionUniqueReference, "");

            sContext[Constants.SubmissionUniqueReference] = _submissionDialogueContext != null ? _submissionDialogueContext.SubmissionReference : null; ;
        }
    }
}

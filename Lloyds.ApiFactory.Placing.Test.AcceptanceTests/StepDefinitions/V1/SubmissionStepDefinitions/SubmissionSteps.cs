//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System.Linq;
using System.Reflection;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionSteps : ApiBaseSteps
    {
        private readonly User _userContext;

        public SubmissionSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext, ScenarioContext scenarioContext) : base(objectContainer, apiTestContext, scenarioContext)
        {
            _userContext = sContext.ContainsKey(Constants.UserContext) ? sContext[Constants.UserContext] as User : new User();
            _submissionContext = sContext.ContainsKey(Constants.SubmissionContext) ? sContext[Constants.SubmissionContext] as Submission : new Submission();
        }
        public SubmissionSteps(IObjectContainer objectContainer, ApiTestContext apiTestContext) : base(objectContainer, apiTestContext) 
        { 
        }

        [Given(@"I set a json payload (.*)")]
        public void GivenISetAJsonPayloadData_ModelSubmission_SubmissionId__Json(string filePath)
        {
            var submission = FileHelper.LoadSubmissionFromFile(filePath);

            var randomString = StringHelper.GenerateRandomString(6);

            submission.SubmissionVersionDescription = $"{submission.SubmissionVersionDescription}-{randomString}";
            submission.SubmissionReference = $"{submission.SubmissionReference}{randomString}";

            sContext[Constants.SubmissionContext] = submission;

            _apiTestContext.AddContent(submission, MimeEnumType.Json);
        }

        [Then(@"the submission response should contain the '(.*)' set to '(.*)'")]
        public void ThenTheSubmissionResponseShouldContainTheSetTo(string field, string value)
        {
            Given("I refresh the Submissioncontext with the response content");
            PropertyInfo[] propertyInfos = typeof(SubmissionDocument).GetProperties(BindingFlags.Public |
                                                         BindingFlags.Static);

            var valueAssert = GetPropertyValue(_submissionContext, field);

            Assert.True(value.Contains(valueAssert.ToString()));
        }

        private object GetPropertyValue(object c, string propertyName)
        {
            return c.GetType().GetProperties()
               .Single(pi => pi.Name == propertyName)
               .GetValue(c, null);
        }

    }
}

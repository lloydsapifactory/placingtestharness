//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Text.RegularExpressions;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;
using static Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums.EntityEnum;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionPUT : ApiBaseSteps
    {
        private readonly SubmissionStepsIII   _submissionSteps;
        private readonly CommonSteps     _commonSteps;
        private readonly AbstractSteps     _abstractSteps;
        private readonly BrokerDepartment     _brokerDepartmentSteps;
        private StateHolder _stateHolder;

        protected User UserContext
        {
            get => sContext[Constants.UserContext] as User;
            set => sContext[Constants.UserContext] = value;
        }
        public SubmissionPUT(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext,
            scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
            _submissionSteps = new SubmissionStepsIII(objectContainer, apiTestContext, scenarioContext, featureContext, _stateHolder);
            _commonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            _abstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            _brokerDepartmentSteps = new BrokerDepartment(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);

            if (!sContext.ContainsKey(Constants.SubmissionRandomStr)) sContext.Add(Constants.SubmissionRandomStr, string.Empty);
        }

        [Given(@"I POST a submission in the background on behalf of the broker '(.*)'")]
        public void POSTSubmissionOnBehalfOfTheBrokerWithBrokerCodeAndDepartmentid(string brokerEmailAddress)
        {
            try
            {
                var submission = new Submission();

                var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

                PostSubmissionCreate(brokerEmailAddress, submissionDataToInsert);

                _stateHolder.CurrentSubmissions.Add(_submissionContext);
                _stateHolder.CurrentBrokerDepartmentId = submission.BrokerDepartmentId;
            }
            catch (Exception ex)
            {   throw new AssertionException($"An error occured trying to POST a submission : {ex.Message}");
            }
        }
        

        [Given(@"I POST a submission in the background on behalf of the broker '(.*)' retrieved multiple departments")]
        public void POSTSubmissionOnBehalfOfTheBrokerWithBrokerCodeAndDepartmentidForMultipleDepartments(string brokerEmailAddress)
        {
            try
            {
                var submission = new Submission();
                MapBrokerCodeAndDepartment(brokerEmailAddress, submission);

                PostSubmissionCreate(brokerEmailAddress, submission);

                _stateHolder.CurrentSubmissions.Add(_submissionContext);
                _stateHolder.CurrentBrokerDepartmentId = submission.BrokerDepartmentId;
            }
            catch (Exception ex)
            {
                throw new AssertionException($"An error occured trying to POST a submission : {ex.Message}");
            }
        }

        [Given(@"I POST (.*) submissions with different version in the background on behalf of the broker '(.*)'")]
        [When(@"I POST (.*) submissions with different version in the background on behalf of the broker '(.*)'")]
        public void POSTSubmissionsWithDifferentVersionNumber(int count, string brokerUserEmailAddress)
        {
            Given("generate a random string of 5 characters and save it to the context");
            CreateAndPostSubmission(count, brokerUserEmailAddress);
        }

        [Given(@"I POST (.*) more submissions with different version for stored Submission")]
        [When(@"I POST (.*) more submissions with different version for stored Submission")]
        public void POSTSubmissionsWithDifferentVersionNumberForSavedSubmission(int count)
        {
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;
            CreateAndPostSubmission(count, brokerUserEmailAddress);
        }

        [Given(@"I prepare (.*) more submissions with different version for stored Submission")]
        [When(@"I prepare (.*) more submissions with different version for stored Submission")]
        public void POSTSubmissionsWithDifferentVersionNumberForSavedSubmissionButDifferentBroker(int count)
        {
            var storedSubmission = _submissionContext;
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;
            CreateSubmission(count, brokerUserEmailAddress, storedSubmission);
        }



        [Given(@"the submission status will be '(.*)'")]
        [Then(@"the submission status will be '(.*)'")]
        public void TheSubmissionStatusWillBe(string submissionStatus)
        {
            try
            { 
                var submissionresponse = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
                if (!submissionresponse.SubmissionStatusCode.HasValue) throw new Exception("Submission Status Code is missing");

                var qstatus = Enum.Parse(typeof(SubmissionStatus), submissionStatus) ;
                Assert.IsTrue(submissionresponse.SubmissionStatusCode.Value == (SubmissionStatus)qstatus, "The submission status do not match");
            }
            catch (Exception ex)
            {   throw new AssertionException($"An error occured trying to read the submission status : {ex.Message}");
            }

        }

        [Given(@"I construct a PUT")]
        [When(@"I construct a PUT")]
        [Given(@"I store the submission context from the response")]
        [Then(@"I store the submission context from the response")]
        public void ConstructAPUT()
        {
            _submissionContext = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
        }

       

        [Given(@"I store the submission in the context")]
        [When(@"I store the submission in the context")]
        [Then(@"I store the submission in the context")]
        public void StoreTheSubmission()
        {
            _submissionContext = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
        }

        [Given(@"I store the submission in the context with a non existng submission")]
        public void GivenIStoreTheSubmissionInTheContextWithANonExistngSubmission()
        {
            _submissionContext.SubmissionReference = Extension.GenerateRandomString(10);
            _submissionContext.SubmissionVersionNumber = 666;

            _SubmissionUnderwriterContext.SubmissionReference = _submissionContext.SubmissionReference;
            _SubmissionUnderwriterContext.SubmissionVersionNumber = _submissionContext.SubmissionVersionNumber.Value;
        }


        [Given(@"I set the SubmissionUniqueReference")]
        [When(@"I set the SubmissionUniqueReference")]
        [Then(@"I set the SubmissionUniqueReference")]
        public void SetTheSubmissionUniqueReference()
        {
            sContext.Set<string>(_submissionContext.Id, Constants.SubmissionUniqueReference);
        }

        [Given(@"I set a valid but not existing SubmissionUniqueReference")]
        [When(@"I set a valid but not existing SubmissionUniqueReference")]
        public void SetTheSubmissionUniqueReferenceForNonExistingId()
        {
            var malformedId = _submissionContext.Id + "2";
            sContext.Set<string>(malformedId, Constants.SubmissionUniqueReference);
        }

        [Given(@"I set the SubmissionUniqueReference from submission update")]
        public void GivenISetTheSubmissionUniqueReferenceFromSubmissionUpdate()
        {
            sContext.Set<string>(_submissionContext.Id, Constants.SubmissionUniqueReference);
        }

        [Given(@"for the submission update the PeriodofCover (.*)has an Insurance duration where the duration number is '(.*)' and the duration unit is '(.*)'")]
        public void GivenForTheSubmissionUpdateThePeriodofCoverOnlyHasAnInsuranceDurationWhereTheDurationNumberIsAndTheDurationUnitIs(string clear, int durationNumber, PeriodDurationUnit durationUnit)
        {
            var periodofchoice = _submissionContext.PeriodOfCover ?? new PeriodChoice();

            if (clear.Equals("only ", StringComparison.OrdinalIgnoreCase))
            {
                periodofchoice = new PeriodChoice();
            }
            periodofchoice.InsuranceDuration = new PeriodDuration() { DurationNumber = durationNumber, DurationUnit = durationUnit };
        }


        [Given(@"I deserialize the submission to json")]
        public void GivenIDeserializeTheSubmissionToJson()
        {
            sContext.Add(Constants.SubmissionAsJSON, JsonConvert.SerializeObject(_submissionContext));
        }


        [Given(@"in the submission JSON the '(.*)' is modified to '(.*)'")]
        public void GivenInTheSubmissionJSONTheIsModifiedTo(string fieldname, string fieldvalue)
        {
            var regexstring = $"\"{fieldname}\"\\:\"[A-Za-z_]+\",";    
            var replacestring = $"\"{fieldname}\":\"{fieldvalue}\",";

            var regex = new Regex(regexstring, RegexOptions.IgnorePatternWhitespace);
            if (regex.Match(sContext[Constants.SubmissionAsJSON] as string).Captures.Count != 1)
                throw new Exception($"Unable to find the field {fieldname}" );
            var submissiondata = regex.Replace(sContext[Constants.SubmissionAsJSON] as string, replacestring);

            sContext[Constants.SubmissionAsJSON] = submissiondata;
        }

        [Given(@"the submission json is saved for the scenario")]
        public void GivenTheSubmissionJsonIsSavedForTheScenario()
        {
            var submissionrequestdata = sContext[Constants.SubmissionAsJSON] as string;
            Console.WriteLine(submissionrequestdata.JsonPrettify());
            _apiTestContext.AddContent(sContext[Constants.SubmissionAsJSON], MimeEnumType.Json);
        }

        [Given(@"I refresh the Submissioncontext with the response content")]
        [When(@"I refresh the Submissioncontext with the response content")]
        [Then(@"I refresh the Submissioncontext with the response content")]
        public void GivenIRefreshTheSubmissioncontext()
        {
            StepsExension.ValidateOkCreatedResponse(_apiTestContext.Response);

            _submissionContext = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
        }

        private void CreateAndPostSubmission(int howMany, string brokerUserEmailAddress)
        {
            try
            {
                var submission = new Submission();
                MapBrokerCodeAndDepartment(brokerUserEmailAddress, submission);

                for (int i = 0; i < howMany; i++)
                {
                    PostSubmissionCreate(brokerUserEmailAddress, submission);

                    _stateHolder.CurrentSubmissions.Add(_submissionContext);
                }

                _stateHolder.CurrentBrokerDepartmentId = submission.BrokerDepartmentId;
            }
            catch (Exception ex)
            {
                throw new AssertionException($"An error occured trying to POST a submission : {ex.Message}");
            }
        }

        private void CreateSubmission(int howMany,
            string brokerUserEmailAddress,
            Submission initialSubmission = null)
        {
            Submission submission = new Submission();
            if (initialSubmission != null)
                submission = initialSubmission;

            MapBrokerCodeAndDepartment(brokerUserEmailAddress, submission);

            for (int i = 0; i < howMany; i++)
            {
                PrepareSubmissionCreate(brokerUserEmailAddress, submission);

                _stateHolder.CurrentSubmissions.Add(_submissionContext);
            }

            _stateHolder.CurrentBrokerDepartmentId = submission.BrokerDepartmentId;
        }

        private void PostSubmissionCreate(string brokerEmailAddress, Submission submission)
        {
            try
            {
                var randomStr = sContext[Constants.SubmissionRandomStr].ToString().Length == 0 ? Extension.GenerateRandomString(5)
                             : sContext[Constants.SubmissionRandomStr].ToString();

                var submissionReference = SubmissionExtension.ReturnSubmissionReference(submission.BrokerCode, randomStr);

                _commonSteps.GivenISetSecurityTokenAndCertificateForTheUser(brokerEmailAddress);
                _submissionSteps.GivenISetTheFeatureSubmissionContextFromTheJsonPayloadData_ModelSubmission__Json(@"Data\1_4_Model\Submission_1.json");
                _submissionSteps.GivenISetTheUserEmailAddressForTheFeatureUser(brokerEmailAddress);
                _submissionSteps.GivenISetTheBrokerCodeForTheFeatureUserAs(Convert.ToInt32(submission.BrokerCode));
                _submissionSteps.GivenForTheFeatureSubmissionContextAddTheBrokerData();
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "SubmissionReference", submissionReference);
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractReference", $"B{submission.BrokerCode}{randomStr}1");
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerDepartmentId", submission.BrokerDepartmentId.ToString());
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ProgrammeReference", $"B{submission.BrokerCode}{randomStr}1");
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ClassOfBusinessCode", "marine_hull");
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "TechnicianUserEmailAddress", brokerEmailAddress);
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractDescription", "TS090919A UMR Description");
                _abstractSteps.GivenForTheFeatureSubmissionContextTheIsAppendedWithCurrentValueOf(EntityEnum.Entity.submission, "InsuredOrReinsured", "SubmissionReference");
                
                if (submission.OriginalPolicyholder != null)
                    _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "OriginalPolicyholder", submission.OriginalPolicyholder);

                if (submission.ContractTypeCode != null)
                    _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractTypeCode", submission.ContractTypeCode.ToString());

                _abstractSteps.GivenTheFeatureSubmissionIsSavedForTheScenario(EntityEnum.Entity.submission);

                _submissionContext = sContext[Constants.SubmissionContext] as Submission;
                UserContext = sContext[Constants.UserContext] as User;

                _commonSteps.WhenIMakeRequestToResource(Method.POST, "/submissions");

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    _commonSteps.ThenPrintTheErrorMessageToTheOutputWindow();
                    throw new AssertionException($"A Bad Request (400) is received when POST a Submission.");
                }

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    throw new AssertionException($"An Internal server error (500) is received when POST a Submission.");
                }

                if ((_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.RequestTimeout) ||
                   ((int)_apiTestContext.Response.StatusCode == 0))
                    throw new AssertionException($"A timeout / no response received has occured when trying to upload documents for {sContext[Constants.SubmissionUniqueReference].ToString()}. ");
            }
            finally
            {
                _apiTestContext.ClearDown();
            }

        }

        private void PrepareSubmissionCreate(string brokerEmailAddress, Submission submission)
        {
            var randomStr = sContext[Constants.SubmissionRandomStr].ToString().Length == 0 ? Extension.GenerateRandomString(5)
                         : sContext[Constants.SubmissionRandomStr].ToString();

            var submissionReference = SubmissionExtension.ReturnSubmissionReference(submission.BrokerCode, randomStr);

            _commonSteps.GivenISetSecurityTokenAndCertificateForTheUser(brokerEmailAddress);
            _submissionSteps.GivenISetTheFeatureSubmissionContextFromTheJsonPayloadData_ModelSubmission__Json(@"Data\1_4_Model\Submission_1.json");
            _submissionSteps.GivenISetTheUserEmailAddressForTheFeatureUser(brokerEmailAddress);
            _submissionSteps.GivenISetTheBrokerCodeForTheFeatureUserAs(Convert.ToInt32(submission.BrokerCode));
            _submissionSteps.GivenForTheFeatureSubmissionContextAddTheBrokerData();
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "SubmissionReference", submissionReference);
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractReference", $"B{submission.BrokerCode}{randomStr}1");
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerDepartmentId", submission.BrokerDepartmentId.ToString());
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ProgrammeReference", $"B{submission.BrokerCode}{randomStr}1");
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ClassOfBusinessCode", "marine_hull");
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "TechnicianUserEmailAddress", brokerEmailAddress);
            _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractDescription", "TS090919A UMR Description");
            _abstractSteps.GivenForTheFeatureSubmissionContextTheIsAppendedWithCurrentValueOf(EntityEnum.Entity.submission, "InsuredOrReinsured", "SubmissionReference");
            _abstractSteps.GivenTheFeatureSubmissionIsSavedForTheScenario(EntityEnum.Entity.submission);

            if (submission.OriginalPolicyholder != null)
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "OriginalPolicyholder", submission.OriginalPolicyholder);

            if (submission.ContractTypeCode != null)
                _abstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractTypeCode", submission.ContractTypeCode.ToString());


            _submissionContext = sContext[Constants.SubmissionContext] as Submission;
            UserContext = sContext[Constants.UserContext] as User;
        }
    }
}

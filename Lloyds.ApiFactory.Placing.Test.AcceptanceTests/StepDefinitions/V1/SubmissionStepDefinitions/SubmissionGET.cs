//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoDi;
using FluentAssertions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums.EntityEnum;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    public class SubmissionGET : ApiBaseSteps
    {
        private CommonSteps _commonSteps;
        private StateHolder _stateHolder;
        private AbstractStepsFilters _filterSteps;

        public SubmissionGET(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext, 
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _commonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            _stateHolder = stateHolder;
            _filterSteps = new AbstractStepsFilters(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            if (!sContext.ContainsKey(Constants.SubmissionRandomStr)) sContext.Add(Constants.SubmissionRandomStr, string.Empty);
        }
        

        [Then(@"collection of submissions should have a total greater than (.*)")]
        public void ThenCollectionOfSubmissionsShouldHaveATotalGreaterThan(int total)
        {
            if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
            _submissionCollectionContext.Total.Should().BeGreaterThan(total, "The expected and actual totals do not match");
        }



        [Given(@"collection of (.*) should have a total equal to (.*)")]
        [When(@"collection of (.*) should have a total equal to (.*)")]
        [Then(@"collection of (.*) should have a total equal to (.*)")]
        public void ThenCollectionOfSubmissionsShouldHaveATotalEqualTo(Entity entity, int total)
        {

            if (entity == Entity.submission || entity == Entity.submissions)
            {
                if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.AreEqual(_submissionCollectionContext.Total, total, $"The expected total ({total}) and actual total ({_submissionCollectionContext.Total}) do not match.");
            }

            if (entity == Entity.submission_dialogue)
            {
                if (_submissionDialogueCollectionContext is null) throw new AssertionException("The submission dialogue collection data is empty.");
                Assert.AreEqual(_submissionDialogueCollectionContext.Total, total, $"The expected total ({total}) and actual total ({_submissionDialogueCollectionContext.Total}) do not match.");
            }

            if (entity == Entity.submission_underwriters)
            {
                if (_submissionUnderwriterCollectionContext is null) throw new AssertionException("The submission collection data is empty.");
                Assert.AreEqual(_submissionUnderwriterCollectionContext.Total, total, $"The expected total ({total}) and actual total ({_submissionUnderwriterCollectionContext.Total}) do not match.");
            }

            if (entity == Entity.underwriter_organisations)
            {
                Assert.AreEqual(_underwriterOrganisationContext.Total, total, "The expected and actual totals do not match.");
            }
        }

        [Then(@"collection of submissions should have subsequent version numbers")]
        public void ThenCollectionOfSubmissionsShouldSubsequentVersionNumbers()
        {
            int? versionNumber = 0;

            foreach (var submission in _submissionCollectionContext.Items.OrderBy(x=>x.SubmissionVersionNumber))
            {
                if (versionNumber.Value != 0)
                    Assert.AreEqual(versionNumber + 1, submission.SubmissionVersionNumber);

                versionNumber = submission.SubmissionVersionNumber;
            }
        }



        [Given(@"I POST a set of submissions from the data")]
        [Given(@"I create a set of submissions from the data")]
        public void GivenICreateASetOfSubmissionsFromTheData(IEnumerable<Submission> submissions)
        {
            var brokerEmailAddress = _stateHolder.LoginUser;
            GetBrokerDepartmentAs(brokerEmailAddress);

            foreach (var submission in submissions)
            {
                var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

                _apiTestContext.AddContent(submissionDataToInsert, MimeEnumType.Json);
                _commonSteps.WhenIMakeRequestToResource(Method.POST, "/submissions");
                _commonSteps.ThenTheResponseStatusCodeShouldBe(201);

                _stateHolder.CurrentSubmissions.Add(submissionDataToInsert);
            }
        }

        [Given(@"I POST a set of (.*) submissions from the data")]
        [Given(@"I create a set of (.*) submissions from the data")]
        public void PostSetOfSubmissions(int count)
        {
            var brokerEmailAddress = _stateHolder.LoginUser;
            GetBrokerDepartmentAs(brokerEmailAddress);

            for (int i = 0; i < count; i++)
            {
                var submission = new Submission()
                {
                    BrokerUserEmailAddress = brokerEmailAddress
                };

                var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

                _apiTestContext.AddContent(submissionDataToInsert, MimeEnumType.Json);
                _commonSteps.WhenIMakeRequestToResource(Method.POST, "/submissions");
                _commonSteps.ThenTheResponseStatusCodeShouldBe(201);

                _stateHolder.CurrentSubmissions.Add(submissionDataToInsert);
            }
        }

        [Given(@"I POST a set of submissions from the invalid data")]
        [Given(@"I create a set of submissions from the invalid data")]
        public void GivenICreateAWrongSetOfSubmissionsFromTheDataReturn4xx(IEnumerable<Submission> submissions)
        {
            var brokerEmailAddress = _stateHolder.LoginUser;
            GetBrokerDepartmentAs(brokerEmailAddress);

            foreach (var submission in submissions)
            {
                var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

                _apiTestContext.AddContent(submissionDataToInsert, MimeEnumType.Json);
                _commonSteps.WhenIMakeRequestToResource(Method.POST, "/submissions");
                _commonSteps.ThenTheResponseStatusCodeShouldBeInRange("4xx");
            }
        }

        [Given(@"generate a random string of (.*) characters and save it to the context")]
        public void GivenGenerateARandomStringOfCharactersAndSaveItToTheContext(int p0)
        {   
            sContext[Constants.SubmissionRandomStr] = Extension.GenerateRandomString(p0);
        }

        [Given(@"generate a random string of (.*) numbers and save it to the context")]
        public void GivenGenerateARandomStringOfNumbersAndSaveItToTheContext(int p0)
        {
            sContext[Constants.SubmissionRandomStr] = Extension.GenerateRandomStringNumbers(p0);
        }

        [Given(@"set up version number Submissions creation")]
        public void SetupSubmissionVersionNumberSubmission()
        {
            sContext[Constants.SubmissionRandomStr] = Extension.GenerateRandomString(6);
        }

        [Then(@"collection of submissions should have no '(.*)' link")]
        public void ThenCollectionOfSubmissionsShouldHaveNoLink(string linkname)
        {
            if (_submissionCollectionContext is null) throw new AssertionException("The submission collection data is empty.");

            Assert.IsTrue(_submissionCollectionContext.Links.Count(a => string.Compare(a.rel, linkname, true) == 0) == 0, 
                    $"The {linkname} link is present.");

        }

        [Given(@"I add a filter on '(.*)' with a value from the response")]
        [When(@"I add a filter on '(.*)' with a value from the response")]
        [Then(@"I add a filter on '(.*)' with a value from the response")]
        public void GivenIAddAFilterOnWithAValueFromTheResponse(string fieldname)
        {
            var fieldvalue = "";

            var submissionproperty = _submissionContext.GetType().GetProperty(fieldname);

            if ((submissionproperty.PropertyType == typeof(DateTimeOffset)) || (submissionproperty.PropertyType == typeof(DateTimeOffset?)))
            {
                var dtoffsetvalue = DateTimeOffset.Parse(fieldname);
                fieldvalue = dtoffsetvalue.ToString("yyyy-MM-dd");
            }
            else
            {
                fieldvalue = ReflectionHelper.GetPropertyValue(_submissionContext, fieldname).ToString();
            }


            // when you inject the item stored within the scenariocontext
            if (fieldvalue.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalue.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalue}");
                fieldvalue = sContext[sContextFieldName].ToString();
            }

            _filterSteps.GivenIAddAFilterOn(string.Empty, fieldname, fieldvalue);
            _stateHolder.ParamsBuilderValueToValidate = fieldvalue;
        }


        [Given(@"I add a filter on '(.*)' with a value from the response collection")]
        [When(@"I add a filter on '(.*)' with a value from the response collection")]
        [Then(@"I add a filter on '(.*)' with a value from the response collection")]
        public void GivenIAddAFilterOnWithAValueFromTheResponseCollection(string fieldname)
        {
            var fieldvalue = "";
            Submission submissionSelected = null;

            if (_submissionCollectionContext.Items.Any())
                submissionSelected = _submissionCollectionContext.Items.FirstOrDefault();
            else
                Assert.Fail($"The collection returned is empty.");

            //TODO: when we dont have string
            fieldvalue = ReflectionHelper.GetPropertyValue(submissionSelected, fieldname).ToString();

            // when you inject the item stored within the scenariocontext
            if (fieldvalue.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalue.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalue}");
                fieldvalue = sContext[sContextFieldName].ToString();
            }

            _filterSteps.GivenIAddAFilterOn(string.Empty, fieldname, fieldvalue);
            _stateHolder.ParamsBuilderValueToValidate = fieldvalue;
        }


        [Given(@"I save the '(.*)' string of the field '(.*)' of submission context")]
        public void GivenISaveTheStringOfTheFieldOfSubmissionContext(string filtertype, string field)
        {
            var value = ReflectionHelper.GetPropertyValue(_submissionContext, field).ToString();

            if (filtertype == "suffix")
                sContext[Constants.SubmissionRandomStr] = value.Substring(value.Length-3);
        }

        [Given(@"I store the initial submission")]
        public void StoreTheInitialSubmission()
        {
            _stateHolder.CurrentSubmission = _submissionContext;
        }

        [Given(@"I restore the initial submission")]
        public void ReStoreTheInitialSubmission()
        {
            _submissionContext = _stateHolder.CurrentSubmission;
        }

        [Given(@"select submission with SubmissionVersionNumber (.*)")]
        public void GivenSelectSubmissionWithSubmissionVersionNumber(int submissionVersionNumber)
        {
            var submissionReference = _submissionContext.SubmissionReference;
            GetSubmissionBySubmissionReferenceAndQVN(submissionReference, submissionVersionNumber.ToString());
        }


        private Submission PrepareSubmissionData(Submission submission, string brokerEmailAddress)
        {
            var mappedSubmission = MapBrokerCodeAndDepartment(brokerEmailAddress, submission);

            SubmissionExtension.SetSubmissionValues(mappedSubmission, _submissionContext);

            return mappedSubmission;
        }
    }
}

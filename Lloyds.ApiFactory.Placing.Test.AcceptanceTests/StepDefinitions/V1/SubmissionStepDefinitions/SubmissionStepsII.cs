//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Reflection;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionStepsII : ApiBaseSteps
    {
        private AbstractSteps AbstractSteps;
        private StateHolder _stateHolder;

        protected User _userContext
        {
            get { return sContext[Constants.UserContext] as User; }
            set { sContext[Constants.UserContext] = value; }
        }
        
        public SubmissionStepsII(IObjectContainer objectContainer,
            ApiTestContext apiTestContext, 
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;
            if (!sContext.ContainsKey(Constants.SubmissionContext)) sContext.Add(Constants.SubmissionContext, new Submission());
            if (!sContext.ContainsKey(Constants.UserContext)) sContext.Add(Constants.UserContext, new User());
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
        }

        [Given(@"I set the user email address for the user '(.*)'")]
        public void GivenISetTheSubmissionJsonPayloadForTheUser(string username)
        {
            _userContext.UserEmailAddress = username;
        }

        [Given(@"I set the broker code as (.*)")]
        public void GivenISetTheBrokerCodeAs(int brokerCode)
        {
            _userContext.BrokerCode = brokerCode;
        }

        [Given(@"for the submission add the broker data")]
        public void GivenForTheSubmissionAddTheBrokerData()
        {
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerUserEmailAddress", _userContext.UserEmailAddress);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "TechnicianUserEmailAddress", _userContext.UserEmailAddress);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerCode", $"{_userContext.BrokerCode}");
        }
                          
        [Given(@"for the submission the '(.*)' property is appended with the current value of '(.*)'")]
        [When(@"for the submission the '(.*)' property is appended with the current value of '(.*)'")]

        public void GivenForTheFeatureSubmissionContextTheIsAppendedWithCurrentValueOf(string propertyName, string propertyToAppend)
        {
            GetProperty(propertyName, out var submissionproperty);

            GetProperty(propertyToAppend, out var submissionpropertyToAppend);

            if (submissionproperty.PropertyType != typeof(string))
                throw new AssertionException(
                    $"Cannot append property {propertyName} with current value of {propertyToAppend}, the property is not of type string");

            if (submissionpropertyToAppend.PropertyType != typeof(string))
                throw new AssertionException(
                    $"Cannot append property {propertyName} with current value of {propertyToAppend} (the appender), the appender is not of type string");

            string value = submissionproperty.GetValue(_submissionContext).ToString();

            string appendValue = submissionpropertyToAppend.GetValue(_submissionContext).ToString();

            string newValue = value + appendValue;

            submissionproperty.SetValue(_submissionContext, newValue);
        }

        private void GetProperty(string propertyName, out PropertyInfo propertyInfo)
        {
            if ((_submissionContext == null) || !(_submissionContext is Submission))
                throw new AssertionException("The scenario cannot find the submission object.");


            propertyInfo = _submissionContext.GetType().GetProperty(propertyName);

            if (propertyInfo is null)
                throw new AssertionException($"The submission scenario cannot find the property {propertyName}");
        }

        [Given(@"for the submission the '(.*)' is an enum '(.*)'")]
        public void GivenForTheSubmissionTheIsAndEnum(string propertyName, ContractType propertyValue)
        {
            if ((_submissionContext == null) || !(_submissionContext is Submission))
                throw new AssertionException("The scenario cannot find the submission object.");
            _submissionContext.ContractTypeCode = propertyValue;
        }

        [Given(@"for the submission the '(.*)' is '(.*)' plus a random string")]
        public void GivenForTheSubmissionTheIsPlusARandomString(string propertyName, string propertyValue)
        {
            propertyValue = $"{propertyValue}{Extension.GenerateRandomString()}";
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, propertyName, propertyValue);
        }

        [Given(@"for the submission the PeriodofCover has an Insurance period where the inception date is '(.*)' and the expiry date is '(.*)'")]
        public void GivenForTheSubmissionThePeriodofCoverHasAnInsurancePeriodWhereTheInceptionDateIsAndTheExpiryDateIs(string inceptionDate, string expiryDate)
        {
            if (!DateTimeOffset.TryParse(inceptionDate, out DateTimeOffset inceptiondate)) throw new AggregateException("The field inception date could not be parsed for a submission");
            if (!DateTimeOffset.TryParse(expiryDate, out DateTimeOffset expirydate)) throw new AggregateException("The field expiry date could not be parsed for a submission");

            var periodofchoice = _submissionContext.PeriodOfCover ?? new PeriodChoice();
            periodofchoice.InsurancePeriod = new PeriodDate() { InceptionDate = inceptiondate, ExpiryDate = expirydate };
            _submissionContext.PeriodOfCover = periodofchoice;
        }


        [Given(@"for the submission the PeriodOfCover has an insurance duration of (.*) (.*)")]
        public void GivenForTheSubmissionThePeriodofCoverHasADurationOf(int value, string unit)
        {
            PeriodDurationUnit uom;
            if (!Enum.TryParse(unit, out uom)) throw new AggregateException("The unit specified is not a valid unif of measurement");
            _submissionContext.PeriodOfCover = new PeriodChoice() { InsuranceDuration = new PeriodDuration{DurationNumber = value, DurationUnit = uom } };
        }

        [Given(@"the submission is saved for the scenario")]
        public void GivenTheSubmissionIsSavedForTheScenario()
        {   _apiTestContext.AddContent(_submissionContext, MimeEnumType.Json);
        }



        [Given(@"I create a JSON payload for POST")]
        public void GivenICreateAJSONPayloadForPOST()
        {
            var submission = new Submission()
            {   BrokerCode = _userContext.BrokerCode.ToString(),
                BrokerUserEmailAddress = _userContext.UserEmailAddress,
                ClassOfBusinessCode = "marine_hull",
                ContractDescription = "TS090919A UMR Description",
                ContractReference = "TS090919AUMR",
                ContractTypeCode = ContractType.direct_insurance_contract,
                CoverTypeCode = "facultative_proportional",
                SubmissionReference = "R2TEST2",
                SubmissionVersionDescription = "R2TEST2 - 3008",
                InsuredOrReinsured="insured",
                OriginalPolicyholder="",
                PeriodOfCover           = new PeriodChoice() { InsurancePeriod= new PeriodDate() {InceptionDate= DateTimeOffset.Parse("2019-01-01") , ExpiryDate= DateTimeOffset.Parse("2019-12-31") } },
                ProgrammeDescription    = "R2 TEST Data",
                ProgrammeReference      = "R2 TEST Data",
                RiskRegionCode          = "worldwide",
                RiskCountryCode         = "GB"
            };

            
            _apiTestContext.AddContent(submission, MimeEnumType.Json);
        }

        [Given(@"the request submission data matches the response submission data")]
        [When(@"the request submission data matches the response submission data")]
        [Then(@"the request submission data matches the response submission data")]
        public void ThenTheRequestSubmissionDataMatchesTheResponseSubmissionData()
        {
            var submissionresponsedata = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
            Assert.IsTrue(_submissionContext.SubmissionEquals(submissionresponsedata), "The response and request objects are not equal.");
        }
        
        [Given(@"for the submission the PeriodofCover (.*)has an Insurance duration where the duration number is '(.*)' and the duration unit is '(.*)'")]
        public void GivenForTheSubmissionThePeriodofCoverHasAnInsuranceDurationWhereTheDurationNumberIsAndTheDurationUnitIs(string clear, int durationNumber, PeriodDurationUnit durationUnit)
        {
            var periodofchoice = _submissionContext.PeriodOfCover ?? new PeriodChoice();

            if (clear.Equals("only ", StringComparison.OrdinalIgnoreCase))
            {
                periodofchoice = new PeriodChoice();
            }
            periodofchoice.InsuranceDuration = new PeriodDuration() { DurationNumber = durationNumber, DurationUnit = durationUnit };
        }

    }
}

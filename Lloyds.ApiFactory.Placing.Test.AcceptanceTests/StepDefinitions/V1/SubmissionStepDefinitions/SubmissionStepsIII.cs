//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using Constants = Lloyds.ApiFactory.Placing.Test.Common.Constants;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionStepsIII : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private readonly AbstractSteps AbstractSteps;

        protected User UserContext 
        {   get => sContext[Constants.UserContext] as User;
            set => sContext[Constants.UserContext] = value;
        }
  
        public SubmissionStepsIII(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext, 
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;
            if (!sContext.ContainsKey(Constants.SubmissionContext)) sContext.Add(Constants.SubmissionContext, new Submission());
            if (!sContext.ContainsKey(Constants.UserContext)) sContext.Add(Constants.UserContext, new User());
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, _stateHolder);
        }


        [Given(@"I set the feature submission context from the json payload (.*)")]
        public void GivenISetTheFeatureSubmissionContextFromTheJsonPayloadData_ModelSubmission__Json(string filePath)
        {
            var path = Path.Combine(Environment.CurrentDirectory, filePath);
            var submissionContext = FileHelper.ReadFile<Submission>(path);
            _submissionContext = submissionContext ?? throw new AssertionException("Unable to create a submission resource from the input json file.");

            //if (sContext.ContainsKey(Constants.SubmissionContext)) sContext[Constants.SubmissionContext] = _submissionContext;

            //  TODO: Refactor
            base.SaveSubmissionToScenarioContext(_submissionContext);
        }

        [Given(@"I set the user email address for the feature user '(.*)'")]
        public void GivenISetTheUserEmailAddressForTheFeatureUser(string userName)
        {
            UserContext.UserEmailAddress = userName;
        }

        [Given(@"I set the broker code for the feature user as (.*)")]
        public void GivenISetTheBrokerCodeForTheFeatureUserAs(int brokerCode)
        {
            UserContext.BrokerCode = brokerCode;
        }

        [Given(@"for the feature submission context add the broker data")]
        public void GivenForTheFeatureSubmissionContextAddTheBrokerData()
        {
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerUserEmailAddress", UserContext.UserEmailAddress);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerCode", UserContext.BrokerCode.ToString());
        }
        

        [Given(@"the request feature submission context matches the response submission data")]
        [When(@"the request feature submission context matches the response submission data")]
        [Then(@"the request feature submission context matches the response submission data")]
        public void ThenTheRequestFeatureSubmissionContextMatchesTheResponseSubmissionData()
        {
            var submissionResponseData = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
            Assert.IsTrue(_submissionContext.SubmissionEquals(submissionResponseData), "The response and request objects are not equal.");
            _stateHolder.CurrentSubmission = submissionResponseData;
        }

        [Then(@"the response contains a validation error '(.*)'")]
        [Then(@"the response contains a message error '(.*)'")]
        [Then(@"the response contains an error '(.*)'")]
        public void ResponseContains(string error)
        {
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest)
                return;

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);

            var hasErrorInvalidation = false;
            var hasErrorInMessage = false;

            if (submissionResponseData?.ValidationErrors != null)
            {
                if (submissionResponseData?.ValidationErrors.Count == 0)
                    hasErrorInMessage = submissionResponseData.Message.Contains(error, StringComparison.OrdinalIgnoreCase);
                else
                {
                    hasErrorInvalidation = submissionResponseData.ValidationErrors.Any(msg =>
                    msg.Error.Contains(error, StringComparison.OrdinalIgnoreCase));
                }
            }
            else if (!string.IsNullOrWhiteSpace(submissionResponseData?.Message))
            {
                hasErrorInMessage = submissionResponseData.Message.Contains(error, StringComparison.OrdinalIgnoreCase);
            }
                        
            if (hasErrorInvalidation == false && hasErrorInMessage == false) //Expected error code not found in the response
            {
                var response = submissionResponseData.ValidationErrors.Any() ? submissionResponseData.ValidationErrors.First().Error :
                    submissionResponseData.Message;

                Assert.Fail($"Couldn't find error in ValidationErrors or Message. \n \n" +
                    $" Response: {response}"); 
            }
        }

        [Then(@"the response contains a validation error '(.*)' for the field ""(.*)""")]
        public void ThenTheResponseContainsAValidationErrorForTheField(string errorMessage, string fieldName)
        {
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest)
            {
                return;
            }

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);

            if (submissionResponseData?.ValidationErrors == null)
                throw new AssertionException("The response content does not hold an error.");

            var error = submissionResponseData.ValidationErrors.FirstOrDefault(va => va.Field.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));

            if (error is null)
                throw new AssertionException($"The submission scenario cannot find any errors for the field {fieldName}");

            Assert.True(error.Error.Contains(errorMessage, StringComparison.InvariantCultureIgnoreCase), $"The validation error {errorMessage} for the field '{fieldName}' is not part of the response.");
        }


        [Then(@"the request feature submission context contains a value for '(.*)'")]
        public void ThenTheRequestFeatureSubmissionContextContainsAValueFor(string fieldvalue)
        {
            StepsExension.ValidateOkCreatedResponse(_apiTestContext.Response);

            var submissionresponsedata = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
            Assert.IsFalse(string.IsNullOrWhiteSpace(submissionresponsedata.SubmissionVersionDescription),$"The response data contains no value for '{fieldvalue}'.");
        }

        [Given(@"for the feature submission context the '(.*)' is '(.*)' plus a random string")]
        public void GivenForTheSubmissionTheIsPlusARandomString(string propertyName, string propertyValue)
        {
            propertyValue = $"{propertyValue}{Extension.GenerateRandomString()}";
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, propertyName, propertyValue);
        }

        [Then(@"the response contains one of the following error messages")]
        public void ThenTheResponseContainsOneOfTheFollowingErrorMessages(Table errorMessages)
        {
            if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.NotFound)
                return;

            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest)
                return;

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);
            if (submissionResponseData?.ValidationErrors == null)
            {
                var errFound = errorMessages.Rows.ToList().Any(msg =>
                  submissionResponseData.Message.Contains(msg[0].ToString(), StringComparison.OrdinalIgnoreCase));

                Assert.IsTrue(errFound, "The errors listed in the table do not exist in the response. \n \n" +
                    $"Response: {submissionResponseData.Message}");
                return;
            }
            else
            {
                if (submissionResponseData?.ValidationErrors.Count() == 0)
                {
                    var errFound = errorMessages.Rows.ToList().Any(msg =>
                 submissionResponseData.Message.Contains(msg[0].ToString(), StringComparison.OrdinalIgnoreCase));

                    Assert.IsTrue(errFound, "The errors listed in the table do not exist in the response. \n \n" +
                        $"Response: {submissionResponseData.Message}");
                    return;
                }
            }

            var validationErrFound = errorMessages.Rows.ToList().Any(msg =>
                submissionResponseData.ValidationErrors.Any(a =>
                    a.Error.Contains(msg[0], StringComparison.InvariantCultureIgnoreCase)));

            Assert.IsTrue(validationErrFound, "The error validation errors listed in the table do not exist in the response. \n \n" +
                $"Response: {submissionResponseData.ValidationErrors.First().Error}");
        }

        [Then(@"the response contains one of the following error messages for the field '(.*)'")]
        public void ThenTheResponseContainsOneOfTheFollowingErrorMessagesForTheField(string fieldName, Table errorMessages)
        {
            //ThenTheResponseContainsOneOfTheFollowingErrorMessages(errorMessages);
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest) return;

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);
            if (submissionResponseData?.ValidationErrors == null)
                throw new AssertionException("The response content does not hold an error.");

            var error = submissionResponseData.ValidationErrors.FirstOrDefault(va => va.Field.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));

            if (error is null)
                throw new AssertionException($"The submission scenario cannot find any errors for the field {fieldName}");

            var errFound = errorMessages.Rows.Any(errorRow => error.Error.Contains(errorRow[0], StringComparison.InvariantCultureIgnoreCase));

            if (!errFound)
                Assert.Fail("The error validation errors listed in the table do not exist in the response.");
        }

        [Given(@"I save the submission response to the submissioncontext")]
        public void GivenISaveTheSubmissionResponseToTheSubmissioncontext()
        {
            _submissionContext = JsonConvert.DeserializeObject<Submission>(_apiTestContext.Response.Content);
        }

        [Given(@"I set the SubmissionReference and the SubmissionDocumentId")]
        [When(@"I set the SubmissionReference and the SubmissionDocumentId")]
        [Then(@"I set the SubmissionReference and the SubmissionDocumentId")]
        public void GivenISetTheSubmissionReference()
        {
            if (!sContext.ContainsKey(Constants.SubmissionReferenceforSubmissionDocument)) sContext.Add(Constants.SubmissionReferenceforSubmissionDocument, "");
            sContext[Constants.SubmissionReferenceforSubmissionDocument] = _submissionContext.SubmissionReference;

            if (!sContext.ContainsKey(Constants.SubmissionDocumentId)) sContext.Add(Constants.SubmissionDocumentId, "");
            sContext[Constants.SubmissionDocumentId] = _submissionContext.Id;
        }

        [Then(@"validate the POST submission context")]
        public void ThenValidateThePOSTSubmission()
        {
            //mandatory fields for Submission
            Assert.IsNotNull(_stateHolder.CurrentSubmission.SubmissionReference, "SubmissionReference is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.SubmissionVersionNumber, "SubmissionVersionNumber is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.TechnicianUserEmailAddress, "TechnicianUserEmailAddress is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.CreatedDateTime, "CreatedDateTime is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.SubmissionStatusCode, "SubmissionStatusCode is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.BrokerDepartmentId, "BrokerDepartmentId is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.BrokerUserEmailAddress, "BrokerUserEmailAddress is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.BrokerCode, "BrokerCode is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.ProgrammeId, "ProgrammeId is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.ContractTypeCode, "ContractTypeCode is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.ContractDescription, "ContractDescription is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.ClassOfBusinessCode, "ClassOfBusinessCode is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.CoverTypeCode, "CoverTypeCode is null");
            Assert.IsNotNull(_stateHolder.CurrentSubmission.PeriodOfCover, "PeriodOfCover is null");

            Assert.AreEqual(_stateHolder.CurrentSubmission.SubmissionReference, _submissionContext.SubmissionReference);
            Assert.AreEqual(_stateHolder.CurrentSubmission.BrokerDepartmentId, _submissionContext.BrokerDepartmentId);
            Assert.AreEqual(_stateHolder.CurrentSubmission.BrokerUserEmailAddress, _submissionContext.BrokerUserEmailAddress);
            Assert.AreEqual(_stateHolder.CurrentSubmission.BrokerCode, _submissionContext.BrokerCode);
            Assert.AreEqual(_stateHolder.CurrentSubmission.ContractTypeCode, _submissionContext.ContractTypeCode);
            Assert.AreEqual(_stateHolder.CurrentSubmission.InsuredOrReinsured, _submissionContext.InsuredOrReinsured);
            Assert.AreEqual(_stateHolder.CurrentSubmission.ClassOfBusinessCode, _submissionContext.ClassOfBusinessCode);
            Assert.AreEqual(_stateHolder.CurrentSubmission.PeriodOfCover.InsurancePeriod.ExpiryDate, _submissionContext.PeriodOfCover.InsurancePeriod.ExpiryDate);
            Assert.AreEqual(_stateHolder.CurrentSubmission.PeriodOfCover.InsurancePeriod.InceptionDate, _submissionContext.PeriodOfCover.InsurancePeriod.InceptionDate);

        }

        [Given(@"for the submission the PeriodOfCover is invalid")]
        public void ThePeriodChoiceIsInvalid()
        {
            var entity = GetProperty(EntityEnum.Entity.submission, "PeriodOfCover", out var property);
            var propertyValue = new PeriodChoice()
            {
                InsuranceDuration = new PeriodDuration()
                {
                    DurationNumber = 3,
                    DurationUnit = PeriodDurationUnit.days
                },
                InsurancePeriod = new PeriodDate()
                {
                    ExpiryDate = DateTime.Now.AddYears(1),
                    InceptionDate = DateTime.Now.AddYears(-1)
                }
            };

            property.SetValue(entity, propertyValue);
        }

        [Given(@"for the submission the PeriodOfCover is different")]
        public void ThePeriodChoiceIsDifferent()
        {
            var entity = GetProperty(EntityEnum.Entity.submission, "PeriodOfCover", out var property);
            var propertyValue = new PeriodChoice()
            {
                InsurancePeriod = new PeriodDate()
                {
                    ExpiryDate = DateTime.Now.AddYears(1),
                    InceptionDate = DateTime.Now.AddYears(-1)
                }
            };

            property.SetValue(entity, propertyValue);
        }


        [Given(@"set all the submission fields based on the broker code of '(.*)'")]
        public void GivenSetAllTheSubmissionFieldsBasedOnTheBrokerCodeOf(string brokerCode)
        {
            var valueToReplace = $"B{brokerCode}";

            _submissionContext.SubmissionReference = valueToReplace + _submissionContext.SubmissionReference.Remove(1, 5);
            _submissionContext.ProgrammeReference = valueToReplace + _submissionContext.ProgrammeReference.Remove(1, 5);
            _submissionContext.ContractReference = valueToReplace + _submissionContext.ContractReference.Remove(1, 5);

        }

        [Given(@"for the (.*) context the BrokerDepartmentId is different")]
        public void SetDifferentBrokerDepartmentId(EntityEnum.Entity entityEnum)
        {
            var entity = GetProperty(entityEnum, "BrokerDepartmentId", out var property);
            var currentBrokerDepartmentId = ReflectionHelper.GetPropertyValue(entity, "BrokerDepartmentId").ToString();
            var newBrokerDepartmentId = "";

            GetBrokerDepartmentAs(_stateHolder.BrokerUserEmailAddress);
            
            foreach(var brokerDepartment in _brokerDepartmentContext.Items)
            {
                if (!brokerDepartment.BrokerDepartmentId.Equals(currentBrokerDepartmentId, StringComparison.OrdinalIgnoreCase))
                {
                    newBrokerDepartmentId = brokerDepartment.BrokerDepartmentId;
                    break;
                }
            }

            if (newBrokerDepartmentId.Length == 0 || newBrokerDepartmentId == currentBrokerDepartmentId)
                Assert.Fail($"Cannot find a different broker department id than {currentBrokerDepartmentId}" +
                    $"for broker {_stateHolder.BrokerUserEmailAddress}.");

            property.SetValue(entity, newBrokerDepartmentId);
        }

        [Given(@"for the (.*) context the BrokerCode is different")]
        public void SetDifferentBrokerCode(EntityEnum.Entity entityEnum)
        {
            var entity = GetProperty(entityEnum, "BrokerCode", out var property);
            var currentBrokerCode = ReflectionHelper.GetPropertyValue(entity, "BrokerCode").ToString();
            var newBrokerCode = "";

            GetBrokerDepartmentAs(_stateHolder.BrokerUserEmailAddress);

            foreach (var brokerDepartment in _brokerDepartmentContext.Items)
            {
                foreach(var brokerCode in brokerDepartment.BrokerCodes)
                {
                    if (!brokerCode.Equals(currentBrokerCode, 
                        StringComparison.OrdinalIgnoreCase))
                    {
                        newBrokerCode = brokerCode;
                        break;
                    }
                }
            }

            if (newBrokerCode.Length == 0 || newBrokerCode == currentBrokerCode)
                Assert.Fail($"Cannot find a different broker code than {currentBrokerCode}" +
                    $"for broker {_stateHolder.BrokerUserEmailAddress}.");

            property.SetValue(entity, newBrokerCode);

            //need to change all relevant fields if we want to to avoid previous validation
            SubmissionExtension.ReplaceSubmissionValue(_submissionContext, "ContractReference", currentBrokerCode, newBrokerCode);
            SubmissionExtension.ReplaceSubmissionValue(_submissionContext, "SubmissionReference", currentBrokerCode, newBrokerCode);
            SubmissionExtension.ReplaceSubmissionValue(_submissionContext, "ProgrammeReference", currentBrokerCode, newBrokerCode);
        }
    }
}

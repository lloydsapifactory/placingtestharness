//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Lloyds.ApiFactory.ViewModel.Abstractions;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Constants = Lloyds.ApiFactory.Placing.Test.Common.Constants;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionStepsAdditional : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private CommonSteps CommonSteps;
        private readonly SubmissionStepsIII SubmissionSteps;
        private readonly AbstractSteps AbstractSteps;

        protected User UserContext 
        {   get => sContext[Constants.UserContext] as User;
            set => sContext[Constants.UserContext] = value;
        }

        public SubmissionStepsAdditional(IObjectContainer objectContainer, 
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder) : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;

            if (!sContext.ContainsKey(Constants.SubmissionContext)) sContext.Add(Constants.SubmissionContext, new Submission());
            if (!sContext.ContainsKey(Constants.UserContext)) sContext.Add(Constants.UserContext, new User());

            CommonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionSteps = new SubmissionStepsIII(objectContainer, apiTestContext, scenarioContext, featureContext, _stateHolder);
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, _stateHolder);
        }        

        [Given(@"for the feature (.*) context the '(.*)' with a random value (.*) (.*)")]
        [When(@"for the feature (.*) context the '(.*)' with a random value (.*) (.*)")]
        [Then(@"for the feature (.*) context the '(.*)' with a random value (.*) (.*)")]
        public void ForTheFeatureSubmissionContextTheWithARandomValueMoreThan(EntityEnum.Entity entity, string field, string assert, int chars)
        {
            object usedObject = null;

            if (entity == EntityEnum.Entity.submission)
                usedObject = _submissionContext;

            if (entity == EntityEnum.Entity.submission_underwriters)
                usedObject = _SubmissionUnderwriterContext;

            if (entity == EntityEnum.Entity.submission_dialogue)
                usedObject = _submissionDialogueContext;

            var newValue = "";

            if (assert == "more than")
                newValue = StringHelper.GenerateRandomString(chars + 1);

            if (assert == "less than")
                newValue = StringHelper.GenerateRandomString(chars - 1);

            var submissionproperty = usedObject.GetType().GetProperty(field);

            if (submissionproperty is null)
                throw new AssertionException($"The submission scenario cannot find the property {field}");

            if ((submissionproperty.PropertyType == typeof(int)) || (submissionproperty.PropertyType == typeof(int?)))
            {
                if (newValue.Length != 0)
                    submissionproperty.SetValue(usedObject, Convert.ToInt32(newValue));
                else
                    submissionproperty.SetValue(usedObject, 0);

                return;
            }

            submissionproperty.SetValue(usedObject, newValue);
        }

        [Then(@"the response contains a validation error for the field ""(.*)"" and characters (.*)")]
        public void ThenTheResponseContainsAValidationErrorForTheField(string fieldName, int chars)
        {
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest)
            {
                return;
            }

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);

            if (submissionResponseData?.ValidationErrors == null)
                throw new AssertionException("The response content does not hold an error.");

            var error = submissionResponseData.ValidationErrors.FirstOrDefault();
            if (error is null)
                throw new AssertionException($"The submission scenario cannot find any errors for the field {fieldName}");

            var containField = submissionResponseData.ValidationErrors.Any(x => x.Error.Contains(fieldName));
            Assert.IsTrue(containField, $"The error message doesnt contain the field of {fieldName}.");

            var containCharAssertion = submissionResponseData.ValidationErrors.Any(x => x.Error.Contains(chars.ToString()));
            Assert.IsTrue(containCharAssertion, $"The error message doesnt contain character assesrtion of {chars}.");
        }

        [Then(@"the response contains a validation error for the field ""(.*)""")]
        public void ThenTheResponseContainsAValidationErrorForTheField(string fieldname)
        {
            if (_apiTestContext.Response.StatusCode != System.Net.HttpStatusCode.BadRequest)
            {
                return;
            }

            var submissionResponseData = JsonConvert.DeserializeObject<ErrorDocument>(_apiTestContext.Response.Content);

            if (submissionResponseData?.ValidationErrors == null)
                throw new AssertionException("The response content does not hold an error.");

            var error = submissionResponseData.ValidationErrors.FirstOrDefault();
            if (error is null)
                throw new AssertionException($"The submission scenario cannot find any errors for the field {fieldname}");

            var containField = submissionResponseData.ValidationErrors.Any(x => x.Error.Contains(fieldname, StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(containField, $"The error message doesnt contain the field of {fieldname}.");
        }


        [Given(@"for the feature (.*) context the email '(.*)' is an invalid email address")]
        public void GivenForTheFeatureSubmissionContextTheEmailIsAnInvalidEmailAddress(EntityEnum.Entity entity, string propertyName)
        {
            object usedObject = SetObject(entity);

            if ((usedObject == null))
                throw new AssertionException("The scenario cannot find the submission object.");

            var submissionproperty = usedObject.GetType().GetProperty(propertyName);

            if (submissionproperty is null)
                throw new AssertionException($"The submission scenario cannot find the property {propertyName}");

           
            submissionproperty.SetValue(usedObject, "fdsa@fdsa.");
        }

        [Given(@"'(.*)' for Submission is null")]
        public void GivenIsNull(string field)
        {
            if ((_submissionContext == null) || !(_submissionContext is Submission))
                throw new AssertionException("The scenario cannot find the submission object.");

            var submissionproperty = _submissionContext.GetType().GetProperty(field);

            if (submissionproperty is null)
                throw new AssertionException($"The submission scenario cannot find the property {field}");

            submissionproperty.SetValue(_submissionContext, null);
        }


        [Then(@"collection of submissions for the field (.*) are between (.*) and (.*)")]
        public void ThenTheSubmissionsOfFieldCreatedDateTimeAreBetweenAnd(string field, DateTime date1, DateTime date2)
        { 
            if (_submissionCollectionContext.Items != null)
            {
                foreach (var item in _submissionCollectionContext.Items)
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field);

                    var dateTimeFound = Convert.ToDateTime(valueAssert);

                    if (dateTimeFound < Convert.ToDateTime(date1))
                        Assert.Fail($"The datetime found {dateTimeFound} is less than {date1}");


                    if (dateTimeFound > Convert.ToDateTime(date2))
                        Assert.Fail($"The datetime found {dateTimeFound} is more than {date2}");
                }
            }
            else
                throw new AssertionException("The submission collection data is empty.");

        }

        [Then(@"collection of submissions for the field (.*) are on (.*)")]
        public void ThenCollectionOfSubmissionsForTheFieldCreatedDateTimeAreOn(string field, DateTime date)
        {
            if (_submissionCollectionContext.Items != null)
            {
                foreach (var item in _submissionCollectionContext.Items)
                {
                    var dateTimeFound = ReflectionHelper.GetPropertyValue(item, field);

                    if (dateTimeFound is DateTimeOffset dto)
                    {
                        if (dto.UtcDateTime != DateTime.MinValue)
                        {
                            int result = DateTime.Compare(date, dto.UtcDateTime);

                            if (result != 0)
                               Assert.Fail($"The datetime found {dateTimeFound} is different than {date}");
                        }
                        else
                            Assert.Fail($"The datetime found {dateTimeFound} is empty.");

                    }

                }
            }
            else
                throw new AssertionException("The submission collection data is empty.");
        }


       [Then(@"validate submission result with the field (.*) with value equal to (.*)")]
        public void ThenValidateResultContainSubmissionAndValidateValue(string fieldname, string value)
        {
            if (_submissionContext != null)
            {
                var propAssert = ReflectionHelper.GetPropertyCc(_submissionContext, fieldname.Trim().ToLower());

                if (propAssert == null)
                    Assert.Fail($"Couldn't find the field {fieldname}.");

                var valueAssert = ReflectionHelper.GetPropertyValue(_submissionContext, fieldname);

                if (valueAssert == null)
                    Assert.Fail($"The value of the field {fieldname} is null.");

                Assert.AreEqual(valueAssert.ToString(), value);
                
            }
            else
                throw new AssertionException("The submission document data is empty.");
        }

        [Given(@"I prepare the submission to be posted with the following information")]
        [When(@"I prepare the submission to be posted with the following information")]
        [Then(@"I prepare the submission to be posted with the following information")]
        public void CreateSubmissionOfSetData(Table submissiontable)
        {
            var rowSubmission = submissiontable.CreateSet<Submission>().First();

            GetBrokerDepartmentAs(rowSubmission.BrokerUserEmailAddress);
            var submission = PrepareSubmissionData(rowSubmission, rowSubmission.BrokerUserEmailAddress);

            Given($"I set the user email address for the feature user '{submission.BrokerUserEmailAddress}'");
            Given($"I set the broker code for the feature user as {submission.BrokerCode}");
            Given($"for the feature submission context add the broker data");
            Given($"for the feature submission context the 'SubmissionReference' is '{submission.SubmissionReference}' plus a random string");
            Given($"for the feature submission context the 'ContractReference' is '{submission.ContractReference}'");
            Given($"for the feature submission context the 'ProgrammeReference' is '{submission.ProgrammeReference}'");
            Given($"for the feature submission context the 'ClassOfBusinessCode' is '{submission.ClassOfBusinessCode}'");
            Given($"for the feature submission context the 'ContractDescription' is '{submission.ContractDescription}'");
            Given($"for the feature submission context the 'BrokerDepartmentId' is '{submission.BrokerDepartmentId}'");

            if (submission.OriginalPolicyholder != null)
                Given($"for the feature submission context the 'OriginalPolicyholder' is '{submission.OriginalPolicyholder}'");

            if (submission.CoverTypeCode != null)
                Given($"for the feature submission context the 'CoverTypeCode' is '{submission.CoverTypeCode}'");

            if (submission.PeriodOfCover != null)
                Given($"for the feature submission context the 'PeriodOfCover' is '{submission.PeriodOfCover}'");

            if (submission.ContractTypeCode != null)
                Given($"for the feature submission context the 'ContractTypeCode' is '{submission.ContractTypeCode}'");

            Given("the feature submission is saved for the scenario");
        }

        [Given(@"I prepare the submission context with the following information")]
        public void CreateSubmissionContextOfSetData(Table submissiontable)
        {
            var rowSubmission = submissiontable.CreateSet<Submission>().First();

            if (_submissionContext == null)
                _submissionContext = new Submission();

            if (rowSubmission.BrokerUserEmailAddress != null)
                _submissionContext.BrokerUserEmailAddress = rowSubmission.BrokerUserEmailAddress;

            if (rowSubmission.ContractReference != null)
                _submissionContext.ContractReference = rowSubmission.ContractReference;

            if (rowSubmission.ProgrammeReference != null)
                _submissionContext.ProgrammeReference = rowSubmission.ProgrammeReference;

            if (rowSubmission.ClassOfBusinessCode != null)
                _submissionContext.ClassOfBusinessCode = rowSubmission.ClassOfBusinessCode;

            if (rowSubmission.ContractDescription != null)
                _submissionContext.ContractDescription = rowSubmission.ContractDescription;

            if (rowSubmission.CoverTypeCode != null)
                _submissionContext.CoverTypeCode = rowSubmission.CoverTypeCode;

            if (rowSubmission.PeriodOfCover != null)
                _submissionContext.PeriodOfCover = rowSubmission.PeriodOfCover;

            if (rowSubmission.ContractTypeCode != null)
                _submissionContext.ContractTypeCode = rowSubmission.ContractTypeCode;

            if (rowSubmission.OriginalPolicyholder != null)
                _submissionContext.OriginalPolicyholder = rowSubmission.OriginalPolicyholder;
        }

        [Given(@"I POST a submission in the background on behalf of the broker '(.*)' with the following info")]
        public void GivenIPOSTASubmissionInTheBackgroundOnBehalfOfTheBrokerWithBrokerCodeAndDepartmentid(string brokerEmailAddress,
            Table table)
        {
            var submissions = table.CreateSet<SubmissionCreate>();

            try
            {
                foreach (var q in submissions)
                {
                    CommonSteps.GivenISetSecurityTokenAndCertificateForTheUser(brokerEmailAddress);
                    SubmissionSteps.GivenISetTheFeatureSubmissionContextFromTheJsonPayloadData_ModelSubmission__Json(@"Data\1_4_Model\Submission_1.json");
                    SubmissionSteps.GivenISetTheUserEmailAddressForTheFeatureUser(brokerEmailAddress);
                    SubmissionSteps.GivenISetTheBrokerCodeForTheFeatureUserAs(Convert.ToInt32(q.BrokerCode));
                    SubmissionSteps.GivenForTheFeatureSubmissionContextAddTheBrokerData();
                    SubmissionSteps.GivenForTheSubmissionTheIsPlusARandomString("SubmissionReference", $"{q.SubmissionReference}");
                    SubmissionSteps.GivenForTheSubmissionTheIsPlusARandomString("ContractReference", $"{q.ContractReference}");
                    AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "BrokerDepartmentId", q.BrokerDepartmentId);
                    AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ProgrammeReference", $"{q.ProgrammeReference}");
                    AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ClassOfBusinessCode", $"{q.ClassOfBusinessCode}");
                    AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "TechnicianUserEmailAddress", brokerEmailAddress);
                    AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission, "ContractDescription", $"{q.ContractDescription}");
                    AbstractSteps.GivenTheFeatureSubmissionIsSavedForTheScenario(EntityEnum.Entity.submission);

                    _submissionContext = sContext[Constants.SubmissionContext] as Submission;
                    UserContext = sContext[Constants.UserContext] as User;

                    CommonSteps.WhenIMakeRequestToResource(Method.POST, "/submissions");
                }
            }
            catch (Exception ex)
            {
                throw new AssertionException($"An error occured trying to POST a submission : {ex.Message}");
            }

        }

        [Then(@"I delete all the submissions with '(.*)' is null")]
        public void DeleteAllTheSubmissionUnderwritersWithMinimumCreateddatetimeValue(string field)
        {
            for (int i = 0; i < 500; i++)
            {
                //And("I add a filter on '_order' with a value 'CreatedDateTime'");
                When("I make a GET request to '/submissions' resource and append the filter(s)");
                And("the response HTTP header ContentType should be to application/json type");
                And("the response body should contain a collection of submissions");

                List<Submission> nullOnes = new List<Submission>();

                // todo refactor this to assess the prop differently 
                if (field == "CreatedDateTime")
                    nullOnes = _submissionCollectionContext.Items.Where(x => x.CreatedDateTime == DateTime.MinValue).ToList();
                else if (field == "TechnicianUserEmailAddress")
                    nullOnes = _submissionCollectionContext.Items.Where(x => x.TechnicianUserEmailAddress == null).ToList();

                if (nullOnes.Any())
                {
                    if (field == "CreatedDateTime")
                        DeleteAllSubmissionUnderwritersWithCreateddatetimeIs(nullOnes);
                    else if (field == "TechnicianUserEmailAddress")
                        DeleteAllSubmissionUnderwritersWithNulltechnicianSueremailAddress(nullOnes);
                }
                else
                    break;
            }
        }

        [Then(@"I delete all the submission dialogues with '(.*)' is null")]
        public void DeleteAllTheSubmissionDialoguesWithIsNull(string p0)
        {
            for (int i = 0; i < 500; i++)
            {
                //And("I add a filter on '_order' with a value 'CreatedDateTime'");
                When("I make a GET request to '/SubmissionDialogues' resource");
                And("the response is a valid application/json; charset=utf-8 type of submission dialogue collection");

                // find the sendDocumentIds == null
                var targeted = _submissionDialogueCollectionContext.Items.Where(x => x.SenderDocumentIds == null).ToList();

                if (targeted.Any())
                {
                    foreach (var item in targeted)
                    {
                        if (item.SenderDocumentIds == null)
                        {
                            When($"I make a DELETE request to '/SubmissionDialogues/{item.SubmissionDialogueId}' resource");
                        }
                    }
                }
                else
                    break;
            }
        }

        public void DeleteAllSubmissionUnderwritersWithCreateddatetimeIs(List<Submission> submissions)
        {
            foreach (var item in submissions)
            {
                if (item.CreatedDateTime == DateTime.MinValue)
                {
                    When($"I make a DELETE request to '/submissions/{item.Id}' resource");
                }
            }
        }

        public void DeleteAllSubmissionUnderwritersWithNulltechnicianSueremailAddress(List<Submission> submissions)
        {
            foreach (var item in submissions)
            {
                if (item.TechnicianUserEmailAddress == null)
                {
                    When($"I make a DELETE request to '/submissions/{item.Id}' resource");
                }
            }
        }




    }
}

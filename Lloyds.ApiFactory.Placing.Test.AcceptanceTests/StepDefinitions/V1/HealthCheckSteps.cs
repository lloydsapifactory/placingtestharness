//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{

    [Binding]
    public class HealthCheckSteps : ApiBaseSteps
    {

    }
}

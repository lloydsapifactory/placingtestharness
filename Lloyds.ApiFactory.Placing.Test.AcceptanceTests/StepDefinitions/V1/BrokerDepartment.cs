//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using static Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums.EntityEnum;
using System.Linq;
using System.Collections.Generic;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class BrokerDepartment : ApiBaseSteps
    {
        private StateHolder _stateHolder;

        public BrokerDepartment(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
                : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _stateHolder = stateHolder;
        }


        [Given(@"count of broker departments returned will be = (.*)")]
        [When(@"count of broker departments returned will be = (.*)")]
        [Then(@"count of broker departments returned will be = (.*)")]
        public void ThenTheCountOfDepartmentsReturnedWillBe(int p0)
        {
            StepsExension.ValidateOkResponse(_apiTestContext.Response);

            int actualHttpStatusCode = (int)_apiTestContext.Response.StatusCode;

            Assert.AreEqual(_brokerDepartmentContext.Items.Length, p0);
        }

        [Given(@"count of broker departments returned will be >= (.*)")]
        [When(@"count of broker departments returned will be >= (.*)")]
        [Then(@"count of broker departments returned will be >= (.*)")]
        public void BrokerDepartmentsMoreThanOrEqual(int count)
        {
            Assert.GreaterOrEqual(_brokerDepartmentContext.Items.Length, count);
        }

        [Given(@"I store broker departments")]
        [When(@"I store broker departments")]
        [Then(@"I store broker departments")]
        public void ThenStoreBrokerDepartments()
        {
            _brokerDepartmentContext = JsonConvert.DeserializeObject<BrokerDepartmentCollectionWrapper>(_apiTestContext.Response.Content);
        }

        [Given(@"I store current broker department id")]
        public void StoreCurrentBrokerDepartmentId()
        {
            _stateHolder.CurrentBrokerDepartmentId = _brokerDepartmentContext.Items.First().BrokerDepartmentId;
        }

        [Given(@"I retrieve the broker departments for '(.*)'")]
        public void GivenIRetrieveTheBrokerDepartmentsFor(string brokerUserEmailAddress)
        {
            GetBrokerDepartmentAs(brokerUserEmailAddress);
        }

        [Given(@"I select broker that is in a different broker department with '(.*)'")]
        public void GivenISelectBrokerThatIsInADifferentDepartmentThan(string brokerUserEmailAddress)
        {
            if (_stateHolder.CurrentBrokerDepartmentId == null)
                Assert.Fail("havent stored CurrentBrokerDepartmentId.");

            var currentBrokerDepartmentId = _stateHolder.CurrentBrokerDepartmentId;

            GetBrokerDepartmentAs(brokerUserEmailAddress);

            var brokerDepartments = _brokerDepartmentContext.Items;

            foreach (var bd in brokerDepartments)
            {
                if (bd.BrokerDepartmentId != currentBrokerDepartmentId)
                {
                    var anotherBroker = bd.BrokerUsers.ToList().FirstOrDefault();

                    if (anotherBroker == null)
                        Assert.Fail("cant find another broker from a different department.");

                    _stateHolder.BrokerUserEmailAddress = anotherBroker.BrokerUserEmailAddress;
                }
            }
        }

        [Given(@"I select broker that is in the same department with '(.*)'")]
        public void SelectBrokerFromSameDepartment(string brokerUserEmailAddress)
        {
            GetBrokerDepartmentAs(brokerUserEmailAddress);

            var brokerDepartments = _brokerDepartmentContext.Items;

            foreach (var bd in brokerDepartments)
            {

                foreach (var brokerUser in bd.BrokerUsers)
                {
                    if (!brokerUser.BrokerUserEmailAddress.Equals(brokerUserEmailAddress, System.StringComparison.OrdinalIgnoreCase))
                    {
                        _stateHolder.BrokerUserEmailAddress = brokerUser.BrokerUserEmailAddress;
                    }
                }

            }
        }

        [Given(@"I select broker that is in the same department with the stored broker")]
        public void SelectBrokerFromSameDepartmentStoredBroker()
        {
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;
            GetBrokerDepartmentAs(brokerUserEmailAddress);

            var brokerDepartments = _brokerDepartmentContext.Items;

            foreach (var bd in brokerDepartments)
            {
                foreach (var brokerUser in bd.BrokerUsers)
                {
                    if (!brokerUser.BrokerUserEmailAddress.Equals(brokerUserEmailAddress, System.StringComparison.OrdinalIgnoreCase))
                    {
                        _stateHolder.BrokerUserEmailAddress = brokerUser.BrokerUserEmailAddress;
                    }
                }
            }

            if (brokerUserEmailAddress.Equals(_stateHolder.BrokerUserEmailAddress, System.StringComparison.OrdinalIgnoreCase))
                Assert.Fail("Couldnt find another broker from the same company and department.");
        }

        [Given(@"I select broker that is in a different broker department with the stored broker")]
        public void SelectBrokerThatIsInADifferentDepartmentThanStored()
        {
            var brokerUserEmailAddress = _stateHolder.BrokerUserEmailAddress;

            if (_stateHolder.CurrentBrokerDepartmentId == null)
                Assert.Fail("havent stored CurrentBrokerDepartmentId.");

            var currentBrokerDepartmentId = _stateHolder.CurrentBrokerDepartmentId;

            GetBrokerDepartmentAs(brokerUserEmailAddress);

            var brokerDepartments = _brokerDepartmentContext.Items;

            foreach (var bd in brokerDepartments)
            {
                if (bd.BrokerDepartmentId != currentBrokerDepartmentId)
                {
                    var anotherBroker = bd.BrokerUsers.ToList().FirstOrDefault();

                    if (anotherBroker == null)
                        Assert.Fail("cant find another broker from a different department.");

                    _stateHolder.BrokerUserEmailAddress = anotherBroker.BrokerUserEmailAddress;
                }
            }
        }

        [Then(@"collection of (.*) belong to the saved broker or another broker in the same department")]
        public void ThenSubmissionsBelongToTheSavedBrokerOrAnotherBrokerInTheSameDepartment(Entity entity)
        {
            var array = SetArrayCollection(entity);

            GivenIRetrieveTheBrokerDepartmentsFor(_stateHolder.BrokerUserEmailAddress);

            var brokerDepartments = _brokerDepartmentContext.Items;

            SubmissionExtension.ValidateSubmissionsForSameDepartment(array, brokerDepartments, _stateHolder.BrokerUserEmailAddress);
        }

        [Given(@"I select (.*) broker department id")]
        public void SelectInvalidBrokerDepartmentId(string status)
        {
            string invalidBrokerDepartmentId = "666";

            if (status == "invalid")
            {
                if (!_brokerDepartmentContext.Items.Any(x => x.BrokerDepartmentId == invalidBrokerDepartmentId))
                    sContext[Constants.BrokerDepartmentId] = invalidBrokerDepartmentId;
            }
            else
            {
                sContext[Constants.BrokerDepartmentId] = _brokerDepartmentContext.Items.First().BrokerDepartmentId;
            }
        }

    }


}

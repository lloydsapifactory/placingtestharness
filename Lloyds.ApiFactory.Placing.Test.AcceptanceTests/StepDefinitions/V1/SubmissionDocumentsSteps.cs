//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using BoDi;
using FluentAssertions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers.Enums;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.Test.Common.Extensions;
using Lloyds.ApiFactory.Placing.Test.Common.Helper;
using Lloyds.ApiFactory.Placing.Test.Common.Model;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class SubmissionDocumentsSteps : ApiBaseSteps
    {
        private StateHolder _stateHolder;
        private CommonSteps CommonSteps;
        private AbstractSteps AbstractSteps;
        private SubmissionPUT SubmissionPUTSteps;
        private SubmissionStepsIII SubmissionStepsIII;
        private CommonSteps _commonSteps;
        private FeatureContext _featureContext;

        public SubmissionDocumentsSteps(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
                : base(objectContainer, apiTestContext, scenarioContext, featureContext)
        {
            _featureContext = featureContext;
            _stateHolder = stateHolder;
            CommonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            AbstractSteps = new AbstractSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionPUTSteps = new SubmissionPUT(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            SubmissionStepsIII = new SubmissionStepsIII(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            _commonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
        }

        [Given(@"I POST a submission on behalf of the broker '(.*)' with a submission document from the following data")]
        [Given(@"I POST a submission on behalf of the broker '(.*)' with the following submission documents")]
        public void CreateSubmissionOfSetData(string brokerEmailAddress, 
            IEnumerable<SubmissionDocumentCreateModel> submissionDocuments)
        {
            var submission = new Submission(){ BrokerUserEmailAddress = brokerEmailAddress };
            GetBrokerDepartmentAs(brokerEmailAddress);
            var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

            // POST submission
            POSTSubmissionFromModel(submissionDataToInsert);

            foreach (var submissionDocs in submissionDocuments)
            {
                PostSubmissionDocument(submissionDocs);
            }
        }

        [Given(@"I POST (.*) submission with different version on behalf of the broker '(.*)' with a submission document from the following data")]
        [Given(@"I POST (.*) submission with different version on behalf of the broker '(.*)' with the following submission documents")]
        public void CreateSubmissionOfSetDataWithDifferentVersion(int count, 
            string brokerEmailAddress,
            IEnumerable<SubmissionDocumentCreateModel> submissionDocuments)
        {
            var submission = new Submission() { BrokerUserEmailAddress = brokerEmailAddress };
            GetBrokerDepartmentAs(brokerEmailAddress);
            var submissionDataToInsert = PrepareSubmissionData(submission, brokerEmailAddress);

            Given("generate a random string of 5 characters and save it to the context");

            for (int i = 0; i < count; i++)
            {
                POSTSubmissionFromModel(submissionDataToInsert);

                foreach (var submissionDocs in submissionDocuments)
                {
                    PostSubmissionDocument(submissionDocs);
                }
            }
        }

        [Given(@"I POST a submission on behalf of the broker '(.*)' from the following data")]
        public void CreatedSubmissionOfSetData(string brokerEmailAddress,
            IEnumerable<Model.SubmissionDocumentCreateModel> submissionDocuments)
        {
            var submission = SubmissionDocumentExtension.MapDocumentCreateModelToSubmission(brokerEmailAddress, submissionDocuments);

            // POST submission
            POSTSubmissionFromModel(submission);
        }

        [Given(@"I set the SubmissionReference")]
        [When(@"I set the SubmissionReference")]
        [Then(@"I set the SubmissionReference")]
        public void SetTheSubmissionReference()
        {
            if (!sContext.ContainsKey(Constants.SubmissionUniqueReference))
                sContext.Add(Constants.SubmissionUniqueReference, "");

            sContext[Constants.SubmissionUniqueReference] = _submissionContext != null ? _submissionContext.SubmissionReference : null; ;
        }

        [Given(@"I set the SubmissionDocumentId")]
        [When(@"I set the SubmissionDocumentId")]
        [Then(@"I set the SubmissionDocumentId")]
        public void SetTheSubmissionDocumentId()
        {
            if (!sContext.ContainsKey(Constants.SubmissionDocumentId))
                sContext.Add(Constants.SubmissionDocumentId, "");

            sContext[Constants.SubmissionDocumentId] = _submissionDocumentContext != null ? _submissionDocumentContext.Id : _stateHolder.CurrentSubmissionDocument.Id; ;//;
        }

        [Given(@"I refresh the SubmissionDocumentcontext with the response content")]
        [When(@"I refresh the SubmissionDocumentcontext with the response content")]
        [Then(@"I refresh the SubmissionDocumentcontext with the response content")]
        public void RefreshTheSubmissioncontext()
        {
            StepsExension.ValidateOkCreatedResponse(_apiTestContext.Response);

            _submissionDocumentContext = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);
        }
               
        [Given(@"I save document submission update information")]
        [When(@"I save document submission update information")]
        [Then(@"I save document submission update information")]
        public void SaveDocumentSubmissionUpdatecontextInfo()
        {
            SetTheSubmissionDocumentId();
            GivenTheDocumentSubmissionIsSavedForTheScenario();
        }

        [Given(@"I save document submission update information for (.*)")]
        [When(@"I save document submission update information for (.*)")]
        [Then(@"I save document submission update information for (.*)")]
        public void SaveDocumentSubmissionUpdatecontextInfoAction(Method method)
        {
            SaveDocumentSubmissionUpdatecontextInfo();
        }

        [Given(@"I save the submission document response to the submissiondocumentcontext")]
        [When(@"I save the submission document response to the submissiondocumentcontext")]
        [Then(@"I save the submission document response to the submissiondocumentcontext")]
        public void SaveTheSubmissionResponseToTheSubmissioncontext()
        {
            _submissionDocumentContext = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);
        }

        [Given(@"I save the submission documents to be asserted later")]
        [When(@"I save the submission documents to be asserted later")]
        [Then(@"I save the submission documents to be asserted later")]
        public void SaveSubmissionDocuments()
        {
            _stateHolder.CurrentSubmissionDocuments.Add(_submissionDocumentContext);
        }

        [Then(@"submission documents should have at least (.*) document")]
        public void ThenCollectionOfSubmissionsShouldHaveAtLeastDocument(int documentCount)
        {
            if (_submissionDocumentCollectionContext is null)
                throw new AssertionException("The submission documents collection data is empty.");
            Assert.IsTrue(_submissionDocumentCollectionContext.Items.Length >= documentCount, $"Expected number of documents is {documentCount}. Actual is {_submissionDocumentCollectionContext.Items.Length}.");
        }

        
        [When(@"submission documents should have (.*) document")]
        [Then(@"submission documents should have (.*) document")]
        [Then(@"collection of submission documents should have (.*) document")]
        [Then(@"collection of submission documents should have (.*) documents")]
        public void ThenCollectionOfSubmissionsShouldHaveDocument(int documentCount)
        {
            if (_submissionDocumentCollectionContext is null)
                throw new AssertionException("The submission documents collection data is empty.");

            var sameDocuments = _submissionDocumentCollectionContext.Items.Length == documentCount;

            if (!sameDocuments)
            Assert.Fail($"Expected documents:{documentCount} " +
                $"but found:{_submissionDocumentCollectionContext.Items.Length}");
        }

        [Given(@"submission documents should have at least (.*) link")]
        [When(@"submission documents should have at least (.*) link")]
        [Then(@"submission documents should have at least (.*) link")]
        public void CollectionOfSubmissionsShouldHaveAtLeastLink(int linkount)
        {
            if (_submissionDocumentCollectionContext is null)
                throw new AssertionException("The submission documents collection data is empty.");
            Assert.IsTrue(_submissionDocumentCollectionContext.Links.Count >= linkount);
        }

        [Given(@"collection of submission documents should have (.*) placing document")]
        [When(@"collection of submission documents should have (.*) placing document")]
        [Then(@"collection of submission documents should have (.*) placing document")]
        public void CollectionOfSubmissionsShouldHavePlacingDocument(int documentCount)
        {
            if (_submissionDocumentCollectionContext is null)
                throw new AssertionException("The submission documents collection data is empty.");

            Assert.IsTrue(_submissionDocumentCollectionContext.Items.Where(x=>x.DocumentType == "document_placing_slip").ToList().Count == documentCount);
        }

        [Given(@"validate that the submission docments have been added")]
        [When(@"validate that the submission docments have been added")]
        [Then(@"validate that the submission docments have been added")]
        public void ValidateThatThesubmissionDocmentsHaveBeenAdded()
        {
            var added = _stateHolder.CurrentSubmissionDocuments.ToList();
            var found = false;

            foreach(var qd in added)
            {
                if (_submissionDocumentCollectionContext.Items.Any(x => x.SubmissionReference == qd.SubmissionReference))
                {
                    found = true;
                    break;
                }
            }

            Assert.IsTrue(found);
        }


        [Given(@"collection of submission documents should have at least (.*) document")]
        [When(@"collection of submission documents should have at least (.*) document")]
        [Then(@"collection of submission documents should have at least (.*) document")]
        public void CollectionOfSubmissionsShouldHaveAtLEastDocument(int documentCount)
        {
            if (_submissionDocumentCollectionContext is null)
                throw new AssertionException("The submission documents collection data is empty.");
            Assert.IsTrue(_submissionDocumentCollectionContext.Links.Count >= documentCount);
        }


        [Given(@"the feature submission document is saved for the scenario")]
        public void GivenTheDocumentSubmissionIsSavedForTheScenario()
        {
            var submissiondocumentrequest = JsonConvert.SerializeObject(_submissionDocumentContext, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            Console.WriteLine(submissiondocumentrequest.JsonPrettify());
            _apiTestContext.AddContent<SubmissionDocument>(_submissionDocumentContext, MimeEnumType.Json);
            _stateHolder.JsonRequestBody = submissiondocumentrequest;
        }


        [Then(@"validate the results of (.*) with (.*)")]
        public void ThenValidateTheResultsOfDocumentIdWith(string field, string value)
        {
            if (_submissionDocumentCollectionContext.Items != null)
            {
                foreach (var item in _submissionDocumentCollectionContext.Items)
                {
                    var valueAssert = ReflectionHelper.GetPropertyValue(item, field.Trim());

                    Assert.IsTrue(valueAssert.ToString().Contains(value));
                }
            }
            else
                throw new AssertionException("The submission documents collection data is empty.");
        }


        [Given(@"I get a random submission document from the collection")]
        public void GivenIGetARandomSubmissionDocumentFromTheCollection()
        {
            var random = new Random();
            if (_submissionDocumentCollectionContext.Items != null)
            {
                int index = random.Next(_submissionDocumentCollectionContext.Items.Length);

                _submissionDocumentContext = _submissionDocumentCollectionContext.Items[index];
                _submissionDocumentContext.Id = _submissionDocumentCollectionContext.Items[index].Id;

                SetTheSubmissionDocumentId();
                SubmissionDocumentIsSavedForTheRequest();
            }
            else
                Assert.Fail("Couldnt find any submission documents.");
        }

       


        [Given(@"I get a random submission document from the added")]
        [When(@"I get a random submission document from the added")]
        [Then(@"I get a random submission document from the added")]
        public void GetARandomSubmissionDocumentFromTheAdded()
        {
            var random = new Random();
            if (_stateHolder.CurrentSubmissionDocuments.Any())
            {
                int index = random.Next(_stateHolder.CurrentSubmissionDocuments.Count());

                _submissionDocumentContext = _stateHolder.CurrentSubmissionDocuments[index];
                _submissionDocumentContext.Id = _stateHolder.CurrentSubmissionDocuments[index].Id;

                SetTheSubmissionDocumentId();
                SubmissionDocumentIsSavedForTheRequest();
            }
        }

        [Given(@"I get a random submission")]
        [When(@"I get a random submission")]
        [Then(@"I get a random submission")]
        public void GetARandomSubmission()
        {
            var random = new Random();
            if (_submissionCollectionContext.Items.Any())
            {
                int index = random.Next(_submissionCollectionContext.Items.Count());

                _submissionContext = _submissionCollectionContext.Items[index];
                _submissionContext.Id = _submissionCollectionContext.Items[index].Id;

                SetTheSubmissionReference();
            }
        }

        [Given(@"I get a random submission that contains submission documents")]
        [When(@"I get a random submission that contains submission documents")]
        [Then(@"I get a random submission that contains submission documents")]
        public void RetrieveARandomSubmissionThatHasSubmissionDocs()
        {
            if (_submissionCollectionContext.Items.Any())
                FindSubmissionsWithDocs();
            else
                Assert.Fail("Couldnt find any submissions with documents.");
        }

        [Given(@"I get a random submission that contains submission documents with more than (.*) documents")]
        public void GetARandomSubmissionThatHasSubmissionDocs(int count)
        {
            if (_submissionCollectionContext.Items.Any())
                FindSubmissionsWithDocs(count);
            else
                Assert.Fail($"Couldnt find any submissions with more than {count} documents.");
        }        

        [Given(@"I get a submission document from the returned")]
        public void GivenIGetARandomSubmissionDocumentFromTheReturned()
        {
            var random = new Random();
            if (_submissionDocumentCollectionContext.Items.Any())
            {
                int index = random.Next(_submissionDocumentCollectionContext.Items.Count());

                _submissionDocumentContext = _submissionDocumentCollectionContext.Items[index];
                _submissionDocumentContext.Id = _submissionDocumentCollectionContext.Items[index].Id;

                SetTheSubmissionDocumentId();
                SubmissionDocumentIsSavedForTheRequest();
            }
        }


        [Then(@"the submission document response should contain the '(.*)' set to '(.*)'")]
        public void ThenTheResponseBodyShouldContainTheSetTo(string field, string value)
        {
            RefreshTheSubmissioncontext();

            var valueAssert = ReflectionHelper.GetPropertyValue(_submissionDocumentContext, field.Trim());

            valueAssert.ToString().Should().Contain(value);
        }

        [Then(@"the submission document response should contain the '(.*)' set to flag (.*)")]
        public void ThenTheResponseBodyShouldContainTheSetTo(string field, bool value)
        {
            RefreshTheSubmissioncontext();

            var valueAssert = (bool) ReflectionHelper.GetPropertyValue(_submissionDocumentContext, field.Trim());

            valueAssert.Should().Be(value);
        }

        [Given(@"I store a random submission document from the response")]
        public void ThenStoreRandomDocument()
        {
            RefreshTheSubmissioncontext();
        }

        [Given(@"I pick a random submission document from the response")]
        public void GivenIPickARandomSubmissionDocumentFromTheResponse()
        {
            if (_submissionDocumentCollectionContext.Items.Length > 0)
            {
                var rand = new Random();
                var index = rand.Next(_submissionDocumentCollectionContext.Items.Length);
                var randomDoc = _submissionDocumentCollectionContext.Items[index];
                _stateHolder.CurrentSubmissionDocumentCollection = _submissionDocumentCollectionContext;
                _stateHolder.CurrentSubmissionDocument = randomDoc;
                _submissionDocumentContext = randomDoc;

                SetTheSubmissionDocumentId();
            }           
        }

        [Given(@"'(.*)' is null")]
        public void GivenIsNull(string field)
        {
            if ((_submissionDocumentContext == null) || !(_submissionDocumentContext is SubmissionDocument))
                throw new AssertionException("The scenario cannot find the submission object.");

            var submissionproperty = _submissionDocumentContext.GetType().GetProperty(field);

            if (submissionproperty is null)
                throw new AssertionException($"The submission document scenario cannot find the property {field}");
                        
            submissionproperty.SetValue(_submissionDocumentContext, null);
        }

        [Given(@"I set an invalid request body")]
        public void GivenISetAnInvalidRequestBody()
        {
            _submissionDocumentContext.DocumentId = _submissionDocumentContext.DocumentId + "111";
            _submissionDocumentContext.Id = _submissionDocumentContext.Id + "111";
        }

        [Given(@"I add a filter on '(.*)' with relevant value")]
        public void GivenIAddAFilterOnWithAValue(string fieldname)
        {
            var fieldvalue = "";

            fieldvalue = Convert.ToString(ReflectionHelper.GetPropertyValue(_submissionDocumentContext, fieldname));

            // when you inject the item stored within the scenariocontext
            if (fieldvalue.StartsWith("$#"))
            {
                var sContextFieldName = fieldvalue.Substring(2);
                if (!sContext.ContainsKey(sContextFieldName)) throw new AssertionException($"Unable to find the ScenarioContext property {fieldvalue}");
                fieldvalue = sContext[sContextFieldName].ToString();

                _stateHolder.ParamsBuilderValueToValidate = fieldvalue.ToString();
            }

            Given($"I add a '' filter on '{fieldname}' with a value '{fieldvalue}'");
        }

        
        [Then(@"the Id should match with the DocumentId for the collection")]
        public void ThenTheIdShouldMatchWithTheDocumentIdCollection()
        {
            if (_submissionCollectionContext.Items.Length > 0)
            {
                foreach(var item in _submissionDocumentCollectionContext.Items)
                {
                    Assert.IsTrue(item.Id == item.DocumentId, $"Id and DocumentId do not match for the item with submission reference:{item.SubmissionReference}");
                }
            }
        }

        [Given(@"I will save the documentId from the response")]
        [Given(@"I will store the documentId")]
        public void GivenIWillSaveTheDocumentIdFromTheResponse()
        {
            var submissiondocumentresponse = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);
            _stateHolder.DocumentId = submissiondocumentresponse.DocumentId;
        }

        [Given(@"for the submission documents the '(.*)' is stored by the previous document context")]
        public void GivenForTheSubmissionDocumentsTheIsStoredByThePreviousDocumentContext(string p0)
        {
            _submissionDocumentContext.ReplacingDocumentId = _stateHolder.DocumentId;
        }

        [Given(@"for the submission document context the '(.*)' is SubmissionReferenceforSubmissionDocument")]
        public void ForTheSubmissionDocumentContextTheIsSubmissionUniqueReference(string propertyName)
        {
            var propertyvalue = sContext[Constants.SubmissionReferenceforSubmissionDocument].ToString();

            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, propertyName, propertyvalue);

            propertyvalue = $"{propertyvalue}_{_submissionDocumentContext.SubmissionVersionNumber}";
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "DocumentId", propertyvalue);
        }

        [Given(@"for the submission document context the '(.*)' is an invalid SubmissionReferenceforSubmissionDocument")]
        public void TheIsSubmissionUniqueReferenceIsInvalid(string propertyName)
        {
            sContext[Constants.SubmissionReferenceforSubmissionDocument] += "666";
            ForTheSubmissionDocumentContextTheIsSubmissionUniqueReference(propertyName);
        }

        public void PostSubmissionDocument(SubmissionDocumentCreateModel submissionDocumentCreate)
        {
            var fnSize = submissionDocumentCreate.FileSizeInBytes;
            submissionDocumentCreate.FileSizeInBytes = Convert.ToInt32(fnSize);

            // POST submission DOCUMENT
            try
            {
                SetTheSubmissionDocumentContextFromTheJsonPayload(submissionDocumentCreate.JsonPayload);
                
                if (submissionDocumentCreate.SubmissionVersionNumber.HasValue)
                    AbstractSteps.TheSubmissionDocumentContextTheIsInteger(EntityEnum.Entity.submission_documents, "SubmissionVersionNumber", submissionDocumentCreate.SubmissionVersionNumber.Value);

                SetFeatureSteps(submissionDocumentCreate);
                CommonSteps.WhenIPOSTToResourceWithMultipart("/SubmissionDocuments");

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    CommonSteps.ThenPrintTheErrorMessageToTheOutputWindow();
                    throw new AssertionException($"A Bad Request (400) is received when POST a SubmissionDocument for the SubmissionReference {submissionDocumentCreate.SubmissionReference}. " +
                        $"Error content: {_apiTestContext.Response.Content.JsonPrettify()}");
                }

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    throw new AssertionException($"An Internal server error (500) is received when POST a SubmissionDocument." +
                    $"Error content: {_apiTestContext.Response.Content.JsonPrettify()}");
                }

                SaveTheSubmissionResponseToTheSubmissioncontext();
                SaveSubmissionDocuments();
            }
            finally
            {
                _apiTestContext.ClearDown();
            }
        }

        public void PrepareSubmissionDocument(SubmissionDocumentCreateModel submissionDocumentCreate)
        {
            if (!submissionDocumentCreate.FileSizeInBytes.HasValue)
            {
                var fnSize = submissionDocumentCreate.FileSizeInBytes;
                submissionDocumentCreate.FileSizeInBytes = Convert.ToInt32(fnSize);
            }

            SetTheSubmissionDocumentContextFromTheJsonPayload(submissionDocumentCreate.JsonPayload);

            if (submissionDocumentCreate.SubmissionVersionNumber.HasValue)
                AbstractSteps.TheSubmissionDocumentContextTheIsInteger(EntityEnum.Entity.submission_documents, "SubmissionVersionNumber", submissionDocumentCreate.SubmissionVersionNumber.Value);

            SetFeatureSteps(submissionDocumentCreate);
        }

        private void SetFeatureSteps(SubmissionDocumentCreateModel submissionDocumentCreate)
        {
            ForTheSubmissionDocumentContextTheIsSubmissionUniqueReference("SubmissionReference");
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "FileName", submissionDocumentCreate.FileName);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "FileMIMEType", submissionDocumentCreate.FileMIMEType);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "DocumentType", submissionDocumentCreate.DocumentType);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "DocumentDescription", submissionDocumentCreate.DocumentDescription);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "ShareableToAllMarketsFlag", (submissionDocumentCreate.ShareableToAllMarketsFlag ?? false).ToString());
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "ReplacingDocumentId", submissionDocumentCreate.ReplacingDocumentId);
            AbstractSteps.ForTheFeatureContextTheIsString(EntityEnum.Entity.submission_documents, "SuppliedBy", submissionDocumentCreate.SuppliedBy.ToString());
            AbstractSteps.TheSubmissionDocumentContextTheIsInteger(EntityEnum.Entity.submission_documents, "FileSizeInBytes", submissionDocumentCreate.FileSizeInBytes.Value);
            AbstractSteps.GivenIAttachTheFileToTheRequest(submissionDocumentCreate.File);

            SubmissionDocumentIsSavedForTheRequest();
        }


        [Given(@"the submission document is saved for the scenario")]
        [When(@"the submission document is saved for the scenario")]
        [Then(@"the submission document is saved for the scenario")]
        public void SubmissionDocumentIsSavedForTheRequest()
        {
            var submissiondocumentrequest = JsonConvert.SerializeObject(_submissionDocumentContext, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            
            Console.WriteLine(submissiondocumentrequest.JsonPrettify());
            _apiTestContext.AddParameterToRequestBody("metadata", submissiondocumentrequest);
            _stateHolder.JsonRequestBody = submissiondocumentrequest;
        }

        [Given(@"I set the submission document context from the json payload (.*)")]
        public void SetTheSubmissionDocumentContextFromTheJsonPayload(string filePath)
        {
            AbstractSteps.TestContextClearDown();
            var path = Path.Combine(Environment.CurrentDirectory, filePath);
            var submissionDocumentContext = FileHelper.ReadFile<SubmissionDocument>(path);
            _submissionDocumentContext = submissionDocumentContext ?? throw new AssertionException("Unable to create a submission document resource from the input json file.");
        }

        private void POSTSubmissionFromModel(Submission submission)
        {
            try
            {
                SubmissionPUTSteps.POSTSubmissionOnBehalfOfTheBrokerWithBrokerCodeAndDepartmentid(submission.BrokerUserEmailAddress);

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {   CommonSteps.ThenPrintTheErrorMessageToTheOutputWindow();
                    throw new AssertionException($"A Bad Request (400) is received when POST a Submission.");
                }

                if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {   throw new AssertionException($"An Internal server error (500) is received when POST a Submission.");
                }

                if ((_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.RequestTimeout) ||
                   ((int)_apiTestContext.Response.StatusCode == 0))
                    throw new AssertionException($"A timeout / no response received has occured when trying to upload documents for {sContext[Constants.SubmissionUniqueReference].ToString()}. ");
                SubmissionStepsIII.GivenISaveTheSubmissionResponseToTheSubmissioncontext();
                SubmissionStepsIII.GivenISetTheSubmissionReference();
            }
            finally
            {
                _apiTestContext.ClearDown();
            }
        }


        [Given(@"replace the SubmissionDocument context with the following document for the logged in underwriter")]
        public void GivenReplaceTheSubmissionDocumentContextWithTheFollowingDocumentForTheLoggedInUnderwriter(Table table)
        {
            foreach (var docToReplaceRow in table.Rows)
            {
                var documentToReplace = _stateHolder.CurrentSubmissionDocuments
                    .FirstOrDefault(x => x.FileName.Equals(docToReplaceRow["FileName"].ToString(),
                    StringComparison.OrdinalIgnoreCase));

                if (documentToReplace is null) throw new AssertionException("");

                var replaceSubmissionDoc = new SubmissionDocumentCreateModel()
                {
                    DocumentType = documentToReplace.DocumentType,
                    DocumentDescription = documentToReplace.DocumentDescription,
                    SubmissionReference = documentToReplace.SubmissionReference,
                    SubmissionVersionNumber = documentToReplace.SubmissionVersionNumber,
                    ReplacingDocumentId = documentToReplace.DocumentId,
                    JsonPayload = @"Data\1_2_Model\SubmissionDocument_1.json",
                    SuppliedBy = ParticipantRole.U,
                    ShareableToAllMarketsFlag = documentToReplace.ShareableToAllMarketsFlag,
                    File = docToReplaceRow["File"].ToString(),
                    FileMIMEType = docToReplaceRow["FileMimeType"].ToString(),
                    FileName = docToReplaceRow["ReplaceWith"].ToString()
                };

                PostSubmissionDocument(replaceSubmissionDoc);
                //RefreshTheSubmissioncontext();
            }            
        }

        [Then(@"the collection of submission documents contains (.*) document where the ReplacingDocumentId has a value")]
        public void ThenTheCollectionOfSubmissionDocumentsContainsDocumentWhereTheReplacingDocumentIdHasAValue(int p0)
        {
            var countofQDWithReplacingDocId = _submissionDocumentCollectionContext.Items.Count(a => !string.IsNullOrEmpty(a.ReplacingDocumentId));
            Assert.IsTrue(countofQDWithReplacingDocId == p0,
                "Expected {0} documents where the replacing document id is set. Actual documents found {1}.", p0, countofQDWithReplacingDocId);
        }

        [Given(@"the document '(.*)' should contain a valid ReplacingDocumentId")]
        [Then(@"the document '(.*)' should contain a valid ReplacingDocumentId")]
        public void ThenTheDocumentShouldContainAValidReplacingDocumentId(string p0)
        {
            Assert.IsFalse(string.IsNullOrEmpty(_submissionDocumentContext.ReplacingDocumentId), "The document response does not have a replacing documentid.");
        }


        [Then(@"the document '(.*)' should replace '(.*)'")]
        public void ThenTheDocumentShouldReplace(string p0, string p1)
        {
            if (_submissionDocumentContext is null) throw new AssertionException("Unable to find the uploaded document");

            var originalDoc = _stateHolder.CurrentSubmissionDocuments.FirstOrDefault(d => d.DocumentId.Equals(_submissionDocumentContext.ReplacingDocumentId, StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(originalDoc.FileName.Equals(p1, StringComparison.OrdinalIgnoreCase), $"The document {p0} does not hold a reference to the original document {p1}.");
        }

        [Given(@"for the submission document I set an invalid DocumentId")]
        public void SetInvalidDocumentId()
        {
            _submissionDocumentContext.DocumentId += "666";
            _submissionDocumentContext.Id += "666";
        }

        [Given(@"I save SubmissionDocument update information for PUT")]
        public void GivenISaveSubmissionDocumentUpdateInformationForPUT()
        {
            SetTheSubmissionDocumentId();

            var submissiondocumentrequest = JsonConvert.SerializeObject(_submissionDocumentContext, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            var submissionDocumentUpdateRequest = new SubmissionDocumentUpdateModel()
            { DocumentDescription = _submissionDocumentContext.DocumentDescription,
                DocumentType = _submissionDocumentContext.DocumentType,
                DocumentId = _submissionDocumentContext.DocumentId,
                ShareableToAllMarketsFlag = _submissionDocumentContext.ShareableToAllMarketsFlag ?? false,
                SubmissionReference = _submissionDocumentContext.SubmissionReference,
                SubmissionVersionNumber = _submissionDocumentContext.SubmissionVersionNumber is null ? string.Empty : _submissionDocumentContext.SubmissionVersionNumber.ToString()
            };

            var jsonRequest = JsonConvert.SerializeObject(submissionDocumentUpdateRequest, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            Console.WriteLine(jsonRequest.JsonPrettify());

            _apiTestContext.AddContent<SubmissionDocumentUpdateModel>(submissionDocumentUpdateRequest, MimeEnumType.Json);
            _stateHolder.JsonRequestBody = jsonRequest;
        }

        [Then(@"the request submission document context matches the response submission data")]
        public void ThenTheRequestSubmissionDocumentContextMatchesTheResponseSubmissionData()
        {
            var submissiondocumentresponse = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);
            Assert.AreEqual(_submissionDocumentContext, submissiondocumentresponse, "The response and request objects are not equal.");
        }

        [Given(@"I POST a set of submission documents from the data")]
        [When(@"I POST a set of submission documents from the data")]
        public void IPOSTASetOfSubmissionDocumentsFromTheData(Table submissiondoctable)
        {
            var submissiondocumentsmodel = submissiondoctable.CreateSet<SubmissionDocumentModelOutput>();

            for (var iCounter = 0; iCounter < submissiondocumentsmodel.Count(); iCounter++)
            {
                _apiTestContext.ClearDown();
                var filepath = submissiondoctable.Rows[iCounter]["FilePath"].ToString();

                SubmissionDocument submissionDocument = submissiondocumentsmodel.ElementAt(iCounter);

                ForTheSubmissionDocumentContextTheIsSubmissionUniqueReference("SubmissionReference");
                var submissionedocdatatoinsert = SubmissionDocumentExtension.MapFieldsAbsentInHeaderDocument(_submissionDocumentContext, submissionDocument, submissiondoctable.Header.ToList());

                var path = Path.Combine(Environment.CurrentDirectory, filepath);
                _apiTestContext.AddContent(path, MimeEnumType.File);
                var fInfo = new FileInfo(path);
                submissionedocdatatoinsert.FileSizeInBytes = int.Parse(fInfo.Length.ToString());

                var submissiondocumentrequest = JsonConvert.SerializeObject(submissionedocdatatoinsert, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

                _apiTestContext.AddParameterToRequestBody("metadata", submissiondocumentrequest);

                _commonSteps.WhenIPOSTToResourceWithMultipart("/SubmissionDocuments");

                if ((_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.BadRequest))
                {
                    _commonSteps.ThenPrintTheErrorMessageToTheOutputWindow();
                }

                if ((_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.RequestTimeout) ||
                   ((int)_apiTestContext.Response.StatusCode == 0))
                    throw new AssertionException($"A timeout / no response received has occured when trying to upload documents for {sContext[Constants.SubmissionUniqueReference].ToString()}. ");

                submissiondocumentsmodel.ElementAt<SubmissionDocumentModelOutput>(iCounter).ActualOutput = (int)_apiTestContext.Response.StatusCode;
            }
            sContext[Constants.SubmissionDocumentModelCollection] = submissiondocumentsmodel;
        }

        [Given(@"set for POST a set of submission documents from the data")]
        [When(@"set for POST a set of submission documents from the data")]

        public void SetForPOSTASetOfSubmissionDocumentsFromTheData(Table submissiondoctable)
        {
            var submissiondocumentsmodel = submissiondoctable.CreateSet<SubmissionDocumentModelOutput>();

            for (var iCounter = 0; iCounter < submissiondocumentsmodel.Count(); iCounter++)
            {
                _apiTestContext.ClearDown();
                var filepath = submissiondoctable.Rows[iCounter]["FilePath"].ToString();

                SubmissionDocument submissionDocument = submissiondocumentsmodel.ElementAt(iCounter);

                ForTheSubmissionDocumentContextTheIsSubmissionUniqueReference("SubmissionReference");
                var submissionedocdatatoinsert = SubmissionDocumentExtension.MapFieldsAbsentInHeaderDocument(_submissionDocumentContext, submissionDocument, submissiondoctable.Header.ToList());

                var path = Path.Combine(Environment.CurrentDirectory, filepath);
                _apiTestContext.AddContent(path, MimeEnumType.File);
                var fInfo = new FileInfo(path);
                submissionedocdatatoinsert.FileSizeInBytes = int.Parse(fInfo.Length.ToString());

                var submissiondocumentrequest = JsonConvert.SerializeObject(submissionedocdatatoinsert, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

                _apiTestContext.AddParameterToRequestBody("metadata", submissiondocumentrequest);
            }
        }

        [Given(@"I set the current submission documents with the SenderDocumentIds")]
        public void GivenISetTheCurrentSubmissionDocumentsWithTheSenderDocumentIds()
        {
            _stateHolder.CurrentSubmissionDocuments = _stateHolder.CurrentSubmissionDocuments
                .Where(x => _stateHolder.CurrentSenderDocumentIds.Any(y=> y.Equals(x.DocumentId, 
                StringComparison.OrdinalIgnoreCase))).ToList();
        }


        [Then(@"the expected outputs should match the actual outputs")]
        public void ThenTheExpectedOutputsShouldMatchTheActualOutputs()
        {
            var submissiondocumentsmodel = sContext[Constants.SubmissionDocumentModelCollection] as List<SubmissionDocumentModelOutput>;
            Assert.Multiple(() =>
            {
                foreach (var submissionDocument in submissiondocumentsmodel)
                {
                    Assert.AreEqual(submissionDocument.ExpectedOutput, submissionDocument.ActualOutput, $"For the the file : {submissionDocument.FileName} the expected and actual outputs do not match.");
                }
            });
        }


        [When(@"I save the submission document id response to the submissiondocumentidecontext")]
        [Given(@"I save the submission document id response to the submissiondocumentidecontext")]
        public void SaveTheSubmissionDocumentIdResponseToTheSubmissionDocumentIdContext()
        {
            if (!sContext.ContainsKey(Constants.SubmissionDocumentId)) sContext.Add(Constants.SubmissionUniqueReference, "");
            if (_apiTestContext.Response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                return;
            }

            var submissionDocumentId = JsonConvert.DeserializeObject<SubmissionDocument>(_apiTestContext.Response.Content);
            sContext[Constants.SubmissionDocumentId] = submissionDocumentId.Id;
        }

        [Given(@"I prepare submission documents creation from (.*)")]
        public void PrepareSubmissionDocumentCreationStep(string jsonFile)
        {
            SubmissionStepsIII.GivenISaveTheSubmissionResponseToTheSubmissioncontext();
            SubmissionStepsIII.GivenISetTheSubmissionReference();
            SetTheSubmissionDocumentContextFromTheJsonPayload(jsonFile);
        }

        [When(@"the Submission with '(.*)'='(.*)' has (.*) documents")]
        [Then(@"the Submission with '(.*)'='(.*)' has (.*) documents")]
        public void WhenTheSubmissionWithHasDocuments(string property, 
            int versionNumber, 
            int noOfDocuments)
        {
            Then("I reset all the filters");
            Then($"I add a filter on '{property}' with a value '{versionNumber}'");
            Then("I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource and append the filter(s)");
            Then("the response is a valid application/json; charset=utf-8 type of submission documents collection");
            Then($"submission documents should have {noOfDocuments} document");
        }

        [Given(@"get the submission document from version (.*)")]
        public void GivenGetTheSubmissionDocumentFromVersion(int versionNumber)
        {
            _submissionDocumentContext = _submissionDocumentCollectionContext.Items.Where(x => x.SubmissionVersionNumber == versionNumber).FirstOrDefault();
        }


        [Given(@"for the submission documents the SubmissionDocumentId is invalid")]
        public void SetSubmissionDocumentInvalid()
        {
            var newId = "";

            var invalidQMId = Extension.GenerateRandomStringNumbers(4);

            var parts = _submissionDocumentContext.Id.Split("_");
            var last = Enumerable.Last<string>(parts);
            var first = Enumerable.First<string>(parts);
            foreach (var p in parts)
            {
                if (p.Equals(first))
                {
                    newId += p + "_";
                }
                else if (p.Equals(last))
                {
                    newId += invalidQMId;
                }
                else
                {
                    newId += p + "_";
                }
            }

            _submissionDocumentContext.Id = newId;
            _submissionDocumentContext.DocumentId = invalidQMId;
        }

        [Given(@"I set a valid but not existing SubmissionDocumentId")]
        [When(@"I set a valid but not existing SubmissionDocumentId")]
        public void SetTheSubmissionUniqueReferenceForNonExistingId()
        {

            var malformedId = _submissionDocumentContext.Id + "2";
            sContext[Constants.SubmissionDocumentId] = malformedId;
        }

        [Given(@"I store the submission document with filename '(.*)'")]
        public void GivenIStoreTheSubmissionDocumentWithFilename(string filename)
        {
            var correctSubmissionDocument = _stateHolder.CurrentSubmissionDocuments.Where(x => x.FileName.Equals(filename,
                StringComparison.OrdinalIgnoreCase)).First();

            _submissionDocumentContext = correctSubmissionDocument;
        }

        private void FindSubmissionsWithDocs(int numberofDocs = 0)
        {
            var random = new Random();

            for (int i = 0; i < _submissionCollectionContext.Items.Count(); i++)
            {
                int index = random.Next(_submissionCollectionContext.Items.Count());

                _submissionContext = _submissionCollectionContext.Items[index];
                _submissionContext.Id = _submissionCollectionContext.Items[index].Id;

                SetTheSubmissionReference();
                Given("I make a GET request to '/SubmissionDocuments?SubmissionReference={submissionUniqueReference}' resource");
                Given("the response is a valid application/json; charset=utf-8 type of submission documents collection");

                //only when we find a submission with submissionDocuments
                if (_submissionDocumentCollectionContext.Items != null)
                {
                    if (_submissionDocumentCollectionContext.Items.Length > 0)
                        break;
                }
            }
        }
    }
}

//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using System;
using System.Linq;
using System.Text;
using BoDi;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Extensions;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Model;
using Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepHelpers;
using Lloyds.ApiFactory.Placing.Test.Common;
using Lloyds.ApiFactory.Placing.Test.Common.Context;
using Lloyds.ApiFactory.Placing.WebApi.Dto;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.StepDefinitions.V1
{
    [Binding]
    public class UnderwriterOrganisationGET : ApiBaseSteps
    {
        //private Dictionary<string, string> _sParamsBuilder;
        private CommonSteps _commonSteps;
        private StateHolder _stateHolder;

        public UnderwriterOrganisationGET(IObjectContainer objectContainer,
            ApiTestContext apiTestContext,
            ScenarioContext scenarioContext,
            FeatureContext featureContext,
            StateHolder stateHolder)
            : base(objectContainer, apiTestContext, scenarioContext)
        {
            _stateHolder = stateHolder;
            _commonSteps = new CommonSteps(objectContainer, apiTestContext, scenarioContext, featureContext, stateHolder);
            //_sParamsBuilder = new Dictionary<string, string>();
            if (!sContext.ContainsKey(Constants.UnderwriterOrganisationContext)) sContext.Add(Constants.UnderwriterOrganisationContext, new UnderwriterOrganisation());
            if (!sContext.ContainsKey(Constants.BrokerDepartmentContext)) sContext.Add(Constants.BrokerDepartmentContext, new BrokerDepartmentCollectionWrapper());
        }

        [Given(@"I Save Broker Departments")]
        public void ThenISaveBrokerDepartments()
        {
            StepsExension.ValidateOkCreatedResponse(_apiTestContext.Response);

            _brokerDepartmentContext = JsonConvert.DeserializeObject<BrokerDepartmentCollectionWrapper>(_apiTestContext.Response.Content);

            Assert.Greater(_brokerDepartmentContext.Total, 0, "No broker departments found the login user.");
        }

        [Given(@"I add a filter on '(.*)' with the first broker department")]
        [When(@"I add a filter on '(.*)' with the first broker department")]
        [Then(@"I add a filter on '(.*)' with the first broker department")]
        public void GivenIAddAFilterOnWithTheFirstBrokerDepartment(string fieldName)
        {
            _stateHolder._sParamsBuilder.Add(fieldName, _brokerDepartmentContext.Items.First().BrokerDepartmentId);
        }

        [Given(@"I add a filter on '(.*)' with the first broker department adding a whitespace")]
        [When(@"I add a filter on '(.*)' with the first broker department adding a whitespace")]
        [Then(@"I add a filter on '(.*)' with the first broker department adding a whitespace")]
        public void FilterBrokerDepartmentIdWithWhitespace(string fieldName)
        {
            _stateHolder._sParamsBuilder.Add(fieldName, _brokerDepartmentContext.Items.First().BrokerDepartmentId + "    ");
        }


        // this is a request filtering duplicate. I couldnt remove and replace it with the other one, as I found several tests failing
        // and needed bit of refactoring, so instead leaving it here. Looks like it is mostly used in the UnderwriterOrganisation only
        [When(@"I make a GET request to '(.*)' resource with filter\(s\)")]
        public void WhenIMakeAGETRequestToResourceAndAppendTheFilterS(string resource)
        {
            var sBuilder = new StringBuilder();

            if (!_stateHolder._sParamsBuilder.Any())
                throw new Exception("No parameters found to add.");

            _stateHolder._sParamsBuilder.ToList()
                .ForEach(param => sBuilder.Append("&")
                .Append(param.Key).Append("=")
                .Append(param.Value));

            sBuilder.Remove(0, 1);
            sBuilder.Insert(0, $"{resource}?");

            var paramBuilder = sBuilder.ToString().Replace("==", "=");

            _commonSteps.WhenIMakeRequestToResource(RestSharp.Method.GET, paramBuilder);
        }

        [Given(@"I add a filter on '(.*)' with the broker does not belong to")]
        public void GivenIAddAFilterOnWithTheBrokerDoesNotBelongTo(string fieldName)
        {
            _stateHolder._sParamsBuilder.Add(fieldName,$"{_brokerDepartmentContext.Items.First().BrokerDepartmentId}_invalidate");
        }

        [Given(@"I add a filter on '(.*)' with the broker does not belong to '(.*)'")]
        public void GivenIAddAFilterOnWithTheBrokerDoesNotBelongTo(string fieldName, string value)
        {
            _stateHolder._sParamsBuilder.Add(fieldName, value);
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with  '(.*)'")]
        public void GivenIAddAFilterOnWith(string filterType, string fieldName, string value)
        {
            var key = fieldName;

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filterType) ? value : $"{filterType}({value})";
            _stateHolder.ParamsBuilderValueToValidate = value;
        }

        [Given(@"I add a '(.*)' filter on '(.*)' with  part of '(.*)'")]
        public void FilterOnWithPart(string filterType, string fieldName, string value)
        {
            var length = 6;
            var key = fieldName;
            var rand = new Random();
            int randomNum = rand.Next(value.Length - length + 1);
            var valueToReplace = value.Substring(randomNum, length);

            if (!_stateHolder._sParamsBuilder.ContainsKey(key)) _stateHolder._sParamsBuilder.Add(key, string.Empty);

            _stateHolder._sParamsBuilder[key] = string.IsNullOrEmpty(filterType) ? valueToReplace : $"{filterType}({valueToReplace})";
            _stateHolder.ParamsBuilderValueToValidate = valueToReplace;
        }


        [Given(@"I add a filter on '(.*)' with invalid broker id '(.*)'")]
        public void GivenIAddAFilterOnWithInvalidBrokerId(string fieldName, string value)
        {
            _stateHolder._sParamsBuilder.Add(fieldName, value);
        }
       
        [Then(@"collection of underwriter organistations should have a total greater than (.*)")]
        public void ThenCollectionOfUnderwriterOrganistationsShouldHaveATotalGreaterThan(int underwriterOrgsCount)
        {
            StepsExension.ValidateOkResponse(_apiTestContext.Response);

            _underwriterOrganisationContext = JsonConvert.DeserializeObject<UnderwriterOrganisationCollectionWrapper>(_apiTestContext.Response.Content);

            Assert.Greater(_underwriterOrganisationContext.Total, underwriterOrgsCount, "The expected and actual totals do not match.");
        }

        [Then(@"validate underwriter user email address contain the value from the field (.*)")]
        public void ValidateUserEmailAddressWithValue(string field)
        {
            var expectedValue = _stateHolder.ParamsBuilderValueToValidate;

            var uwUsers = _underwriterOrganisationContext.Items.Select(x => x.UnderwriterUsers);

            foreach (var user in uwUsers)
            {
                var emailAddresses = user.Select(x => x.UnderwriterUserEmailAddress);

                foreach(var email in emailAddresses)
                {
                    if (!email.Contains(expectedValue, StringComparison.OrdinalIgnoreCase))
                        Assert.Fail($"Couldnt find {expectedValue} in {email}.");
                }
            }
        }

        [Then(@"validate underwriter user full name contain the value from the field (.*)")]
        public void ValidateUserFullnameWithValue(string field)
        {
            var expectedValue = _stateHolder.ParamsBuilderValueToValidate;

            var uwUsers = _underwriterOrganisationContext.Items.Select(x => x.UnderwriterUsers);

            foreach (var user in uwUsers)
            {
                var emailAddresses = user.Select(x => x.UnderwriterUserFullName);

                foreach (var email in emailAddresses)
                {
                    if (!email.Contains(expectedValue))
                        Assert.Fail($"Couldnt find {expectedValue} in {email}.");
                }
            }
        }

        [Then(@"save the collection of underwriter organistations")]
        public void ThenCollectionOfUnderwriterOrganistations()
        {
            StepsExension.ValidateOkResponse(_apiTestContext.Response);

            _underwriterOrganisationContext = JsonConvert.DeserializeObject<UnderwriterOrganisationCollectionWrapper>(_apiTestContext.Response.Content);
        }

        [Given(@"I set a valid UnderwriterOrganisationId for '(.*)'")]
        public void GivenISetAValidUnderwriterOrganisationId(string uwEmailAddress)
        {
            GetUnderwriterOrganisationFor(uwEmailAddress);
            var uwOrganisationId = _underwriterOrganisationContext.Items.FirstOrDefault().UnderwriterOrganisationId;

            And($"for the submission underwriters the 'UnderwriterOrganisationId' is '{uwOrganisationId}'");
        }




    }
}

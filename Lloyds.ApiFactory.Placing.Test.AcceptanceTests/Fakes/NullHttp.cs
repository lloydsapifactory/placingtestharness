//----------------------------------------------------------------------- 
// Copyright © 2018-2019, The Society of Lloyd’s
//-----------------------------------------------------------------------

using RestSharp;

namespace Lloyds.ApiFactory.Placing.Test.AcceptanceTests.Fakes
{
    public class NullHttp : Http
    {
        public new HttpResponse Get()
        {
            return new HttpResponse();
        }
    }
}
